<?php
require 'vendor/autoload.php';
use Phroute\Phroute\RouteCollector;
use duncan3dc\Laravel\BladeInstance;
use Phroute\Phroute\Exception\HttpRouteNotFoundException;

$blade = new BladeInstance("views");

$router = new RouteCollector();
require 'routes.php';

try{
	$response = (new Phroute\Phroute\Dispatcher($router->getData()))->dispatch($_SERVER['REQUEST_METHOD'], parse_url($_SERVER['REQUEST_URI'], PHP_URL_PATH));
}catch(HttpRouteNotFoundException $e){
	$response = '404 not found';
	http_response_code(404);
}

echo $response;