module.exports = {
	'folders': {
		theme: [
			'main',
			'default',
			'input',
			'button',
			'list',
			'ico',
			'nav',
			'footer',
			'c_circles',
			'c_gift',
			'c_port',
			'c_cubeportfolio',
			'c_results',
			'c_statements',
			'c_tabs_navbar',
			'c_reviews',
			'flipper',
			'fancybox'
		],

		'index': [
			'about',
			'slider',
			'a_bottle',
			'a_chair',
			'a_chat',
			'a_metronom',
			'a_stackdeck',
			'a_zorg',
			'slider',
			'services',
			'news_column',
			'clients_row'
		],

		'web': [
			'main',
			'header',
			'sites',
			'solutions',
			'advantages',
			'approaches',
			'gift',
			'reviews'
		],

		ecommerce: [
			'header',
			'ecommerce',
			'slider',
			'advantages',
			'cost_column',
			'modules',
			'abilities',
			'stages',
			'flipper',
			'costfactors',
			'steps'
		],

		'mobile_sites': [
			'main',
			'header',
			'compare',
			'advantages',
			'stats'
		],

		'cms': [
			'main',
			'header',
			'abante',
			'why',
			'functions',
			'payments'
		],

		'sensor': [
			'header',
			'solution',
			'solutions',
			'scheme',
			'sol-slider',
			'stat',
			'start',
			'advantages'
		],

		about: [
			'main',
			'header',
			'about',
			'advantages',
			'dev',
			'employee'
		],

		'seo': [
			'main',
			'header',
			'animations',
			'advantages',
			'seo',
			'stages',
			'prices'
		],

		'fstyle': [
			'main',
			'header',
			'mosaic_portfolio'
		],

		'analytics': [
			'main',
			'header',
			'solutions'
		],

		'context': [
			'header'
		],

		'context_old': [
			'header',
			'about',
			'services',
			'advantages'
		],

		'mobile_apps': [
			'main',
			'header',
			'holter_img',
			'webisonline'
		],

		'media': [
			'main',
			'header',
			'approach'
		],

		'hosting': [
			'main',
			'header'
		],

		'support': [
			'main',
			'header',
			'prices'
		],

		'photovideo': [
			'main',
			'header',
			'photovideo',
			'start_ready'
		],

		'conversion': [
			'main',
			'header',
			'header_form',
			'solutions',
			'approach',
			'advantages',
			'awards'
		],

		'socials': [
			'main',
			'header',
			'blogs',
			'socials',
			'support',
			'virus'
		],

		'consulting': [
			'main',
			'header',
			'advantages',
			'solutions'
		],

		'pc': [
			'main',
			'header',
			'projects'
		],

		'contact': [
			'main',
			'advantages',
			'contacts',
			'offices',
			'header',
			'requisites'
		],

		'clients': [
			'main',
			'clients'
		],

		'news': [
			'main',
			'header',
			'news_list',
			'news_nav',
			'news_panel',
			'news_all'
		],

		'portfolio': [
			'main',
			'portfolio',
		]
	},

	'files': [
		'helpers'
	]

}