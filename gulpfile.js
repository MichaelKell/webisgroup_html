'use strict';

var gulp = require('gulp'); 
var sass = require('gulp-sass');
var merge = require('merge-stream');
var autoprefixer = require('gulp-autoprefixer');
var sourcemaps = require('gulp-sourcemaps');
var concat = require('gulp-concat');
var livereload = require('gulp-livereload');

var config = require('./gulp.config');
var stylesPath = './assets/scss/';


gulp.task('styles', function() {
    var compileFolders = Object.keys(config.folders).map(function(key) {
        var sources = config.folders[key].map(function(source){ return stylesPath+key+'/'+source+'.scss'; });
        //console.log(sources);
        return SassCompile(sources, key);
    });
    var compileFiles = config.files.map(function(file){
        return SassCompile(stylesPath+file+'.scss', file);
    });
    return merge(compileFolders, compileFiles);
});

gulp.task('js', function(){
    return gulp.src('./assets/js/modules/*.js')
           .pipe(concat('modules.js'))
           .pipe(gulp.dest('./assets/js/'))
           .pipe(livereload());
});

function SassCompile(sources, outputFilename){
    return gulp.src(sources)
    .pipe(sourcemaps.init())
    .pipe(sass({outputStyle: 'expanded'}).on('error', sass.logError))
    .pipe(autoprefixer({browsers: ['last 5 versions']}))
    .pipe(concat(outputFilename+'.css'))
    .pipe(sourcemaps.write('.'))
    .pipe(gulp.dest('./assets/css/'))
    .pipe(livereload());
}

gulp.task('default', ['styles', 'js']);


gulp.task('watch', function () {
    livereload.listen();
    gulp.watch('./assets/scss/**/*.scss', ['styles']);
    gulp.watch('./assets/js/modules/*.js', ['js']);
});
