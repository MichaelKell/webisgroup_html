@extends('site.layout', ['Title' => 'Создание мобильных приложений'])

@section('head-styles')
<link rel="stylesheet" href="assets/css/mobile_apps.css">
@stop

@section('content')
<div class="header">
	<div class="header-inner container">
		<div class="row">
			<div class="col-md-11 col-md-offset-1">
				<h1>
					Создание мобильных<span class="hidden-sm"><br></span> приложений
				</h1>
				<div class="header-thesis">
					Создаем приложения<br> для IOS, Andriod и Windows
				</div>
			</div>
		</div>
	</div>
</div>
<div class="container holter margin-top-m30">
	<div class="row">
		<div class="col-md-6 col-md-offset-1">
			<h2 class="text-left-sm text-center">&laquo;Мобильный холтер&raquo;</h2>
		</div>
	</div>
	<div class="row row-right-sm">
		<div class="col-md-5 col-sm-5 holter-img">
			<img class="img-responsive" src="assets/img/mobile_apps/holter.png" alt="">
		</div>
		<div class="col-md-6 col-md-offset-1 col-sm-7">
			<p>
				Приложение предназначено для самостоятельного снятия электрокардиограммы по Холтеру при помощи устройства &laquo;Кардиофлешка ECG Dongle&raquo; компании &laquo;Нордавинд&raquo;.
				Поставляется в комплекте с прибором, соединяется с ним по Bluetooth.
				Пользователь может анализировать полученные данные самостоятельно или отправить их специалисту прямо из приложения.
			</p>
			<p>
				Проектирование и разработка интерфейса пользователя,
				разработка всех графических элементов.
			</p>
			<div class="font-bold gray margin-bottom-10 text-left-sm text-center">Платформа:</div>
			<div class="row">
				<div class="col-md-6 col-sm-4 text-left-sm text-center">
					<span class="apple-ico margin-right-20"></span><span class="android-ico"></span>
				</div>
				<div class="col-md-6 col-sm-8  text-right-sm text-center-xs margin-top-sm-0 margin-top-xs-20">
					<button class="button button-red-transparent fb-form" data-fancybox-href="#form-popup-default">Узнать больше</button>
				</div>
			</div>
		</div>
	</div>
</div>
<div class="container webisonline margin-top-50">
	<div class="row">
		<div class="col-md-6 col-sm-7 col-sm-offset-5 col-md-offset-6">
			<h2 class="text-right-sm text-center">Мобильное приложение WebisOnline</h2>
		</div>
	</div>
	<div class="row">
		<div class="col-md-6 col-sm-5 webisonline-img">
			<img class="img-responsive" src="assets/img/mobile_apps/webis_online.jpg" alt="">
		</div>
		<div class="col-md-6 col-sm-7 text-right-sm text-left">
		<p>
		Приложение предназначено для операторов-консультантов и руководителей отдела обслуживания клиентов,
		работающих с сервисом онлайн-консультаций WebisOnline. С его помощью операторы могут принимать запросы от посетителей с сайта и отвечать на них, 
		а руководители &ndash; отслеживать в реальном времени работу операторов по множеству параметров.
		</p>
		<p>
		Push-уведомления позволяют всегда оставаться на связи в удобном режиме работы.
		</p>
			<div class="font-bold gray margin-bottom-10 text-right-sm text-center">Платформа:</div>
			<div class="row row-right-sm">
				<div class="col-md-6 col-sm-5 text-right-sm text-center nowrap">
					<span class="apple-ico margin-right-20"></span><span class="android-ico margin-right-20"></span><span class="windows-ico"></span>
				</div>
				<div class="col-md-6 col-sm-7  text-right-sm text-center-xs margin-top-sm-0 margin-top-xs-20">
					<button class="button button-red-transparent fb-form" data-fancybox-href="#form-popup-default">Узнать больше</button>
				</div>
			</div>
		</div>
	</div>
</div>
@stop