@extends('site.layout', ['Title' => 'Адаптация сайта под мобильные устройства'])

@section('head-styles')
<link rel="stylesheet" href="assets/css/mobile_sites.css">
@stop

@section('content')
<div class="header container">
 <a class="page-anchor" data-name="Адаптация сайта" name="up"></a>
 <div class="row">
   <div class="col-md-11 col-md-offset-1">
      <h1>
      	Адаптация сайта<br>
      	под мобильные<br>
      	устройства<br>
      </h1>
   </div>
 </div>
 <div class="header-thesis">
 	Свыше <span class='font-bold' style='color: #1f3e64;'>25 000 000</span> человек в России<br>пользуются мобильным интернетом
 </div>
 <div class="row row-right-sm header-form">
 	<div class="col-md-5 col-sm-12 header-form-img">
 		<img class="img-responsive" src="assets/img/mobile_sites/headerimg.png" alt="">
 	</div>
 	<div class="col-md-7">
 		<div class="header-form-text">
			Мы бесплатно расчитаем стоимость адаптации сайта под мобильные устройства и вышлем коммерческое предложение на указаный вами e-mail:
		</div>
		<div class="row">
			<div class="col-md-12">
				<form class="form-ajax" action="./" method="POST">
					<div class="input-spacing">
						<input name="Name" type="text" class="input" placeholder="Как вас зовут?" data-required="name">
					</div>
					<div class="input-spacing">
						<div class="row">
							<div class="col-md-6 input-spacing-md-0 input-spacing">
								<input name="Phone" type="text" class="input" placeholder="Ваш телефон" data-required="phone">
							</div>
							<div class="col-md-6">
								<input name="Email" type="text" class="input" placeholder="Ваш Email" data-required="email">
							</div>
						</div>
					</div>
					<div class="input-spacing-0 input-spacing-sm">
						<div class="row">
							<div class="col-md-8 input-spacing input-spacing-md-0">
								<input name="Site" type="text" class="input" placeholder="Какой сайт будем адаптировать?">
							</div>
							<div class="col-md-4">
								<button type="submit" class="button button-red-transparent w100 height-as-input">Отправить</button>
							</div>
						</div>
					</div>
				</form>
			</div>
		</div>
 	</div>
 </div>
</div>

<div class="container-fluid compare">
	<a class="page-anchor" data-name="Какие проблемы<br>возникают ?" name="problem"></a>
	<div class="container compare-before">
		<h2 class="text-center">
			Какие проблемы возникают при отображении сайтов на мобильных устройствах?
		</h2>
		<div class="row compare-before-row">
			<div class="col-md-4">
				<div class="row compare-item compare-item-1 compare-item-right">
					<div class="col-md-4 col-sm-3 col-xs-3 compare-item-img">
						<img class="img-responsive" src="assets/img/mobile_sites/compare1.png" alt="">
					</div>
					<div class="col-md-8 col-sm-9 col-xs-9 compare-item-text-col">
						<div class="compare-item-header">Где курсор?</div>
						<div class="compare-item-text">Элементы управления не реагируют на курсор. Ведь на мобильных устройствах его попросту нет.</div>
					</div>
				</div>
				<div class="row compare-item compare-item-2 compare-item-right">
					<div class="col-md-4 col-sm-3 col-xs-3 compare-item-img">
						<img class="img-responsive" src="assets/img/mobile_sites/compare2.png" alt="">
					</div>
					<div class="col-md-8 col-sm-9 col-xs-9 compare-item-text-col">
						<div class="compare-item-header">Ой, что это?</div>
						<div class="compare-item-text">Неоптимизированные изображения. Лишние элементы оформления.</div>
					</div>
				</div>
				<div class="row compare-item compare-item-3 compare-item-right">
					<div class="col-md-4 col-sm-3 col-xs-3 compare-item-img">
						<img class="img-responsive" src="assets/img/mobile_sites/compare3.png" alt="">
					</div>
					<div class="col-md-8 col-sm-9 col-xs-9 compare-item-text-col">
						<div class="compare-item-header">Не могу попасть!</div>
						<div class="compare-item-text">Элементы управления близко расположены или слишком малы — в них сложно попасть пальцем.</div>
					</div>
				</div>
			</div>
			<div class="col-md-4 compare-img">
				<img src="assets/img/mobile_sites/compareimg1.png" alt="">
			</div>
			<div class="col-md-4">
				<div class="row compare-item compare-item-4">
					<div class="col-md-4 col-sm-3 col-xs-3 compare-item-img">
						<img class="img-responsive" src="assets/img/mobile_sites/compare4.png" alt="">
					</div>
					<div class="col-md-8 col-sm-9 col-xs-9 compare-item-text-col">
						<div class="compare-item-header">Где мои очки?</div>
						<div class="compare-item-text">Мелкие, нечитаемые шрифты. Широкие текстовые колонки.</div>
					</div>
				</div>
				<div class="row compare-item compare-item-5">
					<div class="col-md-4 col-sm-3 col-xs-3 compare-item-img">
						<img class="img-responsive" src="assets/img/mobile_sites/compare5.png" alt="">
					</div>
					<div class="col-md-8 col-sm-9 col-xs-9 compare-item-text-col">
						<div class="compare-item-header">Почему не едет?</div>
						<div class="compare-item-text">Не работает управление жестами: прокрутка, увеличение, листание и другие функции.</div>
					</div>
				</div>
				<div class="row compare-item compare-item-6">
					<div class="col-md-4 col-sm-3 col-xs-3 compare-item-img">
						<img class="img-responsive" src="assets/img/mobile_sites/compare6.png" alt="">
					</div>
					<div class="col-md-8 col-sm-9 col-xs-9 compare-item-text-col">
						<div class="compare-item-header">А где остальное?</div>
						<div class="compare-item-text">Сайт не помещается на экране. При просмотре на мобильных устройствах появляется скролл.</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>


<div class="container-fluid compare">
 <a class="page-anchor" data-name="Как отображаются<br>адаптивные сайты?" name="solution"></a>
	<div class="container compare-after">
		<h2 class="text-center">
			А как отображаются адаптивные сайты?
		</h2>
		<div class="row compare-before-row">
			<div class="col-md-4">
				<div class="row compare-item compare-item-7 compare-item-right">
					<div class="col-md-4 col-sm-3 col-xs-3 compare-item-img">
						<img class="img-responsive" src="assets/img/mobile_sites/compare7.png" alt="">
					</div>
					<div class="col-md-8 col-sm-9 col-xs-9 compare-item-text-col">
						<div class="compare-item-header">Удобно использовать</div>
						<div class="compare-item-text">Элементы управления адаптированы для мобильных устройств. Реализована поддержка жестов: прокрутка, увеличение, листание и другие функции.</div>
					</div>
				</div>
				<div class="row compare-item compare-item-8 compare-item-right">
					<div class="col-md-4 col-sm-3 col-xs-3 compare-item-img">
						<img class="img-responsive" src="assets/img/mobile_sites/compare8.png" alt="">
					</div>
					<div class="col-md-8 col-sm-9 col-xs-9 compare-item-text-col">
						<div class="compare-item-header">Легко читать</div>
						<div class="compare-item-text">Крупный шрифт и продуманный текст позволяют поситителям комфортно ознакомиться с ассортиментом</div>
					</div>
				</div>
			</div>
			<div class="col-md-4 compare-img">
				<img src="assets/img/mobile_sites/compareimg2.png" alt="">
			</div>
			<div class="col-md-4">
				<div class="row compare-item compare-item-9">
					<div class="col-md-4 col-sm-3 col-xs-3 compare-item-img">
						<img class="img-responsive" src="assets/img/mobile_sites/compare9.png" alt="">
					</div>
					<div class="col-md-8 col-sm-9 col-xs-9 compare-item-text-col">
						<div class="compare-item-header">Правильные акценты</div>
						<div class="compare-item-text">Сайт дружелюбен и способен привлечь внимание пользователя к нужным товарам, акциям или скидкам </div>
					</div>
				</div>
				<div class="row compare-item compare-item-10">
					<div class="col-md-4 col-sm-3 col-xs-3 compare-item-img">
						<img class="img-responsive" src="assets/img/mobile_sites/compare10.png" alt="">
					</div>
					<div class="col-md-8 col-sm-9 col-xs-9 compare-item-text-col">
						<div class="compare-item-header">Универсальность</div>
						<div class="compare-item-text">Сайт самостоятельно подстраивается под текущий размер экрана. Ваш контент одинаково хорошо будет отображаться и на маленьком и на большом устройстве.</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>


<div class="container-fluid advantages-stats">
 <a class="page-anchor" data-name="Преимущества<br>адаптивного сайта" name="advantages"></a>
	<h2 class="text-center">
		Преимущества адаптивного сайта
	</h2>
	<div class="container advantages">
		<div class="row">
			<div class="col-md-4 advantages-item">
				<div class="advantages-header">Больше клиентов</div>
				<div class="advantages-text">Возрастает доверие к сайту</div>
				<div class="advantages-img">
					<img class="img-responsive" src="assets/img/mobile_sites/circle1.png" alt="">
				</div>
			</div>
			<div class="col-md-4 advantages-item">
				<div class="advantages-header">Больше внимания</div>
				<div class="advantages-text">Пользователи <span class="hidden-md">мобильных устройств</span> <span class="vidible-sm"><br></span>более детально изучают контент</div>
				<div class="advantages-img">
					<img class="img-responsive"  src="assets/img/mobile_sites/circle2.png" alt="">
				</div>
			</div>
			<div class="col-md-4 advantages-item">
				<div class="advantages-header">Больше звонков</div>
				<div class="advantages-text">Удобно звонить в один клик</div>
				<div class="advantages-img">
					<img class="img-responsive"  src="assets/img/mobile_sites/circle3.png" alt="">
				</div>
			</div>
		</div>
	</div>

	<div class="container stats">
		<div class="row">
			<div class="col-md-4 stats-item">
				<div class="stats-img">
					<div class="table-center">
						<img src="assets/img/mobile_sites/advantages1.png" alt="">
					</div>
				</div>
				<div class="stats-header">
					<div class="stats-percent">48%</div>
					<div class="stats-header-text">Пользователей</div>
				</div>
				<div class="stats-text">
					отрицательно воспринимают компанию, сайт которой неудачно работает на мобильных устройствах
				</div>
			</div>
			<div class="col-md-4 stats-item">
				<div class="stats-img">
					<div class="table-center">
						<img src="assets/img/mobile_sites/advantages2.png" alt="">
					</div>
				</div>
				<div class="stats-header">
					<div class="stats-percent">46%</div>
					<div class="stats-header-text">Пользователей</div>
				</div>
				<div class="stats-text">
					отмечают недружелюбный интерфейс сайтов и сложную навигацию
				</div>
			</div>
			<div class="col-md-4 stats-item">
				<div class="stats-img">
					<div class="table-center">
						<img src="assets/img/mobile_sites/advantages3.png" alt="">
					</div>
				</div>
				<div class="stats-header">
					<div class="stats-percent">51%</div>
					<div class="stats-header-text">Рост продаж</div>
				</div>
				<div class="stats-text">
					отмечают недружелюбный интерфейс сайтов и сложную навигацию
				</div>
			</div>
		</div>
	</div>
</div>
@stop