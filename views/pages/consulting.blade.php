@extends('site.layout', ['Title' => 'Внедренческий IT-консалтинг'])

@section('head-styles')
<link rel="stylesheet" href="assets/css/consulting.css">
@stop

@section('content')
<div class="header">
	<div class="header-inner container">
		<div class="row">
			<div class="col-md-11 col-md-offset-1">
				<h1>
					Внедренческий<br>IT-консалтинг
				</h1>
				<div class="header-thesis">
					Мы выстроим контроль и инструменты<br> принятия решений,<br> а вы сфокусируетесь на бизнесе
				</div>
			</div>
		</div>
	</div>
</div>

<div class="advantages container margin-top-lg-m120 margin-top-md-m90 margin-top-sm-m70 margin-top-m20">
	<h2>Как IT-внедрение влияет на бизнес?</h2>
	<div class="advantages-thesis">
		Экономия на изменениях от десятков до сотен тысяч долларов при правильном выборе IT-стратегии
	</div>
	<div class="advantages-row row">
		<div class="col-md-4 col-sm-4 advantages-item">
			<div class="advantages-item-img">
				<img src="assets/img/consulting/advantages1.png" alt="" class="img-responsive m-auto">
			</div>
			<div class="advantages-item-divide"></div>
			<div class="advantages-item-header">
				Рост производительности<br>ИТ-отдела
			</div>
			<div class="advantages-item-percent">
				&nbsp;&nbsp;250%
			</div>
		</div>
		<div class="col-md-4 col-sm-4 advantages-item">
			<div class="advantages-item-img">
				<img src="assets/img/consulting/advantages2.png" alt="" class="img-responsive m-auto">
			</div>
			<div class="advantages-item-divide"></div>
			<div class="advantages-item-header">
				Уменьшение времени выхода<br>услуги на рынок
			</div>
			<div class="advantages-item-percent">
				&nbsp;&nbsp;30-50%
			</div>
		</div>
		<div class="col-md-4 col-sm-4 advantages-item">
			<div class="advantages-item-img">
				<img src="assets/img/consulting/advantages3.png" alt="" class="img-responsive m-auto">
			</div>
			<div class="advantages-item-divide"></div>
			<div class="advantages-item-header">
				Экономия при правильном выборе<br>инсорсинга или аутсорсинга.
			</div>
			<div class="advantages-item-percent">
				&nbsp;&nbsp;20-50%
			</div>
		</div>
	</div>
</div>

<div class="solutions container">
	<h2>
		Мы предлагаем следующие услуги<br>внедренческого it-консалтинга:
	</h2>
	<p class="solutions-thesis">
		В зависимости от вашей задачи мы поможем вам выстроить систему управления <span class="hidden-sm hidden-xs"><br></span>
		и контроля, принять решение по развитию вашего продукта, сформулировать стратегию <span class="hidden-sm hidden-xs"><br></span> 
		работы с ИТ или даже создать ИТ-подразделение.
	</p>
	<div class="statements-xs">
		<div class="col-md-offset-1 col-md-2 col-sm-2 col-sm-offset-1 statements-item">
			<div class="statements-item-img">
				<img class="img-responsive" src="assets/img/consulting/ico1.png" alt="">
			</div>
			<div class="statements-item-text">
				Внедрение системы управления IT
			</div>
		</div>
		<div class="col-md-2 col-sm-2 statements-item">
			<div class="statements-item-img">
				<img class="img-responsive" src="assets/img/consulting/ico2.png" alt="">
			</div>
			<div class="statements-item-text">
				Технологический аудит
			</div>
		</div>
		<div class="col-md-2 col-sm-2 statements-item">
			<div class="statements-item-img">
				<img class="img-responsive" src="assets/img/consulting/ico3.png" alt="">
			</div>
			<div class="statements-item-text">
				Оценка стоимости и сроков разработки
			</div>
		</div>
		<div class="col-md-2 col-sm-2 statements-item">
			<div class="statements-item-img">
				<img class="img-responsive" src="assets/img/consulting/ico4.png" alt="">
			</div>
			<div class="statements-item-text">
				Стартап-менеджмент
			</div>
		</div>
		<div class="col-md-2 col-sm-2 statements-item">
			<div class="statements-item-img">
				<img class="img-responsive" src="assets/img/consulting/ico5.png" alt="">
			</div>
			<div class="statements-item-text">
				  Кризисный менеджмент
			</div>
		</div>
	</div>
</div>

<div class="space-50"></div>
@stop