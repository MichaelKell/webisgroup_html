@extends('site.layout', ['Title' => 'Сенсорные киоски'])

@section('head-styles')
<link rel="stylesheet" href="assets/css/sensor.css">
@stop

@section('footer')
<script type="text/javascript" src="assets/js/sensor.js"></script>
@stop

@section('content')
<div class="header">
    <div class="header-inner container">
        <div class="row">
            <div class="col-md-11 col-md-offset-1">
                <h1>
            Тысячи товаров<br>на одном квадратном метре!
        </h1>
                <div class="header-thesis">
                    Сенсорная торговая система<br> &laquo;Ритейл-терминал&raquo;
                </div>
            </div>
        </div>
    </div>
</div>
<div class="circles container margin-top-lg-m120 margin-top-md-m80 margin-top-sm-m40">
    <div class="row circles-row">
        <div class="circles-item col-md-4 col-sm-4">
            <div class="circles-header">
                <div class="center">
                    Не хватает места<br>в торговом зале?
                </div>
            </div>
            <div class="circles-img"><img class="img-responsive" src="assets/img/sensor/about1.png">
            </div>
        </div>
        <div class="circles-item col-md-4 col-sm-4">
            <div class="circles-header">
                <div class="center">
                    Желаете расширить<br>свой ассортимент?
                </div>
            </div>
            <div class="circles-img"><img class="img-responsive" src="assets/img/sensor/about2.png">
            </div>
        </div>
        <div class="circles-item col-md-4 col-sm-4">
            <div class="circles-header">
                <div class="center">
                    Хотите оптимизировать<br>расходы?
                </div>
            </div>
            <div class="circles-img"><img class="img-responsive" src="assets/img/sensor/about3.png">
            </div>
        </div>
    </div>
</div>

<div class="container-fluid solution">
    <div class="container">
        <div class="solution-header">
            Решение проблемы
        </div>
        <div class="solution-text">
            Интерактивная система «Ритейл-терминал» на базе сенсорных киосков открывает совершенно новые возможности для развития торговли и прекрасно подходит для быстрого запуска новых торговых представительств
        </div>
        <div class="solution-button">
            <button class="button button-red-transparent fb-form" data-fancybox-href="#form-popup-default">Оставить заявку</button>
        </div>
    </div>
</div>

<div class="scheme container">
    <a class="page-anchor" data-name="Как работает систем" name="howitworks"></a>
    <h2>Как работает система &laquo;Ритейл-терминал&raquo; ?</h2>
    <div class="row scheme-row">
        <div class="scheme-item col-md-3 col-sm-6">
            <div class="scheme-img">
                <img src="assets/img/sensor/scheme1.png" alt="">
            </div>
            <div class="scheme-header">
                Администратор
            </div>
            <div class="scheme-text">
                Управление каталогом продукции и следит за наличием товара на складе
            </div>
        </div>
        <div class="scheme-item col-md-3 col-sm-6">
            <div class="scheme-img">
                <img src="assets/img/sensor/scheme2.png" alt="">
            </div>
            <div class="scheme-header">
                Терминал
            </div>
            <div class="scheme-text">
                Отображает весь товар находящийся на складе и других ваших магазинах
            </div>
        </div>
        <div class="scheme-item col-md-3 col-sm-6">
            <div class="scheme-img">
                <img src="assets/img/sensor/scheme3.png" alt="">
            </div>
            <div class="scheme-header">
                Покупатель
            </div>
            <div class="scheme-text">
                Покупатель выбирает товары и оплачивает их в кассе или прямо на терминале банковской картой
            </div>
        </div>
        <div class="scheme-item col-md-3 col-sm-6">
            <div class="scheme-img">
                <img src="assets/img/sensor/scheme4.png" alt="">
            </div>
            <div class="scheme-header">
                Склад
            </div>
            <div class="scheme-text">
                Заказ с терминала поступает на склад, обрабатывается и готовится к доставке довольному покупателю
            </div>
        </div>
    </div>
</div>

<div class="container-fluid solutions">
    <a class="page-anchor" data-name="Примеры решений" name="solutions"></a>
    <h2>Примеры решений</h2>
    <div class="container-fluid sol-slider">
        <div data-slider-bg="assets/img/sensor/slide1_bg.png" class="container sol-slider-item">
            <div class="row">
                <div class="col-md-6 hidden-sm hidden-xs sol-slider-img">
                    <img class="img-responsive" src="assets/img/sensor/slide1.png" alt="">
                </div>
                <div class="col-md-6 sol-slider-inner">
                    <div class="sol-slider-logo">
                        <img class="img-responsive" src="assets/img/sensor/logo1.png" alt="">
                    </div>
                    <div class="sol-slider-text">
                        <p>
                            HomeSegreto - это сеть салонов, в которых представлена качественная итальянская, румынская, немецкая и польская мебель различных ценовых сегментов.
                        </p>
                        <p>
                            Стояла задача запустить франчайзинг-сеть, а в каждом салоне установить сенсорный киоск для работы с широким ассортиментом подобных товаров.
                        </p>
                        <p>
                            Каждому франчайзи, по условиям договора, предоставляется интерактивный web-салон (интерактивный тачскрин терминал).
                        </p>
                    </div>
                </div>
            </div>
        </div>
        <div data-slider-bg="assets/img/sensor/slide2_bg.png" class="container sol-slider-item">
            <div class="row">
                <div class="col-md-6 hidden-sm hidden-xs sol-slider-img">
                    <img class="img-responsive" src="assets/img/sensor/slide2.png" alt="">
                </div>
                <div class="col-md-6 sol-slider-inner">
                    <div class="sol-slider-logo">
                        <img class="img-responsive" src="assets/img/sensor/logo1.png" alt="">
                    </div>
                    <div class="sol-slider-text">
                        <p>
                            HomeSegreto - это сеть салонов, в которых представлена качественная итальянская, румынская, немецкая и польская мебель различных ценовых сегментов.
                        </p>
                        <p>
                            Стояла задача запустить франчайзинг-сеть, а в каждом салоне установить сенсорный киоск для работы с широким ассортиментом подобных товаров.
                        </p>
                        <p>
                            Каждому франчайзи, по условиям договора, предоставляется интерактивный web-салон (интерактивный тачскрин терминал).
                        </p>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="container solutions-callus">
        <div class="row">
            <div class="col-md-2 hidden-sm hidden-xs"></div>
            <div class="col-md-3 col-sm-4 solutions-callus-button">
                <button class="button button-blue fb-form" data-fancybox-href="#form-popup-default">Оставить заявку</button>
            </div>
            <div class="col-md-7 col-sm-8 solutions-callus-text">
                Если вам нужно нестандартное решение, опишите свою задачу,
                <br> мы свяжемся с вами и обсудим возможные варианты
            </div>
        </div>
    </div>
</div>


<div class="container advantages">
    <a class="page-anchor" data-name="Преимущества системы" name="advantages"></a>
    <h2>Преимущества системы</h2>
    <div class="row">
        <div class="col-md-1"></div>
        <div class="col-md-6 advantages-left">
            <div class="advantages-item">
                <table cellpadding="0" cellspacing="0" width="100%">
                    <tr>
                        <td class='advantages-img' valign="top">
                            <img src="assets/img/sensor/advantages1.png" alt="">
                        </td>
                        <td valign="top">
                            <div class="advantages-header">Снижение затрат на открытие новых торговых точек</div>
                            <div class="advantages-text">Открыть новый бизнес теперь легко! Достаточно поставить сенсорный киоск в любом торговом центре</div>
                        </td>
                    </tr>
                </table>
            </div>
            <div class="advantages-item">
                <table cellpadding="0" cellspacing="0" width="100%">
                    <tr>
                        <td class='advantages-img' valign="top">
                            <img src="assets/img/sensor/advantages2.png" alt="">
                        </td>
                        <td valign="top">
                            <div class="advantages-header">Мотивация покупателей</div>
                            <div class="advantages-text">Качественные фотографии на большом и удобном сенсорном экране впечатляют покупателей</div>
                        </td>
                    </tr>
                </table>
            </div>
            <div class="advantages-item">
                <table cellpadding="0" cellspacing="0" width="100%">
                    <tr>
                        <td class='advantages-img' valign="top">
                            <img src="assets/img/sensor/advantages3.png" alt="">
                        </td>
                        <td valign="top">
                            <div class="advantages-header">Быстрое обновление ассортимента</div>
                            <div class="advantages-text">Автоматическое обновление ассортимента и цен сразу по всей сети благодаря интеграции системы с интернет-магазином или 1С</div>
                        </td>
                    </tr>
                </table>
            </div>
            <div class="advantages-item">
                <table cellpadding="0" cellspacing="0" width="100%">
                    <tr>
                        <td class='advantages-img' valign="top">
                            <img src="assets/img/sensor/advantages4.png" alt="">
                        </td>
                        <td valign="top">
                            <div class="advantages-header">Автоматизация ап-селлинга и кросс-продаж</div>
                            <div class="advantages-text">Подбор сопутствующих товаров и услуг, а также возможность настройки предложений в зависимости от условий</div>
                        </td>
                    </tr>
                </table>
            </div>
        </div>
        <div class="col-md-5 advantages-right">
            <img src="assets/img/sensor/advantages.png" alt="">
        </div>
    </div>
</div>

<div class="stat">
    <a class="page-anchor" data-name="Встроенная система<br>статистики" name="statistics"></a>
    <div class="stat-items container">
        <h2>Встроенная система статистики</h2>
        <div class="stat-sub-header">
            В наши торговые терминалы интегрирована система сбора статистики,
            <br> которая отслеживает действия покупателя:
        </div>
        <div class="row">
            <div class="col-md-3 stat-item">
                <table width="100%" cellpadding="0" cellspacing="0">
                    <tr>
                        <td width="1" valign="top" class="stat-item-img">
                            <img src="assets/img/sensor/stat1.png" alt="">
                        </td>
                        <td valign="top" class="stat-item-text">Добавление товара в корзину</td>
                    </tr>
                </table>
            </div>
            <div class="col-md-3 stat-item">
                <table width="100%" cellpadding="0" cellspacing="0">
                    <tr>
                        <td width="1" valign="top" class="stat-item-img">
                            <img src="assets/img/sensor/stat2.png" alt="">
                        </td>
                        <td valign="top" class="stat-item-text">Переход к оплате</td>
                    </tr>
                </table>
            </div>
            <div class="col-md-3 stat-item">
                <table width="100%" cellpadding="0" cellspacing="0">
                    <tr>
                        <td width="1" valign="top" class="stat-item-img">
                            <img src="assets/img/sensor/stat3.png" alt="">
                        </td>
                        <td valign="top" class="stat-item-text">Выбор способа оплаты</td>
                    </tr>
                </table>
            </div>
            <div class="col-md-3 stat-item">
                <table width="100%" cellpadding="0" cellspacing="0">
                    <tr>
                        <td width="1" valign="top" class="stat-item-img">
                            <img src="assets/img/sensor/stat4.png" alt="">
                        </td>
                        <td valign="top" class="stat-item-text">Возврат к каталогу после добавления товара в корзину</td>
                    </tr>
                </table>
            </div>
        </div>
    </div>

    <div class="stat-items-2">
        <div class="container">
            <div class="stat-items-2-header">Это позволит вам:</div>
            <div class="row">
                <div class="col-md-3 stat-item">
                    <table width="100%" cellpadding="0" cellspacing="0">
                        <tr>
                            <td width="1" valign="top" class="stat-item-img">
                                <img src="assets/img/sensor/stat5.png" alt="">
                            </td>
                            <td valign="top" class="stat-item-text">Посчитать количество клиентов не сдавших заказ</td>
                        </tr>
                    </table>
                </div>
                <div class="col-md-3 stat-item">
                    <table width="100%" cellpadding="0" cellspacing="0">
                        <tr>
                            <td width="1" valign="top" class="stat-item-img"><img src="assets/img/sensor/stat6.png" alt="">
                            </td>
                            <td valign="top" class="stat-item-text">Определить товары, которые пользуются наибольшим спросом</td>
                        </tr>
                    </table>
                </div>
                <div class="col-md-3 stat-item">
                    <table width="100%" cellpadding="0" cellspacing="0">
                        <tr>
                            <td width="1" valign="top" class="stat-item-img">
                                <img src="assets/img/sensor/stat7.png" alt="">
                            </td>
                            <td valign="top" class="stat-item-text">Собирать контактные данные с посетителей</td>
                        </tr>
                    </table>
                </div>
                <div class="col-md-3 stat-item">
                    <table width="100%" cellpadding="0" cellspacing="0">
                        <tr>
                            <td width="1" valign="top" class="stat-item-img">
                                <img src="assets/img/sensor/stat8.png" alt="">
                            </td>
                            <td valign="top" class="stat-item-text">Собирать отзывы сотрудников магазина</td>
                        </tr>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="container-fluid start">
    <a class="page-anchor" data-name="С чего начать" name="start"></a>
    <div class="container">
        <h2>С чего начать ?</h2>
        <div class="start-img">
            <img class="img-responsive" src="assets/img/sensor/start2.png" alt="">
            <a href="#form-popup-default" class="start-call">
                <span class="underline">Заполнить</span><br><span class="underline">заявку</span>
            </a>
        </div>
        <div class="row start-row start-row1">
            <div class="col-md-1">&nbsp;</div>
            <div class="col-md-5 start-row-img-col start-row1-img-col">
                <div class="start-row-img start-row1-img">
                    <img src="assets/img/sensor/startimg1.png" alt="">
                </div>
            </div>
            <div class="col-md-5 start-row-text-col start-row1-text-col">
                <div class="start-row-header start-row1-header">Брэндирование</div>
                <div class="start-row-text start-row1-text">
                    Корпуса наших сенсорных киосков могут быть выполнены в любой расцветке, нести на себе вашу фирменную символику или использоваться для рекламы.
                </div>
            </div>
            <div class="col-md-1">&nbsp;</div>
        </div>
        <div class="row start-row start-row2">
            <div class="col-md-1">&nbsp;</div>
            <div class="col-md-5 col-sm-12 start-row-img-col start-row2-img-col">
                <div class="start-row-img start-row2-img">
                    <img src="assets/img/sensor/startimg2.png" alt="">
                </div>
            </div>
            <div class="col-md-5 start-row-text-col start-row2-text-col">
                <div class="start-row-header start-row2-header">Гибкий подход</div>
                <div class="start-row-text start-row2-text">
                    Если для решения вашей задачи не годится стандартное оборудование, то мы готовы спроектировать и внедрить программно-аппаратный комплекс исключительно под ваши потребности.
                </div>
            </div>
            <div class="col-md-1">&nbsp;</div>
        </div>
    </div>
</div>
@stop