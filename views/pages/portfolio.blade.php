@extends('site.layout', ['Title' => 'Портфолио'])

@section('head-styles')
<link rel="stylesheet" href="assets/css/portfolio.css">
@stop

@section('footer')
<script type="text/javascript" src="assets/js/portfolio.js"></script>
@stop

@section('content')
<div class="header-default container-fluid">
	<div class="container">
		<div class="row">
			<div class="col-md-10 col-md-offset-1">
				<h1 class="h1-default">Наши работы</h1>
			</div>
		</div>
	</div>
</div>

<div class="space-55"></div>

<div class="tabs-navbar margin-top-sm-m40 margin-top-xs-m55">
	<div class="container">
		<div class="row">
			<div class="col-sm-12 col-xs-6 tabs-navbar-nav">
				<ul id="tinynav">
					<li class="tabs-navbar-nav-item">
						<a href="#">Сайты</a>
					</li>
					<li class="tabs-navbar-nav-item active">
						<a href="#">Фирменный стиль</a>
					</li>
					<li class="tabs-navbar-nav-item">
						<a href="#">3D и мультимедиа</a>
					</li>
					<li class="tabs-navbar-nav-item">
						<a href="#">Прочее</a>
					</li>
					<li class="tabs-navbar-nav-item">
						<a href="#">Все работы</a>
					</li>
				</ul>
			</div>
		</div>
	</div>
</div>

<div class="space-40"></div>

<div id="cube-portfolio" class="container">
	<div class="row">
        <div id="grid-container" class="cbp-l-grid-masonry">
            <div class="portfolio-list">
				<div class="cbp-item category filter-more1 cbp-l-grid-masonry-height1">
				    <div class="cbp-caption">
					  <a href="#">
				        <div class="cbp-caption-defaultWrap">
				            <img src="assets/img/temp/port1.jpg" alt="">
				        </div>
				        <div class="cbp-caption-activeWrap">
				            <div class="cbp-l-caption-alignCenter">
				                <div class="cbp-l-caption-body">
				                    <span class="cbp-l-caption-title">Создание сайта &laquo;Инвестиционный департамент&raquo;</span>
				                    <div class="cbp-l-caption-text">
				                    	<!--%text%-->
				                    </div>
				                    <div class="cbp-l-caption-icos"><span class="ico-eye"></span></div>
				                </div>
				            </div>  
				        </div>
					  </a>
				    </div>
				</div>
				<div class="cbp-item category filter-more1 cbp-l-grid-masonry-height1">
				    <div class="cbp-caption">
					  <a href="#">
				        <div class="cbp-caption-defaultWrap">
				            <img src="assets/img/temp/port2.jpg" alt="">
				        </div>
				        <div class="cbp-caption-activeWrap">
				            <div class="cbp-l-caption-alignCenter">
				                <div class="cbp-l-caption-body">
				                    <span class="cbp-l-caption-title">Создание сайта &laquo;Инвестиционный департамент&raquo;</span>
				                    <div class="cbp-l-caption-text">
				                    	<!--%text%-->
				                    </div>
				                    <div class="cbp-l-caption-icos"><span class="ico-eye"></span></div>
				                </div>
				            </div>  
				        </div>
					  </a>
				    </div>
				</div>
				<div class="cbp-item category filter-more1 cbp-l-grid-masonry-height1">
				    <div class="cbp-caption">
					  <a href="#">
				        <div class="cbp-caption-defaultWrap">
				            <img src="assets/img/temp/port3.jpg" alt="">
				        </div>
				        <div class="cbp-caption-activeWrap">
				            <div class="cbp-l-caption-alignCenter">
				                <div class="cbp-l-caption-body">
				                    <span class="cbp-l-caption-title">Создание сайта &laquo;Инвестиционный департамент&raquo;</span>
				                    <div class="cbp-l-caption-text">
				                    	<!--%text%-->
				                    </div>
				                    <div class="cbp-l-caption-icos"><span class="ico-eye"></span></div>
				                </div>
				            </div>  
				        </div>
					  </a>
				    </div>
				</div>
				<div class="cbp-item category filter-more1 cbp-l-grid-masonry-height1">
				    <div class="cbp-caption">
					  <a href="#">
				        <div class="cbp-caption-defaultWrap">
				            <img src="assets/img/temp/port4.jpg" alt="">
				        </div>
				        <div class="cbp-caption-activeWrap">
				            <div class="cbp-l-caption-alignCenter">
				                <div class="cbp-l-caption-body">
				                    <span class="cbp-l-caption-title">Создание сайта &laquo;Инвестиционный департамент&raquo;</span>
				                    <div class="cbp-l-caption-text">
				                    	<!--%text%-->
				                    </div>
				                    <div class="cbp-l-caption-icos"><span class="ico-eye"></span></div>
				                </div>
				            </div>  
				        </div>
					  </a>
				    </div>
				</div>
				<div class="cbp-item category filter-more1 cbp-l-grid-masonry-height1">
				    <div class="cbp-caption">
					  <a href="#">
				        <div class="cbp-caption-defaultWrap">
				            <img src="assets/img/temp/port5.jpg" alt="">
				        </div>
				        <div class="cbp-caption-activeWrap">
				            <div class="cbp-l-caption-alignCenter">
				                <div class="cbp-l-caption-body">
				                    <span class="cbp-l-caption-title">Создание сайта &laquo;Инвестиционный департамент&raquo;</span>
				                    <div class="cbp-l-caption-text">
				                    	<!--%text%-->
				                    </div>
				                    <div class="cbp-l-caption-icos"><span class="ico-eye"></span></div>
				                </div>
				            </div>  
				        </div>
					  </a>
				    </div>
				</div>
				<div class="cbp-item category filter-more1 cbp-l-grid-masonry-height1">
				    <div class="cbp-caption">
					  <a href="#">
				        <div class="cbp-caption-defaultWrap">
				            <img src="assets/img/temp/port6.jpg" alt="">
				        </div>
				        <div class="cbp-caption-activeWrap">
				            <div class="cbp-l-caption-alignCenter">
				                <div class="cbp-l-caption-body">
				                    <span class="cbp-l-caption-title">Создание сайта &laquo;Инвестиционный департамент&raquo;</span>
				                    <div class="cbp-l-caption-text">
				                    	<!--%text%-->
				                    </div>
				                    <div class="cbp-l-caption-icos"><span class="ico-eye"></span></div>
				                </div>
				            </div>  
				        </div>
					  </a>
				    </div>
				</div>
				<div class="cbp-item category filter-more1 cbp-l-grid-masonry-height1">
				    <div class="cbp-caption">
					  <a href="#">
				        <div class="cbp-caption-defaultWrap">
				            <img src="assets/img/temp/port7.jpg" alt="">
				        </div>
				        <div class="cbp-caption-activeWrap">
				            <div class="cbp-l-caption-alignCenter">
				                <div class="cbp-l-caption-body">
				                    <span class="cbp-l-caption-title">Создание сайта &laquo;Инвестиционный департамент&raquo;</span>
				                    <div class="cbp-l-caption-text">
				                    	<!--%text%-->
				                    </div>
				                    <div class="cbp-l-caption-icos"><span class="ico-eye"></span></div>
				                </div>
				            </div>  
				        </div>
					  </a>
				    </div>
				</div>
				<div class="cbp-item category filter-more1 cbp-l-grid-masonry-height1">
				    <div class="cbp-caption">
					  <a href="#">
				        <div class="cbp-caption-defaultWrap">
				            <img src="assets/img/temp/port8.jpg" alt="">
				        </div>
				        <div class="cbp-caption-activeWrap">
				            <div class="cbp-l-caption-alignCenter">
				                <div class="cbp-l-caption-body">
				                    <span class="cbp-l-caption-title">Создание сайта &laquo;Инвестиционный департамент&raquo;</span>
				                    <div class="cbp-l-caption-text">
				                    	<!--%text%-->
				                    </div>
				                    <div class="cbp-l-caption-icos"><span class="ico-eye"></span></div>
				                </div>
				            </div>  
				        </div>
					  </a>
				    </div>
				</div>
				<div class="cbp-item category filter-more1 cbp-l-grid-masonry-height1">
				    <div class="cbp-caption">
					  <a href="#">
				        <div class="cbp-caption-defaultWrap">
				            <img src="assets/img/temp/port9.jpg" alt="">
				        </div>
				        <div class="cbp-caption-activeWrap">
				            <div class="cbp-l-caption-alignCenter">
				                <div class="cbp-l-caption-body">
				                    <span class="cbp-l-caption-title">Создание сайта &laquo;Инвестиционный департамент&raquo;</span>
				                    <div class="cbp-l-caption-text">
				                    	<!--%text%-->
				                    </div>
				                    <div class="cbp-l-caption-icos"><span class="ico-eye"></span></div>
				                </div>
				            </div>  
				        </div>
					  </a>
				    </div>
				</div>
				<div class="cbp-item category filter-more1 cbp-l-grid-masonry-height1">
				    <div class="cbp-caption">
					  <a href="#">
				        <div class="cbp-caption-defaultWrap">
				            <img src="assets/img/temp/port10.jpg" alt="">
				        </div>
				        <div class="cbp-caption-activeWrap">
				            <div class="cbp-l-caption-alignCenter">
				                <div class="cbp-l-caption-body">
				                    <span class="cbp-l-caption-title">Создание сайта &laquo;Инвестиционный департамент&raquo;</span>
				                    <div class="cbp-l-caption-text">
				                    	<!--%text%-->
				                    </div>
				                    <div class="cbp-l-caption-icos"><span class="ico-eye"></span></div>
				                </div>
				            </div>  
				        </div>
					  </a>
				    </div>
				</div>
				<div class="cbp-item category filter-more1 cbp-l-grid-masonry-height1">
				    <div class="cbp-caption">
					  <a href="#">
				        <div class="cbp-caption-defaultWrap">
				            <img src="assets/img/temp/port11.jpg" alt="">
				        </div>
				        <div class="cbp-caption-activeWrap">
				            <div class="cbp-l-caption-alignCenter">
				                <div class="cbp-l-caption-body">
				                    <span class="cbp-l-caption-title">Создание сайта &laquo;Инвестиционный департамент&raquo;</span>
				                    <div class="cbp-l-caption-text">
				                    	<!--%text%-->
				                    </div>
				                    <div class="cbp-l-caption-icos"><span class="ico-eye"></span></div>
				                </div>
				            </div>  
				        </div>
					  </a>
				    </div>
				</div>
				<div class="cbp-item category filter-more1 cbp-l-grid-masonry-height1">
				    <div class="cbp-caption">
					  <a href="#">
				        <div class="cbp-caption-defaultWrap">
				            <img src="assets/img/temp/port12.jpg" alt="">
				        </div>
				        <div class="cbp-caption-activeWrap">
				            <div class="cbp-l-caption-alignCenter">
				                <div class="cbp-l-caption-body">
				                    <span class="cbp-l-caption-title">Создание сайта &laquo;Инвестиционный департамент&raquo;</span>
				                    <div class="cbp-l-caption-text">
				                    	<!--%text%-->
				                    </div>
				                    <div class="cbp-l-caption-icos"><span class="ico-eye"></span></div>
				                </div>
				            </div>  
				        </div>
					  </a>
				    </div>
				</div>
				<div class="clearfix"></div>
            </div>
        </div>
	</div>
	<div class="text-center margin-top-35">
		<button id="cube-portfolio-more" class="button button-red-transparent">
			Показать ещё
		</button>
	</div>
</div>

<div class="space-50"></div>
@stop