@extends('site.layout', ['Title' => 'Главная страница'])

@section('head-styles')
<link rel="stylesheet" href="assets/css/index.css">
@stop

@section('footer')
<script src="assets/js/index.js"></script>
@stop

@section('content')
<div class="slider">
    <div id="slider" class="flexslider">
        <ul class="slider-slides">
            <li class="slider-slide" style="background-image:url(assets/img/temp/slide1.jpg);">
                <div class="slider-content container">
                    <div class="slider-description">
                        <div class="slider-header">
                            Создание сайта музыкального конкурса имени П.И. Чайковского
                        </div>
                        <div class="slider-text">
                            Международный конкурс имени П.И. Чайковского является крупнейшим событием в мире классический музыки.
                        </div>
                    </div>
                    <button class="button button-red-transparent slider-button">перейти к портфолио</button>
                        <div id="metronom" class="slider-animation">
                            <div id="metronom_arr"></div>
                        </div>
                    </div>    
            </li>
            <li class="slider-slide" style="background-image:url(assets/img/temp/slide2.jpg);">
                <div class="slider-content container">
                    <div class="slider-description">
                        <div class="slider-header">
                            Создание сайта ZORGTECH
                        </div>
                        <div class="slider-text">
                            Компания Zorgtech – один из лидеров российского рынка разработки и производства сенсорных информационных киосков и программного обеспечения для них.
                        </div>
                    </div>
                    <button class="button button-red-transparent slider-button">перейти к портфолио</button>
                    <div id="zorg" class="slider-animation">
                        <div id="zorg_screen1"></div>
                        <div id="zorg_screen2"></div>
                    </div>
                </div>  
            </li>
            <li class="slider-slide" style="background-image:url(assets/img/temp/slide3.jpg);">
                <div class="slider-content container">
                    <div class="slider-description">
                        <div class="slider-header">
                            Интернет-магазин «Мебель-альянс»
                        </div>
                        <div class="slider-text">
                            На новом сайте «Мебель-Альянс» удобно разместился весь модельный ряд эксклюзивной мебели: столы и стулья, банкетки и пуфики.
                        </div>
                    </div>
                    <button class="button button-red-transparent slider-button">перейти к портфолио</button>
                    <div id="chair_wrap" class="slider-animation">
                        <div id="chair"></div>
                        <div id="chair_shadows"></div>
                    </div>
                </div>  
            </li>
            <li class="slider-slide" style="background-image:url(assets/img/temp/slide4.jpg);">
                <div class="slider-content container">
                    <div class="slider-description">
                        <div class="slider-header">
                            Создание промо-сайта ОАО «Нарзан»
                        </div>
                        <div class="slider-text">
                            «Нарзан» – это всемирно известная торговая марка минеральной воды с богатой историей и традициями.
                        </div>
                    </div>
                    <button class="button button-red-transparent slider-button">перейти к портфолио</button>
                    <div id="bottle" class="slider-animation" data-frame-current="1" style="background: url(assets/img/index/narzan/1.png);">
                        <img data-frame="1" style="display: none;" src="assets/img/index/narzan/1.png">
                        <img data-frame="2" style="display: none;" src="assets/img/index/narzan/2.png">
                        <img data-frame="3" style="display: none;" src="assets/img/index/narzan/3.png">
                        <img data-frame="4" style="display: none;" src="assets/img/index/narzan/4.png">
                        <img data-frame="5" style="display: none;" src="assets/img/index/narzan/5.png">
                        <img data-frame="6" style="display: none;" src="assets/img/index/narzan/6.png">
                        <img data-frame="7" style="display: none;" src="assets/img/index/narzan/7.png">
                        <img data-frame="8" style="display: none;" src="assets/img/index/narzan/8.png">
                        <img data-frame="9" style="display: none;" src="assets/img/index/narzan/9.png">
                        <img data-frame="10" style="display: none;" src="assets/img/index/narzan/10.png">
                        <img data-frame="11" style="display: none;" src="assets/img/index/narzan/11.png">
                        <img data-frame="12" style="display: none;" src="assets/img/index/narzan/12.png">
                        <img data-frame="13" style="display: none;" src="assets/img/index/narzan/13.png">
                        <img data-frame="14" style="display: none;" src="assets/img/index/narzan/14.png">
                        <img data-frame="15" style="display: none;" src="assets/img/index/narzan/15.png">
                    </div>
                </div>  
            </li>
            <li class="slider-slide" style="background-image:url(assets/img/temp/slide51.jpg);">
                <div class="slider-content container">
                    <div id="chat" class="slider-animation">
                        <div id="chat1"></div>
                        <div id="chat2"></div>
                    </div>
                </div>
            </li>

            <li class="slider-slide" style="background-image:url(assets/img/temp/slide6.jpg);">
                <div class="slider-content container">
                    <div class="slider-description">
                        <div class="slider-header">
                            Интернет-магазин StackDeck
                        </div>
                        <div class="slider-text">
                            Компания StackDeck занимается продажей и монтажом строительных материалов из древесно-полимерного композита.
                        </div>
                    </div>
                    <button class="button button-red-transparent slider-button">перейти к портфолио</button>
                    <div id="stackdeck" class="slider-animation" data-frame-current="1" style="background: url(assets/img/index/stackdeck/1.png);">
                        <img data-frame="1" style="display: none;" src="assets/img/index/stackdeck/1.png">
                        <img data-frame="2" style="display: none;" src="assets/img/index/stackdeck/2.png">
                        <img data-frame="3" style="display: none;" src="assets/img/index/stackdeck/3.png">
                        <img data-frame="4" style="display: none;" src="assets/img/index/stackdeck/4.png">
                        <img data-frame="5" style="display: none;" src="assets/img/index/stackdeck/5.png">
                        <img data-frame="6" style="display: none;" src="assets/img/index/stackdeck/4.png">
                        <img data-frame="7" style="display: none;" src="assets/img/index/stackdeck/3.png">
                        <img data-frame="8" style="display: none;" src="assets/img/index/stackdeck/2.png">
                        <img data-frame="9" style="display: none;" src="assets/img/index/stackdeck/1.png">
                    </div>
                </div>    
            </li>
        </ul>
    </div>
</div>


<div class="about container-fluid">
    <div class="container">
        <a class="page-anchor" data-name="О компании" name="about"></a>
        <h2 class="margin-bigger">Немного о нас</h2>
        <div class="row">
            <div class="col-md-12 margin-bottom-30">
            Компания Webis Group была основана в 2000 году. Наши специалисты – высококвалифицированные профессионалы, имеющие многолетний опыт работы. Наши клиенты – коммерческие структуры, общественные организации, холдинговые компании, международные корпорации из России, США и западной Европы.
            </div>
        </div>
        <div class="about-row row">
            <div class="col-md-4 about-item">
                <div class="about-item-header">
                    Делаем cайты<br>уже 15 лет
                </div>
                <div class="about-item-img">
                    <img class="img-responsive m-auto" src="assets/img/index/about1.png" alt="">
                </div>
                <div class="about-item-thesis">идем в ногу со временем</div>
                <div class="about-item-text">
                    Реализуем ваши идеи,<span class="hidden-lg hidden-md"><br></span> дополнив их нашим опытом и знаниями.<span class="hidden-lg hidden-md"><br></span> И никогда не делаем одинаковых сайтов
                </div>
            </div>
            <div class="col-md-4 about-item">
                <div class="about-item-header">
                    Только<br>современные технологии
                </div>
                <div class="about-item-img">
                    <img class="img-responsive m-auto" src="assets/img/index/about2.png" alt="">
                </div>
                <div class="about-item-thesis">+собственные разработки</div>
                <div class="about-item-text">
                    Чат для сайта WebisOnline,<span class="hidden-lg hidden-md"><br></span> Торговая система &laquo;Ритейл-терминал&raquo;,<span class="hidden-lg hidden-md"><br></span> Система управления сайтом Abante CMS
                </div>
            </div>
            <div class="col-md-4 about-item">
                <div class="about-item-header">
                    Более 500<br>довольных клиентов
                </div>
                <div class="about-item-img">
                    <img class="img-responsive m-auto" src="assets/img/index/about3.png" alt="">
                </div>
                <div class="about-item-thesis">красиво и с гарантией</div>
                <div class="about-item-text">
                    Стабильно высокое качество<span class="hidden-lg hidden-md"><br></span> дизайнерских решений и гарантийная<span class="hidden-lg hidden-md"><br></span> поддержка на весь срок службы сайта
                </div>
            </div>
        </div>
    </div>
</div>

<!--next-->
<div class="container-fluid services">
    <a class="page-anchor" data-name="Услуги" name="services"></a>
    <h2 class="margin-bigger">Услуги</h2>
    <div class="container services-desktop">
        <div class="row">
            <div class="services-item col-lg-4 col-md-6 col-sm-6 col-xs-12">
                <div class="services-item-img"> <img src="assets/img/index/services_ico1.png" alt=""></div>
                <div class="services-item-header">Cоздание<br>сайтов</div>
                <div class="services-item-text">Вам нужен качественный сайт?<br>Хотите продавать через интернет?<br>Запустить оригинальный<br>онлайн-проект?<br>Создание сайтов на CMS<br>— наш осн     овной профиль.</div>
            </div>
            <div class="services-item col-lg-4 col-md-6 col-sm-6 col-xs-12">
                <div class="services-item-img"> <img src="assets/img/index/services_ico2.png" alt=""></div>
                <div class="services-item-header">Создание<br>интернет-магазина</div>
                <div class="services-item-text">Наши интернет-магазины:<br>- увеличивают продажи,<br>- расширяют географию продаж,<br>- обеспечивают бесперебойную доставку,<br>- обгоняют конкурентов</div>
            </div>
            <div class="services-item col-lg-4 col-md-6 col-sm-6 col-xs-12">
                <div class="services-item-img"> <img src="assets/img/index/services_ico3.png" alt=""></div>
                <div class="services-item-header">Адаптация сайта<br> для мобильных устройств</div>
                <div class="services-item-text">Наши интернет-магазины:<br>- увеличивают продажи,<br>- расширяют географию продаж,<br>- обеспечивают бесперебойную доставку,<br>- обгоняют конкурентов</div>
            </div>
            <div class="services-item col-lg-4 col-md-6 col-sm-6 col-xs-12">
                <div class="services-item-img"> <img src="assets/img/index/services_ico4.png" alt=""></div>
                <div class="services-item-header">Повышение<br>конверсии</div>
                <div class="services-item-text">Мы используем самые современные<br>технологии для анализа<br>поведения посетителейна сайте<br>и ежедневно совершенствуемся.</div>
            </div>
            <div class="services-item col-lg-4 col-md-6 col-sm-6 col-xs-12">
                <div class="services-item-img"> <img src="assets/img/index/services_ico5.png" alt=""></div>
                <div class="services-item-header">Контекстная<br>реклама</div>  
                <div class="services-item-text">Размещение рекламы на Яндекс,<br>Google, Rambler, Mail.ru<br>и тысячах других сайтов.<br>Низкая стоимость, мгновенный эффект, измеримый результат.</div>
            </div>
            <div class="services-item col-lg-4 col-md-6 col-sm-6 col-xs-12">
                <div class="services-item-img"> <img src="assets/img/index/services_ico6.png" alt=""></div>
                <div class="services-item-header">Продвижение сайтов<br> и SEO копирайтинг</div>
                <div class="services-item-text">Мы обеспечим вашему сайту<br>стабильный приток целевых<br>посетителей из поисковых систем<br>и мотивируем их к контакту.</div>
            </div>
            <div class="clearfix"></div>
        </div>
    </div>
</div>


@include('partials._reviews')

<!--portfolio-->
<div id="cube-portfolio" class="container padding-bottom">
    <a class="page-anchor" data-name="Портфолио" name="portfolio"></a>
    <h2 class="margin-bigger">Портфолио</h2>
    <div class="row">
        <div id="filters-container" class="cbp-l-filters-alignCenter" >
            <div data-filter="*" class="cbp-filter-item-active cbp-filter-item">
                Все работы <div class="cbp-filter-counter"></div>
            </div>
            <div data-filter=".apartments" class="cbp-filter-item">
                Сайты <div class="cbp-filter-counter"></div>
            </div>
            <div data-filter=".houses" class="cbp-filter-item">
                Фирменный стиль <div class="cbp-filter-counter"></div>
            </div>
            <div data-filter=".architecture" class="cbp-filter-item">
                3D и мультимедиа <div class="cbp-filter-counter"></div>
            </div>
            <div data-filter=".filter-more1" class="cbp-filter-item">
                Полиграфия <div class="cbp-filter-counter"></div>
            </div>
            <div data-filter=".filter-more2" class="cbp-filter-item">
                Прочее <div class="cbp-filter-counter"></div>
            </div>
        </div>
        <div id="grid-container" class="cbp-l-grid-masonry">
            <div class="portfolio-list">
                <div class="cbp-item apartments houses filter-more1 cbp-l-grid-masonry-height1">
                    <div class="cbp-caption">
                        <a href="#">
                            <div class="cbp-caption-defaultWrap">
                                <img src="assets/img/temp/port1.jpg" alt="">
                            </div>
                            <div class="cbp-caption-activeWrap">
                                <div class="cbp-l-caption-alignCenter">
                                    <a href="#" class="cbp-l-caption-body">   
                                        <span class="cbp-l-caption-title">Создание сайта &laquo;Инвестиционный департамент&raquo;</span>
                                        <div class="cbp-l-caption-text">
                                          <!--text-->
                                        </div>
                                        <div class="cbp-l-caption-icos">
                                          <span class="ico-eye" href="#"></span>
                                        </div>                                          
                                    </a>
                                </div>
                            </div>
                        </a>
                    </div>
                </div>
                <div class="cbp-item apartments houses filter-more1 cbp-l-grid-masonry-height1">
                    <div class="cbp-caption">
                        <a href="#">
                            <div class="cbp-caption-defaultWrap">
                                <img src="assets/img/temp/port2.jpg" alt="">
                            </div>
                            <div class="cbp-caption-activeWrap">
                                <div class="cbp-l-caption-alignCenter">
                                    <a href="#" class="cbp-l-caption-body">   
                                        <span class="cbp-l-caption-title">Создание сайта &laquo;Инвестиционный департамент&raquo;</span>
                                        <div class="cbp-l-caption-text">
                                          <!--text-->
                                        </div>
                                        <div class="cbp-l-caption-icos">
                                          <span class="ico-eye" href="#"></span>
                                        </div>                                          
                                    </a>
                                </div>
                            </div>
                        </a>
                    </div>
                </div>
                <div class="cbp-item apartments filter-more1 cbp-l-grid-masonry-height1">
                    <div class="cbp-caption">
                        <a href="#">
                            <div class="cbp-caption-defaultWrap">
                                <img src="assets/img/temp/port3.jpg" alt="">
                            </div>
                            <div class="cbp-caption-activeWrap">
                                <div class="cbp-l-caption-alignCenter">
                                    <a href="#" class="cbp-l-caption-body">   
                                        <span class="cbp-l-caption-title">Создание сайта &laquo;Инвестиционный департамент&raquo;</span>
                                        <div class="cbp-l-caption-text">
                                          <!--text-->
                                        </div>
                                        <div class="cbp-l-caption-icos">
                                          <span class="ico-eye" href="#"></span>
                                        </div>                                          
                                    </a>
                                </div>
                            </div>
                        </a>
                    </div>
                </div>
                <div class="cbp-item apartments architecture filter-more1 cbp-l-grid-masonry-height1">
                    <div class="cbp-caption">
                        <a href="#">
                            <div class="cbp-caption-defaultWrap">
                                <img src="assets/img/temp/port4.jpg" alt="">
                            </div>
                            <div class="cbp-caption-activeWrap">
                                <div class="cbp-l-caption-alignCenter">
                                    <a href="#" class="cbp-l-caption-body">   
                                        <span class="cbp-l-caption-title">Создание сайта &laquo;Инвестиционный департамент&raquo;</span>
                                        <div class="cbp-l-caption-text">
                                          <!--text-->
                                        </div>
                                        <div class="cbp-l-caption-icos">
                                          <span class="ico-eye" href="#"></span>
                                        </div>                                          
                                    </a>
                                </div>
                            </div>
                        </a>
                    </div>
                </div>
                <div class="cbp-item apartments architecture filter-more1 cbp-l-grid-masonry-height1">
                    <div class="cbp-caption">
                        <a href="#">
                            <div class="cbp-caption-defaultWrap">
                                <img src="assets/img/temp/port5.jpg" alt="">
                            </div>
                            <div class="cbp-caption-activeWrap">
                                <div class="cbp-l-caption-alignCenter">
                                    <a href="#" class="cbp-l-caption-body">   
                                        <span class="cbp-l-caption-title">Создание сайта &laquo;Инвестиционный департамент&raquo;</span>
                                        <div class="cbp-l-caption-text">
                                          <!--text-->
                                        </div>
                                        <div class="cbp-l-caption-icos">
                                          <span class="ico-eye" href="#"></span>
                                        </div>                                          
                                    </a>
                                </div>
                            </div>
                        </a>
                    </div>
                </div>
                <div class="cbp-item apartments filter-more1 filter-more2 cbp-l-grid-masonry-height1">
                    <div class="cbp-caption">
                        <a href="#">
                            <div class="cbp-caption-defaultWrap">
                                <img src="assets/img/temp/port6.jpg" alt="">
                            </div>
                            <div class="cbp-caption-activeWrap">
                                <div class="cbp-l-caption-alignCenter">
                                    <a href="#" class="cbp-l-caption-body">   
                                        <span class="cbp-l-caption-title">Создание сайта &laquo;Инвестиционный департамент&raquo;</span>
                                        <div class="cbp-l-caption-text">
                                          <!--text-->
                                        </div>
                                        <div class="cbp-l-caption-icos">
                                          <span class="ico-eye" href="#"></span>
                                        </div>                                          
                                    </a>
                                </div>
                            </div>
                        </a>
                    </div>
                </div>
                <div class="clearfix"></div>
            </div>
        </div>
    </div>
</div>

<div class="news-column" class="container-fluid">
    <h2 class="margin-bigger">Новости</h2>
    <div class="container">
        <div class="row">
            <div class="news-column-item col-md-4 padding-col-35">
                <div class="news-column-header">
                    <div class="news-column-header-inner">
                        Заголовок новости
                    </div>
                </div>
                <div class="news-column-img">
                    <a href="#">
                        <div class="overlay-eye"></div>
                        <img class="img-responsive" src="assets/img/temp/news1.jpg">
                    </a>
                </div>
                <div class="news-column-text">
                    Здесь будет краткое описание новости, чтобы было понятно о чем эта новость не переходя по ссылке
                </div>
            </div>
            <div class="news-column-item col-md-4 padding-col-35">
                <div class="news-column-header">
                    <div class="news-column-header-inner">
                        Заголовок новости
                    </div>
                </div>
                <div class="news-column-img">
                    <a href="#">
                        <div class="overlay-eye"></div>
                        <img class="img-responsive" src="assets/img/temp/news1.jpg">
                    </a>
                </div>
                <div class="news-column-text">
                    Здесь будет краткое описание новости, чтобы было понятно о чем эта новость не переходя по ссылке
                </div>
            </div>
            <div class="news-column-item col-md-4 padding-col-35">
                <div class="news-column-header">
                    <div class="news-column-header-inner">
                        Заголовок новости
                    </div>
                </div>
                <div class="news-column-img">
                    <a href="#">
                        <div class="overlay-eye"></div>
                        <img class="img-responsive" src="assets/img/temp/news1.jpg">
                    </a>
                </div>
                <div class="news-column-text">
                    Здесь будет краткое описание новости, чтобы было понятно о чем эта новость не переходя по ссылке
                </div>
            </div>
        </div>
    </div>
</div>

<div id="clients-row" class="container-fluid oh">
    <div class="container">
        <a class="page-anchor" data-name="Наши клиенты" name="clients"></a>
        <h2 class="white margin-bigger">Наши клиенты</h2>
    </div>
    <div class="clients-scrollable">
        <div class="clients-scroller">
            <div class="clients-item">
                <img src="assets/img/temp/clientslogo1.png" alt="">
            </div>
            <div class="clients-item">
                <img src="assets/img/temp/clientslogo2.png" alt="">
            </div>
            <div class="clients-item">
                <img src="assets/img/temp/clientslogo3.png" alt="">
            </div>
            <div class="clients-item">
                <img src="assets/img/temp/clientslogo4.png" alt="">
            </div>
            <div class="clients-item">
                <img src="assets/img/temp/clientslogo5.png" alt="">
            </div>
            <div class="clients-item">
                <img src="assets/img/temp/clientslogo6.png" alt="">
            </div>
            <div class="clients-item">
                <img src="assets/img/temp/clientslogo7.png" alt="">
            </div>
            <div class="clients-item">
                <img src="assets/img/temp/clientslogo8.png" alt="">
            </div>
            <div class="clients-item">
                <img src="assets/img/temp/clientslogo9.png" alt="">
            </div>
        </div>
    </div>
    <div class="container text-center">
        <a href="#">
            <button class="button button-red-transparent clients-button">Перейти к портфолио</button>
        </a>
    </div>
</div>

@stop