@extends('site.layout', ['Title' => 'Продвижение сайтов и SEO копирайтинг'])

@section('head-styles')
<link rel="stylesheet" href="assets/plugins/amcharts/style.css">
<link rel="stylesheet" href="assets/css/seo.css">
@stop

@section('footer')
<script type="text/javascript" src="assets/plugins/amcharts/amcharts.js"></script>
<script type="text/javascript" src="assets/plugins/amcharts/amstock.js"></script>
<script type="text/javascript" src="assets/plugins/amcharts/themes/light.js"></script>
<script type="text/javascript" src="assets/plugins/amcharts/serial.js"></script>
@stop

@section('content')
<div class="header">
    <div class="header-inner container">
        <div class="row">
            <div class="col-md-10 col-sm-12 col-xs-12 col-lg-offset-1">
                <h1>
            Продвижение сайтов <span class="hidden-sm hidden-xs"><br></span>
            и SEO копирайтинг
          </h1>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-offset-1 col-lg-10">
                <div class="header-search">
                    <a href="#form-popup-default" class="header-search-field fb-form">
                        <span class="header-search-field-text">Миллионы людей используют поисковые системы каждый день. <span class="hidden-sm hidden-xs">Это океан возможностей.</span></span>
                    </a>
                    <div class="header-search-space">
                        &nbsp;
                    </div>
                    <a href="#form-popup-default" class="header-search-btn fb-form">
                    </a>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="advantages container">
    <div class="row">
        <div class="advantages-item col-md-4 col-sm-4 col-xs-12">
            <div class="advantages-item-inner">
                <div class="advantages-header">
                    <div class="center">Выведем сайт
                        <br>в топ</div>
                </div>
                <div class="advantages-img"><img class="img-responsive" src="assets/img/seo/circle1.png">
                </div>
            </div>
            <div class="advantages-text">
                Ваш сайт попадет на верхние строки выдачи результатов поиска «Яндекса», Google и других поисковиков.
            </div>
        </div>
        <div class="advantages-item col-md-4 col-sm-4 col-xs-12">
            <div class="advantages-item-inner">
                <div class="advantages-header">
                    <div class="center">Увеличим
                        <br>посещаемость</div>
                </div>
                <div class="advantages-img"><img class="img-responsive" src="assets/img/seo/circle2.png">
                </div>
            </div>
            <div class="advantages-text">
                Вы привлечете большое количество покупателей и расширите продажи
            </div>
        </div>
        <div class="advantages-item col-md-4 col-sm-4 col-xs-12">
            <div class="advantages-item-inner">
                <div class="advantages-header">
                    <div class="center">Честно
                        <br>и с гарантией</div>
                </div>
                <div class="advantages-img"><img class="img-responsive" src="assets/img/seo/circle3.png">
                </div>
            </div>
            <div class="advantages-text">
                Мы продвигаем сайты только честными методами, поэтому ваш сайт не будет исключен из поисковой выдачи
            </div>
        </div>
    </div>
</div>

<div class="results container">
    <h2>Как выглядит результат продвижения:</h2>
    <div class="results-panel">
        <div class="row">
            <div class="results-panel-current col-md-6 col-sm-6 col-xs-6">
                Проект:
                <a href="#" target="_blank" class="results-panel-project"></a>
            </div>
        </div>
    </div>
    <div id="results-slides" class="results-slides">
        <div class="results-slide" data-project="www.krascks.ru">
            <div class="row">
                <div class="results-slide-img col-md-6 padding-right-sm-60">
                    <img src="assets/img/seo/results_img.png" class="img-responsive" alt="">
                </div>
                <div class="results-slide-content col-md-6">
                    <div class="results-header">Ключевые моменты реализации</div>
                    <div class="results-slide-item">
                        <div class="results-slide-item-img">
                            <img src="assets/img/seo/results_ico1.png" class="img-responsive" alt="">
                        </div>
                        <div class="results-slide-item-text">
                            Настройка Google Analytics
                        </div>
                    </div>
                    <div class="results-slide-item">
                        <div class="results-slide-item-img">
                            <img src="assets/img/seo/results_ico2.png" class="img-responsive" alt="">
                        </div>
                        <div class="results-slide-item-text">
                            Подготовка рекомендаций по оптимизации рекламных кампаний
                        </div>
                    </div>
                    <div class="results-slide-item">
                        <div class="results-slide-item-img">
                            <img src="assets/img/seo/results_ico3.png" class="img-responsive" alt="">
                        </div>
                        <div class="results-slide-item-text">
                            Разработка сценариев ремаркетинга для запуска рекламных объявлений
                        </div>
                    </div>
                    <div class="results-slide-item">
                        <div class="results-slide-item-img">
                            <img src="assets/img/seo/results_ico4.png" class="img-responsive" alt="">
                        </div>
                        <div class="results-slide-item-text">
                            Разработка формы отчётности под основные KPI, аккумулирующей в себе статистику из рекламных систем, систем веб-аналитики и отслеживания звонков
                        </div>
                    </div>
                </div>
            </div>
            <div class="row results-slide-charts">
                <div class="col-md-6">
                    <div class="results-header text-center">Количество заявок <span class="ico-chart-up"></span> <span class="font-bold f-32">61</span><span class="font-bold f-20">%</span>
                    </div>
                    <div id="chart-left-1" class="results-slide-chart"></div>
                </div>
                <div class="col-md-6">
                    <div class="results-header text-center">Стоимость заявки <span class="ico-chart-down"></span> <span class="font-bold f-32">23</span><span class="font-bold f-20">%</span>
                    </div>
                    <div id="chart-right-1" class="results-slide-chart"></div>
                </div>
            </div>
        </div>

        <div class="results-slide display-none" data-project="www.krascks.ru">
            <div class="row">
                <div class="results-slide-img col-md-6 padding-right-sm-60">
                    <img src="assets/img/seo/results_img.png" class="img-responsive" alt="">
                </div>
                <div class="results-slide-content col-md-6">
                    <div class="results-header">Ключевые моменты реализации</div>
                    <div class="results-slide-item">
                        <div class="results-slide-item-img">
                            <img src="assets/img/seo/results_ico1.png" class="img-responsive" alt="">
                        </div>
                        <div class="results-slide-item-text">
                            Настройка Google Analytics
                        </div>
                    </div>
                    <div class="results-slide-item">
                        <div class="results-slide-item-img">
                            <img src="assets/img/seo/results_ico2.png" class="img-responsive" alt="">
                        </div>
                        <div class="results-slide-item-text">
                            Подготовка рекомендаций по оптимизации рекламных кампаний
                        </div>
                    </div>
                    <div class="results-slide-item">
                        <div class="results-slide-item-img">
                            <img src="assets/img/seo/results_ico3.png" class="img-responsive" alt="">
                        </div>
                        <div class="results-slide-item-text">
                            Разработка сценариев ремаркетинга для запуска рекламных объявлений
                        </div>
                    </div>
                    <div class="results-slide-item">
                        <div class="results-slide-item-img">
                            <img src="assets/img/seo/results_ico4.png" class="img-responsive" alt="">
                        </div>
                        <div class="results-slide-item-text">
                            Разработка формы отчётности под основные KPI, аккумулирующей в себе статистику из рекламных систем, систем веб-аналитики и отслеживания звонков
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-6">
                    <div class="results-header text-center">Количество заявок <span class="ico-chart-up"></span> <span class="font-bold f-32">61</span><span class="font-bold f-20">%</span>
                    </div>
                    <div id="chart-left-2" class="results-slide-chart"></div>
                </div>
                <div class="col-md-6">
                    <div class="results-header text-center">Стоимость заявки <span class="ico-chart-down"></span> <span class="font-bold f-32">23</span><span class="font-bold f-20">%</span>
                    </div>
                    <div id="chart-right-2" class="results-slide-chart"></div>
                </div>
            </div>
        </div>
    </div>
    <div class="text-center margin-top-25">
        <button class="button button-red-transparent fb-form" data-fancybox-href="#form-popup-default">Хочу такой же результат</button>
    </div>
</div>

<div class="seo container">
    <h2>Продвижение сайтов - это:</h2>
    <div class="row">
        <div class="col-md-6 seo-item">
            <div class="seo-item-inner">
                <div class="seo-item-ico">
                    <img src="assets/img/seo/seo1.png" alt="">
                </div>
                <div class="seo-item-content">
                    <div class="seo-item-header">Повышение качества сайта</div>
                    <div class="seo-item-text">
                        Это процесс, который занимает длительное время, а значит, выглядит естественно с точки зрения поисковых систем (проще говоря, сайт не забанят)
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-6 seo-item">
            <div class="seo-item-inner">
                <div class="seo-item-ico">
                    <img src="assets/img/seo/seo2.png" alt="">
                </div>
                <div class="seo-item-content">
                    <div class="seo-item-header">Доверие клиентов</div>
                    <div class="seo-item-text">
                        Посетители считают, что сайты, находящиеся на первых строках поисковой выдачи, обладают необходимым уровнем качества, более достоверны и надежны
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-6 seo-item">
            <div class="seo-item-inner">
                <div class="seo-item-ico">
                    <img src="assets/img/seo/seo3.png" alt="">
                </div>
                <div class="seo-item-content">
                    <div class="seo-item-header">Целевые посетители</div>
                    <div class="seo-item-text">
                        По целевым запросам ваш сайт будет находиться на первых строках поиска и будет привлекать больше целевых посетителей
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-6 seo-item">
            <div class="seo-item-inner">
                <div class="seo-item-ico">
                    <img src="assets/img/seo/seo4.png" alt="">
                </div>
                <div class="seo-item-content">
                    <div class="seo-item-header">Продвижение бренда</div>
                    <div class="seo-item-text">
                        Оптимизация сайта позволяет не только привлечь посетителей и увеличить продажи, но и заняться продвижением бренда
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="stages">
    <h2>Этапы продвижения сайтов</h2>
    <div class="container-fluid stages-inner">
        <div class="container">
            <div class="stages-item">
                <div class="stages-item-inner">
                    <div class="stages-arrow"></div>
                    <div class="stages-number">1</div>
                    <div class="stages-content">
                        <div class="stages-header">Проводим анализ сайта и рынка</div>
                        <div class="stages-text">
                            Мы анализируем тематику сайта и определяем количество запросов по тематике. В результате определяем потенциал продвижения вашего сайта. Далее анализируем положение сайта относительно конкурентов
                        </div>
                    </div>
                </div>
            </div>
            <div class="stages-item">
                <div class="stages-item-inner">
                    <div class="stages-arrow"></div>
                    <div class="stages-number">2</div>
                    <div class="stages-content">
                        <div class="stages-header">Подбираем стратегию и запросы</div>
                        <div class="stages-text">
                            Мы анализируем сегмент рынка, вычленяем запросы, по которым переходят именно потенциальные покупатели. Это позволит наиболее эффективно использовать бюджет и обеспечит максимальную отдачу от проводимых работ.
                        </div>
                    </div>
                </div>
            </div>
            <div class="stages-item">
                <div class="stages-item-inner">
                    <div class="stages-arrow"></div>
                    <div class="stages-number">3</div>
                    <div class="stages-content">
                        <div class="stages-header">Оптимизируем структуру сайта</div>
                        <div class="stages-text">
                            Поисковым системам важно качество и уникальность информации, чёткая структура разделов и понятные заголовки страниц. А пользователям важно быстро решить свои задачи, и вся необходимая информация должна быть легко доступна.
                        </div>
                    </div>
                </div>
            </div>
            <div class="stages-item">
                <div class="stages-item-inner">
                    <div class="stages-arrow"></div>
                    <div class="stages-number">4</div>
                    <div class="stages-content">
                        <div class="stages-header">Создаем контент</div>
                        <div class="stages-text">
                            Смысл копирайтинга в том, чтобы написать текст, который является легким и понятным пользователю, и в то же время содержит определенное количество ключевых слов, используемых в поиске. SEO-копирайтинг позволяет сделать информацию доступной, и повысить рейтинг сайта в поисковых системах.
                        </div>
                    </div>
                </div>
            </div>
            <div class="stages-item">
                <div class="stages-item-inner">
                    <div class="stages-arrow"></div>
                    <div class="stages-number">5</div>
                    <div class="stages-content">
                        <div class="stages-header">Работаем с внешними факторами</div>
                        <div class="stages-text">
                            Наши специалисты проводят ежедневную работу над внешними факторами с учётом тенденций и изменений поисковых алгоритмов. Очень важно и, в то же время, очень сложно грамотно распорядиться ссылочным «весом» сайта, чтобы поисковые системы восприняли проводимые работы положительно.
                        </div>
                    </div>
                </div>
            </div>
            <div class="stages-item">
                <div class="stages-item-inner">
                    <div class="stages-arrow"></div>
                    <div class="stages-number">6</div>
                    <div class="stages-content">
                        <div class="stages-header">Анализируем и корректируем</div>
                        <div class="stages-text">
                            Мы ежемесячно готовим отчеты и проводим анализ статистики посещаемости и поведения посетителей на сайте. По результатам этого анализа мы вырабатываем рекомендации, которые позволят повысить эффективность работы по продвижению и увеличить продажи с сайта
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="container">
    <h2 class="margin-top-50 margin-bottom-30">SEO-Копирайтинг<span class="hidden-sm hidden-xs"><br></span>как часть поисковой оптимизации</h2>
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <p>
                В рамках оптимизации сайта мы также проводим работу с контентом. SEO-копирайтинг – это изменение текста таким образом, чтобы он отвечал параметрам поискового запроса.
            </p>
            <p>
                Мы предлагает вам грамотный SEO-копирайтинг, выполненный лучшими специалистами в этой области. Мы имеем многолетний опыт работы по продвижению сайтов как в России, так и за рубежом.
            </p>
            <p>
                Работая с нашей компанией, Вы сможете легко и быстро получить то, что Вам нужно.
            </p>
            <b>Мы составим для Вас:</b>
            <div class="statements-xs margin-top-20">
                <div class="statements-item col-md-3 col-sm-3">
                    <div class="statements-item-img">
                        <img class="img-responsive" src="assets/img/seo/principles1.jpg" alt="">
                    </div>
                    <div class="statements-item-text">Оригинальный контент сайта</div>
                </div>
                <div class="statements-item col-md-3 col-sm-3">
                    <div class="statements-item-img">
                        <img class="img-responsive" src="assets/img/seo/principles2.jpg" alt="">
                    </div>
                    <div class="statements-item-text">Пресс-релизы и рекламные статьи</div>
                </div>
                <div class="statements-item col-md-3 col-sm-3">
                    <div class="statements-item-img">
                        <img class="img-responsive" src="assets/img/seo/principles3.jpg" alt="">
                    </div>
                    <div class="statements-item-text">Рекламные слоганы</div>
                </div>
                <div class="statements-item col-md-3 col-sm-3">
                    <div class="statements-item-img">
                        <img class="img-responsive" src="assets/img/seo/principles4.jpg" alt="">
                    </div>
                    <div class="statements-item-text">Любые тексты с использованием SEO-копирайтинга</div>
                </div>
            </div>
            <div class="text-center margin-top-20">
                <button class="button button-red-transparent fb-form" data-fancybox-href="#form-popup-default">Оставить заявку</button>
            </div>
        </div>
    </div>
</div>

<div class="prices">
    <div class="container">
        <div class="row">
            <div class="col-md-5 col-md-offset-1">
                <div class="font-regular text-upper f-20 margin-bottom-md-15 margin-bottom-sm-5">Стоимость продвижения сайта</div>
                <div class="font-regular f-18 white margin-bottom-10">от <span class="f-32">20 000</span> рублей в месяц</div>
                <div>
                    Мы рассчитываем стоимость в зависимости от конкуренции на рынке и ваших планов по бюджету кампании.
                    <br>
                    <a href="#" class="fb-form font-bold" data-fancybox-href="#form-popup-default">Сделайте запрос</a>, и мы в кратчайшие сроки подготовим коммерческое предложение.
                </div>
            </div>
            <div class="col-md-5 col-md-offset-1 margin-top-md-0 margin-top-xs-40">
                <div class="font-regular text-upper f-20 margin-bottom-md-15 margin-bottom-sm-5">Сроки продвижения сайта</div>
                <div class="font-regular f-18 white margin-bottom-10">От <span class="f-32">2 месяцев</span>
                </div>
                <div>
                    Сроки продвижения также зависят от многих факторов. Среди них возраст сайта, конкуренция на рынке, частота обновления информации на сайте.
                    <br> Мы оцениваем сроки продвижения индивидуальо для каждого проекта.
                </div>
            </div>
        </div>
    </div>
</div

@stop