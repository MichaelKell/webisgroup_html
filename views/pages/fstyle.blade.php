@extends('site.layout', ['Title' => 'Фирменный стиль'])


@section('head-styles')
<link rel="stylesheet" href="assets/css/fstyle.css">
@stop

@section('footer')
<script src="assets/js/fstyle.js"></script>
@stop

@section('content')
<div class="header">
	<div class="header-inner container">
		<div class="row">
			<div class="col-md-11 col-md-offset-1">
				<h1>
					Уникальность и узнаваемость
				</h1>
				<div class="header-thesis">
					Разработка логотипа, фирменного стиля,<span class="hidden-xs"><br></span> упаковки и полиграфии
				</div>
			</div>
		</div>
	</div>
</div>

<div class="about container margin-top-lg-m110 margin-top-md-m100 margin-top-sm-m90 margin-top-xs-m70">
	<h2>
		Фирменный стиль - это лицо компании
	</h2>
	<div class="text-center">
	Нейминг, разработка логотипа и слогана, фирменные сайты, мультимедиа-презентации:<span class="hidden-xs hidden-sm"><br></span>
	мы поможем вашему бизнесу обрести узнаваемое лицо и начать новый этап развития.<span class="hidden-xs hidden-sm"><br></span>
	Вы практически сразу заметите значительные изменения.
	</div>
</div>


<div class="circles container">
	<div class="row">
		<div class="circles-item col-md-4 col-sm-4">
			<div class="circles-header"><div class="center">Нэйминг<br>и брендинг</div></div>
			<div class="circles-img"><img class="img-responsive" src="assets/img/fstyle/circle3.png"></div>
			<div class="circles-text">
			Запускаете новую компанию?<br>
			Готовите новый проект?<br>
			Думаете о ребрендинге?<br>
			Правильно определить аудиторию,
			создать бренд, сформулировать
			ключевые ценности – мы поможем
			решить все эти вопросы.
			Мы разработаем символику, в которую
			с первого взгляда влюбятся ваши клиенты.
			</div>
		</div>
		<div class="circles-item col-md-4 col-sm-4">
			<div class="circles-header"><div class="center">Логотип<br>и фирменный стиль</div></div>
			<div class="circles-img"><img class="img-responsive" src="assets/img/fstyle/circle2.png"></div>
			<div class="circles-text">
			В условиях современной конкуренции
			затеряться на рынке слишком просто. 
			Основополагающим фактором для выживания
			является узнаваемость бренда, умение
			выделиться на фоне конкурентов.
			Создание узнаваемого логотипа и разработка
			фирменного стиля &ndash; первоочередные инструменты
			для достижения этой цели.
			</div>
		</div>
		<div class="circles-item col-md-4 col-sm-4">
			<div class="circles-header"><div class="center">Полиграфия<br>и упаковка</div></div>
			<div class="circles-img"><img class="img-responsive" src="assets/img/fstyle/circle1.png"></div>
			<div class="circles-text">
				Упаковочные материалы &ndash; один из базовых видов рекламы.
				Они зачастую являются единственным инструментом
				коммуникации с целевой аудиторией. 
				Качество упаковки и печатной рекламы очень важны
				для успешного контакта с вашими покупателями и раскрутки
				вашего бренда.
			</div>
		</div>
	</div>
</div>

<div class="container">
	<div class="row">
		<div class="col-md-8 col-md-offset-2">
			<div class="divide"></div>
		</div>
	</div>
	<div class="text-center margin-top-20">
		<button class="button button-red-transparent fb-form" data-fancybox-href="#form-popup-default">Узнать больше</button>
	</div>
</div>

<div class="containe-fluid margin-top-50">
	<h2>Наши работы</h2>
	<div  id="js-grid-mosaic" class="mosaic-portfolio cbp cbp-l-grid-mosaic">
		<div>
			<div class="mosaic-portfolio-item mosaic-portfolio-item-high cbp-item">
				<div class="cbp-caption">
					<div class="cbp-caption-defaultWrap">
						<img src="assets/img/temp/fs_portfolio1.jpg" alt="">
					</div>
					<div class="cbp-caption-activeWrap">
						<div class="mosaic-portfolio-logo">
							<img src="assets/img/temp/fs_portfolio_logo1.png" class="img-responsive m-auto" alt="">
						</div>
						<div class="mosaic-portfolio-text">
							<p>
							Компания «Интел-тур» отличается 
							умным подходом к организации 
							отдыха – что отражено в названии.
							</p>
							<p>
							Были разработаны несколько 
							элементов фирменного стиля: 
							логотип компании и слоган, 
							визитные карточки, 
							папка для документов, 
							фирменные бланки, 
							конверты. 
							</p>
							<p>
							Выработаны указания для 
							дальнейшего использования 
							элементов при разработке 
							рекламных материалов 
							и сайта компании.
							</p>
						</div>
						<div class="mosaic-portfolio-button">
							<button class="button button-empty white">Узнать больше</button>
						</div>
					</div>
				</div>
			</div>
			<div class="mosaic-portfolio-item mosaic-portfolio-item-long cbp-item">
				<div class="cbp-caption">
					<div class="cbp-caption-defaultWrap">
						<img src="assets/img/temp/fs_portfolio2.jpg" alt="">
					</div>
					<div class="cbp-caption-activeWrap">
						<div class="mosaic-portfolio-text">
							<p>
							Компания «Интел-тур» отличается 
							умным подходом к организации 
							отдыха – что отражено в названии.
							</p>
							<p>
							Были разработаны несколько 
							элементов фирменного стиля: 
							логотип компании и слоган, 
							визитные карточки, 
							папка для документов, 
							фирменные бланки, 
							конверты. 
							</p>
							<p>
							Выработаны указания для 
							дальнейшего использования 
							элементов при разработке 
							рекламных материалов 
							и сайта компании.
							</p>
						</div>
						<div class="mosaic-portfolio-button">
							<button class="button button-empty white">Узнать больше</button>
						</div>
					</div>
				</div>
			</div>
			<div class="mosaic-portfolio-item mosaic-portfolio-item-normal cbp-item">
				<div class="cbp-caption">
					<div class="cbp-caption-defaultWrap">
						<img src="assets/img/temp/fs_portfolio3.jpg" alt="">
					</div>
					<div class="cbp-caption-activeWrap">
						<div class="mosaic-portfolio-text">
							<p>
							Компания «Интел-тур» отличается 
							умным подходом к организации 
							отдыха – что отражено в названии.
							</p>
							<p>
							Были разработаны несколько 
							элементов фирменного стиля: 
							логотип компании и слоган, 
							визитные карточки.
							</p>
						</div>
						<div class="mosaic-portfolio-button">
							<button class="button button-empty white">Узнать больше</button>
						</div>
					</div>
				</div>
			</div>
			<div class="mosaic-portfolio-item mosaic-portfolio-item-normal cbp-item">
				<div class="cbp-caption">
					<div class="cbp-caption-defaultWrap">
						<img src="assets/img/temp/fs_portfolio4.jpg" alt="">
					</div>
					<div class="cbp-caption-activeWrap">
						<div class="mosaic-portfolio-text">
							<p>
							Компания «Интел-тур» отличается 
							умным подходом к организации 
							отдыха – что отражено в названии.
							</p>
							<p>
							Были разработаны несколько 
							элементов фирменного стиля: 
							логотип компании и слоган, 
							визитные карточки.
							</p>
						</div>
						<div class="mosaic-portfolio-button">
							<button class="button button-empty white">Узнать больше</button>
						</div>
					</div>
				</div>
			</div>
			<div class="mosaic-portfolio-item mosaic-portfolio-item-normal cbp-item">
				<div class="cbp-caption">
					<div class="cbp-caption-defaultWrap">
						<img src="assets/img/temp/fs_portfolio5.jpg" alt="">
					</div>
					<div class="cbp-caption-activeWrap">
						<div class="mosaic-portfolio-text">
							<p>
							Компания «Интел-тур» отличается 
							умным подходом к организации 
							отдыха – что отражено в названии.
							</p>
							<p>
							Были разработаны несколько 
							элементов фирменного стиля: 
							логотип компании и слоган, 
							визитные карточки.
							</p>
						</div>
						<div class="mosaic-portfolio-button">
							<button class="button button-empty white">Узнать больше</button>
						</div>
					</div>
				</div>
			</div>
			<div class="mosaic-portfolio-item mosaic-portfolio-item-normal cbp-item">
				<div class="cbp-caption">
					<div class="cbp-caption-defaultWrap">
						<img src="assets/img/temp/fs_portfolio6.jpg" alt="">
					</div>
					<div class="cbp-caption-activeWrap">
						<div class="mosaic-portfolio-text">
							<p>
							Компания «Интел-тур» отличается 
							умным подходом к организации 
							отдыха – что отражено в названии.
							</p>
							<p>
							Были разработаны несколько 
							элементов фирменного стиля: 
							логотип компании и слоган, 
							визитные карточки.
							</p>
						</div>
						<div class="mosaic-portfolio-button">
							<button class="button button-empty white">Узнать больше</button>
						</div>
					</div>
				</div>
			</div>
			<div class="mosaic-portfolio-item mosaic-portfolio-item-long cbp-item">
				<div class="cbp-caption">
					<div class="cbp-caption-defaultWrap">
						<img src="assets/img/temp/fs_portfolio7.jpg" alt="">
					</div>
					<div class="cbp-caption-activeWrap">
						<div class="mosaic-portfolio-text">
							<p>
							Компания «Интел-тур» отличается 
							умным подходом к организации 
							отдыха – что отражено в названии.
							</p>
							<p>
							Были разработаны несколько 
							элементов фирменного стиля: 
							логотип компании и слоган, 
							визитные карточки.
							</p>
						</div>
						<div class="mosaic-portfolio-button">
							<button class="button button-empty white">Узнать больше</button>
						</div>
					</div>
				</div>
			</div>
			<div class="mosaic-portfolio-item mosaic-portfolio-item-high cbp-item">
				<div class="cbp-caption">
					<div class="cbp-caption-defaultWrap">
						<img src="assets/img/temp/fs_portfolio8.jpg" alt="">
					</div>
					<div class="cbp-caption-activeWrap">
						<div class="mosaic-portfolio-logo">
							<img src="assets/img/temp/fs_portfolio_logo1.png" class="img-responsive m-auto" alt="">
						</div>
						<div class="mosaic-portfolio-text">
							<p>
							Компания «Интел-тур» отличается 
							умным подходом к организации 
							отдыха – что отражено в названии.
							</p>
							<p>
							Были разработаны несколько 
							элементов фирменного стиля: 
							логотип компании и слоган, 
							визитные карточки, 
							папка для документов, 
							фирменные бланки, 
							конверты. 
							</p>
							<p>
							Выработаны указания для 
							дальнейшего использования 
							элементов при разработке 
							рекламных материалов 
							и сайта компании.
							</p>
						</div>
						<div class="mosaic-portfolio-button">
							<button class="button button-empty white">Узнать больше</button>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

<div class="margin-top-20 text-center">
	<a href="#">
		<button class="button button-red-transparent">Все работы</button>
	</a>
</div>


<div class="approach container">
	<h2>Как мы работаем?</h2>
	<p class="text-center">
		Любые инвестиции, направленные на поддержку бренда, должны окупаться.<span class="hidden-xs"><br></span>
		Поэтому сбор и анализ маркетинговой информации &ndash; важный этап, требующий немало времени.<span class="hidden-xs"><br></span>
		Полный цикл разработки мероприятий может занять от нескольких недель до полугода &ndash;<span class="hidden-xs"><br></span>
		все зависит от особенностей проекта.
		
	</p>
	<div class="statements-xs">
		<div class="col-md-offset-1 col-md-2 col-sm-2 col-sm-offset-1 statements-item">
			<div class="statements-item-img">
				<img class="img-responsive" src="assets/img/fstyle/ico1.png" alt="">
			</div>
			<div class="statements-item-text">
				Встречаемся <span class="hidden-sm hidden-xs"><br></span>и обсуждаем проект
			</div>
		</div>
		<div class="col-md-2 col-sm-2 statements-item">
			<div class="statements-item-img">
				<img class="img-responsive" src="assets/img/fstyle/ico2.png" alt="">
			</div>
			<div class="statements-item-text">
				Собираем информацию, анализируем рынок
			</div>
		</div>
		<div class="col-md-2 col-sm-2 statements-item">
			<div class="statements-item-img">
				<img class="img-responsive" src="assets/img/fstyle/ico3.png" alt="">
			</div>
			<div class="statements-item-text">
				Согласовываем этапы, цены и сроки
			</div>
		</div>
		<div class="col-md-2 col-sm-2 statements-item">
			<div class="statements-item-img">
				<img class="img-responsive" src="assets/img/fstyle/ico4.png" alt="">
			</div>
			<div class="statements-item-text">
				 Разрабатывем и согласовываем концепцию
			</div>
		</div>
		<div class="col-md-2 col-sm-2 statements-item">
			<div class="statements-item-img">
				<img class="img-responsive" src="assets/img/fstyle/ico5.png" alt="">
			</div>
			<div class="statements-item-text">
				  Готовим все необходимые элементы
			</div>
		</div>
	</div>

</div>

<? if(1==0) { ?>
<div class="cost container">
	<h2 class="margin-top-50">Цены и сроки</h2>
	<p class="text-center">
		Инвестиции в поддержку бренда должны окупаться, поэтому существенную часть работы 
		занимает сбор и анализ информации. Полный цикл разработки зависит от сложности проекта 
		и занимает обычно от трех до шести месяцев. 
	</p>
	<div class="row">
		<div class="col-md-4 col-sm-4 margin-bottom-20">
			<img src="http://placehold.it/300x440" class="img-responsive m-auto" alt="">
		</div>
		<div class="col-md-4 col-sm-4 margin-bottom-20">
			<img src="http://placehold.it/300x440" class="img-responsive m-auto" alt="">
		</div>
		<div class="col-md-4 col-sm-4 margin-bottom-20">
			<img src="http://placehold.it/300x440" class="img-responsive m-auto" alt="">
		</div>
	</div>
</div>
<? } ?>
<div class="space-50"></div>

@stop