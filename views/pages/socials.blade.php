@extends('site.layout', ['Title' => 'Продвижение в социальных сетях'])

@section('head-styles')
<link rel="stylesheet" href="assets/css/socials.css">
@stop

@section('content')
<div class="header">
	<div class="header-inner container">
		<div class="row">
			<div class="col-md-11 col-md-offset-1">
				<h1>
					Продвижение<br>
					в социальных<br>
					сетях и блогах
				</h1>
			</div>
		</div>
	</div>
</div>

<div class="monitoring container margin-top-lg-m140 margin-top-md-m120 margin-top-sm-m120 margin-top-xs-m90">
	<div class="row">
		<div class="col-md-8 col-sm-12 col-md-offset-4">
			<h2 class="text-left-md">
				Мониторинг социальных медиа
			</h2>
		</div>
	</div>
	<div class="row">
		<div class="col-md-4 col-sm-4 monitoring-img">
			<img class="img-responsive m-auto" src="assets/img/socials/monitoring1.png" alt="">
		</div>
		<div class="col-md-8 col-sm-8">
			<p>
				Суть мониторинга &ndash; отслеживание упоминаний о продвигаемом продукте или бренде
				по множеству параметров: ссылки на официальные сайты, отзывы, обзоры и другие факторы.
				Такой анализ позволяет определить:
			</p>
			<ul class="list list-arrow">
				<li>
					какова тональность отзывов о продукте, компании, услугах или бренде: негативная, позитивная или нейтральная;
				</li>
				<li>
					его самые сильные и самые слабые стороны по мнению пользователей;
				</li>
				<li>
					площадки, на которых продукт / услуга / компания обсуждаются наиболее активно;
				</li>
				<li>
					положение относительно конкурентов: с кем сравнивают, по каким критериям, в чью пользу.
				</li>
			</ul>
			<p>
				Постоянный мониторинг предоставляет возможность оперативно реагировать на любые действия потребителей и,
				таким образом, заблаговременно бороться с негативными настроениями или поддерживать позитивное отношение аудитории. 
			</p>
		</div>
	</div>
	<h3>Мониторинг социльных медиа позволяет устранить причину до того,<span class="hidden-sm hidden-xs"><br></span>как она станет проблемой</h3>
	<div class="statements-xs">
		<div class="col-md-3 col-sm-3 statements-item">
			<div class="statements-item-img">
				<img class="img-responsive" src="assets/img/socials/ico1.png" alt="">
			</div>
			<div class="statements-item-text">
				Мониторинг всего<span class="hidden-sm hidden-xs"><br></span>информационного поля
			</div>
		</div>
		<div class="col-md-3 col-sm-3 statements-item">
			<div class="statements-item-img">
				<img class="img-responsive" src="assets/img/socials/ico2.png" alt="">
			</div>
			<div class="statements-item-text">
				Максимально полный<br>сбор данных
			</div>
		</div>
		<div class="col-md-3 col-sm-3 statements-item">
			<div class="statements-item-img">
				<img class="img-responsive" src="assets/img/socials/ico3.png" alt="">
			</div>
			<div class="statements-item-text">
				Анализ публикаций<br>и комментариев
			</div>
		</div>
		<div class="col-md-3 col-sm-3 statements-item">
			<div class="statements-item-img">
				<img class="img-responsive" src="assets/img/socials/ico4.png" alt="">
			</div>
			<div class="statements-item-text">
				Качественная поддержка клиентов
			</div>
		</div>
	</div>
</div>

<div class="container margin-top-20">
  	<h2 class="text-center">Заказать мониторинг:</h2>
	<form class="form-ajax" action="./" method="POST">
		<input type="hidden" name="form" value="">
		<input type="hidden" name="required" value="Name, Email, Phone, Question">
		<input type="hidden" name="action" value="doPostForm">
		<input type="hidden" name="mailSubject" value="Запрос на сайте webisgroup.ru">
		<div class="row">
			<div class="col-lg-8 col-md-10 col-lg-offset-2 col-md-offset-1">
				<div class="row">
					<div class="col-md-6">
						<div class="input-spacing">
							<div class="input-group input-group-before">
								<span class="input-group-addon"><img src="assets/img/ico_input_user_red.png" alt="Ваше имя"></span>
								<input data-required="Name" placeholder="Как вас зовут?" type="text" name="Name" value="" class="input">
							</div>
						</div>
						<div class="input-spacing">
							<div class="input-group input-group-before">
								<span class="input-group-addon"><img src="assets/img/ico_input_phone_red.png" alt="Ваш телефон"></span>
								<input data-required="Phone" placeholder="Ваш телефон" type="text" name="Phone" value="" class="input">
							</div>
						</div>
						<div class="input-spacing">
							<div class="input-group input-group-before">
								<span class="input-group-addon"><img src="assets/img/ico_input_envelope_red.png" alt="Ваш email"></span>
								<input data-required="Email" placeholder="Ваш Email" type="text" name="Email" value="" class="input">
							</div>
						</div>
					</div>
					<div class="col-md-6 input-spacing input-spacing-md-0">
						<textarea data-required="Question" name="Question" placeholder="Пожалуйста, кратко опишите вашу задачу" class="input height-as-input-3"></textarea>
					</div>
				</div>
				<div class="text-center input-spacing-last">
					<button type="submit" class="button button-red-transparent">Отправить заявку</button>
				</div>
			</div>
		</div>
	</form>
</div>

<div class="socials container-fluid margin-top-50">
	<div class="container">
		<div class="row">
			<div class="col-md-8">
				<h2 class="text-left-sm margin-bottom-0 margin-top-0">Продвижение в основных социальных cетях:</h2>
			</div>
		</div>
		<div class="row row-right-sm">
			<div class="col-md-4 col-sm-4 col-xs-12 socials-img">
				<img class="img-responsive m-auto" src="assets/img/socials/socials.png" alt="">
			</div>
			<div class="col-md-8 col-sm-8 col-xs-12">
				<div class="text-left-sm text-center-xs font-regular f-32 lh-12 margin-bottom-20">Вконтакте и Facebook</div>
				<p>
					Работа в соцсетях &ndash; эффективный способ сформировать у потенциальной клиентской аудитории желаемое впечатление о продвигаемом продукте или услуге.
					Такая работа дает возможность обеспечить долгосрочные отношения с клиентами.
				</p>
				<p>
					<table cellspacing="0" cellpadding="0" width="100%">
						<tr>
							<td width="1%" valign="top" style="padding-right: 13px;">
								<img src="assets/img/socials/vk_ico.png" alt="">
							</td>
							<td valign="top">
								Наиболее посещаемой сегодня является сеть <span class="font-regular">«ВКонтакте»</span>.
								Она не только представляет собой удобное средство для общения, 
								но и является обширнейшей базой данных потенциальной аудитории
								для бизнеса любого направления. Больше половины трафика СНГ приходится
								на данный социальный ресурс. Ежедневно его посещают более 36 млн пользователей, доля россиян составляет 70%. 
							</td>
						</tr>
					</table>
				</p>
				<p>
					<table cellspacing="0" cellpadding="0" width="100%">
						<tr>
							<td width="1%" valign="top" style="padding-right: 13px;">
								<img src="assets/img/socials/fb_ico.png" alt="">
							</td>
							<td valign="top">
								Крупнейшей в мире является сеть <span class="font-regular">Facebook</span>.
								Около 13 млн русскоязычных пользователей практически ежедневно посещают этот сайт. 
								Из них около 40% россиян.
							</td>
						</tr>
					</table>
				</p>
			</div>
		</div>
		<div>
			<h3>Мы делаем комплексную рекламу<span class="hidden-sm hidden-xs"><br></span>в социальных сетях:</h3>
			<div class="statements-xs">
				<div class="row">
				<div class="statements-item col-md-2 col-sm-2">
					<div class="statements-item-img">
						<img class="img-responsive" src="assets/img/socials/ico5.png" alt="">
					</div>
					<div class="statements-item-text">
						Создание и продвижение сообществ и групп
					</div>
				</div>
				<div class="statements-item col-md-2 col-sm-2">
					<div class="statements-item-img">
						<img class="img-responsive" src="assets/img/socials/ico6.png" alt="">
					</div>
					<div class="statements-item-text">
						Распространение материалов, фото, видео
					</div>
				</div>
				<div class="statements-item col-md-2 col-sm-2">
					<div class="statements-item-img">
						<img class="img-responsive" src="assets/img/socials/ico7.png" alt="">
					</div>
					<div class="statements-item-text">
						Оптимизация и интеграция сайтов и соцсетей
					</div>
				</div>
				<div class="statements-item col-md-2 col-sm-2">
					<div class="statements-item-img">
						<img class="img-responsive" src="assets/img/socials/ico8.png" alt="">
					</div>
					<div class="statements-item-text">
						Таргетированные рекламные кампании
					</div>
				</div>
				<div class="statements-item col-md-2 col-sm-2">
					<div class="statements-item-img">
						<img class="img-responsive" src="assets/img/socials/ico9.png" alt="">
					</div>
					<div class="statements-item-text">
						Конкурсы, флешмобы, прочие мероприятия
					</div>
				</div>
				<div class="statements-item col-md-2 col-sm-2">
					<div class="statements-item-img">
						<img class="img-responsive" src="assets/img/socials/ico10.png" alt="">
					</div>
					<div class="statements-item-text">
						Создание и продвижение приложений
					</div>
				</div>
				</div>
			</div>
			<div class="margin-top-20 text-center">
				<button class="button button-blue-light fb-form" data-fancybox-href="#form-popup-default">Заказать продвижение в социальных сетях</button>
			</div>
		</div>
	</div>
</div>


<div class="twitter container-fluid">
	<div class="container">
		<div class="row">
			<div class="col-md-8 col-sm-12 col-md-offset-4">
				<h2 class="text-left-md">
					Продвижение в Twitter
				</h2>
			</div>
		</div>
		<div class="row">
			<div class="col-md-4 col-sm-4 twitter-img">
				<img class="img-responsive m-auto" src="assets/img/socials/twitter.png" alt="">
			</div>
			<div class="col-md-8 col-sm-8">
				<div>
					<p>
						Сервис микроблогов <b>Twitter</b> пользуется значительной популярностью у миллионов пользователей,
						поэтому представляет собой немалый интерес для рекламной активности.
						Этот сервис пользователи также относят к соцсетям, он входит в десятку наиболее популярных в сети.
					</p>
					<h3 class="text-left-sm">Что мы делаем:</h3>
					<div class="statements-sm">
						<div class="col-md-3 col-sm-3 statements-item">
							<div class="statements-item-img">
								<img class="img-responsive" src="assets/img/socials/ico11.png" alt="">
							</div>
							<div class="statements-item-text">
								Создаём<br>свой дизайн<span class="hidden-sm hidden-xs"><br></span> для микроблога
							</div>
						</div>
						<div class="col-md-3 col-sm-3 statements-item">
							<div class="statements-item-img">
								<img class="img-responsive" src="assets/img/socials/ico12.png" alt="">
							</div>
							<div class="statements-item-text">
								Эффективно продвигаем микроблог
							</div>
						</div>
						<div class="col-md-3 col-sm-3 statements-item">
							<div class="statements-item-img">
								<img class="img-responsive" src="assets/img/socials/ico13.png" alt="">
							</div>
							<div class="statements-item-text">
								Увеличиваем охват целевой аудитории
							</div>
						</div>
						<div class="col-md-3 col-sm-3 statements-item">
							<div class="statements-item-img">
								<img class="img-responsive" src="assets/img/socials/ico14.png" alt="">
							</div>
							<div class="statements-item-text">
								Проводим флешмобы<span class="hidden-sm hidden-xs"><br></span>и акции
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="text-center margin-top-20">
			<button class="button button-red-transparent fb-form" data-fancybox-href="#form-popup-default">Задать вопрос или оставить заявку</button>
		</div>
	</div>
</div>

<div class="blogs container-fluid">
	<div class="container">
		<h2>Продвижение в блогосфере, работа с лидерами мнений</h2>
		<div class="blogs-thesis">
			Одной из наиболее благоприятных для продвижения услуг и товаров в сети является блогосфера, пространство блогов.
			Здесь царит свобода слова, поэтому данная область неоценима с точки зрения возможности достоверно определить общественное мнение.
			Если говорить о продвижении в социальных сетях, любой хороший специалист порекомендует начать с мониторинга блогосферы.
		</div>
		<div class="row">
			<div class="col-md-4 blogs-col">
				<div class="blogs-col-img">
					<div class="blogs-col-img-inner">
						<img class="img-responsive m-auto" src="assets/img/socials/blogs1.png" alt="">
					</div>
				</div>
				<div class="blogs-col-header">Blogger Relations</div>
				<ul class="list list-arrow">
					<li>
						Анонсирование важных событий, обсуждение блогерами деятельности компании, общение на оффлайновых мероприятиях;
					</li>
					<li>
						Активная работа с лидерами мнений &ndash; известными блогерами с множеством читателей, пишущими на темы, затрагивающие ваш бизнес;
					</li>
					<li>
						Модерация конкретных авторов, групп и сообществ, которые могли бы участвовать в процессе продвижения ваших услуг или товаров.
					</li>
				</ul>
			</div>	
			<div class="col-md-4 blogs-col">
				<div class="blogs-col-img">
					<div class="blogs-col-img-inner">
						<img class="img-responsive m-auto" src="assets/img/socials/blogs2.png" alt="">
					</div>
				</div>
				<div class="blogs-col-header">Ведение корпоративного блога</div>
				<ul class="list list-arrow">
					<li>
						Оперативная реакция на кризисные ситуации: критику, потенциальные скандалы;
					</li>
					<li>
						Возможность качественной обратной связи с целевой аудиторией;
					</li>
					<li>
						Быстрое предоставление пользователям любой востребованной информации о компании
					</li>
					<li>
						Предоставление информации о компании для журналистов
					</li>
					<li>
						Формирование ядра аудитории: группы клиентов с лояльным отношением.
					</li>
				</ul>
			</div>
			<div class="col-md-4 blogs-col">
				<div class="blogs-col-img">
					<div class="blogs-col-img-inner">
						<img class="img-responsive m-auto" src="assets/img/socials/blogs3.png" alt="">
					</div>
				</div>
				<div class="blogs-col-header">Топ &laquo;Яндекс.блогов&raquo;: рейтинг блогов Рунета</div>
				<p>
					Попадание в ТОП равнозначно размещению на главной странице поисковика собственного баннера;
				</p>
				<p>
					Возможность получения доступа к многомиллионной аудитории для топового блога;
				</p>
				<p>
					Качественная и экспертная аудитория: наиболее популярные блогеры, ежедневно обсуждающие актуальные темы.
				</p>
			</div>
		</div>
		<div class="text-center margin-top-20">
			<button class="button button-blue-light fb-form" data-fancybox-href="#form-popup-default">
				Узнать больше или оставить заявку
			</button>
		</div>
	</div>
</div>

<div class="negative container-fluid">
	<div class="container">
		<div class="row">
			<div class="col-md-8 col-sm-12 col-md-offset-4">
				<h2 class="text-left-md">
					Работа с негативом
				</h2>
			</div>
		</div>
		<div class="row">
			<div class="col-md-4 col-sm-4 negative-img">
				<img class="img-responsive m-auto" src="assets/img/socials/negative.png" alt="">
			</div>
			<div class="col-md-8 col-sm-8">
				<p>
					Иногда конкуренты распространяют в соцсетях негативную информацию, которая способна
					в значительной степени навредить имиджу компании. Кроме откровенно заказных материалов, возможно также появление негативных отзывов
					от недовольных клиентов. 
				</p>
				<p class="font-regular">
					Мы можем реализовать комплекс эффективных мероприятий, способных полностью нейтрализовать негативные последствия таких публикаций:
				</p>
				<div class="statements-sm">
					<div class="col-md-3 col-sm-3 statements-item">
						<div class="statements-item-img">
							<img class="img-responsive" src="assets/img/socials/ico15.png" alt="">
						</div>
						<div class="statements-item-text">
							Устранение недовольства:<br>от негатива &ndash;<span class="hidden-sm hidden-xs"><br></span> к взаимодействию
						</div>
					</div>
					<div class="col-md-3 col-sm-3 statements-item">
						<div class="statements-item-img">
							<img class="img-responsive" src="assets/img/socials/ico16.png" alt="">
						</div>
						<div class="statements-item-text">
							Контратака:<br>запуск встречной<span class="hidden-sm hidden-xs"><br></span> PR-компании
						</div>
					</div>
					<div class="col-md-3 col-sm-3 statements-item">
						<div class="statements-item-img">
							<img class="img-responsive" src="assets/img/socials/ico17.png" alt="">
						</div>
						<div class="statements-item-text">
							Разъяснения<br>и работа над имиджем при помощи СМИ
						</div>
					</div>
					<div class="col-md-3 col-sm-3 statements-item">
						<div class="statements-item-img">
							<img class="img-responsive" src="assets/img/socials/ico18.png" alt="">
						</div>
						<div class="statements-item-text">
							Коррекция<span class="hidden-sm hidden-xs"><br></span> поисковой<span class="hidden-sm hidden-xs"><br></span>выдачи
						</div>
					</div>
				</div>
				<p>
					Для того, чтобы не допустить катастрофического разрастания негатива, следует максимально оперативно реагировать на появление таких материалов.
					Чем раньше начнётся работа по нейтрализации, тем меньше вреда принесут негативные упоминания.
					Если такую работу не проводить, негатив может распространяться лавинообразно, что приведёт к значительному росту издержек на восстановление репутации.
				</p>
			</div>
		</div>
		<div class="margin-top-20 text-center">
			<button class="button button-red-transparent fb-form" data-fancybox-href="#form-popup-default">Узнать больше</button>
		</div>
	</div>
</div>

<div class="virus container-fluid">
	<div class="container">
		<h2>Вирусный маркетинг</h2>
		<div class="virus-thesis">
			<div class="margin-bottom-5">
			Суть вирусного маркетинга &ndash; распространение не просто рекламной информации, а такого контента, которым пользователи захотят делиться друг с другом: 
			провокационного, полезного, интересного или смешного. При этом послание должно нести информацию о вашем предложении. 
			Это может быть разовая акция или многоступенчатая рекламная кампания, интригующая пользователей.
			</div>
			<div class="margin-bottom-5">
				Мы проводим как комплексные вирусные кампании, так и оказываем отдельные услуги в рамках таких кампаний:
			</div>
		</div>
		<div class="row">
			<div class="col-md-4 virus-item">
				<div class="virus-item-header">
					Анализ аудитории,<span class="hidden-xs"><br></span> подбор форматов<span class="hidden-sm hidden-xs"><br></span> и площадок
				</div>
				<div class="virus-item-img">
					<img class="m-auto img-responsive" src="assets/img/socials/virus1.png" alt="">
				</div>
			</div>
			<div class="col-md-4 virus-item">
				<div class="virus-item-header">
					Креатив и производство<span class="hidden-xs"><br></span> для рекламной<span class="hidden-sm hidden-xs"><br></span>кампании
				</div>
				<div class="virus-item-img">
					<img class="m-auto img-responsive" src="assets/img/socials/virus2.png" alt="">
				</div>
			</div>
			<div class="col-md-4 virus-item">
				<div class="virus-item-header">
					Оценка эффективности, <br> модерация и мониторинг кампании
				</div>
				<div class="virus-item-img">
					<img class="m-auto img-responsive" src="assets/img/socials/virus3.png" alt="">
				</div>
			</div>
		</div>
		<div class="text-center margin-top-md-50 margin-top-sm-20 margin-top-20">
			<button class="button button-blue-dark fb-form" data-fancybox-href="#form-popup-default">Запустить кампанию</button>
		</div>
	</div>
</div>


<div class="apps container-fluid">
	<div class="container">
		<div class="row">
			<div class="col-md-8 col-sm-12 col-md-offset-4">
				<h2 class="text-left-md">
					Разработка приложений для социальных сетей
				</h2>
			</div>
		</div>
		<div class="row">
			<div class="col-md-4 col-sm-4 apps-img">
				<img class="img-responsive m-auto" src="assets/img/socials/apps.png">
			</div>
			<div class="col-md-8 col-sm-8">
				<p>
					Приложения для социальных сетей &ndash; эффективный инструмент, обеспечивающий коммуникацию и широкий охват пользователей.
					Такие приложения широко используются всеми социальными сетями. Через них можно получить доступ к списку друзей или узнать контакты пользователя, что важно для продаж.
					Полученная информация может быть эффективно использована для обеспечения лояльного отношения к бренду и других целей.
					Грамотное использование приложений поможет эффективно развивать бизнес в сети.
				</p>
				<p class="font-regular">
					Мы разрабатываем приложения любой сложности:
				</p>
				<div class="statements-sm">
					<div class="col-md-3 col-sm-3 statements-item">
						<div class="statements-item-img">
							<img class="img-responsive" src="assets/img/socials/ico19.png" alt="">
						</div>
						<div class="statements-item-text">
							Полезные сервисы
						</div>
					</div>
					<div class="col-md-3 col-sm-3 statements-item">
						<div class="statements-item-img">
							<img class="img-responsive" src="assets/img/socials/ico20.png" alt="">
						</div>
						<div class="statements-item-text">
							Бизнес приложения
						</div>
					</div>
					<div class="col-md-3 col-sm-3 statements-item">
						<div class="statements-item-img">
							<img class="img-responsive" src="assets/img/socials/ico21.png" alt="">
						</div>
						<div class="statements-item-text">
							Игры
						</div>
					</div>
					<div class="col-md-3 col-sm-3 statements-item">
						<div class="statements-item-img">
							<img class="img-responsive" src="assets/img/socials/ico22.png" alt="">
						</div>
						<div class="statements-item-text">
							Магазины
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="margin-top-20 text-center">
			<button class="button button-red-transparent fb-form" data-fancybox-href="#form-popup-default">Узнать больше</button>
		</div>
	</div>
</div>

<div class="support container-fluid">
	<div class="container">
		<h3 class="margin-bottom-60">Каждый клиент взаимодействует с персональным менеджером, который:</h3>
		<div class="statements-sm">
			<div class="col-md-3 col-sm-3 statements-item">
				<div class="statements-item-img">
					<img class="img-responsive" src="assets/img/socials/ico23.png" alt="">
				</div>
				<div class="statements-item-text font-regular margin-top-15">
					Согласует стратегию<br>и все рекламные материалы<br>перед запуском кампании
				</div>
			</div>
			<div class="col-md-3 col-sm-3 statements-item">
				<div class="statements-item-img">
					<img class="img-responsive" src="assets/img/socials/ico24.png" alt="">
				</div>
				<div class="statements-item-text font-regular margin-top-15">
					Контролирует ход кампании<span class="hidden-sm hidden-xs"><br></span> и следит за качеством<span class="hidden-sm hidden-xs"><br></span> на каждом этапе
				</div>
			</div>
			<div class="col-md-3 col-sm-3 statements-item">
				<div class="statements-item-img">
					<img class="img-responsive" src="assets/img/socials/ico25.png" alt="">
				</div>
				<div class="statements-item-text font-regular margin-top-15">
					Готовит отчёты<span class="hidden-sm hidden-xs"><br></span> и рекомендации<span class="hidden-sm hidden-xs"><br></span> по рекламной кампании;
				</div>
			</div>
			<div class="col-md-3 col-sm-3 statements-item">
				<div class="statements-item-img">
					<img class="img-responsive" src="assets/img/socials/ico26.png" alt="">
				</div>
				<div class="statements-item-text font-regular margin-top-15">
					Консультирует клиента<span class="hidden-sm hidden-xs"><br></span>и привлекает нужных<span class="hidden-sm hidden-xs"><br></span>профильных специалистов.
				</div>
			</div>
		</div>
	</div>
</div>


@stop