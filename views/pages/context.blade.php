@extends('site.layout', ['Title' => 'Контекстная реклама'])

@section('head-styles')
	<link rel="stylesheet" href="assets/css/analytics.css">
	<link rel="stylesheet" href="assets/css/context.css">
@stop

@section('footer')
	<script src="assets/js/context.js"></script>
@stop

@section('content')
<div class="header">
	<div class="header-inner container">
		<div class="row">
			<div class="col-md-11 col-md-offset-1">
				<h1>
					Контекстная реклама
				</h1>
				<div class="header-slogan">
					Точно в цель!
				</div>
				<div class="header-thesis">
					<div class="margin-bottom-15">Тщательная подготовка<br>рекламной компании</div>
					<div>Максимальный результат <br>при оптимальном бюджете</div>
				</div>
			</div>
		</div>
		<div class="header-riffle"></div>
		<div class="header-dog"></div>
		<div class="header-cloud"></div>
		<div class="header-calldog-area"></div>
	</div>
</div>

<div class="header-form container">
	<div class="row">
	 	<div class="col-md-6 col-sm-12 header-form-img">
	 		<img class="img-responsive m-auto" src="assets/img/context/form_img.png" alt="">
	 	</div>
	 	<div class="col-md-6 col-sm-12 header-form-form">
	 		<div class="header-form-text">
				Оставьте заявку на контекстную рекламу:
			</div>
			<div class="row">
				<div class="col-md-12">
					<form class="form-ajax" action="./" method="POST">
						<div class="input-spacing">
							<input name="Name" type="text" class="input" placeholder="Как вас зовут?" data-required="name">
						</div>
						<div class="input-spacing">
							<div class="row">
								<div class="col-md-6 input-spacing-md-0 input-spacing">
									<input name="Phone" type="text" class="input" placeholder="Ваш телефон" data-required="phone">
								</div>
								<div class="col-md-6">
									<input name="Email" type="text" class="input" placeholder="Ваш Email" data-required="email">
								</div>
							</div>
						</div>
						<div class="input-spacing-0 input-spacing-sm">
							<div class="row">
								<div class="col-md-8 input-spacing input-spacing-md-0">
									<input name="Site" type="text" class="input" placeholder="Адрес вашего сайта">
								</div>
								<div class="col-md-4">
									<button type="submit" class="button button-red-transparent w100 height-as-input">Отправить</button>
								</div>
							</div>
						</div>
					</form>
				</div>
			</div>
	 	</div>
	</div>
</div>

<div class="solutions">
	<div class="container">
		<div class="row">
			<div class="col-md-4 solutions-img">
				<img class="m-auto img-responsive" src="assets/img/context/circle1.png" alt="">
			</div>
			<div class="col-md-7 solutions-inner">
				<div class="solutions-header">Зачем нужна контекстная реклама?</div>
				<p class="solutions-text">
					Привлекайте на свой сайт только тех посетителей, кто заинтересован в вашем предложении.<br>
					При продвижении любого бренда наилучший результат достигается благодаря точности действий и нацеленности на целевую аудиторию. Если о вашем бизнесе узнают заинтересованные посетители, они с высокой вероятностью сделают у вас заказ.
				</p>
				<div class="solutions-button">
			      <button class="button button-blue-light button-collapse" data-action="show" data-target="#solutions-content1" data-anchor="#context" data-group="context">
						<span class="button-collapse-ico-down"></span>
			            <span class="button-collapse-text"><span class="button-collapse-text-decoration">Узнать больше</span></span>
			      </button>
			      <button class="button button-blue-light button-collapse display-none" data-action="hide" data-target="#solutions-content1" data-anchor="#context" data-anchor-offset="-350">
						<span class="button-collapse-ico-up"></span>
			            <span class="button-collapse-text"><span class="button-collapse-text-decoration">Свернуть</span></span>
			      </button>
				</div>
			</div>
		</div>
	</div>
</div>

<div class="solutions-content" id="solutions-content1">
	<a name="context"></a>
	<div class="solutions-row shadow-top-inset bg-blue-light container-fluid">
		<div class="container">
			<div class="row">
				<div class="solutions-row-header col-md-7 col-md-offset-1 text-left-md text-center">
					Задачи, решаемые контекстной рекламой:
				</div>
			</div>
			<div class="row row-right-md">
				<div class="col-md-4 col-sm-12 solutions-row-img">
					<img class="img-responsive m-auto" src="assets/img/context/solutions1.png" alt="">
				</div>
							
				<div class="col-md-7 col-offset-md-1">
					<div class="font-bold margin-bottom-5">Её видят только заинтересованные пользователи</div>
					<p>
						Поисковые системы помнят запросы каждого пользователя, анализируют их и предлагают только ту рекламу,
которая может его заинтересовать: строитель увидит рекламу специнструмента и материалов, рыбак – рыболовные снасти и специализированное оборудование, автомобилист - рекламу автомобилей или аксессуаров.
					</p>
					<div class="font-bold margin-bottom-5">Значительно повышает количество продаж и конверсию</div>
					<p>
					Целенаправленный показ рекламы – это гарантия того, что пользователь, посетивший ваш сайт, уже заинтересован в покупке
					или ином ожидаемом вами действии. Поэтому показатели конверсии (отношения количества заказов к количеству посетителей) заметно увеличиваются, а стоимость контакта &nbsp; снижается.
					</p>
					<div class="font-bold margin-bottom-5">Снижает расходы на рекламные кампании</div>
					<p>
						В отличие от классических форм массовой рекламы (например, рекламы на телевидении), точная настройка контекстной рекламной кампании позволяет привлекать нужных посетителей при минимальной стоимости перехода. В результате при значительно меньших затратах сайт будет привлекать больше качественных посетителей, готовых к контакту с вашей компанией.
					</p>
				</div>
			</div>
		</div>
	</div>

	<div class="solutions-row container-fluid">
		<div class="container">
			<div class="row">
				<div class="solutions-row-header col-md-7 col-md-offset-4 text-left-md text-center">
					Как мы готовим контекстные кампании:
				</div>
			</div>
			<div class="row">
				<div class="col-md-4 col-sm-12 solutions-row-img">
					<img class="img-responsive m-auto" src="assets/img/context/solutions2.png" alt="">
				</div>
				<div class="col-md-7">
					<div class="font-bold margin-bottom-5">Плотное сотрудничество с заказчиком</div>
					<p>
						В процессе работы мы постоянно обсуждаем стратегию и ход рекламной кампании с заказчиком. Это позволяет нам добиться максимальной отдачи от рекламной кампании. 
					</p>
					<div class="font-bold margin-bottom-5">Команда опытных профессионалов</div>
					<p>
					Наши сотрудники сертифицированы, обладают значительным опытом и регулярно принимают участие в отраслевых тренингах и семинарах. 
					Мы постоянно совершенствуемся - наши специалисты всегда в курсе всех новостей и последних изменений в отрасли.
					Слаженное взаимодействие внутри команды позволяет вырабатывать нестандартные решения, быстро их реализовывать и интегрировать с сайтом.
					Результат - рост количества заказов у наших клиентов.
					</p>					
					<div class="font-bold margin-bottom-5">Эффективные инструменты для веб-аналитики</div>
					<p>
					Постоянный анализ рекламных кампаний позволяет обеспечить точное таргетирование рекламы на целевую аудиторию. 
					Мы исключаем лишние, неэффективные и ненужные показы: только целевые и заинтересованные в контакте пользователи, только нужные, продающие страницы сайта.
					</p>					
					<div class="font-bold margin-bottom-5">Индивидуальный менеджер</div>
					<p>
					Специалист по контекстной рекламе, который будет работать в тесном контакте с вами, 
					позволит вам сосредоточиться на том, что действительно важно, не отвлекаясь на технические нюансы.
					Вы всегда сможете получить квалифицированные ответы на свои вопросы, консультации и рекомендации по развитию вашего бизнеса в сети.
					</p>
				</div>
			</div>
		</div>
	</div>

	<div class="solutions-row bg-blue-light container-fluid">
		<div class="container">
			<div class="row">
				<div class="solutions-row-header col-md-7 col-md-offset-1 text-left-md text-center">
					Почему выбирают нас?
				</div>
			</div>
			<div class="row row-right-md">
				<div class="col-md-3 col-sm-12 solutions-row-img">
					<img class="img-responsive m-auto" src="assets/img/context/solutions3.png" alt="">
				</div>
				<div class="col-md-8 col-md-offset-1">
					<div class="font-bold margin-bottom-5">Люди &ndash; ключ к успеху</div>
					<p>
						Каждый наш сотрудник &ndash; высококвалифицированный специалист. Группа таких специалистов &ndash; спецназ, способный эффективно справиться с любыми задачами.
					</p>
					<div class="font-bold margin-bottom-5">Никаких шаблонных решений &ndash; только индивидуальный подход</div>
					<p>
						Мы нацелены на долгосрочное эффективное сотрудничество с клиентами. Поэтому шаблонный подход здесь исключён. При работе над каждым проектом мы полностью вникаем в его специфику. Мы стремимся создать условия, чтобы от сотрудничества выигрывал каждый, ориентируемся на достижение ключевых показателей успешности реализации проекта.
					</p>
					<div class="font-bold margin-bottom-5">Мы в курсе последних тенденций</div>
					<p>
						Наши специалисты регулярно принимают активное участие в семинарах и конференциях по интернет-маркетингу.
						При работе мы используем новейшие технологии и собственные разработки, а не просто размещаем рекламу.
						Такой подход к организации деятельности позволяет заказчикам оптимизировать расходы,
						а нам &ndash; добиться максимальной эффективности рекламной кампании.
					</p>
					<div class="font-bold margin-bottom-5">Мы готовы к сложным и нестандартным задачам</div>
					<p>
						Наши веб-аналитики осуществляют скрупулёзный анализ комплекса показателей рекламной кампании.
						Это позволяет нам заблаговременно выявлять и устранять слабые места в рекламе. 
						Результат &ndash; сокращение времени для достижения поставленных целей.
					</p>
					<p class="text-center text-left-md">
						<button class="button button-red-transparent fb-form" data-fancybox-href="#form-popup-default">Заказать рекламную кампанию</button>
					</p>
				</div>
			</div>
		</div>
	</div>


	<div class="oh text-center shadow-bottom-inset bg-blue-light container-fluid">
		<button class="button button-blue-light button-collapse m-auto margin-bottom-m5" data-action="hide" data-target="#solutions-content1" data-anchor="#context" data-anchor-offset="-350">
			<span class="button-collapse-ico-up"></span>
			<span class="button-collapse-text"><span class="button-collapse-text-decoration">Свернуть</span></span>
		</button>
	</div>
</div>


<div class="solutions">
	<div class="container">
		<div class="row row-right-md">
			<div class="col-md-4 solutions-img">
				<img class="m-auto img-responsive" src="assets/img/context/circle2.png" alt="">
			</div>
			<div class="col-md-7 solutions-inner padding-top-md-10">
				<div class="solutions-header">Этапы и стоимость работ</div>
				<p class="solutions-text">
					Качественное планирование &ndash; одна из главных составляющих каждой рекламной кампании.
					При разбиении кампании на подготовительный этап и этап реализации мы можем наилучшим образом учесть все особенности товара или услуги и наилучшим образом скоординировать работу всей команды.
				</p>
				<div class="solutions-button">
			      <button class="button button-blue-light button-collapse" data-action="show" data-target="#solutions-content2" data-anchor="#steps" data-group="context">
						<span class="button-collapse-ico-down"></span>
			            <span class="button-collapse-text"><span class="button-collapse-text-decoration">Узнать больше</span></span>
			      </button>
			      <button class="button button-blue-light button-collapse display-none" data-action="hide" data-target="#solutions-content2" data-anchor="#steps" data-anchor-offset="-350">
						<span class="button-collapse-ico-up"></span>
			            <span class="button-collapse-text"><span class="button-collapse-text-decoration">Свернуть</span></span>
			      </button>
				</div>
			</div>
		</div>
	</div>
</div>

<div class="solutions-content" id="solutions-content2">
	<a name="steps"></a>
	<div class="solutions-row shadow-top-inset bg-blue-light container-fluid">
		<div class="container">
			<div class="row row-right-md">
				<div class="col-md-4 col-sm-12 solutions-row-img">
					<img class="img-responsive m-auto" src="assets/img/context/solutions4.png" alt="">
				</div>
				<div class="col-md-7 col-offset-md-1">
					<div class="font-bold margin-bottom-5">Определение точных целей рекламной кампании</div>
					<p>
						Мы работаем с заказчиком для того, чтобы определить цели будущей кампании. 
						Это может быть рост узнаваемости бренда, рост продаж, увеличение количества посетителей сайта.
						От определения целей зависит разработка стратегии рекламной кампании.
						При этом цели должны ставиться максимально четко: например, не просто увеличение посещений, 
						а обеспечение не менее 100 посещений, которые завершаются заказом товара или услуги.
					</p>
					<div class="font-bold margin-bottom-5">Тщательный анализ аудитории и конкурентов</div>
					<p>
					Основополагающим фактором при подготовке контекстной рекламной кампании является мониторинг рынка.
					Без данного этапа невозможна работа по продвижению масштабных проектов.
					Характер и динамика сегмента рынка, в котором представлена ваша компания,
					позволяют правильно разработать основную стратегию. 
					</p>
					<p>
						При этом в рассмотрение берутся не только количество запросов по ключевым словам и спрос на товар или услугу,
						но и политика, которую избрали для работы конкуренты вашей компании, динамические особенности рынка,
						основные тренды, используемые в рекламе, и множество других аспектов.
						Чем более комплексно осуществляется анализ, тем эффективнее будет разрабатываемая стратегия контекстной рекламы.
						К тому же, такой подход позволяет существенно снизить риски.
					</p>
					
				</div>
			</div>
		</div>
	</div>
	<div class="solutions-row container-fluid">
		<div class="container">
			<div class="row">
				<div class="col-md-4 col-sm-12 solutions-row-img">
					<img class="img-responsive m-auto" src="assets/img/context/solutions5.png" alt="">
				</div>
				<div class="col-md-7">
					<div class="font-bold margin-bottom-5">Планирование результатов: ювелирная точность настройки</div>
					<p>
						Если KPI кампании уже определены, наши специалисты готовят прогноз для избранной стратегии:
						увеличение переходов, покупателей или звонков.
						При планировании предлагается наиболее эффективная стратегия и концепция для объявлений,
						в будущем способных принести максимальный результат.
						Значительный опыт работы в самых разнообразных тематиках является базой,
						которая предоставляет нам возможность давать прогнозы высокой точности.
						Также этому способствуют данные прогнозирования систем контекстной рекламы.
					</p>
					<div class="font-bold margin-bottom-5">Подготовка и запуск рекламной кампании</div>
					<p>
						Мы согласовываем с заказчиком подробный план действий.
						После этого начинается этап работ по подготовке объявлений.
						Когда все объявления согласованы, мы размещаем их на выбранных площадках,
						настраиваем систему аналитики на интернет-ресурсе.
						Настраиваем геотаргетинга и проводим другие необходимые настройки,
						позволяющие максимально точно нацелить объявления на аудиторию.
					</p>
					<p>
						Отдельное объявление подготавливается для каждого из выбранных ранее запросов.
						Конечно, при этом задача становится более объемной, но именно такой подход способствует
						повышению эффективности рекламной кампании.
					</p>
					
					<div class="font-bold margin-bottom-5">Мониторинг, обратная связь и оптимизация кампании</div>
					<p>
						После запуска кампании наша работа не заканчивается: 
						анализ показателей количества и стоимости переходов и KPI позволяет нам получать
						комплексные статистические данные для анализа эффективности кампании.
						При анализе рассматриваются все факторы в комплексе: конверсия, поведение на сайте, сезонность...
						Это позволяет своевременно корректировать кампанию, оптимизировать расход средств.
						Кроме того, такой анализ дает базу для рекомендаций по самому сайту,
						его функциональности и удобству для посетителей.
					</p>
					<div class="font-bold margin-bottom-5">Документирование кампании</div>
					<p>
						Каждый этап любой рекламной кампании, организованной нашими специалистами, 
						обязательно сопровождается соответствующими документами. Фиксируются требования и условия, изменения в кампанию,
						регулярно предоставляются отчеты со статистической информацией.
						Периодичность такого документооборота согласовывается с заказчиком индивидуально.
					</p>
				</div>
			</div>
		</div>
	</div>
	<div class="solutions-row bg-blue-light container-fluid">
		<div class="container">
			<div class="row">
				<div class="solutions-row-header col-md-7 col-md-offset-1 text-left-md text-center">
					Стоимость
				</div>
			</div>
			<div class="row row-right-md">
				<div class="col-md-4 col-sm-12 solutions-row-img">
					<img class="img-responsive m-auto" src="assets/img/context/solutions6.png" alt="">
				</div>
				<div class="col-md-7 col-offset-md-1">
					<p>
						Мы готовы взяться за разработку, настройку и ведение кампаний с бюджетом от 30 000 рублей в месяц.
					</p>
					<!--<div class="font-bold margin-bottom-5">Скидки</div>
					<p>
						Использование сервиса &laquo;Яндекс.Директ&raquo; для размещения контекстной рекламы позволяет пользоваться накопительными скидками.
						В зависимости от бюджета кампании можно рассчитывать на предоставление скидки в размере до 25%.
						Скидки формируются в зависимости от объема оплаченных услуг в течение календарного года.
					</p>-->
					<p class="text-center text-left-md">
						<button class="button button-red-transparent fb-form" data-fancybox-href="#form-popup-default">Заказать рекламную кампанию</button>
					</p>
				</div>
			</div>
		</div>
	</div>
	<div class="oh text-center shadow-bottom-inset bg-blue-light container-fluid">
		<button class="button button-blue-light button-collapse m-auto margin-bottom-m5" data-action="hide" data-target="#solutions-content2"  data-anchor="#steps" data-anchor-offset="-350">
			<span class="button-collapse-ico-up"></span>
			<span class="button-collapse-text"><span class="button-collapse-text-decoration">Свернуть</span></span>
		</button>
	</div>
</div>


<div class="solutions">
	<div class="container">
		<div class="row">
			<div class="col-md-4 solutions-img">
				<img class="m-auto img-responsive" src="assets/img/context/circle3.png" alt="">
			</div>
			<div class="col-md-7 solutions-inner padding-top-md-20">
				<div class="solutions-header">Гарантия достижения результата</div>
				<p class="solutions-text">
					Мы используем систему ключевых показателей эффективности (KPI)
					и считаем показатели возврата вложений в рекламу (ROI) для того,
					чтобы результат нашей работы был максимально прозрачным и исчисляемым.
				</p>
				<div class="solutions-button">
			      <button class="button button-blue-light button-collapse" data-action="show" data-target="#solutions-content3" data-anchor="#result" data-group="context">
						<span class="button-collapse-ico-down"></span>
			            <span class="button-collapse-text"><span class="button-collapse-text-decoration">Узнать больше</span></span>
			      </button>
			      <button class="button button-blue-light button-collapse display-none" data-action="hide" data-target="#solutions-content3" data-anchor="#result" data-anchor-offset="-350">
						<span class="button-collapse-ico-up"></span>
			            <span class="button-collapse-text"><span class="button-collapse-text-decoration">Свернуть</span></span>
			      </button>
				</div>
			</div>
		</div>
	</div>
</div>

<div class="solutions-content" id="solutions-content3">
	<a name="result"></a>
	<div class="solutions-row shadow-top-inset bg-blue-light container-fluid">
		<div class="container">
			<div class="row">
				<div class="solutions-row-header col-md-7 col-md-offset-1 text-left-md text-center">
					Цели нашей работы
				</div>
			</div>
			<div class="row row-right-md">
				<div class="col-md-4 col-sm-12 solutions-row-img">
					<img class="img-responsive m-auto" src="assets/img/context/solutions7.png" alt="">
				</div>
				<div class="col-md-7 col-offset-md-1">
					<p>
						Любая успешная коммерческая деятельность предполагает получение прибыли.
						При нерациональном расходовании средств на организацию рекламных кампаний
						желаемой прибыли достичь невозможно. Комплексный подход, учет множества факторов,
						профессиональная веб-аналитика &ndash; все это дает возможность создать условия,
						при которых денежные средства, вложенные в рекламную кампанию, 
						в кратчайшие сроки начнут окупаться.
					</p>					
				</div>
			</div>
		</div>
	</div>
	<div class="solutions-row container-fluid">
		<div class="container">
			<div class="row">
				<div class="solutions-row-header col-md-7 col-md-offset-4 text-left-md text-center">
					Как мы планируем и оцениваем эффективность кампании
				</div>
			</div>
			<div class="row">
				<div class="col-md-4 col-sm-12 solutions-row-img">
					<img class="img-responsive m-auto" src="assets/img/context/solutions10.png" alt="">
				</div>
				<div class="col-md-7">
					<ul>
						<li>
							Согласовываем с заказчиком ключевые показатели эффективности кампании: стоимость 
							привлечения заказа (cost per action), соотношение бюджета кампании и желаемой прибыли и многие другие факторы,
						</li>
						<li>
							Производим тестирование выбранной стратегии, которое позволяет определить сроки получения целевых значений определенных ранее параметров,
						</li>
						<li>
							В процессе сбора статистических данных определяем необходимые изменения, направленные на повышение эффективности кампании,
						</li>
						<li>
							Если возникает необходимость, производим коррекцию величин целевых критериев.
						</li>
					</ul>
				</div>
			</div>
		</div>
	</div>
	<div class="solutions-row bg-blue-light container-fluid">
		<div class="container">
			<div class="row">
				<div class="solutions-row-header col-md-7 col-md-offset-1 text-left-md text-center">
					Ожидаемый результат от рекламы
				</div>
			</div>
			<div class="row row-right-md">
				<div class="col-md-4 col-sm-12 solutions-row-img">
					<img class="img-responsive m-auto" src="assets/img/context/solutions8.png" alt="">
				</div>
				<div class="col-md-7 col-offset-md-1">
					<p>
						Как правило, мы планируем рекламные кампании таким образом, чтобы обеспечить
						рост продаж у клиента на 40-50% от объема, который был зафиксирован на момент обращения.
						Кроме этой очевидной задачи, мы решаем вопросы повышения узнаваемости компании на рынке и улучшения имиджа бренда.
						При этом мы не забываем уделять внимание оптимизации затрат заказчика на рекламную кампанию, и своевременно вносить
						необходимые коррективы. Это обеспечивается посредством анализа статистических данных и динамики продаж.
					</p>
				</div>
			</div>
		</div>
	</div>
	<div class="solutions-row container-fluid">
		<div class="container">
			<div class="row">
				<div class="solutions-row-header col-md-7 col-md-offset-4 text-left-md text-center">
					И всё же, почему мы?
				</div>
			</div>
			<div class="row">
				<div class="col-md-4 col-sm-12 solutions-row-img">
					<img class="img-responsive m-auto" src="assets/img/context/solutions9.png" alt="">
				</div>
				<div class="col-md-7">
					<p>
					Потому что у наших специалистов многолетний успешный опыт в контекстной рекламе.<br>
					Потому что мы постоянно отслеживаем новинки и тенденции для улучшения собственных результатов.<br>
					Потому что мы умеем максимально точно настраивать рекламные кампании и качественно анализировать их эффективность после запуска.<br>
					Всё это позволяет нам обеспечить отличный результат для клиентов.
					</p>
					<p class="text-center text-left-md">
						<button class="button button-red-transparent fb-form" data-fancybox-href="#form-popup-default">Заказать рекламную кампанию</button>
					</p>
				</div>
			</div>
		</div>
	</div>
	<div class="oh text-center shadow-bottom-inset container-fluid">
		<button class="button button-blue-light button-collapse m-auto margin-bottom-m5" data-action="hide" data-target="#solutions-content3" data-anchor="#result" data-anchor-offset="-350">
			<span class="button-collapse-ico-up"></span>
			<span class="button-collapse-text"><span class="button-collapse-text-decoration">Свернуть</span></span>
		</button>
	</div>
</div>

<!---->
<div class="solutions solutions-last">
	<div class="container">
		<div class="row row-right-md">
			<div class="col-md-4 solutions-img">
				<img class="m-auto img-responsive" src="assets/img/context/circle4.png" alt="">
			</div>
			<div class="col-md-7 solutions-inner padding-top-md-45">
				<div class="solutions-header">Аудит контекстной рекламы</div>
				<p class="solutions-text">
					Ваши расходы на рекламу растут, но клиентов не становится больше?<br>
					Вам поможет подробный аудит ваших рекламных кампаний. 
				</p>
				<div class="solutions-button">
			      <button class="button button-blue-light button-collapse" data-action="show" data-target="#solutions-content4" data-anchor="#audit" data-group="context">
						<span class="button-collapse-ico-down"></span>
			            <span class="button-collapse-text"><span class="button-collapse-text-decoration">Узнать больше</span></span>
			      </button>
			      <button class="button button-blue-light button-collapse display-none" data-action="hide" data-target="#solutions-content4" data-anchor="#audit" data-anchor-offset="-350">
						<span class="button-collapse-ico-up"></span>
			            <span class="button-collapse-text"><span class="button-collapse-text-decoration">Свернуть</span></span>
			      </button>
				</div>
			</div>
		</div>
	</div>
</div>

<div class="solutions-content" id="solutions-content4">
	<a name="audit"></a>
	<div class="solutions-row shadow-top-inset bg-blue-light container-fluid">
		<div class="container">
			<div class="row">
				<div class="solutions-row-header col-md-7 col-md-offset-1 text-left-md text-center">
					Для чего нужен аудит рекламы?
				</div>
			</div>
			<div class="row row-right-md">
				<div class="col-md-4 col-sm-12 solutions-row-img">
					<img class="img-responsive m-auto" src="assets/img/context/solutions7.png" alt="">
				</div>
				<div class="col-md-7 col-offset-md-1">
					<p>
						При аудите производится оценка качества рекламной кампании по множеству параметров на всех этапах: планирования, настройки, проведения. 
						Также профессиональный аудит подразумевает анализ рекламной активности конкурентов, что позволяет повысить эффективность рекламы заказчика.

					</p>
					<p class="font-bold">Необходимость в поведении аудита рекламной кампании возникает в следующих случаях:</p>
					<ul>
						<li>
							Чрезмерно раздутый бюджет рекламной кампании,
						</li>
						<li>
							Низкая конверсия: есть посетители, но нет заказов,
						</li>
						<li>
							Сомнения в текущем подрядчике по ведению рекламы,
						</li>
						<li>
							любые другие случаи, вызывающие сомнения в эффективности рекламы, когда деньги уходят, а результата нет.
						</li>
					</ul>
				</div>
			</div>
		</div>
	</div>
	<div class="solutions-row container-fluid">
		<div class="container">
			<div class="row">
				<div class="solutions-row-header col-md-7 col-md-offset-4 text-left-md text-center">
					Этапы работ по аудиту кампании
				</div>
			</div>
			<div class="row">
				<div class="col-md-4 col-sm-12 solutions-row-img">
					<img class="img-responsive m-auto" src="assets/img/context/solutions10.png" alt="">
				</div>
				<div class="col-md-7">
					<div class="font-bold margin-bottom-5">Анализ используемых рекламных площадок;</div>
					<p>
						Оценка целесообразности и эффективности избранных рекламных инструментов и методик контекстной рекламы для конкретной задачи;
					</p>
					<div class="font-bold margin-bottom-5">Анализ структуры кампании</div>
					<p>
						Рассмотрение соответствия кампании сегменту рынка, в котором работает заказчик, а также ранее использованных площадок;
					</p>
					<div class="font-bold margin-bottom-5">Анализ выбранных ключевых запросов</div>
					<p>
						Оценка широты подбора запросов, их стоимости, соответствия рекламному предложению и площадке;
					</p>
					<div class="font-bold margin-bottom-5">Анализ текстов объявлений</div>
					<p>
						Оценка текстов объявлений, правильности их составления, способности привлечь внимание целевой аудитории;
					</p>
					<div class="font-bold margin-bottom-5">Маркетинговый аудит сайта</div>
					<p>
						Аудит направлен на оценку юзабилити, изучение действий посетителей, выявление причин снижения конверсии;
					</p>
					<div class="font-bold margin-bottom-5">Анализ настроек кампании</div>
					<p>
						Такой анализ позволяет определить правильность выбора региона, времени и других параметров в соответствии с характеристиками
						продвигаемого продукта;
					</p>
					<div class="font-bold margin-bottom-5">Анализ стратегии управления ставками</div>
					<p>
						Оцениваем стоимость перехода на сайт, эффективность распределения бюджета по разным кампаниям, соответствия среднему чеку и охвату аудитории;И
					</p>
					<div class="font-bold margin-bottom-5">Изучение рекламной активности конкурентов</div>
					<p>
						Использование автоматизированных средств анализа вместе с ручным изучением позволяет определить работающие приёмы и рекомендации, внедрение которых в рекламную кампанию позволит повысить ее эффективность.
					</p>
				</div>
			</div>
		</div>
	</div>
	<div class="solutions-row bg-blue-light container-fluid">
		<div class="container">
			<div class="row">
				<div class="solutions-row-header col-md-7 col-md-offset-1 text-left-md text-center">
					Результат аудита кампании
				</div>
			</div>
			<div class="row row-right-md">
				<div class="col-md-4 col-sm-12 solutions-row-img">
					<img class="img-responsive m-auto" src="assets/img/context/solutions8.png" alt="">
				</div>
				<div class="col-md-7 col-offset-md-1">
					<p>
						Результат аудита &ndash; подробный отчет, в котором будут детально описаны результаты проверки по каждому из критериев.
						В отчёте мы предложим рекомендации по устранению выявленных слабых мест кампании и сайта.
					</p>
				</div>
			</div>
		</div>
	</div>
	<div class="solutions-row container-fluid">
		<div class="container">
			<div class="row">
				<div class="solutions-row-header col-md-7 col-md-offset-4 text-left-md text-center">
					Всё ещё сомневаетесь?
				</div>
			</div>
			<div class="row">
				<div class="col-md-4 col-sm-12 solutions-row-img">
					<img class="img-responsive m-auto" src="assets/img/context/solutions11.png" alt="">
				</div>
				<div class="col-md-7">
					<ul>
						<li>мы умеем обеспечивать одни из самых низких цен за посетителя в Рунете;</li>
						<li>мы увеличиваем отдачу от рекламного бюджета на 40-50%</li>
						<li>мы умеем точно настраивать кампании на всех основных площадках &ndash; это подтверждено их сертификатами.</li>
					</ul>
				</div>
			</div>
		</div>
	</div>
	<div class="solutions-row bg-blue-light container-fluid">
		<div class="container">
			<div class="row">
				<div class="solutions-row-header col-md-7 col-md-offset-1 text-left-md text-center">
					Стоимость и сроки аудита кампании
				</div>
			</div>
			<div class="row row-right-md">
				<div class="col-md-4 col-sm-12 solutions-row-img">
					<img class="img-responsive m-auto" src="assets/img/context/solutions12.png" alt="">
				</div>
				<div class="col-md-7 col-offset-md-1">
					<p>
						Стоимость аудита - от 10 000 рублей. Стоимость зависит от количества кампаний, их объёма и количества рекламных площадок.
						Эти же факторы влияют и на время, необходимое на проведение аудита.<br>
						После получения отчета и рекомендаций вы сможете внедрить их самостоятельно или заказать оптимизацию кампаний у нас.
					</p>
					<div class="margin-bottom-5 font-bold">Все отношения конфиденциальны</div>
					<p>
						Предоставление услуг осуществляется на основании договора, который заключается между нами и клиентом.
						Данный договор обеспечивает персоналу доступ к рекламным аккаунтам заказчика,
						а одним из его пунктов являются условия конфиденциальности, которые обязаны соблюдать все наши специалисты.
						Поэтому вы можете не переживать относительно передаваемой нам и получаемой информации.
						Аудит начинается после подписания договора с заказчиком и внесения 100% предоплаты.
					</p>
					<button class="button button-red-transparent fb-form" data-fancybox-href="#form-popup-default">Заказать рекламную кампанию</button>
				</div>
			</div>
		</div>
	</div>
	<div class="oh text-center shadow-bottom-inset bg-blue-light container-fluid">
		<button class="button button-blue-light button-collapse m-auto margin-bottom-m5" data-action="hide" data-target="#solutions-content4" data-anchor="#audit" data-anchor-offset="-350">
			<span class="button-collapse-ico-up"></span>
			<span class="button-collapse-text"><span class="button-collapse-text-decoration">Свернуть</span></span>
		</button>
	</div>
</div>

@stop