@extends('site.layout', ['Title' => 'Разработка программных комплексов'])

@section('head-styles')
  <link rel="stylesheet" href="assets/css/support.css">
@stop

@section('content')
<div class="header">
	<div class="header-inner container">
		<div class="row">
			<div class="col-md-11 col-md-offset-1">
				<h1>
					Поддержка сайтов
				</h1>
			</div>
		</div>
	</div>
</div>

<div class="about container margin-top-lg-m140 margin-top-md-m120 margin-top-sm-m100 margin-top-m40">
	<h2>Зачем платить за поддержку сайта?</h2>
	<div class="text-center">
		Очевидно, что во время эксплуатации сайта часто возникает необходимость <span class="hidden-xs"><br></span>
		внесения некоторых изменений в его содержание или внешний вид. <span class="hidden-xs"><br></span>
		Мы предлагаем комплекс услуг по обновлению и поддержке сайтов:<br><br>
	</div>
</div>

<div class="circles container">
	<div class="row">
	<div class="circles-item col-md-4 col-sm-4 col-xs-6">
		<div class="circles-header"><div class="center">Изменение<br>содержимого сайта</div></div>
		<div class="circles-img"><img class="img-responsive" src="assets/img/support/circle1.png"></div>
	</div>
	<div class="circles-item col-md-4 col-sm-4 col-xs-6">
		<div class="circles-header"><div class="center">Изменение<br>дизайна сайта</div></div>
		<div class="circles-img"><img class="img-responsive" src="assets/img/support/circle2.png"></div>
	</div>
	<div class="circles-item col-md-4 col-sm-4 col-xs-6">
		<div class="circles-header"><div class="center">Сохранение резервных<br>копий сайта</div></div>
		<div class="circles-img"><img class="img-responsive" src="assets/img/support/circle3.png"></div>
	</div>
	<div class="circles-item col-md-4 col-sm-4 col-xs-6">
		<div class="circles-header"><div class="center">Защита от взломов<br>и вирусов</div></div>
		<div class="circles-img"><img class="img-responsive" src="assets/img/support/circle4.png"></div>
	</div>
	<div class="circles-item col-md-4 col-sm-4 col-xs-6">
		<div class="circles-header"><div class="center">Обеспечение корректной<br>работы ресурса</div></div>
		<div class="circles-img"><img class="img-responsive" src="assets/img/support/circle5.png"></div>
	</div>
	<div class="circles-item col-md-4 col-sm-4 col-xs-6">
		<div class="circles-header"><div class="center">Консультирование<br>по вопросам работы сайта</div></div>
		<div class="circles-img"><img class="img-responsive" src="assets/img/support/circle6.png"></div>
	</div>
	</div>
</div>

<div class="prices container">
	<h2>Сколько стоит поддержка сайта?</h2>
	<p class="text-center">
		Стоимость работ по поддержке сайта напрямую отражает время и усилия, на них затраченные. <span class="hidden-xs"><br></span>
		Благодаря доступной системе расчета стоимости услуг и ежемесячным отчетам, <span class="hidden-xs"><br></span>
		Вы сможете легко контролировать, на что расходуется Ваш бюджет.
	</p>
	<ul class="nav nav-tabs prices-tabs">
	    <li class="active"><a href="#once" data-toggle="tab">Разовая поддержка сайта</a></li>
	    <li><a href="#monthly" data-toggle="tab">Ежемесячная поддержка сайта</a></li>
	    <li><a href="#actions" data-toggle="tab">Скидки и акции</a></li>
  	</ul>
	<div class="tab-content">
	    <div class="tab-pane active fade in" id="once">
	    	<table width="100%" class="prices-table">
	    		<tr>
	    			<td width="90%">Разовое изменение контента (изменить телефон, адрес и т. п.)</td>
	    			<td>500 <i class="fa fa-rub"></i></td>
	    		</tr>
	    		<tr>
	    			<td>Верстка и размещение текста 1 страница (A4, Times New Roman, 12 кегль)</td>
	    			<td>500 <i class="fa fa-rub"></i></td>
	    		</tr>
	    		<tr>
	    			<td>Создание новой страницы с добавлением меню или ссылки</td>
	    			<td>800 <i class="fa fa-rub"></i></td>
	    		</tr>
	    		<tr>
	    			<td>Веб оптимизация и размещение изображения (1 изображение)</td>
	    			<td>250 <i class="fa fa-rub"></i></td>
	    		</tr>
	    		<tr>
	    			<td>Верстка и размещение таблицы (1 штука, до 10 строк)</td>
	    			<td>500 <i class="fa fa-rub"></i></td>
	    		</tr>
	    		<tr>
	    			<td>Изменение пункта меню или ссылки</td>
	    			<td>500 <i class="fa fa-rub"></i></td>
	    		</tr>	
	    		<tr>
	    			<td colspan="2">Перенос сайта на другой хостинг</td>
	    		</tr>
	    		<tr>
	    			<td>Перенос стандартного веб-сайта</td>
	    			<td>2000 <i class="fa fa-rub"></i></td>
	    		</tr>
	    		<tr>
	    			<td>Перенос веб-сайта, базы данных и скриптов</td>
	    			<td>4000 <i class="fa fa-rub"></i></td>
	    		</tr>
	    		<tr>
	    			<td>Перенос веб-сайта построенного на CMS (Bitrix, Joomla, Wordpress и т. п.)</td>
	    			<td>8000 <i class="fa fa-rub"></i></td>
	    		</tr>
	    	</table>
	    </div>
	    <div class="tab-pane fade" id="monthly">
	    	<table width="100%" class="prices-table">
	    		<tr>
	    			<td width="90%">Разовое изменение контента (изменить телефон, адрес и т. п.)</td>
	    			<td>500 <i class="fa fa-rub"></i></td>
	    		</tr>
	    		<tr>
	    			<td>Верстка и размещение текста 1 страница (A4, Times New Roman, 12 кегль)</td>
	    			<td>500 <i class="fa fa-rub"></i></td>
	    		</tr>
	    		<tr>
	    			<td>Создание новой страницы с добавлением меню или ссылки</td>
	    			<td>800 <i class="fa fa-rub"></i></td>
	    		</tr>
	    		<tr>
	    			<td>Веб оптимизация и размещение изображения (1 изображение)</td>
	    			<td>250 <i class="fa fa-rub"></i></td>
	    		</tr>
	    		<tr>
	    			<td>Верстка и размещение таблицы (1 штука, до 10 строк)</td>
	    			<td>500 <i class="fa fa-rub"></i></td>
	    		</tr>
	    		<tr>
	    			<td>Изменение пункта меню или ссылки</td>
	    			<td>500 <i class="fa fa-rub"></i></td>
	    		</tr>	
	    		<tr>
	    			<td colspan="2">Перенос сайта на другой хостинг</td>
	    		</tr>
	    		<tr>
	    			<td>Перенос стандартного веб-сайта</td>
	    			<td>2000 <i class="fa fa-rub"></i></td>
	    		</tr>
	    		<tr>
	    			<td>Перенос веб-сайта, базы данных и скриптов</td>
	    			<td>4000 <i class="fa fa-rub"></i></td>
	    		</tr>
	    		<tr>
	    			<td>Перенос веб-сайта построенного на CMS (Bitrix, Joomla, Wordpress и т. п.)</td>
	    			<td>8000 <i class="fa fa-rub"></i></td>
	    		</tr>
	    	</table>
	    </div>
	    <div class="tab-pane fade" id="actions">
	    	<table width="100%" class="prices-table">
	    		<tr>
	    			<td width="90%">Разовое изменение контента (изменить телефон, адрес и т. п.)</td>
	    			<td>500 <i class="fa fa-rub"></i></td>
	    		</tr>
	    		<tr>
	    			<td>Верстка и размещение текста 1 страница (A4, Times New Roman, 12 кегль)</td>
	    			<td>500 <i class="fa fa-rub"></i></td>
	    		</tr>
	    		<tr>
	    			<td>Создание новой страницы с добавлением меню или ссылки</td>
	    			<td>800 <i class="fa fa-rub"></i></td>
	    		</tr>
	    		<tr>
	    			<td>Веб оптимизация и размещение изображения (1 изображение)</td>
	    			<td>250 <i class="fa fa-rub"></i></td>
	    		</tr>
	    		<tr>
	    			<td>Верстка и размещение таблицы (1 штука, до 10 строк)</td>
	    			<td>500 <i class="fa fa-rub"></i></td>
	    		</tr>
	    		<tr>
	    			<td>Изменение пункта меню или ссылки</td>
	    			<td>500 <i class="fa fa-rub"></i></td>
	    		</tr>	
	    		<tr>
	    			<td colspan="2">Перенос сайта на другой хостинг</td>
	    		</tr>
	    		<tr>
	    			<td>Перенос стандартного веб-сайта</td>
	    			<td>2000 <i class="fa fa-rub"></i></td>
	    		</tr>
	    		<tr>
	    			<td>Перенос веб-сайта, базы данных и скриптов</td>
	    			<td>4000 <i class="fa fa-rub"></i></td>
	    		</tr>
	    		<tr>
	    			<td>Перенос веб-сайта построенного на CMS (Bitrix, Joomla, Wordpress и т. п.)</td>
	    			<td>8000 <i class="fa fa-rub"></i></td>
	    		</tr>
	    	</table>
	    </div>
	</div>
</div>

<div class="space-50"></div>

@stop