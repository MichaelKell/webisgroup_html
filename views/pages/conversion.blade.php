@extends('site.layout', ['Title' => 'Повышение конверсии'])

@section('head-styles')
<link rel="stylesheet" href="assets/plugins/amcharts/style.css">
<link rel="stylesheet" href="assets/css/conversion.css">
@stop

@section('footer')
<script type="text/javascript" src="assets/plugins/amcharts/amcharts.js"></script>
<script type="text/javascript" src="assets/plugins/amcharts/amstock.js"></script>
<script type="text/javascript" src="assets/plugins/amcharts/themes/light.js"></script>
<script type="text/javascript" src="assets/plugins/amcharts/serial.js"></script>
@stop

@section('content')
<div class="header">
	<div class="header-inner container">
		<div class="row">
			<div class="col-md-11 col-md-offset-1">
				<h1>
					Повышение конверсии <span class="hidden-xs"><br></span>и маркетинговый аудит сайта
				</h1>
			</div>
		</div>
	</div>
</div>

<div class="header-form container margin-top-lg-m200 margin-top-md-m140 margin-top-sm-m100">
	<div class="header-form-header row">
		<div class="col-md-8">
	 		Желаете превратить посетителей в покупателей?<span class="hidden-xs hidden-sm"><br></span> Мы знаем, как это сделать!
	 	</div>
	</div>
	<div class="row row-right-sm">
	 	<div class="col-md-5 col-sm-12 header-form-img padding-right-lg-90">
	 		<img class="img-responsive m-auto" src="assets/img/conversion/form_img.png" alt="">
	 	</div>
	 	<div class="col-md-7 col-sm-12">
	 		<div class="header-form-text">
				Оставьте заявку на проведение аудита вашего сайта:
			</div>
			<div class="row">
				<div class="col-md-12">
					<form class="form-ajax" action="./" method="POST">
						<div class="input-spacing">
							<input name="Name" type="text" class="input" placeholder="Как вас зовут?" data-required="name">
						</div>
						<div class="input-spacing">
							<div class="row">
								<div class="col-md-6 input-spacing-md-0 input-spacing">
									<input name="Phone" type="text" class="input" placeholder="Ваш телефон" data-required="phone">
								</div>
								<div class="col-md-6">
									<input name="Email" type="text" class="input" placeholder="Ваш Email" data-required="email">
								</div>
							</div>
						</div>
						<div class="input-spacing-0 input-spacing-sm">
							<div class="row">
								<div class="col-md-8 input-spacing input-spacing-md-0">
									<input name="Site" type="text" class="input" placeholder="Адрес вашего сайта">
								</div>
								<div class="col-md-4">
									<button type="submit" class="button button-red-transparent w100 height-as-input">Отправить</button>
								</div>
							</div>
						</div>
					</form>
				</div>
			</div>
	 	</div>
	</div>
</div>


<div class="results container">
    <h2>Как выглядит реальный результат:</h2>
    <div class="results-panel">
      	<div class="row">
        	<div class="results-panel-current col-md-6 col-sm-6 col-xs-6">
          		Проект: <a href="#" target="_blank" class="results-panel-project"></a>
        	</div>
        	<div class="results-panel-button col-md-6 col-sm-6 col-xs-6">
          		<button class="button button-blue-dark"><span class="ico-rotate-right"></span>&nbsp;&nbsp;Другой пример</button>
        	</div>
      	</div>
    </div>
    <div id="results-slides" class="results-slides">
      	<div class="results-slide" data-project="www.rc-go.ru">
        	<div class="row">
          		<div class="results-slide-img col-md-6 padding-right-sm-60">
            		<img src="assets/img/conversion/results_img.png" class="img-responsive" alt="">
          		</div>
          		<div class="results-slide-content col-md-6">
            		<div class="results-header">Ключевые моменты реализации</div>
            		<div class="results-slide-item">
              			<div class="results-slide-item-img">
                			<img src="assets/img/conversion/results_ico1.png" class="img-responsive" alt="">
              			</div>
              			<div class="results-slide-item-text">
                			Полностью переработан процесс заказа, разработаны инструменты для оптимизации работы менеджеров
              			</div>
            		</div>
            		<div class="results-slide-item">
              			<div class="results-slide-item-img">
                			<img src="assets/img/conversion/results_ico2.png" class="img-responsive" alt="">
              			</div>
              			<div class="results-slide-item-text">
                			Добавлены автоматический расчёт доставки и онлайн-оплата
              			</div>
            		</div>
            		<div class="results-slide-item">
              			<div class="results-slide-item-img">
                			<img src="assets/img/conversion/results_ico3.png" class="img-responsive" alt="">
              			</div>
              			<div class="results-slide-item-text">
                			Добавлены связи между товарами: сопутствующие, аналоги, запчасти
              			</div>
            		</div>
            		<div class="results-slide-item">
              			<div class="results-slide-item-img">
                			<img src="assets/img/conversion/results_ico4.png" class="img-responsive" alt="">
              			</div>
              			<div class="results-slide-item-text">
                			Изменён дизайн страницы товара для повышения удобства заказа, добавлена оперативная информация о наличии товаров.
              			</div>
            		</div>
          		</div>
        	</div>
	        <div class="row results-slide-charts">
	          	<div class="col-md-6">
	            	<div class="results-header text-center">
	            		<span class="ico-users-gray"></span>&nbsp;&nbsp;Количество посетителей <span class="ico-chart-up"></span> <span class="font-bold f-32">51</span><span class="font-bold f-20">%</span>
	            	</div>
	            	<div id="chart-left-1" class="results-slide-chart"></div>
	          	</div>
	          	<div class="col-md-6">
		            <div class="results-header text-center">
		            	<span class="ico-cart-gray"></span>&nbsp;&nbsp;Количество покупок <span class="ico-chart-up"></span> <span class="font-bold f-32">80</span><span class="font-bold f-20">%</span>
		            </div>
		            <div id="chart-right-1" class="results-slide-chart"></div>
	          	</div>
	        </div>
      	</div>

      	<div class="results-slide display-none" data-project="www.rc-go.ru">
        	<div class="row">
          		<div class="results-slide-img col-md-6 padding-right-sm-60">
            		<img src="assets/img/conversion/results_img.png" class="img-responsive" alt="">
          		</div>
          		<div class="results-slide-content col-md-6">
            		<div class="results-header">Ключевые моменты реализации</div>
            		<div class="results-slide-item">
              			<div class="results-slide-item-img">
                			<img src="assets/img/conversion/results_ico1.png" class="img-responsive" alt="">
              			</div>
              			<div class="results-slide-item-text">
                			Полностью переработан процесс заказа, разработаны инструменты для оптимизации работы менеджеров
              			</div>
            		</div>
            		<div class="results-slide-item">
              			<div class="results-slide-item-img">
                			<img src="assets/img/conversion/results_ico2.png" class="img-responsive" alt="">
              			</div>
              			<div class="results-slide-item-text">
                			Добавлены автоматический расчёт доставки и онлайн-оплата
              			</div>
            		</div>
            		<div class="results-slide-item">
              			<div class="results-slide-item-img">
                			<img src="assets/img/conversion/results_ico3.png" class="img-responsive" alt="">
              			</div>
              			<div class="results-slide-item-text">
                			Добавлены связи между товарами: сопутствующие, аналоги, запчасти
              			</div>
            		</div>
            		<div class="results-slide-item">
              			<div class="results-slide-item-img">
                			<img src="assets/img/conversion/results_ico4.png" class="img-responsive" alt="">
              			</div>
              			<div class="results-slide-item-text">
                			Изменён дизайн страницы товара для повышения удобства заказа, добавлена оперативная информация о наличии товаров.
              			</div>
            		</div>
          		</div>
        	</div>
        	<div class="row">
            	<div class="col-md-6">
              		<div class="results-header text-center">
              			<span class="ico-users-gray"></span>&nbsp;&nbsp;Количество посетителей <span class="ico-chart-up"></span> <span class="font-bold f-32">51</span><span class="font-bold f-20">%</span>
              		</div>
              		<div id="chart-left-2" class="results-slide-chart"></div>
            	</div>
            	<div class="col-md-6">
              		<div class="results-header text-center"><span class="ico-cart-gray"></span>&nbsp;&nbsp;Количество покупок <span class="ico-chart-up"></span> <span class="font-bold f-32">80</span><span class="font-bold f-20">%</span></div>
              		<div id="chart-right-2" class="results-slide-chart"></div>
            	</div>
        	</div>
      	</div>
    </div>
    <div class="text-center margin-top-25">
      	<button class="button button-red-transparent fb-form" data-fancybox-href="#form-popup-default">Хочу такой же результат</button>
    </div>
</div>


<div class="solutions">
	<h2>Частые проблемы при заказе услуг<span class="hidden-xs"><br></span> по повышению конверсии</h2>
	<div class="container">
		<div class="row">
			<div class="col-md-12">
				<div class="row">
					<div class="col-md-6 col-sm-6 solutions-item">
						<div class="solutions-header">
							<div class="red">Проблема №1</div>
							Найти профессионалов
						</div>
						<div class="solutions-text">
							Все компании одинаковые. Вы не можете заранее узнать,
							что компания предложит для решения вашей задачи.
							Проверить качество услуги можно только подписав договор
							и оплатив счет. 
						</div>
					</div>
					<div class="col-md-6 col-sm-6 solutions-item">
						<div class="solutions-header">
							<div class="green">				
								Наше решение
							</div>
							Вы понимаете, что заказываете
						</div>
						<div class="solutions-text">
							Мы отвечаем за качество предоставляемых услуг.
							Еще «на берегу» мы вникаем в ваши задачи и обговариваем
							методы их решения 
						</div>			
					</div>
					<div class="col-md-6 col-sm-6 solutions-item">
						<div class="solutions-header">			
							<div class="red">Проблема №2</div>		
							Все хотят нажиться по быстрому
						</div>
						<div class="solutions-text">
							Компания никак не мотивирует себя - цена за услугу
							не зависит от величины сайта и объёмов работы,
							Задача компании - взять как можно больше денег сразу
							несмотря на посещаемость, объём и экономические
							возможности своих клиентов. 
						</div>			
					</div>
					<div class="col-md-6 col-sm-6 solutions-item">
						<div class="solutions-header">
							<div class="green">				
								Наше решение
							</div>
							Точная настройка под клиента
						</div>
						<div class="solutions-text">
							Вникаем в ваш бизнес и ориентируемся на критерии оценки
							результата, которые мы определили заранее.
							Наши специалисты по маркетингу и конверсии подбирают
							решение специально для вас, не заставляя вас переплачивать
						</div>			
					</div>
					<div class="col-md-6 col-sm-6 solutions-item">
						<div class="solutions-header">			
							<div class="red">Проблема №3</div>		
							Больше рекламы вместо конверсии
						</div>
						<div class="solutions-text">
							Маскируясь под слово «конверсия»вам просто продают
							ещё «немного» Директа и СЕО, даже не пытаясь проводить
							анализ сайта, заменяя конверсию просто большим
							количеством заявок.
						</div>			
					</div>
					<div class="col-md-6 col-sm-6 solutions-item">
						<div class="solutions-header">
							<div class="green">				
								Наше решение
							</div>
							Превращаем посетителей в клиентов
						</div>
						<div class="solutions-text">
							Работаем над графическими элементами и подачей информаци.
							После наших работ конверсия вашего сайта увеличится надолго.
						</div>			
					</div>
					<div class="col-md-6 col-sm-6 solutions-item">
						<div class="solutions-header">			
							<div class="red">Проблема №4</div>		
							Тянут время и деньги
						</div>
						<div class="solutions-text">
							Вам предлагают бесплатный аудит сайта, проводимый
							автоматическими системами и сразу советуют вам заказать
							у них «продающий» сайт. 
						</div>			
					</div>
					<div class="col-md-6 col-sm-6 solutions-item">
						<div class="solutions-header">
							<div class="green">
								Наше решение
							</div>
							Не навязываем лишних услуг
						</div>
						<div class="solutions-text">
							Вам не придется заказывать новый сайт, если ваш старый все еще 
							может приносить прибыль после того, как мы над ним поработаем.
							Мы проводим аудит сайта вручную и даем ответ на главный вопрос: 
							— «Как увеличить прибыль?».
						</div>			
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

<div class="advantages">
	<h2>Увеличиваем вашу прибыль</h2>
	<div class="container">
		<div class="row">
		 	<div class="col-lg-3 col-md-3 col-sm-6 col-xs-12 advantages-item flipper-container">
				<div class="flipper">
					<div class="flipper-front">
						<div class="advantages-img">
							<div class="v-middle">
								<img src="assets/img/conversion/flipper1.png" alt="">
							</div>
						</div>
						<div class="advantages-header">
							Здесь и сейчас
						</div>
						<div class="advantages-item-bg"></div>
					</div>
					<div class="flipper-back">
						<div class="advantages-text">  
							В рамках предварительного анализа даём вам «быстрое» решение, которое вы можете легко сделать самостоятельно и оценить нашу компетенцию.
						</div>
						<div class="advantages-item-bg"></div>
					</div>
				</div>
		    </div>
		 	<div class="col-lg-3 col-md-3 col-sm-6 col-xs-12 advantages-item flipper-container">
				<div class="flipper">
					<div class="flipper-front">
						<div class="advantages-img">
							<div class="v-middle">
								<img src="assets/img/conversion/flipper2.png" alt="">
							</div>
						</div>
						<div class="advantages-header">
							И большим,<br>и малым
						</div>
						<div class="advantages-item-bg"></div>
					</div>
					<div class="flipper-back">
						<div class="advantages-text">
							Формируем стоимость исходя из объёма работ. Вы оплачиваете наше время, а не волшебную «услугу»
						</div>
						<div class="advantages-item-bg"></div>
					</div>
				</div>
		    </div>
		 	<div class="col-lg-3 col-md-3 col-sm-6 col-xs-12 advantages-item flipper-container">
				<div class="flipper">
					<div class="flipper-front">
						<div class="advantages-img">
							<div class="v-middle">
								<img src="assets/img/conversion/flipper3.png" alt="">
							</div>
						</div>
						<div class="advantages-header">
							Прозрачность<br>мотивации
						</div>
						<div class="advantages-item-bg"></div>
					</div>
					<div class="flipper-back">
						<div class="advantages-text">
							Персонально для каждого клиента устанавливаем премию за достигнутый результат.
						</div>
						<div class="advantages-item-bg"></div>
					</div>
				</div>
		    </div>
		 	<div class="col-lg-3 col-md-3 col-sm-6 col-xs-12 advantages-item flipper-container">
				<div class="flipper">
					<div class="flipper-front">
						<div class="advantages-img">
							<div class="v-middle">
								<img src="assets/img/conversion/flipper4.png" alt="">
							</div>
						</div>
						<div class="advantages-header">
							Никаких<br>роботов
						</div>
						<div class="advantages-item-bg"></div>
					</div>
					<div class="flipper-back">
						<div class="advantages-text">
							Изучаем сайт клиента и формируем задание на улучшение сайта для людей, а не для поисковых роботов
						</div>
						<div class="advantages-item-bg"></div>
					</div>
				</div>
		    </div>
		 	<div class="col-lg-3 col-md-3 col-sm-6 col-xs-12 advantages-item flipper-container">
				<div class="flipper">
					<div class="flipper-front">
						<div class="advantages-img">
							<div class="v-middle">
								<img src="assets/img/conversion/flipper5.png" alt="">
							</div>
						</div>
						<div class="advantages-header">
							Точно в цель
						</div>
						<div class="advantages-item-bg"></div>
					</div>
					<div class="flipper-back">
						<div class="advantages-text">
							Работаем именно с конверсией. Все прочие услуги вы можете делать самостоятельно, заказать у нас или в другой компании
						</div>
						<div class="advantages-item-bg"></div>
					</div>
				</div>
		    </div>
		 	<div class="col-lg-3 col-md-3 col-sm-6 col-xs-12 advantages-item flipper-container">
				<div class="flipper">
					<div class="flipper-front">
						<div class="advantages-img">
							<div class="v-middle">
								<img src="assets/img/conversion/flipper6.png" alt="">
							</div>
						</div>
						<div class="advantages-header">
							Конверсия<br>в движении
						</div>
						<div class="advantages-item-bg"></div>
					</div>
					<div class="flipper-back">
						<div class="advantages-text">
							Даём решение здесь и сейчас изучая реакцию покупателей на изменения, после чего даём новые рекомендации.
						</div>
						<div class="advantages-item-bg"></div>
					</div>
				</div>
		    </div>
		 	<div class="col-lg-3 col-md-3 col-sm-6 col-xs-12 advantages-item flipper-container">
				<div class="flipper">
					<div class="flipper-front">
						<div class="advantages-img">
							<div class="v-middle">
								<img src="assets/img/conversion/flipper7.png" alt="">
							</div>
						</div>
						<div class="advantages-header">
							Широта взгляда
						</div>
						<div class="advantages-item-bg"></div>
					</div>
					<div class="flipper-back">
						<div class="advantages-text">
							Оцениваем «оффлайновые» особенности бизнеса, учитывая их в рекомендациях
						</div>
						<div class="advantages-item-bg"></div>
					</div>
				</div>
		    </div>
		 	<div class="col-lg-3 col-md-3 col-sm-6 col-xs-12 advantages-item flipper-container">
		      	<div class="flipper">
		        	<div class="flipper-front">
		          		<div class="advantages-img">
		            		<div class="v-middle">
		              			<img src="assets/img/conversion/flipper8.png" alt="">
		            		</div>
		          		</div>
		          		<div class="advantages-header">
		            		Тайные закупки
		          		</div>
		          		<div class="advantages-item-bg"></div>
		        	</div>
		        	<div class="flipper-back">
		          		<div class="advantages-text">
		            		Проводим 5 тайных закупок, выявляем основные проблемы менеджмента, пишем рекомендации.
		          		</div>
		          		<div class="advantages-item-bg"></div>
		        	</div>
		      </div>
		    </div>
		</div>
	</div>
</div>


<div class="approach">
	<h2 class="white">Наш подход к работе</h2>
	<div class="approach-inner container">
		<div class="approach-item-a">
			Анализируем статистику кликов и навигации для выявления «проблемных» мест
		</div>
		<div class="approach-item-a">
			Проводим анализ конкурентов
		</div>
		<div class="approach-item-a">
			Определяем уникальные торговые преимущества (УТП)
		</div>
		<div class="approach-item-a">
			Пишем продающие тексты
		</div>
		<div class="approach-item-a">
			Выявляем лучшую стратегию с помощью A/B тестирования посадочных страниц
		</div>
		<div class="approach-item-a">
			 Повышаем юзабилити сайта
		</div>
		<div class="approach-item-a">
			Настраиваем отслеживание звонков и заявок с сайта
		</div>
	</div>
	<div class="approach-more container">
		<div class="row">
			<div class="approach-more-item col-md-4">
				<div class="approach-more-item-img">
					<img src="assets/img/conversion/approach1.png" class="img-responsive" alt="">
				</div>
				<div class="approach-more-item-header">
					Не замыкаем<br>процессы на себя
				</div>
				<div class="approach-more-item-text">
					Необходимые доработки 
					и рекламу, при желании, 
					вы можете заказать у своих 
					подрядчиков по своим 
					критериям.
				</div>
			</div>
			<div class="approach-more-item col-md-4">
				<div class="approach-more-item-img">
					<img src="assets/img/conversion/approach2.png" class="img-responsive" alt="">
				</div>
				<div class="approach-more-item-header">
					Не используем<br>тарифы<br>
				</div>
				<div class="approach-more-item-text">
					Единые цены не позволяют 
					подойти к каждому сайту детально: 
					маленький и большой сайт 
					требуют разного 
					времени на изучение. 
				</div>
			</div>
			<div class="approach-more-item col-md-4">
				<div class="approach-more-item-img">
					<img src="assets/img/conversion/approach3.png" class="img-responsive" alt="">
				</div>
				<div class="approach-more-item-header">
					Чётко закрепляем<br>показатели<br>и нашу оплату
				</div>
				<div class="approach-more-item-text">
					На основе преддоговорного 
					анализа формируем чёткую 
					стоимость наших услуг 
					в зависимости от результата, 
					который достигнете вы.
				</div>
			</div>
		</div>
	</div>
</div>	

<div class="awards container margin-top-sm-m40 margin-top-xs-20">
	<div class="row">
		<div class="awards-item col-md-3 col-sm-3 col-xs-6">
			<img src="assets/img/conversion/awards1.png" alt="">
		</div>
		<div class="awards-item col-md-3 col-sm-3 col-xs-6">
			<img src="assets/img/conversion/awards2.png" alt="">
		</div>
		<div class="awards-item col-md-3 col-sm-3 col-xs-6">
			<img src="assets/img/conversion/awards3.png" alt="">
		</div>
		<div class="awards-item col-md-3 col-sm-3 col-xs-6">
			<img src="assets/img/conversion/awards4.png" alt="">
		</div>
	</div>
</div>

@include('partials._reviews')

@stop