@extends('site.layout', ['Title' => 'Веб аналитика'])

@section('head-styles')
<link rel="stylesheet" href="assets/css/analytics.css">
@stop

@section('content')
<div class="header">
	<div class="header-inner container">
		<div class="row">
			<div class="col-md-11 col-md-offset-1">
				<h1>
					Веб аналитика
				</h1>
				<div class="header-thesis">
					Ищем слабые места<br>
					Рекламных кампаний сайтов
				</div>
			</div>
		</div>
	</div>
</div>

<div class="about container margin-top-lg-m160 margin-top-md-m140 margin-top-sm-m125">
	<div class="row">
		<div class="col-md-7 col-md-offset-1">
			<h2 class="text-left margin-bottom-20">Для чего нужна веб-аналитика?</h2>
			Когда рекламные кампании, организованные в сети, не дают желаемого результата,
			несмотря на немалые материальные затраты, возникает вопрос: в чем же причина?<br>
			Веб-аналитика является единственным способом достоверно установить, почему кампания неэффективна.
			Такое исследование, помимо выявления проблем, позволяет разработать рекомендации для их устранения и точно настроить рекламную кампанию на целевую аудиторию.<br>
		</div>
	</div>
</div>

<h2 class="margin-top-60 margin-bottom-20">Что мы делаем:</h2>
<div class="solutions">
	<div class="container">
		<div class="row">
			<div class="col-md-4 solutions-img">
				<img class="m-auto img-responsive" src="assets/img/analytics/circle1.jpg" alt="">
			</div>
			<div class="col-md-7 solutions-inner">
				<div class="solutions-header">Анализируем сайт и рекламную кампанию</div>
				<p class="solutions-text">
					Невозможно повысить эффективность любого механизма, в том числе и рекламного,
					если не представлять себе точно причины его неправильной работы (и как он должен работать в идеале).<br>
					Мы проведём глубокий анализ вашей рекламной кампании в комплексе и выясним, почему реклама работает недостаточно эффективно,
					почему низкая конверсия на сайте, и как это исправить.
				</p>
				<div class="solutions-button">
			      <button class="button button-blue-light button-collapse" data-action="show" data-target="#solutions-content1" data-anchor="#analysis" data-group="analytics">
						<span class="button-collapse-ico-down"></span>
			            <span class="button-collapse-text"><span class="button-collapse-text-decoration">Узнать больше</span></span>
			      </button>
			      <button class="button button-blue-light button-collapse display-none" data-action="hide" data-target="#solutions-content1" data-anchor="#analysis" data-anchor-offset="-350">
						<span class="button-collapse-ico-up"></span>
			            <span class="button-collapse-text"><span class="button-collapse-text-decoration">Свернуть</span></span>
			      </button>
				</div>
			</div>
		</div>
	</div>
</div>
<div class="solutions-content" id="solutions-content1">
	<a name="analysis"></a>
	<div class="solutions-row shadow-top-inset bg-blue-light container-fluid">
		<div class="container">
			<div class="row">
				<div class="solutions-row-header col-md-7 col-md-offset-1 text-left-md text-center">
					Цель услуги
				</div>
			</div>
			<div class="row row-right-md">
				<div class="col-md-4 col-sm-12 solutions-row-img">
					<img class="img-responsive m-auto" src="assets/img/analytics/solutions1.png" alt="">
				</div>
				<div class="col-md-7 col-offset-md-1">
					<div class="font-bold margin-bottom-5">Анализ эффективности сайта</div>
					<p>
						Вы - владелец или администратор коммерческого веб-ресурса, и вам кажется, 
						что прибыли могли быть и повыше? 
						Вас расстраивает посещаемость вашего сайта или слишком низкое количество 
						новых регистраций? 
						Мы предлагаем простое решение проблемы – анализ эффективности сайта.
					</p>
					<div class="font-bold margin-bottom-5">Чем поможем</div>
					<p>
						Анализ сайта поможет повысить эффективность сайта как инструмента продаж; 
						найти и убрать причины, кaоторые снижают конверсию посетителей в клиентов 
						на сайте, увеличить количество заявок/регистраций/продаж без увеличения 
						рекламных бюджетов с помощью изменения функционала и внешнего вида сайта.
					</p>
				</div>
			</div>
		</div>
	</div>

	<div class="solutions-row container-fluid">
		<div class="container">
			<div class="row">
				<div class="solutions-row-header col-md-7 col-md-offset-4 text-left-md text-center">
					Этапы работы
				</div>
			</div>
			<div class="row">
				<div class="col-md-4 col-sm-12 solutions-row-img">
					<img class="img-responsive m-auto" src="assets/img/analytics/solutions2.png" alt="">
				</div>
				<div class="col-md-7">
					<p>
						Что же конкретно делают наши специалисты, проводя анализ сайта?
					</p>
					<div class="font-bold margin-bottom-15">Работа по анализу сайта проходит в несколько этапов:</div>
					<ul>
						<li>определение целевых действий посетителей;</li>
						<li>установка и настройка систем веб-аналитики;<br> сбор статистических данных;</li>
						<li>проведение анализа эффективности ресурса после сбора данных;</li>
						<li>создание отчета с подробным описанием выявленных проблем и рекомендациями наших специалистов.</li>
					</ul>
				</div>
			</div>
		</div>
	</div>

	<div class="solutions-row bg-blue-light container-fluid">
		<div class="container">
			<div class="row">
				<div class="solutions-row-header col-md-7 col-md-offset-1 text-left-md text-center">
					Результат
				</div>
			</div>
			<div class="row row-right-md">
				<div class="col-md-5 col-sm-12 solutions-row-img">
					<img class="img-responsive m-auto" src="assets/img/analytics/solutions3.png" alt="">
				</div>
				<div class="col-md-6">
					<p>
						В результате вы получите фактически готовое руководство к действию. 
						Мы оценим, насколько эффективен ваш веб-ресурс. 
						Найдем причины, которые мешают посетителям превратиться в клиентов. 
						Расскажем вам о них и даже дадим подсказки, что конкретно нужно 
						сделать, чтобы волшебство конверсии все-таки свершилось! 
						И все это за рекордно короткое для столь серьезной работы время 
						– в среднем около 35 дней.
					</p>
				</div>
			</div>
		</div>
	</div>

	<div class="solutions-row container-fluid">
		<div class="container">
			<div class="row">
				<div class="solutions-row-header col-md-7 col-md-offset-4 text-left-md text-center">
					Почему мы?
				</div>
			</div>
			<div class="row">
				<div class="col-md-4 col-sm-12 solutions-row-img">
					<img class="img-responsive m-auto" src="assets/img/analytics/solutions4.png" alt="">
				</div>
				<div class="col-md-7">
					<p>
						Мы виртуозно применяем богатый арсенал инструментов для решения 
						практически любых задач в рамках своей профессии. 
						Анализируя поведение пользователей на сайте, мы используем 
						Google Analytics, Яндекс Метрику, ClickTale, Visual Website Optimizer и т. д. 
					</p>
					<p>
						Нами также осуществляется оценка всех рекламных каналов 
						в интернет-маркетинге – SEO, контекстной и медийной рекламы, SMM,
						а также нестандартных видов рекламы, таких, например, 
						как приложения в соцсетях.
					</p>
					<p class="font-bold">
						Повышение конверсии в 3-4 раза
					</p>
					<p>
						В результате каждый наш клиент получает ощутимую выгоду. 
						Доказанный факт: после работы наших мастеров конверсия 
						повышается в 3-4 раза. 
						Неудивительно, ведь они проводят максимально глубокий анализ 
						– от их глаз не скрывается ничего.
					</p>
				</div>
			</div>
		</div>
	</div>

	<div class="cost solutions-row bg-blue-light container-fluid">
		<div class="container">
			<div class="row">
				<div class="solutions-row-header col-md-7 col-md-offset-1 text-left-md text-center">
					Стоимость услуги
				</div>
			</div>
			<div class="row row-right-md">
				<div class="col-md-5 col-sm-12 solutions-row-img cost-img">
					<img class="img-responsive m-auto" src="assets/img/analytics/solutions5.png" alt="">
				</div>
				<div class="col-md-6">
					<p>
						Стоимость проведения анализа сайта зависит от конкретной задачи 
						- сложности анализа, структуры сайта, необходимых для решения задачи 
						систем веб-аналитики и составляет от 60 тыс. руб.
					</p>
					<p class="font-bold">
						Сроки выполнения работ - 30-40 дней.
					</p>
					<p class="text-center text-left-md">
						<button class="button button-red-transparent fb-form" data-fancybox-href="#form-popup-default">Заказать анализ сайта</button>
					</p>
				</div>
			</div>
		</div>
	</div>

	<div class="oh text-center shadow-bottom-inset bg-blue-light container-fluid">
		<button class="button button-blue-light button-collapse m-auto margin-bottom-m5" data-action="hide" data-target="#solutions-content1" data-anchor="#analysis" data-anchor-offset="-350">
			<span class="button-collapse-ico-up"></span>
			<span class="button-collapse-text"><span class="button-collapse-text-decoration">Свернуть</span></span>
		</button>
	</div>
</div>
<!---->
<div class="solutions">
	<div class="container">
		<div class="row row-right-md">
			<div class="col-md-4 solutions-img">
				<img class="m-auto img-responsive" src="assets/img/analytics/circle2.jpg" alt="">
			</div>
			<div class="col-md-7 solutions-inner">
				<div class="solutions-header">Настраиваем системы веб-аналитики</div>
				<p class="solutions-text">
					Без правильно настроенных современных инструментов анализа невозможно проанализировать всю воронку продаж
					и выявить слабые места.<br>Мы организуем сбор всей необходимой информации о вашем онлайн-бизнесе: 
					источники посетителей, самые популярные страницы, продолжительность посещения, процент отказов и другие важные показатели.
				</p>
				<div class="solutions-button">
			      <button class="button button-blue-light button-collapse" data-action="show" data-target="#solutions-content2" data-anchor="#setting" data-group="analytics">
						<span class="button-collapse-ico-down"></span>
			            <span class="button-collapse-text"><span class="button-collapse-text-decoration">Узнать больше</span></span>
			      </button>
			      <button class="button button-blue-light button-collapse display-none" data-action="hide" data-target="#solutions-content2" data-anchor="#setting" data-anchor-offset="-350">
						<span class="button-collapse-ico-up"></span>
			            <span class="button-collapse-text"><span class="button-collapse-text-decoration">Свернуть</span></span>
			      </button>
				</div>
			</div>
		</div>
	</div>
</div>
<div class="solutions-content" id="solutions-content2">
	<a name="setting"></a>
	<div class="solutions-row shadow-top-inset bg-blue-light container-fluid">
		<div class="container">
			<div class="row">
				<div class="solutions-row-header col-md-7 col-md-offset-1 text-left-md text-center">
					Цель услуги
				</div>
			</div>
			<div class="row row-right-md">
				<div class="col-md-4 col-sm-12 solutions-row-img">
					<img class="img-responsive m-auto" src="assets/img/analytics/solutions1.png" alt="">
				</div>
				<div class="col-md-7 col-offset-md-1">
					<div class="font-bold margin-bottom-5">Анализ эффективности сайта</div>
					<p>
						Вы - владелец или администратор коммерческого веб-ресурса, и вам кажется, 
						что прибыли могли быть и повыше? 
						Вас расстраивает посещаемость вашего сайта или слишком низкое количество 
						новых регистраций? 
						Мы предлагаем простое решение проблемы – анализ эффективности сайта.
					</p>
					<div class="font-bold margin-bottom-5">Чем поможем</div>
					<p>
						Анализ сайта поможет повысить эффективность сайта как инструмента продаж; 
						найти и убрать причины, кaоторые снижают конверсию посетителей в клиентов 
						на сайте, увеличить количество заявок/регистраций/продаж без увеличения 
						рекламных бюджетов с помощью изменения функционала и внешнего вида сайта.
					</p>
				</div>
			</div>
		</div>
	</div>

	<div class="solutions-row container-fluid">
		<div class="container">
			<div class="row">
				<div class="solutions-row-header col-md-7 col-md-offset-4 text-left-md text-center">
					Этапы работы
				</div>
			</div>
			<div class="row">
				<div class="col-md-4 col-sm-12 solutions-row-img">
					<img class="img-responsive m-auto" src="assets/img/analytics/solutions2.png" alt="">
				</div>
				<div class="col-md-7">
					<p>
						Что же конкретно делают наши специалисты, проводя анализ сайта?
					</p>
					<div class="font-bold margin-bottom-15">Работа по анализу сайта проходит в несколько этапов:</div>
					<ul>
						<li>определение целевых действий посетителей;</li>
						<li>установка и настройка систем веб-аналитики;<br> сбор статистических данных;</li>
						<li>проведение анализа эффективности ресурса после сбора данных;</li>
						<li>создание отчета с подробным описанием выявленных проблем и рекомендациями наших специалистов.</li>
					</ul>
				</div>
			</div>
		</div>
	</div>

	<div class="solutions-row bg-blue-light container-fluid">
		<div class="container">
			<div class="row">
				<div class="solutions-row-header col-md-7 col-md-offset-1 text-left-md text-center">
					Результат
				</div>
			</div>
			<div class="row row-right-md">
				<div class="col-md-5 col-sm-12 solutions-row-img">
					<img class="img-responsive m-auto" src="assets/img/analytics/solutions3.png" alt="">
				</div>
				<div class="col-md-6">
					<p>
						В результате вы получите фактически готовое руководство к действию. 
						Мы оценим, насколько эффективен ваш веб-ресурс. 
						Найдем причины, которые мешают посетителям превратиться в клиентов. 
						Расскажем вам о них и даже дадим подсказки, что конкретно нужно 
						сделать, чтобы волшебство конверсии все-таки свершилось! 
						И все это за рекордно короткое для столь серьезной работы время 
						– в среднем около 35 дней.
					</p>
				</div>
			</div>
		</div>
	</div>

	<div class="solutions-row container-fluid">
		<div class="container">
			<div class="row">
				<div class="solutions-row-header col-md-7 col-md-offset-4 text-left-md text-center">
					Почему мы?
				</div>
			</div>
			<div class="row">
				<div class="col-md-4 col-sm-12 solutions-row-img">
					<img class="img-responsive m-auto" src="assets/img/analytics/solutions4.png" alt="">
				</div>
				<div class="col-md-7">
					<p>
						Мы виртуозно применяем богатый арсенал инструментов для решения 
						практически любых задач в рамках своей профессии. 
						Анализируя поведение пользователей на сайте, мы используем 
						Google Analytics, Яндекс Метрику, ClickTale, Visual Website Optimizer и т. д. 
					</p>
					<p>
						Нами также осуществляется оценка всех рекламных каналов 
						в интернет-маркетинге – SEO, контекстной и медийной рекламы, SMM,
						а также нестандартных видов рекламы, таких, например, 
						как приложения в соцсетях.
					</p>
					<p class="font-bold">
						Повышение конверсии в 3-4 раза
					</p>
					<p>
						В результате каждый наш клиент получает ощутимую выгоду. 
						Доказанный факт: после работы наших мастеров конверсия 
						повышается в 3-4 раза. 
						Неудивительно, ведь они проводят максимально глубокий анализ 
						– от их глаз не скрывается ничего.
					</p>
				</div>
			</div>
		</div>
	</div>

	<div class="cost solutions-row bg-blue-light container-fluid">
		<div class="container">
			<div class="row">
				<div class="solutions-row-header col-md-7 col-md-offset-1 text-left-md text-center">
					Стоимость услуги
				</div>
			</div>
			<div class="row row-right-md">
				<div class="col-md-5 col-sm-12 solutions-row-img cost-img">
					<img class="img-responsive m-auto" src="assets/img/analytics/solutions5.png" alt="">
				</div>
				<div class="col-md-6">
					<p>
						Стоимость проведения анализа сайта зависит от конкретной задачи 
						- сложности анализа, структуры сайта, необходимых для решения задачи 
						систем веб-аналитики и составляет от 60 тыс. руб.
					</p>
					<p class="font-bold">
						Сроки выполнения работ - 30-40 дней.
					</p>
					<p class="text-center text-left-md">
						<button class="button button-red-transparent fb-form" data-fancybox-href="#form-popup-default">Заказать анализ сайта</button>
					</p>
				</div>
			</div>
		</div>
	</div>

	<div class="oh text-center shadow-bottom-inset bg-blue-light container-fluid">
		<button class="button button-blue-light button-collapse m-auto margin-bottom-m5" data-action="hide" data-target="#solutions-content2" data-anchor="#setting" data-anchor-offset="-350">
			<span class="button-collapse-ico-up"></span>
			<span class="button-collapse-text"><span class="button-collapse-text-decoration">Свернуть</span></span>
		</button>
	</div>
</div>
<!---->
<div class="solutions">
	<div class="container">
		<div class="row">
			<div class="col-md-4 solutions-img">
				<img class="m-auto img-responsive" src="assets/img/analytics/circle3.jpg" alt="">
			</div>
			<div class="col-md-7 solutions-inner">
				<div class="solutions-header">Проводим пост-клик анализ</div>
				<p class="solutions-text">
					Проблемы могут возникать на любом этапе: реклама может не привлекать посетителей, сайт может быть недостаточно мотивирующим к заказу,
					либо сам процесс заказа может быть чересчур сложным для заинтересовавшихся посетителей.<br>
					Мы проверим все факторы, отследим полный путь потенциального клиента и примем меры для того, чтобы реклама работала, а бизнес развивался.
					Для этого мы точно настраиваем цели действий посетителей на сайте: заход на нужную страницу, запрос дополнительной информации, обращение в компанию, резервирование товара, оформление заказа и другие нужные события.
					Так мы получаем исчисляемые показатели, позволяющие делать выводы об эффективности как рекламы, так и самого сайта.
				</p>
				<div class="solutions-button">
			      <button class="button button-blue-light button-collapse" data-action="show" data-target="#solutions-content3" data-anchor="#postclick" data-group="analytics">
						<span class="button-collapse-ico-down"></span>
			            <span class="button-collapse-text"><span class="button-collapse-text-decoration">Узнать больше</span></span>
			      </button>
			      <button class="button button-blue-light button-collapse display-none" data-action="hide" data-target="#solutions-content3" data-anchor="#postclick" data-anchor-offset="-350">
						<span class="button-collapse-ico-up"></span>
			            <span class="button-collapse-text"><span class="button-collapse-text-decoration">Свернуть</span></span>
			      </button>
				</div>
			</div>
		</div>
	</div>
</div>
<div class="solutions-content" id="solutions-content3">
	<a name="postclick"></a>
	<div class="solutions-row shadow-top-inset bg-blue-light container-fluid">
		<div class="container">
			<div class="row">
				<div class="solutions-row-header col-md-7 col-md-offset-1 text-left-md text-center">
					Цель услуги
				</div>
			</div>
			<div class="row row-right-md">
				<div class="col-md-4 col-sm-12 solutions-row-img">
					<img class="img-responsive m-auto" src="assets/img/analytics/solutions1.png" alt="">
				</div>
				<div class="col-md-7 col-offset-md-1">
					<div class="font-bold margin-bottom-5">Анализ эффективности сайта</div>
					<p>
						Вы - владелец или администратор коммерческого веб-ресурса, и вам кажется, 
						что прибыли могли быть и повыше? 
						Вас расстраивает посещаемость вашего сайта или слишком низкое количество 
						новых регистраций? 
						Мы предлагаем простое решение проблемы – анализ эффективности сайта.
					</p>
					<div class="font-bold margin-bottom-5">Чем поможем</div>
					<p>
						Анализ сайта поможет повысить эффективность сайта как инструмента продаж; 
						найти и убрать причины, кaоторые снижают конверсию посетителей в клиентов 
						на сайте, увеличить количество заявок/регистраций/продаж без увеличения 
						рекламных бюджетов с помощью изменения функционала и внешнего вида сайта.
					</p>
				</div>
			</div>
		</div>
	</div>

	<div class="solutions-row container-fluid">
		<div class="container">
			<div class="row">
				<div class="solutions-row-header col-md-7 col-md-offset-4 text-left-md text-center">
					Этапы работы
				</div>
			</div>
			<div class="row">
				<div class="col-md-4 col-sm-12 solutions-row-img">
					<img class="img-responsive m-auto" src="assets/img/analytics/solutions2.png" alt="">
				</div>
				<div class="col-md-7">
					<p>
						Что же конкретно делают наши специалисты, проводя анализ сайта?
					</p>
					<div class="font-bold margin-bottom-15">Работа по анализу сайта проходит в несколько этапов:</div>
					<ul>
						<li>определение целевых действий посетителей;</li>
						<li>установка и настройка систем веб-аналитики;<br> сбор статистических данных;</li>
						<li>проведение анализа эффективности ресурса после сбора данных;</li>
						<li>создание отчета с подробным описанием выявленных проблем и рекомендациями наших специалистов.</li>
					</ul>
				</div>
			</div>
		</div>
	</div>

	<div class="solutions-row bg-blue-light container-fluid">
		<div class="container">
			<div class="row">
				<div class="solutions-row-header col-md-7 col-md-offset-1 text-left-md text-center">
					Результат
				</div>
			</div>
			<div class="row row-right-md">
				<div class="col-md-5 col-sm-12 solutions-row-img">
					<img class="img-responsive m-auto" src="assets/img/analytics/solutions3.png" alt="">
				</div>
				<div class="col-md-6">
					<p>
						В результате вы получите фактически готовое руководство к действию. 
						Мы оценим, насколько эффективен ваш веб-ресурс. 
						Найдем причины, которые мешают посетителям превратиться в клиентов. 
						Расскажем вам о них и даже дадим подсказки, что конкретно нужно 
						сделать, чтобы волшебство конверсии все-таки свершилось! 
						И все это за рекордно короткое для столь серьезной работы время 
						– в среднем около 35 дней.
					</p>
				</div>
			</div>
		</div>
	</div>

	<div class="solutions-row container-fluid">
		<div class="container">
			<div class="row">
				<div class="solutions-row-header col-md-7 col-md-offset-4 text-left-md text-center">
					Почему мы?
				</div>
			</div>
			<div class="row">
				<div class="col-md-4 col-sm-12 solutions-row-img">
					<img class="img-responsive m-auto" src="assets/img/analytics/solutions4.png" alt="">
				</div>
				<div class="col-md-7">
					<p>
						Мы виртуозно применяем богатый арсенал инструментов для решения 
						практически любых задач в рамках своей профессии. 
						Анализируя поведение пользователей на сайте, мы используем 
						Google Analytics, Яндекс Метрику, ClickTale, Visual Website Optimizer и т. д. 
					</p>
					<p>
						Нами также осуществляется оценка всех рекламных каналов 
						в интернет-маркетинге – SEO, контекстной и медийной рекламы, SMM,
						а также нестандартных видов рекламы, таких, например, 
						как приложения в соцсетях.
					</p>
					<p class="font-bold">
						Повышение конверсии в 3-4 раза
					</p>
					<p>
						В результате каждый наш клиент получает ощутимую выгоду. 
						Доказанный факт: после работы наших мастеров конверсия 
						повышается в 3-4 раза. 
						Неудивительно, ведь они проводят максимально глубокий анализ 
						– от их глаз не скрывается ничего.
					</p>
				</div>
			</div>
		</div>
	</div>

	<div class="cost solutions-row bg-blue-light container-fluid">
		<div class="container">
			<div class="row">
				<div class="solutions-row-header col-md-7 col-md-offset-1 text-left-md text-center">
					Стоимость услуги
				</div>
			</div>
			<div class="row row-right-md">
				<div class="col-md-5 col-sm-12 solutions-row-img cost-img">
					<img class="img-responsive m-auto" src="assets/img/analytics/solutions5.png" alt="">
				</div>
				<div class="col-md-6">
					<p>
						Стоимость проведения анализа сайта зависит от конкретной задачи 
						- сложности анализа, структуры сайта, необходимых для решения задачи 
						систем веб-аналитики и составляет от 60 тыс. руб.
					</p>
					<p class="font-bold">
						Сроки выполнения работ - 30-40 дней.
					</p>
					<p class="text-center text-left-md">
						<button class="button button-red-transparent fb-form" data-fancybox-href="#form-popup-default">Заказать анализ сайта</button>
					</p>
				</div>
			</div>
		</div>
	</div>

	<div class="oh text-center shadow-bottom-inset bg-blue-light container-fluid">
		<button class="button button-blue-light button-collapse m-auto margin-bottom-m5" data-action="hide" data-target="#solutions-content3" data-anchor="#postclick" data-anchor-offset="-350"> 
			<span class="button-collapse-ico-up"></span>
			<span class="button-collapse-text"><span class="button-collapse-text-decoration">Свернуть</span></span>
		</button>
	</div>
</div>
<!---->
<div class="solutions">
	<div class="container">
		<div class="row row-right-md">
			<div class="col-md-4 solutions-img">
				<img class="m-auto img-responsive" src="assets/img/analytics/circle4.jpg" alt="">
			</div>
			<div class="col-md-7 solutions-inner">
				<div class="solutions-header">Предоставляем постоянную аналитическую поддержку</div>
				<p class="solutions-text">
					Для сложных бизнесов в высококонкурентной среде одного отчёта с рекомендациями может быть не достаточно: конкуренты тоже не стоят на месте, требовательная аудитория отдаёт предпочтение тем сайтам, которые удобнее, быстрее, понятнее, привлекательнее.<br>
					В рамках аналитической поддержки мы можем заниматься постоянным отслеживанием эффективности рекламы
					и конверсии сайта и регулярно предоставлять отчёты и рекомендации по улучшению целевых показателей.
				</p>
				<div class="solutions-button">
			      <button class="button button-blue-light button-collapse" data-action="show" data-target="#solutions-content4" data-anchor="#support" data-group="analytics">
						<span class="button-collapse-ico-down"></span>
			            <span class="button-collapse-text"><span class="button-collapse-text-decoration">Узнать больше</span></span>
			      </button>
			      <button class="button button-blue-light button-collapse display-none" data-action="hide" data-target="#solutions-content4" data-anchor="#support" data-anchor-offset="-350">
						<span class="button-collapse-ico-up"></span>
			            <span class="button-collapse-text"><span class="button-collapse-text-decoration">Свернуть</span></span>
			      </button>
				</div>
			</div>
		</div>
	</div>
</div>
<div class="solutions-content" id="solutions-content4">
	<a name="support"></a>
	<div class="solutions-row shadow-top-inset bg-blue-light container-fluid">
		<div class="container">
			<div class="row">
				<div class="solutions-row-header col-md-7 col-md-offset-1 text-left-md text-center">
					Цель услуги
				</div>
			</div>
			<div class="row row-right-md">
				<div class="col-md-4 col-sm-12 solutions-row-img">
					<img class="img-responsive m-auto" src="assets/img/analytics/solutions1.png" alt="">
				</div>
				<div class="col-md-7 col-offset-md-1">
					<div class="font-bold margin-bottom-5">Анализ эффективности сайта</div>
					<p>
						Вы - владелец или администратор коммерческого веб-ресурса, и вам кажется, 
						что прибыли могли быть и повыше? 
						Вас расстраивает посещаемость вашего сайта или слишком низкое количество 
						новых регистраций? 
						Мы предлагаем простое решение проблемы – анализ эффективности сайта.
					</p>
					<div class="font-bold margin-bottom-5">Чем поможем</div>
					<p>
						Анализ сайта поможет повысить эффективность сайта как инструмента продаж; 
						найти и убрать причины, кaоторые снижают конверсию посетителей в клиентов 
						на сайте, увеличить количество заявок/регистраций/продаж без увеличения 
						рекламных бюджетов с помощью изменения функционала и внешнего вида сайта.
					</p>
				</div>
			</div>
		</div>
	</div>

	<div class="solutions-row container-fluid">
		<div class="container">
			<div class="row">
				<div class="solutions-row-header col-md-7 col-md-offset-4 text-left-md text-center">
					Этапы работы
				</div>
			</div>
			<div class="row">
				<div class="col-md-4 col-sm-12 solutions-row-img">
					<img class="img-responsive m-auto" src="assets/img/analytics/solutions2.png" alt="">
				</div>
				<div class="col-md-7">
					<p>
						Что же конкретно делают наши специалисты, проводя анализ сайта?
					</p>
					<div class="font-bold margin-bottom-15">Работа по анализу сайта проходит в несколько этапов:</div>
					<ul>
						<li>определение целевых действий посетителей;</li>
						<li>установка и настройка систем веб-аналитики;<br> сбор статистических данных;</li>
						<li>проведение анализа эффективности ресурса после сбора данных;</li>
						<li>создание отчета с подробным описанием выявленных проблем и рекомендациями наших специалистов.</li>
					</ul>
				</div>
			</div>
		</div>
	</div>

	<div class="solutions-row bg-blue-light container-fluid">
		<div class="container">
			<div class="row">
				<div class="solutions-row-header col-md-7 col-md-offset-1 text-left-md text-center">
					Результат
				</div>
			</div>
			<div class="row row-right-md">
				<div class="col-md-5 col-sm-12 solutions-row-img">
					<img class="img-responsive m-auto" src="assets/img/analytics/solutions3.png" alt="">
				</div>
				<div class="col-md-6">
					<p>
						В результате вы получите фактически готовое руководство к действию. 
						Мы оценим, насколько эффективен ваш веб-ресурс. 
						Найдем причины, которые мешают посетителям превратиться в клиентов. 
						Расскажем вам о них и даже дадим подсказки, что конкретно нужно 
						сделать, чтобы волшебство конверсии все-таки свершилось! 
						И все это за рекордно короткое для столь серьезной работы время 
						– в среднем около 35 дней.
					</p>
				</div>
			</div>
		</div>
	</div>

	<div class="solutions-row container-fluid">
		<div class="container">
			<div class="row">
				<div class="solutions-row-header col-md-7 col-md-offset-4 text-left-md text-center">
					Почему мы?
				</div>
			</div>
			<div class="row">
				<div class="col-md-4 col-sm-12 solutions-row-img">
					<img class="img-responsive m-auto" src="assets/img/analytics/solutions4.png" alt="">
				</div>
				<div class="col-md-7">
					<p>
						Мы виртуозно применяем богатый арсенал инструментов для решения 
						практически любых задач в рамках своей профессии. 
						Анализируя поведение пользователей на сайте, мы используем 
						Google Analytics, Яндекс Метрику, ClickTale, Visual Website Optimizer и т. д. 
					</p>
					<p>
						Нами также осуществляется оценка всех рекламных каналов 
						в интернет-маркетинге – SEO, контекстной и медийной рекламы, SMM,
						а также нестандартных видов рекламы, таких, например, 
						как приложения в соцсетях.
					</p>
					<p class="font-bold">
						Повышение конверсии в 3-4 раза
					</p>
					<p>
						В результате каждый наш клиент получает ощутимую выгоду. 
						Доказанный факт: после работы наших мастеров конверсия 
						повышается в 3-4 раза. 
						Неудивительно, ведь они проводят максимально глубокий анализ 
						– от их глаз не скрывается ничего.
					</p>
				</div>
			</div>
		</div>
	</div>

	<div class="cost solutions-row bg-blue-light container-fluid">
		<div class="container">
			<div class="row">
				<div class="solutions-row-header col-md-7 col-md-offset-1 text-left-md text-center">
					Стоимость услуги
				</div>
			</div>
			<div class="row row-right-md">
				<div class="col-md-5 col-sm-12 solutions-row-img cost-img">
					<img class="img-responsive m-auto" src="assets/img/analytics/solutions5.png" alt="">
				</div>
				<div class="col-md-6">
					<p>
						Стоимость проведения анализа сайта зависит от конкретной задачи 
						- сложности анализа, структуры сайта, необходимых для решения задачи 
						систем веб-аналитики и составляет от 60 тыс. руб.
					</p>
					<p class="font-bold">
						Сроки выполнения работ - 30-40 дней.
					</p>
					<p class="text-center text-left-md">
						<button class="button button-red-transparent fb-form" data-fancybox-href="#form-popup-default">Заказать анализ сайта</button>
					</p>
				</div>
			</div>
		</div>
	</div>

	<div class="oh text-center shadow-bottom-inset bg-blue-light container-fluid">
		<button class="button button-blue-light button-collapse m-auto margin-bottom-m5" data-action="hide" data-target="#solutions-content4" data-anchor="#support" data-anchor-offset="-350">
			<span class="button-collapse-ico-up"></span>
			<span class="button-collapse-text"><span class="button-collapse-text-decoration">Свернуть</span></span>
		</button>
	</div>
</div>
<!-- -->
<div class="solutions">
	<div class="container">
		<div class="row">
			<div class="col-md-4 solutions-img">
				<img class="m-auto img-responsive" src="assets/img/analytics/circle5.jpg" alt="">
			</div>
			<div class="col-md-7 solutions-inner">
				<div class="solutions-header">Отслеживаем источники звонков</div>
				<p class="solutions-text">
					Телефонные звонки нельзя оставлять без внимания: многим клиентам удобнее позвонить, чем сделать заказ на сайте.
					Источниками могут быть сайт, различная печатная реклама, объявления в онлайн-каталогах,
					даже записи в картографических сервисах. <br>
					Мы сможем отслеживать источники звонков в вашу компанию и поймём, какие из рекламных носителей наиболее эффективны в вашем случае.
				</p>
				<div class="solutions-button">
			      <button class="button button-blue-light button-collapse" data-action="show" data-target="#solutions-content5" data-anchor="#tracking" data-group="analytics">
						<span class="button-collapse-ico-down"></span>
			            <span class="button-collapse-text"><span class="button-collapse-text-decoration">Узнать больше</span></span>
			      </button>
			      <button class="button button-blue-light button-collapse display-none" data-action="hide" data-target="#solutions-content5" data-anchor="#tracking" data-anchor-offset="-350">
						<span class="button-collapse-ico-up"></span>
			            <span class="button-collapse-text"><span class="button-collapse-text-decoration">Свернуть</span></span>
			      </button>
				</div>
			</div>
		</div>
	</div>
</div>
<div class="solutions-content" id="solutions-content5">
	<a name="tracking"></a>
	<div class="solutions-row shadow-top-inset bg-blue-light container-fluid">
		<div class="container">
			<div class="row">
				<div class="solutions-row-header col-md-7 col-md-offset-1 text-left-md text-center">
					Цель услуги
				</div>
			</div>
			<div class="row row-right-md">
				<div class="col-md-4 col-sm-12 solutions-row-img">
					<img class="img-responsive m-auto" src="assets/img/analytics/solutions1.png" alt="">
				</div>
				<div class="col-md-7 col-offset-md-1">
					<div class="font-bold margin-bottom-5">Анализ эффективности сайта</div>
					<p>
						Вы - владелец или администратор коммерческого веб-ресурса, и вам кажется, 
						что прибыли могли быть и повыше? 
						Вас расстраивает посещаемость вашего сайта или слишком низкое количество 
						новых регистраций? 
						Мы предлагаем простое решение проблемы – анализ эффективности сайта.
					</p>
					<div class="font-bold margin-bottom-5">Чем поможем</div>
					<p>
						Анализ сайта поможет повысить эффективность сайта как инструмента продаж; 
						найти и убрать причины, кaоторые снижают конверсию посетителей в клиентов 
						на сайте, увеличить количество заявок/регистраций/продаж без увеличения 
						рекламных бюджетов с помощью изменения функционала и внешнего вида сайта.
					</p>
				</div>
			</div>
		</div>
	</div>

	<div class="solutions-row container-fluid">
		<div class="container">
			<div class="row">
				<div class="solutions-row-header col-md-7 col-md-offset-4 text-left-md text-center">
					Этапы работы
				</div>
			</div>
			<div class="row">
				<div class="col-md-4 col-sm-12 solutions-row-img">
					<img class="img-responsive m-auto" src="assets/img/analytics/solutions2.png" alt="">
				</div>
				<div class="col-md-7">
					<p>
						Что же конкретно делают наши специалисты, проводя анализ сайта?
					</p>
					<div class="font-bold margin-bottom-15">Работа по анализу сайта проходит в несколько этапов:</div>
					<ul>
						<li>определение целевых действий посетителей;</li>
						<li>установка и настройка систем веб-аналитики;<br> сбор статистических данных;</li>
						<li>проведение анализа эффективности ресурса после сбора данных;</li>
						<li>создание отчета с подробным описанием выявленных проблем и рекомендациями наших специалистов.</li>
					</ul>
				</div>
			</div>
		</div>
	</div>

	<div class="solutions-row bg-blue-light container-fluid">
		<div class="container">
			<div class="row">
				<div class="solutions-row-header col-md-7 col-md-offset-1 text-left-md text-center">
					Результат
				</div>
			</div>
			<div class="row row-right-md">
				<div class="col-md-5 col-sm-12 solutions-row-img">
					<img class="img-responsive m-auto" src="assets/img/analytics/solutions3.png" alt="">
				</div>
				<div class="col-md-6">
					<p>
						В результате вы получите фактически готовое руководство к действию. 
						Мы оценим, насколько эффективен ваш веб-ресурс. 
						Найдем причины, которые мешают посетителям превратиться в клиентов. 
						Расскажем вам о них и даже дадим подсказки, что конкретно нужно 
						сделать, чтобы волшебство конверсии все-таки свершилось! 
						И все это за рекордно короткое для столь серьезной работы время 
						– в среднем около 35 дней.
					</p>
				</div>
			</div>
		</div>
	</div>

	<div class="solutions-row container-fluid">
		<div class="container">
			<div class="row">
				<div class="solutions-row-header col-md-7 col-md-offset-4 text-left-md text-center">
					Почему мы?
				</div>
			</div>
			<div class="row">
				<div class="col-md-4 col-sm-12 solutions-row-img">
					<img class="img-responsive m-auto" src="assets/img/analytics/solutions4.png" alt="">
				</div>
				<div class="col-md-7">
					<p>
						Мы виртуозно применяем богатый арсенал инструментов для решения 
						практически любых задач в рамках своей профессии. 
						Анализируя поведение пользователей на сайте, мы используем 
						Google Analytics, Яндекс Метрику, ClickTale, Visual Website Optimizer и т. д. 
					</p>
					<p>
						Нами также осуществляется оценка всех рекламных каналов 
						в интернет-маркетинге – SEO, контекстной и медийной рекламы, SMM,
						а также нестандартных видов рекламы, таких, например, 
						как приложения в соцсетях.
					</p>
					<p class="font-bold">
						Повышение конверсии в 3-4 раза
					</p>
					<p>
						В результате каждый наш клиент получает ощутимую выгоду. 
						Доказанный факт: после работы наших мастеров конверсия 
						повышается в 3-4 раза. 
						Неудивительно, ведь они проводят максимально глубокий анализ 
						– от их глаз не скрывается ничего.
					</p>
				</div>
			</div>
		</div>
	</div>

	<div class="cost solutions-row bg-blue-light container-fluid">
		<div class="container">
			<div class="row">
				<div class="solutions-row-header col-md-7 col-md-offset-1 text-left-md text-center">
					Стоимость услуги
				</div>
			</div>
			<div class="row row-right-md">
				<div class="col-md-5 col-sm-12 solutions-row-img cost-img">
					<img class="img-responsive m-auto" src="assets/img/analytics/solutions5.png" alt="">
				</div>
				<div class="col-md-6">
					<p>
						Стоимость проведения анализа сайта зависит от конкретной задачи 
						- сложности анализа, структуры сайта, необходимых для решения задачи 
						систем веб-аналитики и составляет от 60 тыс. руб.
					</p>
					<p class="font-bold">
						Сроки выполнения работ - 30-40 дней.
					</p>
					<p class="text-center text-left-md">
						<button class="button button-red-transparent fb-form" data-fancybox-href="#form-popup-default">Заказать анализ сайта</button>
					</p>
				</div>
			</div>
		</div>
	</div>

	<div class="oh text-center shadow-bottom-inset bg-blue-light container-fluid">
		<button class="button button-blue-light button-collapse m-auto margin-bottom-m5" data-action="hide" data-target="#solutions-content5" data-anchor="#tracking" data-anchor-offset="-350">
			<span class="button-collapse-ico-up"></span>
			<span class="button-collapse-text"><span class="button-collapse-text-decoration">Свернуть</span></span>
		</button>
	</div>
</div>
<!---->
<div class="solutions solutions-last">
	<div class="container">
		<div class="row row-right-md">
			<div class="col-md-4 solutions-img">
				<img class="m-auto img-responsive" src="assets/img/analytics/circle6.jpg" alt="">
			</div>
			<div class="col-md-7 solutions-inner">
				<div class="solutions-header">Делаем мобильную аналитику</div>
				<p class="solutions-text">
					Вы задумывались о том, что около половины посетителей вашего сайта могут заходить на него со своих телефонов или планшетов?
					Необходимо обеспечить для них удобство работы и все необходимые возможности для осуществления заказа или другого нужного действия.<br>
					Мы проанализируем сайт на удобство для мобильных пользователей и скорость загрузки,
					подготовим подробные рекомендации по его улучшению и, по согласованию, сможем их реализовать.
				</p>
				<div class="solutions-button">
			      <button class="button button-blue-light button-collapse" data-action="show" data-target="#solutions-content6" data-anchor="#mobileanalytics" data-group="analytics">
						<span class="button-collapse-ico-down"></span>
			            <span class="button-collapse-text"><span class="button-collapse-text-decoration">Узнать больше</span></span>
			      </button>
			      <button class="button button-blue-light button-collapse display-none" data-action="hide" data-target="#solutions-content6" data-anchor="#mobileanalytics" data-anchor-offset="-350">
						<span class="button-collapse-ico-up"></span>
			            <span class="button-collapse-text"><span class="button-collapse-text-decoration">Свернуть</span></span>
			      </button>
				</div>
			</div>
		</div>
	</div>
</div>
<div class="solutions-content" id="solutions-content6">
	<a name="mobileanalytics"></a>
	<div class="solutions-row shadow-top-inset bg-blue-light container-fluid">
		<div class="container">
			<div class="row">
				<div class="solutions-row-header col-md-7 col-md-offset-1 text-left-md text-center">
					Цель услуги
				</div>
			</div>
			<div class="row row-right-md">
				<div class="col-md-4 col-sm-12 solutions-row-img">
					<img class="img-responsive m-auto" src="assets/img/analytics/solutions1.png" alt="">
				</div>
				<div class="col-md-7 col-offset-md-1">
					<div class="font-bold margin-bottom-5">Анализ эффективности сайта</div>
					<p>
						Вы - владелец или администратор коммерческого веб-ресурса, и вам кажется, 
						что прибыли могли быть и повыше? 
						Вас расстраивает посещаемость вашего сайта или слишком низкое количество 
						новых регистраций? 
						Мы предлагаем простое решение проблемы – анализ эффективности сайта.
					</p>
					<div class="font-bold margin-bottom-5">Чем поможем</div>
					<p>
						Анализ сайта поможет повысить эффективность сайта как инструмента продаж; 
						найти и убрать причины, кaоторые снижают конверсию посетителей в клиентов 
						на сайте, увеличить количество заявок/регистраций/продаж без увеличения 
						рекламных бюджетов с помощью изменения функционала и внешнего вида сайта.
					</p>
				</div>
			</div>
		</div>
	</div>

	<div class="solutions-row container-fluid">
		<div class="container">
			<div class="row">
				<div class="solutions-row-header col-md-7 col-md-offset-4 text-left-md text-center">
					Этапы работы
				</div>
			</div>
			<div class="row">
				<div class="col-md-4 col-sm-12 solutions-row-img">
					<img class="img-responsive m-auto" src="assets/img/analytics/solutions2.png" alt="">
				</div>
				<div class="col-md-7">
					<p>
						Что же конкретно делают наши специалисты, проводя анализ сайта?
					</p>
					<div class="font-bold margin-bottom-15">Работа по анализу сайта проходит в несколько этапов:</div>
					<ul>
						<li>определение целевых действий посетителей;</li>
						<li>установка и настройка систем веб-аналитики;<br> сбор статистических данных;</li>
						<li>проведение анализа эффективности ресурса после сбора данных;</li>
						<li>создание отчета с подробным описанием выявленных проблем и рекомендациями наших специалистов.</li>
					</ul>
				</div>
			</div>
		</div>
	</div>

	<div class="solutions-row bg-blue-light container-fluid">
		<div class="container">
			<div class="row">
				<div class="solutions-row-header col-md-7 col-md-offset-1 text-left-md text-center">
					Результат
				</div>
			</div>
			<div class="row row-right-md">
				<div class="col-md-5 col-sm-12 solutions-row-img">
					<img class="img-responsive m-auto" src="assets/img/analytics/solutions3.png" alt="">
				</div>
				<div class="col-md-6">
					<p>
						В результате вы получите фактически готовое руководство к действию. 
						Мы оценим, насколько эффективен ваш веб-ресурс. 
						Найдем причины, которые мешают посетителям превратиться в клиентов. 
						Расскажем вам о них и даже дадим подсказки, что конкретно нужно 
						сделать, чтобы волшебство конверсии все-таки свершилось! 
						И все это за рекордно короткое для столь серьезной работы время 
						– в среднем около 35 дней.
					</p>
				</div>
			</div>
		</div>
	</div>

	<div class="solutions-row container-fluid">
		<div class="container">
			<div class="row">
				<div class="solutions-row-header col-md-7 col-md-offset-4 text-left-md text-center">
					Почему мы?
				</div>
			</div>
			<div class="row">
				<div class="col-md-4 col-sm-12 solutions-row-img">
					<img class="img-responsive m-auto" src="assets/img/analytics/solutions4.png" alt="">
				</div>
				<div class="col-md-7">
					<p>
						Мы виртуозно применяем богатый арсенал инструментов для решения 
						практически любых задач в рамках своей профессии. 
						Анализируя поведение пользователей на сайте, мы используем 
						Google Analytics, Яндекс Метрику, ClickTale, Visual Website Optimizer и т. д. 
					</p>
					<p>
						Нами также осуществляется оценка всех рекламных каналов 
						в интернет-маркетинге – SEO, контекстной и медийной рекламы, SMM,
						а также нестандартных видов рекламы, таких, например, 
						как приложения в соцсетях.
					</p>
					<p class="font-bold">
						Повышение конверсии в 3-4 раза
					</p>
					<p>
						В результате каждый наш клиент получает ощутимую выгоду. 
						Доказанный факт: после работы наших мастеров конверсия 
						повышается в 3-4 раза. 
						Неудивительно, ведь они проводят максимально глубокий анализ 
						– от их глаз не скрывается ничего.
					</p>
				</div>
			</div>
		</div>
	</div>

	<div class="cost solutions-row bg-blue-light container-fluid">
		<div class="container">
			<div class="row">
				<div class="solutions-row-header col-md-7 col-md-offset-1 text-left-md text-center">
					Стоимость услуги
				</div>
			</div>
			<div class="row row-right-md">
				<div class="col-md-5 col-sm-12 solutions-row-img cost-img">
					<img class="img-responsive m-auto" src="assets/img/analytics/solutions5.png" alt="">
				</div>
				<div class="col-md-6">
					<p>
						Стоимость проведения анализа сайта зависит от конкретной задачи 
						- сложности анализа, структуры сайта, необходимых для решения задачи 
						систем веб-аналитики и составляет от 60 тыс. руб.
					</p>
					<p class="font-bold">
						Сроки выполнения работ - 30-40 дней.
					</p>
					<p class="text-center text-left-md">
						<button class="button button-red-transparent fb-form" data-fancybox-href="#form-popup-default">Заказать анализ сайта</button>
					</p>
				</div>
			</div>
		</div>
	</div>

	<div class="oh text-center shadow-bottom-inset bg-blue-light container-fluid">
		<button class="button button-blue-light button-collapse m-auto margin-bottom-m5" data-action="hide" data-target="#solutions-content6" data-anchor="#mobileanalytics" data-anchor-offset="-350">
			<span class="button-collapse-ico-up"></span>
			<span class="button-collapse-text"><span class="button-collapse-text-decoration">Свернуть</span></span>
		</button>
	</div>
</div>
@include('partials._reviews')
@stop