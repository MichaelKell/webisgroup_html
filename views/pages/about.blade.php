@extends('site.layout', ['Title' => 'О компании'])

@section('head-styles')
<link rel="stylesheet" type="text/css" href="assets/css/about.css">
@stop

@section('content')
<div class="header">
	<div class="container">
		<div class="row">
			<div class="col-md-10 col-md-offset-2 col-sm-offset-2 col-sm-10">
		  		<h1>О компании</h1>
		  	</div>
		</div>
	 </div>
</div>

<div class="tabs-navbar margin-top-md-m110 margin-top-sm-m80">
	<div class="container">
		<div class="row">
			<div class="col-md-10 col-md-offset-1 col-sm-12 col-xs-6 tabs-navbar-nav">
				<ul id="tinynav">
					<li class="tabs-navbar-nav-item active">
						<a href="#">О нас</a>
					</li>
					<li class="tabs-navbar-nav-item">
						<a href="#">Новости</a>
					</li>
					<li class="tabs-navbar-nav-item">
						<a href="#">Вакансии</a>
					</li>
				</ul>
			</div>
		</div>
	</div>
</div>

<div class="intro container">
	<div class="row">
		<div class="col-md-8 col-md-offset-2">
			Компания Webis Group была основана в 2000 году. Наши специалисты – 
			высококвалифицированные профессионалы, имеющие многолетний опыт работы. 
			Наши клиенты – коммерческие структуры, общественные организации, холдинговые 
			компании, международные корпорации из России, США и западной Европы.
		</div>
	</div>
</div>


<div class="about container">
	<div class="row">
		<div class="col-md-9">
			<h2>Почему мы?</h2>
		</div>
		<div class="col-md-3 hidden-sm hidden-xs">
			<h2>Наша история</h2>
		</div>
	</div>
	<div class="row">
		<div class="about-company col-md-5">
			<h4>
				Работаем с 2000 года
			</h4>
			<ul>
				<li>Квалифицированная техническая поддержка</li>
				<li>Опытные специалисты</li>
				<li>Отлаженные бизнес-процессы</li>
				<li>Современные стандарты разработки</li>
				<li>Автоматизация ведения проектов</li>
				<li>Постоянный контроль качества</li>
				<li>Бесплатные консультации</li>
				<li>Гарантия на всё время работы проекта</li>
			</ul>
			<h4>
				Страны присутствия
			</h4>
			<div class="about-countries margin-left-m5 margin-right-m5">
				<div class="col-xs-3">
					<div class="about-countries-img">
						<img src="assets/img/about/flag1.png">
					</div>
					<div class="about-countries-text">Россия</div>
				</div>
				<div class="col-xs-3">
					<div class="about-countries-img">
						<img src="assets/img/about/flag2.png">
					</div>
					<div class="about-countries-text">ЕС</div>
				</div>
				<div class="col-xs-3">
					<div class="about-countries-img">
						<img src="assets/img/about/flag3.png">
					</div>
					<div class="about-countries-text">США</div>
				</div>
				<div class="col-xs-3">
					<div class="about-countries-img">
						<img src="assets/img/about/flag4.png">
					</div>
					<div class="about-countries-text">Канада</div>
				</div>
			</div>
			<p class="text-center text-left-md">
				<img src="assets/img/about/cebra.png">
			</p>
			<p>
				Webis Group - член Канадской деловой ассоциации в России и Евразии (CERBA)
			</p>
			<h4>Наши ценности</h4>
			<p>
			В нашей работе мы придерживаемся одной основной мотивации: результат работы должен быть эффективным. Перед началом работы мы всегда анализируем целевую аудиторию, моделируем её поведение и ожидания. Эту информацию мы используем при планировании работы - будь то создание сайта, реклама в интернете, разработка фирменного стиля, мультимедиа-презентаций или другие виды работ.
			</p>
			<p>
			Результат нашей работы – высококачественный продукт, помогающий решать поставленные задачи. Высококачественный дизайн привлекает внимание целевой аудитории. Мощные и гибкие программные решения собственной разработки позволяют гарантировать надёжность и стабильность работы и предоставляют богатые возможности для расширения функциональности интернет-проекта.
			</p>
		</div>
		<div class="about-projects col-md-4">
			<h4>Свыше 300 проектов</h4>
			<ul>
				<li>Производство</li>
				<li>Телекоммуникации</li>
				<li>IT</li>
				<li>Финансы</li>
				<li>Строительство</li>
				<li>Медицина</li>
				<li>Нефтегазовый сектор</li>
				<li>Логистика</li>
				<li>Добыча полезных ископаемых</li>
				<li>Индустрия красоты</li>
				<li>Торговля</li>
				<li>Недвижимость</li>
			</ul>
			<h4>Более 1000 рекламных площадок</h4>
			<div class="about-platforms">
				<div class="about-platforms-item">
					<img src="assets/img/about/platform1.png">
				</div>
				<div class="about-platforms-item">
					<img src="assets/img/about/platform2.png">
				</div>
				<div class="about-platforms-item">
					<img src="assets/img/about/platform3.png">
				</div>
				<div class="about-platforms-item">
					<img src="assets/img/about/platform4.png">
				</div>
				<div class="about-platforms-item">
					<img src="assets/img/about/platform5.png">
				</div>
				<div class="about-platforms-item">
					<img src="assets/img/about/platform6.png">
				</div>
				<div class="about-platforms-item">
					<img src="assets/img/about/platform7.png">
				</div>
				<div class="about-platforms-item">
					<img src="assets/img/about/platform8.png">
				</div>
			</div>
			<div class="text-center font-bold gray f-20">
				... и многое другое
			</div>
		</div>
		<div class="about-history col-md-3 text-center">
			<h2 class="visible-sm visible-xs">Наша история</h2>
			<img src="assets/img/about/timeline.png">
		</div>
	</div>
</div>

<div class="employee container">
	<h2>
		Наши сотрудники
	</h2>
	<div class="employee-items row">
		<div class="employee-item col-md-4 col-sm-6">
			<div class="employee-content contactable" style="background-image: url(assets/img/about/employee1.jpg)">
				<div class="employee-info">
					<div class="employee-info-name">Фамилия Имя</div>
					<div class="employee-info-position">Должность</div>
					<div class="employee-info-contact">
						<button class="button button-red-transparent">Написать письмо</button>
					</div>
				</div>
			</div>
		</div>
		<div class="employee-item col-md-4 col-sm-6">
			<div class="employee-content contactable" style="background-image: url(assets/img/about/employee2.jpg)">
				<div class="employee-info">
					<div class="employee-info-name">Фамилия Имя</div>
					<div class="employee-info-position">Должность</div>
					<div class="employee-info-contact">
						<button class="button button-red-transparent">Написать письмо</button>
					</div>
				</div>
			</div>
		</div>
		<div class="employee-item col-md-4 col-sm-6">
			<div class="employee-content contactable" style="background-image: url(assets/img/about/employee3.jpg)">
				<div class="employee-info">
					<div class="employee-info-name">Фамилия Имя</div>
					<div class="employee-info-position">Должность</div>
					<div class="employee-info-contact">
						<button class="button button-red-transparent">Написать письмо</button>
					</div>
				</div>
			</div>
		</div>
		<div class="employee-item col-md-4 col-sm-6">
			<div class="employee-content contactable" style="background-image: url(assets/img/about/employee1.jpg)">
				<div class="employee-info">
					<div class="employee-info-name">Фамилия Имя</div>
					<div class="employee-info-position">Должность</div>
					<div class="employee-info-contact">
						<button class="button button-red-transparent">Написать письмо</button>
					</div>
				</div>
			</div>
		</div>
		<div class="employee-item col-md-4 col-sm-6">
			<div class="employee-content contactable" style="background-image: url(assets/img/about/employee2.jpg)">
				<div class="employee-info">
					<div class="employee-info-name">Фамилия Имя</div>
					<div class="employee-info-position">Должность</div>
					<div class="employee-info-contact">
						<button class="button button-red-transparent">Написать письмо</button>
					</div>
				</div>
			</div>
		</div>
		<div class="employee-item col-md-4 col-sm-6">
			<div class="employee-content contactable" style="background-image: url(assets/img/about/employee3.jpg)">
				<div class="employee-info">
					<div class="employee-info-name">Фамилия Имя</div>
					<div class="employee-info-position">Должность</div>
					<div class="employee-info-contact">
						<button class="button button-red-transparent">Написать письмо</button>
					</div>
				</div>
			</div>
		</div>
		<div class="employee-item col-md-4 col-sm-6">
			<div class="employee-content contactable" style="background-image: url(assets/img/about/employee1.jpg)">
				<div class="employee-info">
					<div class="employee-info-name">Фамилия Имя</div>
					<div class="employee-info-position">Должность</div>
					<div class="employee-info-contact">
						<button class="button button-red-transparent">Написать письмо</button>
					</div>
				</div>
			</div>
		</div>
		<div class="employee-item col-md-4 col-sm-6">
			<div class="employee-content contactable" style="background-image: url(assets/img/about/employee2.jpg)">
				<div class="employee-info">
					<div class="employee-info-name">Фамилия Имя</div>
					<div class="employee-info-position">Должность</div>
					<div class="employee-info-contact">
						<button class="button button-red-transparent">Написать письмо</button>
					</div>
				</div>
			</div>
		</div>
		<div class="employee-item col-md-4 col-sm-6">
			<div class="employee-content contactable" style="background-image: url(assets/img/about/employee3.jpg)">
				<div class="employee-info">
					<div class="employee-info-name">Фамилия Имя</div>
					<div class="employee-info-position">Должность</div>
					<div class="employee-info-contact">
						<button class="button button-red-transparent">Написать письмо</button>
					</div>
				</div>
			</div>
		</div>
		<div class="employee-item col-md-4 col-sm-6">
			<div class="employee-content contactable" style="background-image: url(assets/img/about/employee1.jpg)">
				<div class="employee-info">
					<div class="employee-info-name">Фамилия Имя</div>
					<div class="employee-info-position">Должность</div>
					<div class="employee-info-contact">
						<button class="button button-red-transparent">Написать письмо</button>
					</div>
				</div>
			</div>
		</div>
		<div class="employee-item col-md-4 col-sm-6">
			<div class="employee-content contactable" style="background-image: url(assets/img/about/employee2.jpg)">
				<div class="employee-info">
					<div class="employee-info-name">Фамилия Имя</div>
					<div class="employee-info-position">Должность</div>
					<div class="employee-info-contact">
						<button class="button button-red-transparent">Написать письмо</button>
					</div>
				</div>
			</div>
		</div>
		<div class="employee-item col-md-4 col-sm-6">
			<div class="employee-content contactable" style="background-image: url(assets/img/about/employee3.jpg)">
				<div class="employee-info">
					<div class="employee-info-name">Фамилия Имя</div>
					<div class="employee-info-position">Должность</div>
					<div class="employee-info-contact">
						<button class="button button-red-transparent">Написать письмо</button>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>


<div class="dev container">
	<h2>
		Наши разработки
	</h2>
	<div class="row">
		<div class="dev-text text-default col-md-8 col-md-offset-2">
			<p>
			Мы разработали собственную систему управления сайтом - <a href="#">Abante CMS</a> для того, 
			чтобы обеспечить максимальный уровень функциональности, стабильности, 
			удобства в эксплуатации и поддержки интернет-проектов наших клиентов. 
			На основе системы Abante мы разработали удобные и мощные пакетные решения 
			для создания <a href="#">интернет-магазинов:</a> Basic и Advanced.
			</p>
			<p>
			Собственные программные решения Webis Group эффективно используются 
			в бизнес-процессах как наших клиентов, так и нашей компании:
			</p>
		</div>
		<div class="statements statements-xs">
			<div class="statements-item col-md-3 col-sm-6">
				<div class="statements-item-img">
					<img src="assets/img/about/ico1.png" class="img-responsive"></img>
				</div>
				<div class="statements-item-text">
					<b>&laquo;WebisOnline&raquo;</b> &ndash; простая в установке и обслуживании система онлайн-консультаций;
				</div>
			</div>
			<div class="statements-item col-md-3 col-sm-6">
				<div class="statements-item-img">
					<img src="assets/img/about/ico2.png" class="img-responsive"></img>
				</div>
				<div class="statements-item-text">
					<b>WeBiz</b> &ndash; система мониторинга позиций сайтов в поисковых системах и управления контекстной рекламой
				</div>
			</div>
			<div class="statements-item col-md-3 col-sm-6">
				<div class="statements-item-img">
					<img src="assets/img/about/ico3.png" class="img-responsive"></img>
				</div>
				<div class="statements-item-text">
					<b>MyCasher</b> &ndash; удобная и мощная система для учёта, планирования и анализа личных и корпоративных финансов;
				</div>
			</div>
			<div class="statements-item col-md-3 col-sm-6">
				<div class="statements-item-img">
					<img src="assets/img/about/ico4.png" class="img-responsive"></img>
				</div>
				<div class="statements-item-text">
					<b>Planner</b> &ndash; онлайн-система для распределённого управления проектами.
				</div>
			</div>
		</div>
		<div class="statements-xs">
			<div class="statements-item col-md-3 col-sm-6">
				<div class="statements-item-img">
					<img src="assets/img/about/ico5.png" class="img-responsive"></img>
				</div>
				<div class="statements-item-text">
					<b>Q-rating</b> &ndash; система обратной связи для клиентов электронных очередей
				</div>
			</div>
			<div class="statements-item col-md-3 col-sm-6">
				<div class="statements-item-img">
					<img src="assets/img/about/ico6.png" class="img-responsive"></img>
				</div>
				<div class="statements-item-text">
					<b>Экоокна</b> &ndash; сеть автономных сенсорных торговых терминалов
				</div>
			</div>
			<div class="statements-item col-md-3 col-sm-6">
				<div class="statements-item-img">
					<img src="assets/img/about/ico7.png" class="img-responsive"></img>
				</div>
				<div class="statements-item-text">
					<b>Abante CMS</b> &ndash; универсальная, мощная и надёжная система управления сайтом
				</div>
			</div>
			<div class="statements-item col-md-3 col-sm-6">
				<div class="statements-item-img">
					<img src="assets/img/about/ico8.png" class="img-responsive"></img>
				</div>
				<div class="statements-item-text">
					<b>HomeSegreto</b> &ndash; виртуальные мебельные салоны на основе сенсорных терминалов
				</div>
			</div>
		</div>
	</div>
</div>

<div class="container advantages">
	<h2>Наши преимущества</h2>
	<div class="row">
		<div class="advantages-text text-default col-md-8 col-md-offset-2">
			<p>
			Наше основное преимущество — взаимопонимание на всех этапах работы. 
			Мы сделаем всё, что нужно, для того, чтобы у Вас был превосходный результат, 
			— а Вы в нём найдёте множество интересных задумок и решений, которые просто 
			невозможно описать в нескольких словах. 
			</p>
		</div>
		<div class="advantages-items">
			<div class="advantages-item col-md-3 col-sm-6">
				<div class="advantages-item-img">
					<img src="assets/img/about/advantages1.png" class="img-responsive">
				</div>
				<div class="advantages-item-text">
					Тщательное планирование перед началом работы &ndash; для того, чтобы избежать ненужных расходов и реализовать все необходимые возможности
				</div>
			</div>
			<div class="advantages-item col-md-3 col-sm-6">
				<div class="advantages-item-img">
					<img src="assets/img/about/advantages2.png" class="img-responsive">
				</div>
				<div class="advantages-item-text">
					Мощная система управления сайтом &ndash; Abante CMS, программный комплекс собственной разработки
				</div>
			</div>
			<div class="advantages-item col-md-3 col-sm-6">
				<div class="advantages-item-img">
					<img src="assets/img/about/advantages3.png" class="img-responsive">
				</div>
				<div class="advantages-item-text">
					Точное нацеливание на целевую аудиторию &ndash; для достижения максимальной эффективности от результата
				</div>
			</div>
			<div class="advantages-item col-md-3 col-sm-6">
				<div class="advantages-item-img">
					<img src="assets/img/about/advantages4.png" class="img-responsive">
				</div>
				<div class="advantages-item-text">
					Гарантийная поддержка на весь срок службы сайта.
				</div>
			</div>
		</div>
	</div>
</div>


<div class="space-30"></div>

@stop