@extends('site.layout', ['Title' => 'Разработка программных комплексов'])

@section('head-styles')
<link rel="stylesheet" href="assets/css/pc.css">
@stop

@section('content')
<div class="header">
	<div class="header-inner container">
		<div class="row">
			<div class="col-md-11 col-md-offset-1">
				<h1>
					Разработка программных<span class="hidden-sm"><br></span> комплексов
				</h1>
				<div class="header-thesis">
					Создаем нестандартные решения<br> в интернете и за его пределами
				</div>
			</div>
		</div>
	</div>
</div>

<div class="projects container margin-top-sm-m120 margin-top-m90">
	<h2 class="text-center">Реализованные проекты</h2>
	<div class="row">
		<div class="col-md-4 col-sm-12 col-md-offset-1 projects-img">
			<img src="assets/img/pc/projects1.png" class="img-responsive m-auto" alt="">
		</div>
		<div class="col-md-6">
			<h3 class="projects-header">Web<span class="green">is</span> online</h3>
			<div class="projects-text">
				<p>Онлайн-консультант для Вашего сайта. WebisOnline увеличивает конверсию на вашем сайте и среднюю стоимость покупки от 30 до 200%.</p>
				<ul class="list list-default">
					<li>Общайтесь с вашими клиентами с помощью чата поддержки или принимайте звонки click-to-call с сайта</li>
					<li>Работает на обычных компьютерах и мобильных устройствах</li>
					<li>Наиболее рациональный и эффективный способ общения с клиентами на сайте.</li>
					<li>Интеллектуальная система вовлечения в диалог позволяет предложить общение посетителю сайта в самый подходящий момент и на интересующую его тему.</li>
				</ul>
			</div>
			<div class="projects-button">
				<button class="button button-green button-collapse" data-action="show" data-target="#projects-content1">
					<span class="button-collapse-ico-down"></span>
				    <span class="button-collapse-text"><span class="button-collapse-text-decoration">Узнать больше</span></span>
				</button>
				<button class="button button-green button-collapse display-none" data-action="hide" data-target="#projects-content1">
					<span class="button-collapse-ico-up"></span>
				    <span class="button-collapse-text"><span class="button-collapse-text-decoration">Свернуть</span></span>
				</button>
			</div>
		</div>
	</div>
</div>
<div id="projects-content1" class="projects-content shadow-top-inset">
	<a name="webisonline"></a>
	<div class="container">
		<div class="row">
			<div class="col-md-10 col-md-offset-1">
				<div class="text-default">
					<p><b>Основные возможности сервиса:</b></p>
					<ul>
						<li>Чат между посетителем сайта и менеджером в компании,</li>
						<li>Формирование базы потенциальных клиентов,</li>
						<li>Голосовые звонки click-to-call с сайта менеджеру в компанию,</li>
						<li>Автоприглашения целевых посетителей к диалогу,</li>
						<li>Рейтинги: оценка работы консультанта посетителем,</li>
						<li>Статистика: история диалогов и звонков, количество обращений, время отклика и другие параметры,</li>
						<li>Настройка дизайна: выбор оформления и цвета для чата посетителя и автоприглашений,</li>
						<li>Виджет «Перезвоните мне»</li>
					</ul>
					<p>и многие другие возможности, набор которых постоянно расширяется.</p>
					<p>Узнайте больше на <a href="http://webisonline.ru" target="_blank">webisonline.ru</a></p>
				</div>
			</div>
		</div>
	</div>
	<div class="oh text-center shadow-bottom-inset container-fluid">
		<button class="button button-red-transparent button-collapse m-auto margin-bottom-m5" data-action="hide" data-target="#projects-content1">
			<span class="button-collapse-ico-up"></span>
			<span class="button-collapse-text"><span class="button-collapse-text-decoration">Свернуть</span></span>
		</button>
	</div>
</div>

<div class="projects container">
	<div class="row row-right-sm">
		<div class="col-md-4 col-sm-12 projects-img">
			<img src="assets/img/pc/projects2.png" class="img-responsive" alt="">
		</div>
		<div class="col-md-6">
			<h3 class="projects-header violet-dark">Q-rating</h3>
			<div class="projects-text">
				<p>
					Система обратной связи для электронной очереди Q-Matic. Позволяет организациям, использующим электронную очередь, задавать обслуживаемым клиентам вопросы при помощи планшета, установленного у рабочего места сотрудника, и получать от них обратную связь по оказываемым им услугам.
				</p>
				<p>
					Все результаты опросов привязываются к конкретному посетителю и позволяют собирать подробную статистику об удовлетворённости клиентов при оказании конкретных услуг и при работе с конкретными специалистами.
				</p>
			</div>
			<div class="projects-button">
				<button class="button button-violet-dark button-collapse" data-action="show" data-target="#projects-content2">
					<span class="button-collapse-ico-down"></span>
				    <span class="button-collapse-text"><span class="button-collapse-text-decoration">Узнать больше</span></span>
				</button>
				<button class="button button-violet-dark button-collapse display-none" data-action="hide" data-target="#projects-content2">
					<span class="button-collapse-ico-up"></span>
				    <span class="button-collapse-text"><span class="button-collapse-text-decoration">Свернуть</span></span>
				</button>
			</div>
		</div>
	</div>
</div>
<div id="projects-content2" class="projects-content shadow-top-inset">
	<a name="qrating"></a>
	<div class="container">
		<div class="row">
			<div class="col-md-10 col-md-offset-1">
				<div class="text-default">
					<p><b>Основные возможности системы Q-Rating:</b></p>
					<ul>
						<li>Централизованное управление сценариями (наборами вопросов, задаваемых посетителю),</li>
						<li>Контекстно-зависимые сценарии: возможность настроить показ разных вопросов при оказании разных услуг, в разных отделениях и в разное время,</li>
						<li>Ветвящиеся сценарии: следующие вопросы зависят от ответов на предыдущие,</li>
						<li>Управление показом рекламы в режиме ожидания посетителя,</li>
						<li>Оперативная отправка результатов на центральный сервер,</li>
						<li>Лёгкое брендирование: загрузка своего логотипа и фирменных цветов или полностью своего оформления для опросника,</li>
						<li>Мониторинг системы в реальном времени: статистика доступности системы в отделениях и на конкретных устройствах, возможности для глубокой диагностики техническими специалистами,</li>
						<li>Поддержка различных редакций системы электронной очереди Q-Matic и различной серверной инфраструктуры,</li>
						<li>Централизованное обновление системы на всех клиентских устройствах во всех отделениях</li>
					</ul>
				</div>
			</div>
		</div>
	</div>
	<div class="oh text-center shadow-bottom-inset container-fluid">
		<button class="button button-red-transparent button-collapse m-auto margin-bottom-m5" data-action="hide" data-target="#projects-content2">
			<span class="button-collapse-ico-up"></span>
			<span class="button-collapse-text"><span class="button-collapse-text-decoration">Свернуть</span></span>
		</button>
	</div>
</div>

<div class="projects container">
	<div class="row">
		<div class="col-md-4 col-sm-12 col-md-offset-1 projects-img">
			<img src="assets/img/pc/projects3.png" class="img-responsive m-auto" alt="">
		</div>
		<div class="col-md-6">
			<h3 class="projects-header orange">Системы автоматизации для бизнеса</h3>
			<div class="projects-text">
				<p>Мы разрабатываем, внедряем и обслуживаем программно-аппаратные комплексы для автоматизации бизнеса, связанного с обслуживанием клиентов:
				системы контроля доступа, охранные системы, электронные очереди и другие направления</p>
				<p>Наши клиенты - фитнес-центры, бассейны и бани, лечебные учреждения, отели и санатории, рестораны и кинотеатры.</p>
				
			</div>
			<div class="projects-button">
				<button class="button button-orange button-collapse" data-action="show" data-target="#projects-content3">
					<span class="button-collapse-ico-down"></span>
				    <span class="button-collapse-text"><span class="button-collapse-text-decoration">Узнать больше</span></span>
				</button>
				<button class="button button-orange button-collapse display-none" data-action="hide" data-target="#projects-content3">
					<span class="button-collapse-ico-up"></span>
				    <span class="button-collapse-text"><span class="button-collapse-text-decoration">Свернуть</span></span>
				</button>
			</div>
		</div>
	</div>
</div>
<div id="projects-content3" class="projects-content shadow-top-inset">
	<a name="b2b"></a>
	<div class="container">
		<div class="row">
			<div class="col-md-10 col-md-offset-1">
				<div class="text-default">
					<p><b>SmartForge</b>, подразделение Webis Group, занимается решениями для автоматизации:</p>
					<ul>
						<li>Бассейнов, бань, аквапарков, фитнес-центров,</li>
						<li>Отелей и домов отдыха,</li>
						<li>Ресторанов, клубов, кафе,</li>
						<li>Кинотеатров,</li>
						<li>Столовых,</li>
						<li>Платных стоянок,</li>
						<li>Школ и ВУЗов,</li>
						<li>Медицинских учреждений.</li>
					</ul>
					<p>
						Мы поставляем решения под ключ &ndash; от проектирования системы, разработки или внедрения ПО, интеграции с выбранным оборудованием до послепродажного обслуживания:
					</p>
					<ul>
						<li>Системы контроля и разграничения доступа</li>
						<li>Системы учёта рабочего времени</li>
						<li>Системы контроля обхода территории</li>
						<li>Системы контроля доступа к сейфам и депозитным ячейкам</li>
						<li>Организация платного доступа</li>
						<li>Электронные очереди</li>
					</ul>
					<p><b>MedLink</b> - система для комплексной автоматизации медицинских учреждений. Позволяет решать следующие основные задачи:</p>
					<ul>
						<li>Упорядочивание деятельности всех специалистов: врачей, лаборантов, медсестёр,</li>
						<li>Оперативный контроль назначаемого лечения,</li>
						<li>Помощь в постановке диагноза,</li>
						<li>Учёт приёма любых материалов и выдачи медикаментов,</li>
						<li>Планирование лечебных процедур,</li>
						<li>Учёт загруженности специалистов и кабинетов,</li>
						<li>Автоматизация подготовки отчётов.</li>
					</ul>
					<p>Узнайте больше на <a href="http://www.smartforge.ru" target="_blank">smartforge.ru</a> и на <a href="http://medlink.su" target="_blank">medlink.su</a></p>
				</div>
			</div>
		</div>
	</div>
	<div class="oh text-center shadow-bottom-inset container-fluid">
		<button class="button button-red-transparent button-collapse m-auto margin-bottom-m5" data-action="hide" data-target="#projects-content3">
			<span class="button-collapse-ico-up"></span>
			<span class="button-collapse-text"><span class="button-collapse-text-decoration">Свернуть</span></span>
		</button>
	</div>
</div>



<div class="projects container">
	<div class="row row-right-sm">
		<div class="col-md-4 col-sm-12 projects-img">
			<img src="assets/img/pc/projects4.png" class="img-responsive" alt="">
		</div>
		<div class="col-md-6">
			<h3 class="projects-header gray">Abante CMS</h3>
			<div class="projects-text">
				<p>Мощная и удобная, система управления сайтом Abante CMS развивается с 2000 года. Её основные преимущества - простота в использовании, быстродействие и гибкость в настройке.</p>
				<p>
					Система работает на множестве сайтов в России и за её пределами: интернет-магазины, новостные порталы, промо-сайты...
				</p>
			</div>
			<div class="projects-button">
				<button class="button button-gray button-collapse" data-action="show" data-target="#projects-content4" >
					<span class="button-collapse-ico-down"></span>
				    <span class="button-collapse-text"><span class="button-collapse-text-decoration">Узнать больше</span></span>
				</button>
				<button class="button button-gray button-collapse display-none" data-action="hide" data-target="#projects-content4">
					<span class="button-collapse-ico-up"></span>
				    <span class="button-collapse-text"><span class="button-collapse-text-decoration">Свернуть</span></span>
				</button>
			</div>
		</div>
	</div>
</div>
<div id="projects-content4" class="projects-content shadow-top-inset">
	<a name="abante"></a>
	<div class="container">
		<div class="row">
			<div class="col-md-10 col-md-offset-1">
				<div class="text-default">
					<p>Abante CMS - наша собственная разработка, профессиональная система управления для вашего сайта. Её основные преимущества:</p>
					<ul>
						<li>Простота в использовании: не требуется специальных знаний для того, чтобы быстро начать работу с сайтом,</li>
						<li>Множество удобных инструментов для эффективной ежедневной работы,</li>
						<li>Гибкость в настройке и лёгкость масштабирования: и интерфейс администратора, и сам сайт могут неограниченно дополняться новыми функциями,</li>
						<li>Лёгкая интеграция с внешними сервисами: платёжные системы, ERP, CRM-системы и другие,</li>
						<li>Простота технической поддержки: система имеет открытый код и может самостоятельно поддерживаться практически любым специалистом,</li>
						<li>Низкие требования к ресурсам хостинга: занимает мало места и не перегружает память и процессор,</li>
						<li>Высокая защищённость к перегрузкам и попыткам взлома.</li>
					</ul>
					<p>
						<a href="#">Узнайте больше о системе Abante CMS</a>
					</p>
				</div>
			</div>
		</div>
	</div>
	<div class="oh text-center shadow-bottom-inset container-fluid">
		<button class="button button-red-transparent button-collapse m-auto margin-bottom-m5" data-action="hide" data-target="#projects-content4">
			<span class="button-collapse-ico-up"></span>
			<span class="button-collapse-text"><span class="button-collapse-text-decoration">Свернуть</span></span>
		</button>
	</div>
</div>

<div class="projects container">
	<div class="row">
		<div class="col-md-4 col-sm-12 col-md-offset-1 projects-img">
			<img src="assets/img/pc/projects5.jpg" class="img-responsive m-auto" alt="">
		</div>
		<div class="col-md-6 margin-top-80">
			<h3 class="projects-header green-dark">Экоокна</h3>
			<div class="projects-text">
				<p>
					Сеть торгово-информационных терминалов для известного производителя окон.
				</p>
				<p>
					Назначение системы - упростить сбор лидов из профильных розничных магазинов и дать возможность оформить заказ прямо на терминале.
				</p>
			</div>
			<div class="projects-button">
				<button class="button button-green-sea button-collapse" data-action="show" data-target="#projects-content5">
					<span class="button-collapse-ico-down"></span>
				    <span class="button-collapse-text"><span class="button-collapse-text-decoration">Узнать больше</span></span>
				</button>
				<button class="button button-green-sea button-collapse display-none" data-action="hide" data-target="#projects-content5">
					<span class="button-collapse-ico-up"></span>
				    <span class="button-collapse-text"><span class="button-collapse-text-decoration">Свернуть</span></span>
				</button>
			</div>
		</div>
	</div>
</div>
<div id="projects-content5" class="projects-content shadow-top-inset">
	<a name="ecookna"></a>
	<div class="container">
		<div class="row">
			<div class="col-md-10 col-md-offset-1">
				<div class="text-default">
					<p>
						Сеть торгово-информационных терминалов для компании &laquo;Экоокна&raquo; предназначена для решения следующих задач:
					</p>
					<ul>
						<li>Повышение узнаваемости бренда,</li>
						<li>Сбор входящих контактов от посетителей профильных розничных магазинов,</li>
						<li>Ознакомление посетителей с ассортиментом компании,</li>
						<li>Предоставление посетителям возможности оценить ориентировочную стоимость окна и вызвать замерщика,</li>
						<li>Осуществлять прямые продажи через терминал с оплатой на кассе магазина,</li>
					</ul>
					<p>Основные возможности системы:</p>
					<ul>
						<li>Удобный и понятный интерфейс, адаптируемый под разные размеры экрана на информационных киосках,</li>
						<li>Конфигуратор окна, позволяющий выбрать желаемый тип окна, размеры, фурнитуру и другие параметры, посчитать ориентировочную стоимость и сразу отправить заявку в компанию,</li>
						<li>Работа без необходимости подключения к интернету,</li>
						<li>Печать документов для оплаты прямо на терминале,</li>
						<li>Интеграция с внутренней ERP-системой для централизованного управления каталогом продукции и настройками конфигуратора окон,</li>
						<li>Централизованное управление всей сетью терминалов: мониторинг доступности, обновление ПО,</li>
						<li>Управление рекламными модулями,</li>
						<li>Надёжный обмен информацией с центральным сервером с защитой от сбоев при передаче,</li>
						<li>Тонкая настройка: возможность управления рекламой, каталогом и конфигуратором на конкретном киоске или группе киосков,</li>
						<li>Интерфейс кассира: возможность отмечать оплаченные заказы при помощи встроенного сканера штрих-кодов.</li>
					</ul>
				</div>
			</div>
		</div>
	</div>
	<div class="oh text-center shadow-bottom-inset container-fluid">
		<button class="button button-red-transparent button-collapse m-auto margin-bottom-m5" data-action="hide" data-target="#projects-content5">
			<span class="button-collapse-ico-up"></span>
			<span class="button-collapse-text"><span class="button-collapse-text-decoration">Свернуть</span></span>
		</button>
	</div>
</div>

<div class="projects container">
	<div class="row row-right-sm">
		<div class="col-md-4 col-sm-12 projects-img">
			<img src="assets/img/pc/projects6.png" class="img-responsive" alt="">
		</div>
		<div class="col-md-6">
			<h3 class="projects-header"><span class="blue">Home</span>segreto</h3>
			<div class="projects-text">
				<p>
					Программный комплекс &laquo;Виртуальный мебельный салон&raquo; устанавливается на широкоформатных сенсорных терминалах в магазинах широкой дилерской сети компании HomeSegreto.
				</p>
				<p>
					Предназначение комплекса &ndash; позволить покупателям даже в небольших салонах с комфортом ознакомиться с полным ассортиментом продукции компании.
				</p>
			</div>
			<div class="projects-button">
				<button class="button button-blue-dark button-collapse" data-action="show" data-target="#projects-content6">
					<span class="button-collapse-ico-down"></span>
				    <span class="button-collapse-text"><span class="button-collapse-text-decoration">Узнать больше</span></span>
				</button>
				<button class="button button-blue-dark button-collapse display-none" data-action="hide" data-target="#projects-content6">
					<span class="button-collapse-ico-up"></span>
				    <span class="button-collapse-text"><span class="button-collapse-text-decoration">Свернуть</span></span>
				</button>
			</div>
		</div>
	</div>
</div>
<div id="projects-content6" class="projects-content shadow-top-inset">
	<a name="homesegreto"></a>
	<div class="container">
		<div class="row">
			<div class="col-md-10 col-md-offset-1">
				<div class="text-default">
					<p>
						Высококачественные фотографии изысканной мебели, которые можно листать руками на широкоформатном экране, производят сногсшибательный эффект. Простой и удобный интерфейс позволяет любому посетителю мебельного салона с лёгкостью подобрать себе мебель по вкусу.</p>
					</p>
					<p>Основные возможности системы:</p>
					<ul>
						<li>Простой и понятный интерфейс, оптимизированный для самых широких экранов,</li>
						<li>Централизованное управление всей сетью терминалов с мониторингом доступности каждого устройства,</li>
						<li>Возможность указывать варианты исполнения (отделка, обивка) для каждого предмета в каталоге,</li>
						<li>Возможность загружать любое количество фотографий по каждому товару,</li>
						<li>Удобная корзина заказа,</li>
						<li>Моментальное оформление заказа с отправкой в головной офис и распечаткой бланка,</li>
						<li>Работа без необходимости подключения к интернету,</li>
						<li>Помехоустойчивый обмен данными с центральным сервером,</li>
						<li>Централизованное обновление ПО на терминалах.</li>
					</ul>
				</div>
			</div>
		</div>
	</div>
	<div class="oh text-center shadow-bottom-inset container-fluid">
		<button class="button button-red-transparent button-collapse m-auto margin-bottom-m5" data-action="hide" data-target="#projects-content6">
			<span class="button-collapse-ico-up"></span>
			<span class="button-collapse-text"><span class="button-collapse-text-decoration">Свернуть</span></span>
		</button>
	</div>
</div>



@stop