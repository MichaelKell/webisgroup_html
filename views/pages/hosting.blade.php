@extends('site.layout', ['Title' => 'Хостинг'])

@section('head-styles')
  <link rel="stylesheet" href="assets/css/hosting.css">
@stop

@section('content')
<div class="header">
	<div class="header-inner container">
		<div class="row">
			<div class="col-md-11 col-md-offset-1">
				<h1>
					Надежный дом<br>для вашего сайта
				</h1>
				<div class="header-thesis">
					Оптимизированные и защищенные серверы<span class="hidden-xs"><br></span> ежедневное резервное копирование
				</div>
			</div>
		</div>
	</div>
</div>

<div class="about container margin-top-lg-m120 margin-top-md-m100 margin-top-sm-m90 margin-top-m40">
	<h2>Что нужно знать о хостинге?</h2>
	<p class="text-center">
		Хостинг — это услуга по предоставлению вычислительных мощностей на оборудовании <span class="hidden-xs"><br></span>
		некоторой компании с постоянным доступом к сети Интернет. <span class="hidden-xs"><br></span>
		Проще говоря, услуга хостинга обеспечивает постоянную доступность вашего сайта.
	</p>
	<p class="font-regular text-center">
		Каким критериям должен отвечать хороший хостинг?
	</p>
</div>

<div class="circles container">
	<div class="row">
		<div class="circles-item col-md-4 col-sm-4 col-xs-6">
			<div class="circles-header"><div class="center">Объем хранимой<br>информации</div></div>
			<div class="circles-img"><img class="img-responsive" src="assets/img/hosting/circle1.png"></div>
		</div>
		<div class="circles-item col-md-4 col-sm-4 col-xs-6">
			<div class="circles-header"><div class="center">Безопасность<br>информации</div></div>
			<div class="circles-img"><img class="img-responsive" src="assets/img/hosting/circle2.png"></div>
		</div>
		<div class="circles-item col-md-4 col-sm-4 col-xs-6">
			<div class="circles-header"><div class="center">Устойчивость<br>к нагрузкам</div></div>
			<div class="circles-img"><img class="img-responsive" src="assets/img/hosting/circle3.png"></div>
		</div>
		<div class="circles-item col-md-4 col-sm-4 col-xs-6">
			<div class="circles-header"><div class="center">Поддержка доменов<br>третьего уровня</div></div>
			<div class="circles-img"><img class="img-responsive" src="assets/img/hosting/circle4.png"></div>
		</div>
		<div class="circles-item col-md-4 col-sm-4 col-xs-6">
			<div class="circles-header"><div class="center">Близость к целевой<br>аудитории</div></div>
			<div class="circles-img"><img class="img-responsive" src="assets/img/hosting/circle5.png"></div>
		</div>
		<div class="circles-item col-md-4 col-sm-4 col-xs-6">
			<div class="circles-header"><div class="center">Почтовые ящики<br>в вашем домене</div></div>
			<div class="circles-img"><img class="img-responsive" src="assets/img/hosting/circle6.png"></div>
		</div>
	</div>
</div>

<div class="container">
	<h2>Доверьте размещение своего сайта<span class="hidden-xs"><br></span> профессионалам</h2>
	<p class="text-center">
		Производительные серверы в высококлассных дата-центрах в Европе и в России с мощными каналами<span class="hidden-xs"><br></span>
		Достаточное количество всех ресурсов для вашего проекта<span class="hidden-xs"><br></span>
		Ежедневное резервное копирование в отдельное хранилище<span class="hidden-xs"><br></span>
		Установка, настройка и управление выделенными серверами<span class="hidden-xs"><br></span>
	</p>
	<div class="statements-xs margin-top-md-30">
		<div class="col-md-2 col-sm-2 statements-item">
			<div class="statements-item-img">
				<img class="img-responsive" src="assets/img/hosting/ico1.png" alt="">
			</div>
			<div class="statements-item-text">
				Поможем с общими вопросами
			</div>
		</div>
		<div class="col-md-2 col-sm-2 statements-item">
			<div class="statements-item-img">
				<img class="img-responsive" src="assets/img/hosting/ico2.png" alt="">
			</div>
			<div class="statements-item-text">
				 Оформим документы
			</div>
		</div>
		<div class="col-md-2 col-sm-2 statements-item">
			<div class="statements-item-img">
				<img class="img-responsive" src="assets/img/hosting/ico3.png" alt="">
			</div>
			<div class="statements-item-text">
				Перенесем сайт<br> от другого провайдера
			</div>
		</div>
		<div class="col-md-2 col-sm-2 statements-item">
			<div class="statements-item-img">
				<img class="img-responsive" src="assets/img/hosting/ico4.png" alt="">
			</div>
			<div class="statements-item-text">
				 Настроим хостинг
			</div>
		</div>
		<div class="col-md-2 col-sm-2 statements-item">
			<div class="statements-item-img">
				<img class="img-responsive" src="assets/img/hosting/ico5.png" alt="">
			</div>
			<div class="statements-item-text">
				Отладим почту
			</div>
		</div>
		<div class="col-md-2 col-sm-2 statements-item">
			<div class="statements-item-img">
				<img class="img-responsive" src="assets/img/hosting/ico6.png" alt="">
			</div>
			<div class="statements-item-text">
				Ответим на ваши вопросы
			</div>
		</div>
	</div>
	<div class="text-center margin-top-25">
		<button class="button button-red-transparent fb-form" data-fancybox-href="#form-popup-default">Узнать больше</button>
	</div>
</div>
<div class="space-50"></div>

@stop