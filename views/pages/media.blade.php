{{-- Медийная реклама --}}

@extends('site.layout')

@section('head-styles')
<link rel="stylesheet" href="assets/css/media.css">
@stop

@section('content')
<div class="header">
	<div class="header-inner container">
		<div class="row">
			<div class="col-md-11 col-md-offset-1">
				<h1>
					Медийная реклама<br>в интернете
				</h1>
			</div>
		</div>
	</div>
</div>

<div class="about margin-top-lg-m145 margin-top-md-m100 margin-top-sm-m30">
	<h2>
		Чем хороша медийная реклама?
	</h2>
	<div class="text-center">
		Целью использования медийной рекламы в сети является не только увеличение клиентской аудитории,<br>
		но и обеспечение ее качества. Посетители интернет-ресурса должны быть способны в полной мере<br>
		оценить предложение и обратиться в компанию.<br>
		Поэтому мы так настраиваем рекламу, чтобы она была доступна максимально широкой аудитории.<br>
		Для этого мы используем современные инструменты: поведенческий и тематический таргетинг.
	</div>
</div>

<div class="circles container">
	<div class="row">
		<div class="circles-item col-md-4 col-sm-4">
			<div class="circles-header"><div class="center">Повышение узнаваемости<br>продукта или бренда</div></div>
			<div class="circles-img"><img class="img-responsive" src="assets/img/media/circle1.png"></div>
		</div>
		<div class="circles-item col-md-4 col-sm-4">
			<div class="circles-header"><div class="center">Быстрое привлечение<br>аудитории на сайт</div></div>
			<div class="circles-img"><img class="img-responsive" src="assets/img/media/circle2.png"></div>
		</div>
		<div class="circles-item col-md-4 col-sm-4">
			<div class="circles-header"><div class="center">Богатые возможности<br>для таргетирования рекламы</div></div>
			<div class="circles-img"><img class="img-responsive" src="assets/img/media/circle3.png"></div>
		</div>
	</div>
</div>

<div class="text-center">
	<button class="button button-red-transparent fb-form" data-fancybox-href="#form-popup-default">Подготовить медиаплан</button>
</div>

<div class="approach container">
	<h2>
		Как мы работаем:
	</h2>
	<div class="row">
		<div class="col-md-4 col-sm-4 approach-item">
			<div class="approach-img">
				<img class="img-responsive" src="assets/img/media/ico1.png" alt="">
			</div>
			<div class="approach-header">
				Медиапланирование<span class="hidden-sm hidden-xs"><br>&nbsp;</span>
			</div>
			<div class="approach-text">		
				Мы подготовим предложения по позиционированию рекламы и стратегию по повышению спроса и узнаваемости.
				Подберём подходящие площадки для проведения рекламы, которые позволят наилучшим образом охватить и привлечь целевую аудиторию.
				Минимизируем стоимость контакта с компанией.
			</div>
		</div>
		<div class="col-md-4 col-sm-4 approach-item">
			<div class="approach-img">
				<img class="img-responsive" src="assets/img/media/ico2.png" alt="">
			</div>
			<div class="approach-header">
				Разработка рекламных баннеров
			</div>
			<div class="approach-text">		
				Сделать эффективный баннер не просто: для решения задач кампании нужна хорошая креативная идея и качественный дизайн,
				позволяющий точно, ярко и доходчиво донести до аудитории нужное сообщение, обеспечить запоминаемость бренда.
			</div>
		</div>
		<div class="col-md-4 col-sm-4 approach-item">
			<div class="approach-img">
				<img class="img-responsive" src="assets/img/media/ico3.png" alt="">
			</div>
			<div class="approach-header">
				Контроль проведения<br>кампании
			</div>
			<div class="approach-text">		
				Мы отслеживаем обратную связь от привлечённых посетителей и при необходимости можем незамедлительно вносить любые корректировки
				для повышения эффективности кампании: подстраивать таргетинг, менять баннер, вносить другие изменения.
				
			</div>
		</div>
	</div>
</div>

<div class="space-30"></div>

@stop