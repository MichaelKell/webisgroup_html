@extends('site.layout', ['Title' => 'Клиенты'])

@section('head-styles')
<link rel="stylesheet" href="assets/css/clients.css">
@stop

@section('footer')
<script type="text/javascript" src="assets/plugins/isotope.pkgd.min.js"></script>
<script type="text/javascript" src="assets/js/clients.js"></script>
@stop

@section('content')
<div class="header-default container-fluid">
	<div class="container">
		<div class="row">
			<div class="col-md-10 col-md-offset-1">
				<h1 class="h1-default">Наши клиенты</h1>
			</div>
		</div>
	</div>
</div>

<div class="about container">
	<div class="row">
		<div class="col-md-10 col-md-offset-1">
			<p>
				Наши клиенты ценят нас за креативный подход, инициативность и профессионализм во всём – от первых встреч и обсуждения намерений до поддержки и консультаций по эксплуатации готовых проектов.
			</p>
			<p>
				Мы высоко ценим и оправдываем доверие этих и многих других наших уважаемых клиентов:
			</p>
		</div>
	</div>
</div>

<div class="clients container">
	<div class="row">
		<div class="col-md-10 col-md-offset-1">
			<div id="clients" class="row clients-row">
				<div class="clients-item col-md-6">
					<div class="clients-img">
						<img class="img-responsive" src="assets/img/temp/client1.jpg" alt="">
					</div>
					<div class="clients-text">
						C момента основания компании «Экоокна» численность её сотрудников увеличилась с 8 (октябрь 2002 г.) до 700 (2015 г.) человек. Число офисов продаж выросло с 5 до 70. Выпуск основного вида продукции — пластиковых окон — возрос с 15 до 1000 изделий в сутки. Общий объём производственных площадей на данный момент составляет свыше 15000 кв. м.
					</div>
					<div class="clients-divide"></div>
					<div class="clients-about text-default">
						Создание ПО для интерактивных торговых терминалов
					</div>
				</div>
				<div class="clients-item col-md-6">
					<div class="clients-img">
						<img class="img-responsive" src="assets/img/temp/client2.jpg" alt="">
					</div>
					<div class="clients-text">
						Q-MATIC – шведский разработчик, производитель и поставщик электронных решений по управлению очередью, позволяющих сделать процесс обслуживания клиента более эффективным. Q-MATIC – мировой лидер в области электронных систем управления очередью, осуществивший более 40 000 внедрений по всему миру. Представители компании работают более чем в 110 странах мира.
					</div>
					<div class="clients-divide"></div>
					<div class="clients-about text-default">
						Создание и продвижение сайтов<br>Создание ПО для оценки качества обслуживания
					</div>
				</div>
				<div class="clients-item col-md-6">
					<div class="clients-img">
						<img class="img-responsive" src="assets/img/temp/client3_.jpg" alt="">
					</div>
					<div class="clients-text">
						Одна из крупнейших страховых и ассистанс-компаний в Европе, предлагает широкий спектр услуг по медицинскому, автострахованию и помощи на дорогах.
					</div>
					<div class="clients-divide"></div>
					<div class="clients-about text-default">
						Добыча данных (data mining)<br>Разработка системы мобильных приложений Digital RSA для автоматизации клиентского бизнеса
					</div>
				</div>
				<div class="clients-item col-md-6">
					<div class="clients-img">
						<img class="img-responsive" src="assets/img/temp/client3.jpg" alt="">
					</div>
					<div class="clients-text">
						Промышленный розлив минеральной воды Нарзан начат в июне 1894 года. В наше время «Нарзан» — это элитная торговая марка с богатой историей и традициями. ОАО «Нарзан» тщательно следит за всеми стадиями производства, сегодня открытое акционерное общество «Нарзан» — это высококлассное предприятие, оснащенное новейшим оборудованием, использующее сберегающие технологии, укомплектованное прекрасными специалистами, мастерами своего дела. В последние годы предприятие всё больше затрагивает вопросы рационального недропользования, и ставит перед собой задачу производства с минимальным ущербом для природы или вовсе исключив любое влияние.
					</div>
					<div class="clients-divide"></div>
					<div class="clients-about text-default">
						Создание промо-сайта
					</div>
				</div>
				<div class="clients-item col-md-6">
					<div class="clients-img">
						<img class="img-responsive" src="assets/img/temp/client4.jpg" alt="">
					</div>
					<div class="clients-text">
						Издательство «Дрофа» более 20 лет выпускает литературу для всех уровней российского образования: дошкольного, школьного, среднего профессионального и высшего. Ежегодный тираж книг составляет в среднем 40 млн экземпляров. 25% наименований в Федеральном перечне учебников Министерства образования и науки Российской Федерации составляют книги «Дрофы». 
						С издательством сотрудничают более шести тысяч авторов, среди которых широко известные ученые, педагоги, преподаватели, методисты и психологи.
					</div>
					<div class="clients-divide"></div>
					<div class="clients-about text-default">
						Создание и поддержка сайтов<br>Хостинг<br>Реклама в социальных сетях
					</div>
				</div>
				<div class="clients-item col-md-6">
					<div class="clients-img">
						<img class="img-responsive" src="assets/img/temp/client5.jpg" alt="">
					</div>
					<div class="clients-text">
						«ФОЛЬКСВАГЕН Груп Рус» объединяет на российском рынке работу шести марок концерна Volkswagen — Volkswagen, SKODA, Audi, Volkswagen Коммерческие автомобили, Bentley и Lamborghini. «ФОЛЬКСВАГЕН Груп Рус» также являлась генеральным партнером Зимних Олимпийских и Паралимпийских игр 2014 года в г. Сочи в категории «Автомобили». Компания предоставила Оргкомитету «Сочи-2014» более 3 000 автомобилей брендов Volkswagen, Audi, SKODA и Volkswagen Коммерческие автомобили для обеспечения перевозок гостей во время подготовки и проведения Игр 2014 года. Значительная часть автомобилей была произведена на заводах компании в России.
					</div>
					<div class="clients-divide"></div>
					<div class="clients-about text-default">
						Разработка системы повышения лояльности для посетителей дилерских центров на основе сенсорных терминалов
					</div>
				</div>
				<div class="clients-item col-md-6">
					<div class="clients-img">
						<img class="img-responsive" src="assets/img/temp/client8.jpg" alt="">
					</div>
					<div class="clients-text">
						Основанная в 1919 году, компания «Halliburton» является одной из ведущих мировых сервисных компаний, предоставляющих весь спектр современных технологий и услуг для нефтегазовой отрасли. Компания ведет деятельность в более чем 80 странах и насчитывает свыше 60 тысяч человек, работающих по всему миру. Подразделения компании предоставляют услуги и технологические решения на протяжении всего периода эксплуатации месторождения: начиная от определения местонахождения углеводородов, моделирования месторождения - до бурения, оценки параметров пласта, строительства, заканчивания скважин и оптимизации добычи.
					</div>
					<div class="clients-divide"></div>
					<div class="clients-about text-default">
						Создание и поддержка сайта
					</div>
				</div>
				<div class="clients-item col-md-6">
					<div class="clients-img">
						<img class="img-responsive" src="assets/img/temp/client9.jpg" alt="">
					</div>
					<div class="clients-text">
						Всероссийская Государственная Телевизионная и Радиовещательная Компания - это три общенациональных телеканала: "Россия", "Культура", "Спорт"; телеканал "Бибигон"; 89 региональных телерадиокомпаний; информационный канал "Вести"; телеканал "РТР-Планета", русская версия телеканала "Евроньюс"; три радиостанции - "Радио России", "Маяк", "Культура" и государственный интернет-канал "Россия", который объединяет более двадцати интернет-ресурсов.
					</div>
					<div class="clients-divide"></div>
					<div class="clients-about text-default">
						Создание сайта для подразделения<br>Разработка веб-версии программного комплекса "Вести.Тулбар".
					</div>
				</div>
				<div class="clients-item col-md-6">
					<div class="clients-img">
						<img class="img-responsive" src="assets/img/temp/client10.jpg" alt="">
					</div>
					<div class="clients-text">
						Сеть универсамов «Fix Price» предлагает покупателю широкий ассортимент необходимых в быту товаров для всей семьи по одинаковой низкой выгодной цене. Основное преимущество магазинов Fix Price – удобное расположение и минимальные цены, результат эффективной минимизации издержек и использования современных технологий для управления ресурсами и логистикой.
						Сеть постоянно развивается: на данный момент открыто более 380 действующих магазинов по всей России.
					</div>
					<div class="clients-divide"></div>
					<div class="clients-about text-default">
						Создание и поддержка сайта
					</div>
				</div>
				<div class="clients-item col-md-6">
					<div class="clients-img">
						<img class="img-responsive" src="assets/img/temp/client45.jpg" alt="">
					</div>
					<div class="clients-text">
						Компания Zorgtech — крупнейший производитель сенсорных информационных киосков. Компания Zorgtech внедряет инновационные интерактивные цифровые решения. Zorgtech является полноценным поставщиком услуг, сочетая бизнес-анализ, пользовательский интерфейс и промышленный дизайн, разработку программного обеспечения, изготовление, монтаж, постоянную поддержку и обслуживание. Это позволяет предоставлять экспертные консультации на всех этапах проекта, а главное, Zorgtech может взять на себя полную ответственность за качество решения, которое полностью отвечает вашим требованиям.
					</div>
					<div class="clients-divide"></div>
					<div class="clients-about text-default">
						Создание и поддержка сайта<br>Хостинг
					</div>
				</div>
				<div class="clients-item col-md-6">
					<div class="clients-img">
						<img class="img-responsive" src="assets/img/temp/client42.jpg" alt="">
					</div>
					<div class="clients-text">
						Особые экономические зоны технико-внедренческого типа создаются в целях увеличения доли присутствия России на мировых рынках высокотехнологичной продукции, отработки механизмов концентрации в современных условиях интеллектуальных и других ресурсов на определенной территории для решения приоритетных задач в научно-технической сфере. Особая экономическая зона в Дубне открывает большие возможности для развития инновационного бизнеса, производства наукоемкой продукции и вывода ее на российские и международные рынки.
					</div>
					<div class="clients-divide"></div>
					<div class="clients-about text-default">
						Создание сайтов<br>Поддержка сайтов<br>Хостинг
					</div>
				</div>
				<div class="clients-item col-md-6">
					<div class="clients-img">
						<img class="img-responsive" src="assets/img/temp/client15.jpg" alt="">
					</div>
					<div class="clients-text">
						Шведская компания Ecolean является разработчиком, производителем и поставщиком оборудования розлива и упаковки «кувшин» - уникальных решений для производителей молочной продукции и других напитков. Компания присутствует на российском рынке с 1998 г. Среди клиентов компании такие производители молока и молочной продукции как «Молвест», «Юнимилк», «Лактис», «Давлеканово» и многие другие. 
						Легкая упаковка Ecolean затмевает все более тяжелые виды упаковок. Вес пакета Ecolean примерно на 50-60% ниже, чем вес традиционных картонных упаковок или бутылок для жидких пищевых продуктов. Минимальное использование сырьевых материалов позволяет уменьшить потребление энергии в процессе производства, облегчить транспортировку и утилизацию отходов.
					</div>
					<div class="clients-divide"></div>
					<div class="clients-about text-default">
						Создание макетов дизайна упаковки продукции
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<div class="space-30"></div>
@stop