@extends('site.layout', ['Title' => 'Создание интернет-магазинов'])

@section('head-styles')
<link rel="stylesheet" href="assets/css/ecommerce.css">
@stop

@section('footer')
<script type="text/javascript" src="assets/js/ecommerce.js"></script>
@stop

@section('content')
<div class="header">
    <div class="header-inner container">
        <div class="row">
            <div class="col-md-11 col-md-offset-1">
                <h1>
                  Делаем интернет-магазины,<span class="hidden-xs hidden-sm"><br></span> которые умеют работать и продавать
                </h1>
            </div>
        </div>
    </div>
</div>


<div class="ecommerce container margin-top-lg-m110 margin-top-md-m130 margin-top-sm-m110 margin-top-xs-m55">
    <h2 class="text-center">Прямо сейчас наши магазины:</h2>
    <div class="ecommerce-content">
        <div class="ecommerce-img">
            <img src="assets/img/ecommerce/ecommerce.png">
        </div>
        <ul class="ecommerce-items">
            <li class="ecommerce-item">Увеличивают продажи <span class="visible-lg-inline"><br></span>розничного бизнеса</li>
            <li class="ecommerce-item">Выходят <span class="visible-lg-inline"><br></span>на новые рынки</li>
            <li class="ecommerce-item">Управляют <span class="visible-lg-inline"><br></span>логистикой</li>
            <li class="ecommerce-item">Поддерживают работу <span class="visible-lg-inline"><br></span>дилерской сети</li>
            <li class="ecommerce-item">Обгоняют <span class="visible-lg-inline"><br></span>конкурентов</li>
            <li class="ecommerce-item">Присматривают <span class="visible-lg-inline"><br></span>за складом</li>
        </ul>
    </div>
</div>




<div id="slider-container" class="slider container-fluid hidden-xs">
    <a class="page-anchor" data-name="Что такое хороший интернет-магазин" name="good"></a>
    <h2>Хороший интернет-магазин &ndash; это:</h2>
    <div id="slider" class="slider-slider flexslider clearfix">
        <ul class="slider-slides slides">
        	<li style="background-image:url(assets/img/ecommerce/slide1.png);"></li>
        	<li style="background-image:url(assets/img/ecommerce/slide2.png);"></li>
        	<li style="background-image:url(assets/img/ecommerce/slide3.png);"></li>
        	<li style="background-image:url(assets/img/ecommerce/slide4.png);"></li>
        	<li style="background-image:url(assets/img/ecommerce/slide5.png);"></li>
        </ul>
    </div>
    <div class="slider-arrows container">
        <div class="slider-direction-button prev"></div>
    	<div class="slider-direction-button next"></div>
    </div>
</div>


<div class="container advantages">
    <h2>Мы разрабатываем интернет-магазины под ключ</h2>
    <div class="advantages-thesis">Мы готовы продажи быстро и без головной боли<br>запустить продажи вашего товара через интернет-магазин</div>
    <div class="row">
        <div class="advantages-item col-md-4 col-sm-4">
            <div class="advantages-header">Создать сайт</div>
            <div class="advantages-img">
                <img class="img-responsive" src="assets/img/ecommerce/adv1_1.png">
            </div>
            <div class="advantages-text">
                <p>Продумываем и создаём отличный сайт с высокой конверсией</p>
                <p>Подключаем инструменты для удобной работы:</p>
                <ul class="list list-default-blue">
                    <li>платёжные системы,</li>
                    <li>программа лояльности,</li>
                    <li>«Яндекс-маркет» для быстрого привлечения покупателей</li>
                </ul>
                <p>Разрабатываем дополнительный функционал специально для вашего сайта</p>
                <p>Вы получаете сайт, на котором удобно делать покупки, и которым легко управлять</p>
            </div>
        </div>
        <div class="advantages-item col-md-4 col-sm-4">
            <div class="advantages-header">Отладить логистику</div>
            <div class="advantages-img">
                <img class="img-responsive" src="assets/img/ecommerce/adv2_1.png">
            </div>
            <div class="advantages-text">
                <p>Подключаем управление логистикой:</p>
                <ul class="list list-default-blue">
                    <li>модуль автоматического расчёта стоимости доставки,</li>
                    <li>сервис фулфилмента (складское хранение, комплектация и отправка заказов, перечисление оплаты владельцу),</li>
                    <li>курьерские службы для автоматизации, обработки и отправки заказов (если есть свой склад)</li>
                </ul>
                <p>При необходимости — глубоко автоматизируем внутренние логистические процессы магазина, взаимодействие с поставщиками и с CRM-системами</p>
            </div>
        </div>
        <div class="advantages-item col-md-4 col-sm-4">
            <div class="advantages-header">Поднять продажи</div>
            <div class="advantages-img">
                <img class="img-responsive" src="assets/img/ecommerce/adv3_1.png">
            </div>
            <div class="advantages-text">
                <p>Устанавливаем и настраиваем онлайн-консультант для увеличения конверсии</p>
                <p>Планируем и проводим рекламные кампании для привлечения целевых покупателей</p>
                <p>Занимаемся продвижением сайта в поисковиках Яндекс и Google</p>
                <p>Следим за конверсией на сайте, работаем над её увеличением</p>
                <p>Продвигаем ваши товары среди пользователей социальных сетей повышая лояльность к вашему бренду (SMM)</p>
            </div>
        </div>
    </div>
</div>

<div class="container">
    <a class="page-anchor" data-name="Сколько стоит интернет-магазин ?" name="price"></a>
    <h2 class="margin-bigger">Сколько стоит интернет-магазин ?</h2>
    <div class="row">
        <div class="cost-column cost-column-1 col-lg-7">
            <div class="cost-column-header">
                <div class="text-center"><img src="assets/img/ecommerce/cost_col1.png"></div>
                <div class="text-center text-upper f-20 margin-top-20">Интернет-магазин</div>
                <div class="text-center">
                    <span class="f-16">от</span> <span class="f-30">130 000</span> <span class="f-16"><i class="fa fa-rub lh-12"></i></span>
                </div>
            </div>
            <div class="cost-column-inner">
                <div class="row">
                    <div class="col-md-6">
                        <ul class="list list-arrow-violet">
                            <li>Уникальный дизайн</li>
                            <li>Адаптация для мобильных устройств</li>
                            <li>Базовая SEO-оптимизация</li>
                            <li>Многоуровневый каталог</li>
                            <li>E-mail уведомления</li>
                            <li>Онлайн-консультант</li>
                        </ul>
                    </div>
                    <div class="col-md-6">
                        <ul class="list list-arrow-violet">
                            <li>Кнопки соц. сетей</li>
                            <li>Интеграция с Яндекс Маркет</li>
                            <li>Регистрация в поисковых системах</li>
                            <li>Установка счетчиков</li>
                            <li>Корпоративная почта</li>
                        </ul>
                    </div>
                </div>
                <div class="modules">
                    <div class="modules-header">
                        <div class="modules-header-inner">
                            Модули:
                        </div>
                    </div>
                    <div class="row">
                        <div class="modules-item">
                            <div class="modules-img">
                                <img src="assets/img/ecommerce/module1.png">
                            </div>
                            <div class="modules-text">&laquo;С этим товаром покупают&raquo;</div>
                        </div>
                        <div class="modules-item">
                            <div class="modules-img">
                                <img src="assets/img/ecommerce/module2.png">
                            </div>
                            <div class="modules-text">Поиск и сортировка</div>
                        </div>
                        <div class="modules-item">
                            <div class="modules-img">
                                <img src="assets/img/ecommerce/module3.png">
                            </div>
                            <div class="modules-text">Покупка в один клик</div>
                        </div>
                        <div class="modules-item">
                            <div class="modules-img">
                                <img src="assets/img/ecommerce/module4.png">
                            </div>
                            <div class="modules-text">Статистика продаж</div>
                        </div>
                        <div class="modules-item">
                            <div class="modules-img">
                                <img src="assets/img/ecommerce/module5.png">
                            </div>
                            <div class="modules-text">Личный кабинет</div>
                        </div>
                        <div class="modules-item">
                            <div class="modules-img">
                                <img src="assets/img/ecommerce/module6.png">
                            </div>
                            <div class="modules-text">Скидки и акции</div>
                        </div>
                        <div class="modules-item">
                            <div class="modules-img">
                                <img src="assets/img/ecommerce/module7.png">
                            </div>
                            <div class="modules-text">Оплата онлайн</div>
                        </div>
                        <div class="clearfix"></div>
                    </div>
                </div>
            </div>
            <div class="text-center">
                <button class="button button-red-transparent fb-form" data-fancybox-href="#form-popup-default">Заказать магазин</button>
            </div>
        </div>
        <div class="cost-column cost-column-2 col-lg-4 col-lg-offset-1">
            <div class="cost-column-header">
                <div class="text-center">
                    <img src="assets/img/ecommerce/cost_col2.png">
                </div>
                <div class="text-center text-upper f-20 lh-12 margin-top-20">Дополнительные<br>возможности</div>
            </div>
            <div class="row cost-column-inner">
                <div class="col-lg-12 col-md-6">
                    <ul class="list list-arrow-green">
                        <li>Подбор товаров</li>
                        <li>Онлайн-оплата</li>
                        <li>Система скидок и программы лояльности</li>
                        <li>SMS-уведомления</li>
                    </ul>
                </div>
                <div class="col-lg-12 col-md-6">
                    <ul class="list list-arrow-green">
                        <li>Интеграция служб доставки</li>
                        <li>Интеграция со складской системой (1С или другой)</li>
                        <li>Яндекс.Директ/Google Adwords</li>
                        <li>SEO</li>
                        <li>Повышение конверсии</li>
                    </ul>
                </div>
            </div>
            <div class="pos-rel text-center">
                <button class="button button-green fb-form" data-fancybox-href="#form-popup-default">Подробнее</button>
            </div>
        </div>
    </div>
</div>

<div class="port">
    <h2>Примеры работ в нашем портфолио</h2>
    <div class="port-row" class="single-row container-fluid">
        <div class="port-slider">
            <div class="port-slider-item" style="background: url(assets/img/temp/port1.jpg) center left no-repeat;">
                <div class="port-slider-content">
                    <a href="#">
                        <div class="port-slider-content-middle">
                            <div class="port-slider-content-animate">
                                <div class="port-slider-header">Создание сайта «Инвестиционный департамент»</div>
                                <div class="port-slider-text">
                                    Сайт для компании ориентирован на привлечение клиентов и партнеров по бизнесу. Пользователям очень легко ориентироваться в ценах, с помощью имеющегося на сайте
                                </div>
                                <div class="port-slider-icos">
                                    <span class="ico-eye"></span>
                                </div>
                            </div>
                        </div>
                    </a>
                </div>
            </div>
            <div class="port-slider-item" style="background: url(assets/img/temp/port2.jpg) center center no-repeat;">
                <div class="port-slider-content">
                    <a href="#">
                        <div class="port-slider-content-middle">
                            <div class="port-slider-content-animate">
                                <div class="port-slider-header">Создание сайта «Инвестиционный департамент»</div>
                                <div class="port-slider-text">
                                    Сайт для компании ориентирован на привлечение клиентов и партнеров по бизнесу. Пользователям очень легко ориентироваться в ценах, с помощью имеющегося на сайте
                                </div>
                                <div class="port-slider-icos">
                                    <span class="ico-eye"></span>
                                </div>
                            </div>
                        </div>
                    </a>
                </div>
            </div>
            <div class="port-slider-item" style="background: url(assets/img/temp/port3.jpg) center center no-repeat;">
                <div class="port-slider-content">
                    <a href="#">
                        <div class="port-slider-content-middle">
                            <div class="port-slider-content-animate">
                                <div class="port-slider-header">Создание сайта «Инвестиционный департамент»</div>
                                <div class="port-slider-text">
                                    Сайт для компании ориентирован на привлечение клиентов и партнеров по бизнесу. Пользователям очень легко ориентироваться в ценах, с помощью имеющегося на сайте
                                </div>
                                <div class="port-slider-icos">
                                    <span class="ico-eye"></span>
                                </div>
                            </div>
                        </div>
                    </a>
                </div>
            </div>
            <div class="port-slider-item" style="background: url(assets/img/temp/port4.jpg) center center no-repeat;">
                <div class="port-slider-content">
                    <a href="#">
                        <div class="port-slider-content-middle">
                            <div class="port-slider-content-animate">
                                <div class="port-slider-header">Создание сайта «Инвестиционный департамент»</div>
                                <div class="port-slider-text">
                                    Сайт для компании ориентирован на привлечение клиентов и партнеров по бизнесу. Пользователям очень легко ориентироваться в ценах, с помощью имеющегося на сайте
                                </div>
                                <div class="port-slider-icos">
                                    <span class="ico-eye"></span>
                                </div>
                            </div>
                        </div>
                    </a>
                </div>
            </div>
            <div class="port-slider-item" style="background: url(assets/img/temp/port5.jpg) center center no-repeat;">
                <div class="port-slider-content">
                    <a href="#">
                        <div class="port-slider-content-middle">
                            <div class="port-slider-content-animate">
                                <div class="port-slider-header">Создание сайта «Инвестиционный департамент»</div>
                                <div class="port-slider-text">
                                    Сайт для компании ориентирован на привлечение клиентов и партнеров по бизнесу. Пользователям очень легко ориентироваться в ценах, с помощью имеющегося на сайте
                                </div>
                                <div class="port-slider-icos">
                                    <span class="ico-eye"></span>
                                </div>
                            </div>
                        </div>
                    </a>
                </div>
            </div>
            <div class="port-slider-item" style="background: url(assets/img/temp/port6.jpg) center center no-repeat;">
                <div class="port-slider-content">
                    <a href="#">
                        <div class="port-slider-content-middle">
                            <div class="port-slider-content-animate">
                                <div class="port-slider-header">Создание сайта «Инвестиционный департамент»</div>
                                <div class="port-slider-text">
                                    Сайт для компании ориентирован на привлечение клиентов и партнеров по бизнесу. Пользователям очень легко ориентироваться в ценах, с помощью имеющегося на сайте
                                </div>
                                <div class="port-slider-icos">
                                    <span class="ico-eye"></span>
                                </div>
                            </div>
                        </div>
                    </a>
                </div>
            </div>
            <div class="port-slider-item" style="background: url(assets/img/temp/port7.jpg) center center no-repeat;">
                <div class="port-slider-content">
                    <a href="#">
                        <div class="port-slider-content-middle">
                            <div class="port-slider-content-animate">
                                <div class="port-slider-header">Создание сайта «Инвестиционный департамент»</div>
                                <div class="port-slider-text">
                                    Сайт для компании ориентирован на привлечение клиентов и партнеров по бизнесу. Пользователям очень легко ориентироваться в ценах, с помощью имеющегося на сайте
                                </div>
                                <div class="port-slider-icos">
                                    <span class="ico-eye"></span>
                                </div>
                            </div>
                        </div>
                    </a>
                </div>
            </div>
            <div class="port-slider-item" style="background: url(assets/img/temp/port8.jpg) center center no-repeat;">
                <div class="port-slider-content">
                    <a href="#">
                        <div class="port-slider-content-middle">
                            <div class="port-slider-content-animate">
                                <div class="port-slider-header">Создание сайта «Инвестиционный департамент»</div>
                                <div class="port-slider-text">
                                    Сайт для компании ориентирован на привлечение клиентов и партнеров по бизнесу. Пользователям очень легко ориентироваться в ценах, с помощью имеющегося на сайте
                                </div>
                                <div class="port-slider-icos">
                                    <span class="ico-eye"></span>
                                </div>
                            </div>
                        </div>
                    </a>
                </div>
            </div>
            <div class="port-slider-item" style="background: url(assets/img/temp/port9.jpg) center center no-repeat;">
                <div class="port-slider-content">
                    <a href="#">
                        <div class="port-slider-content-middle">
                            <div class="port-slider-content-animate">
                                <div class="port-slider-header">Создание сайта «Инвестиционный департамент»</div>
                                <div class="port-slider-text">
                                    Сайт для компании ориентирован на привлечение клиентов и партнеров по бизнесу. Пользователям очень легко ориентироваться в ценах, с помощью имеющегося на сайте
                                </div>
                                <div class="port-slider-icos">
                                    <span class="ico-eye"></span>
                                </div>
                            </div>
                        </div>
                    </a>
                </div>
            </div>
            <div class="port-slider-item" style="background: url(assets/img/temp/port10.jpg) center center no-repeat;">
                <div class="port-slider-content">
                    <a href="#">
                        <div class="port-slider-content-middle">
                            <div class="port-slider-content-animate">
                                <div class="port-slider-header">Создание сайта «Инвестиционный департамент»</div>
                                <div class="port-slider-text">
                                    Сайт для компании ориентирован на привлечение клиентов и партнеров по бизнесу. Пользователям очень легко ориентироваться в ценах, с помощью имеющегося на сайте
                                </div>
                                <div class="port-slider-icos">
                                    <span class="ico-eye"></span>
                                </div>
                            </div>
                        </div>
                    </a>
                </div>
            </div>
            <div class="port-slider-item" style="background: url(assets/img/temp/port11.jpg) center center no-repeat;">
                <div class="port-slider-content">
                    <a href="#">
                        <div class="port-slider-content-middle">
                            <div class="port-slider-content-animate">
                                <div class="port-slider-header">Создание сайта «Инвестиционный департамент»</div>
                                <div class="port-slider-text">
                                    Сайт для компании ориентирован на привлечение клиентов и партнеров по бизнесу. Пользователям очень легко ориентироваться в ценах, с помощью имеющегося на сайте
                                </div>
                                <div class="port-slider-icos">
                                    <span class="ico-eye"></span>
                                </div>
                            </div>
                        </div>
                    </a>
                </div>
            </div>
            <div class="port-slider-item" style="background: url(assets/img/temp/port12.jpg) center center no-repeat;">
                <div class="port-slider-content">
                    <a href="#">
                        <div class="port-slider-content-middle">
                            <div class="port-slider-content-animate">
                                <div class="port-slider-header">Создание сайта «Инвестиционный департамент»</div>
                                <div class="port-slider-text">
                                    Сайт для компании ориентирован на привлечение клиентов и партнеров по бизнесу. Пользователям очень легко ориентироваться в ценах, с помощью имеющегося на сайте
                                </div>
                                <div class="port-slider-icos">
                                    <span class="ico-eye"></span>
                                </div>
                            </div>
                        </div>
                    </a>
                </div>
            </div>
            <div class="port-slider-item" style="background: url(assets/img/temp/port13.jpg) center center no-repeat;">
                <div class="port-slider-content">
                    <a href="#">
                        <div class="port-slider-content-middle">
                            <div class="port-slider-content-animate">
                                <div class="port-slider-header">Создание сайта «Инвестиционный департамент»</div>
                                <div class="port-slider-text">
                                    Сайт для компании ориентирован на привлечение клиентов и партнеров по бизнесу. Пользователям очень легко ориентироваться в ценах, с помощью имеющегося на сайте
                                </div>
                                <div class="port-slider-icos">
                                    <span class="ico-eye"></span>
                                </div>
                            </div>
                        </div>
                    </a>
                </div>
            </div>
            <div class="port-slider-item" style="background: url(assets/img/temp/port14.jpg) center center no-repeat;">
                <div class="port-slider-content">
                    <a href="#">
                        <div class="port-slider-content-middle">
                            <div class="port-slider-content-animate">
                                <div class="port-slider-header">Создание сайта «Инвестиционный департамент»</div>
                                <div class="port-slider-text">
                                    Сайт для компании ориентирован на привлечение клиентов и партнеров по бизнесу. Пользователям очень легко ориентироваться в ценах, с помощью имеющегося на сайте
                                </div>
                                <div class="port-slider-icos">
                                    <span class="ico-eye"></span>
                                </div>
                            </div>
                        </div>
                    </a>
                </div>
            </div>
        </div>
    </div>
</div>


<div class="container margin-top-30">
    <h2 class="text-center">Заказать магазин или задать вопрос:</h2>
    <form class="form-ajax" action="./" method="POST">
        <input type="hidden" name="form" value="">
        <input type="hidden" name="required" value="Name, Email, Phone, Question">
        <input type="hidden" name="action" value="doPostForm">
        <input type="hidden" name="mailSubject" value="Запрос на сайте webisgroup.ru">
        <div class="row">
            <div class="col-lg-8 col-md-10 col-lg-offset-2 col-md-offset-1">
                <div class="row">
                    <div class="col-md-6">
                        <div class="input-spacing">
                            <div class="input-group input-group-before">
                                <span class="input-group-addon"><img src="assets/img/ico_input_user_red.png" alt="Ваше имя"></span>
                                <input data-required="Name" placeholder="Как вас зовут?" type="text" name="Name" value="" class="input">
                            </div>
                        </div>
                        <div class="input-spacing">
                            <div class="input-group input-group-before">
                                <span class="input-group-addon"><img src="assets/img/ico_input_phone_red.png" alt="Ваш телефон"></span>
                                <input data-required="Phone" placeholder="Ваш телефон" type="text" name="Phone" value="" class="input">
                            </div>
                        </div>
                        <div class="input-spacing">
                            <div class="input-group input-group-before">
                                <span class="input-group-addon"><img src="assets/img/ico_input_envelope_red.png" alt="Ваш email"></span>
                                <input data-required="Email" placeholder="Ваш Email" type="text" name="Email" value="" class="input">
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6 input-spacing input-spacing-md-0">
                        <textarea data-required="Question" name="Question" placeholder="Пожалуйста, кратко опишите вашу задачу" class="input height-as-input-3"></textarea>
                    </div>
                </div>
                <div class="text-center input-spacing-last">
                    <button type="submit" class="button button-red-transparent">Отправить заявку</button>
                </div>
            </div>
        </div>
    </form>
</div>


<div class="container-fluid stages-row stages-row-1 margin-top-50">
    <a class="stages-anchor" name="stage1"></a>
    <div class="container">
        <div class="row">
            <div class="col-md-6 stages-img">
                <img class="img-responsive" src="assets/img/ecommerce/stage1.png">
            </div>
            <div class="col-md-6 stages-inner">
                <div class="stages-header">
                    Возможности вашего
                    <br>интернет-магазина:
                </div>
                <div class="stages-text">
                    <p>
                        Мы знаем, что хороший интернет-магазин - это не только красивый сайт, но и инструмент, оптимизированный под ваши бизнес-процессы.
                    </p>
                    <p>
                        Мы проанализируем ваши потребности и особенности рынка и сделаем красивый, удобный и эффективный магазин, который будет расти вместе с вашим бизнесом.
                    </p>
                </div>
                <button class="button button-green-dark button-collapse" data-action="show" data-target=".abilities">
                    <span class="button-collapse-ico-down"></span>
                    <span class="button-collapse-text">Узнать больше</span>
                </button>
                <button class="button button-green-dark button-collapse display-none" data-action="hide" data-target=".abilities">
                    <span class="button-collapse-ico-up"></span>
                    <span class="button-collapse-text">Свернуть</span>
                </button>
            </div>
        </div>
    </div>
</div>

<div class="container-fluid stages-row-data abilities">
    <div class="container">
        <div class="row">
            <div class="col-md-3 col-sm-4 flipper-container abilities-item">
                <div class="flipper">
                    <div class="flipper-front">
                        <div class="abilities-header">Управление
                            <br>контентом</div>
                        <div class="abilities-ico">
                            <img src="assets/img/ecommerce/abilities1.png">
                        </div>
                        <div class="abilities-bg green"></div>
                    </div>
                    <div class="flipper-back">
                        <ul>
                            <li>Неограниченный объем артикульной базы</li>
                            <li>Произвольное управление структурой каталога</li>
                            <li>Удобное добавление/редактирование товаров</li>
                            <li>Автоматическая подгонка изображений</li>
                        </ul>
                        <div class="abilities-bg green"></div>
                    </div>
                </div>
            </div>
            <div class="col-md-3 col-sm-4 flipper-container abilities-item">
                <div class="flipper">
                    <div class="flipper-front">
                        <div class="abilities-header">Интеграция с внешними сервисами</div>
                        <div class="abilities-ico">
                            <img src="assets/img/ecommerce/abilities2.png">
                        </div>
                        <div class="abilities-bg red"></div>
                    </div>
                    <div class="flipper-back">
                        <div class="abilities-bg red"></div>
                        <ul>
                            <li>Синхронизация артикульной базы и цен с 1С и другими системами</li>
                            <li>Отображение актуальных остатков по складам</li>
                            <li>Обратная выгрузка заказов в 1С</li>
                            <li>Ручной и автоматический режимы синхронизации</li>
                        </ul>
                    </div>
                </div>
            </div>
            <div class="col-md-3 col-sm-4 flipper-container abilities-item">
                <div class="flipper">
                    <div class="flipper-front">
                        <div class="abilities-header">Простое оформление заказа</div>
                        <div class="abilities-ico">
                            <img src="assets/img/ecommerce/abilities3.png">
                        </div>
                        <div class="abilities-bg purple"></div>
                    </div>
                    <div class="flipper-back">
                        <ul>
                            <li>Удобная корзина заказа</li>
                            <li>Возможность автоматической регистрации новых покупателей</li>
                            <li>Онлайн-оплата: подключение любых платёжных систем</li>
                            <li>Автоматическое формирование документов для безналичной оплаты</li>
                        </ul>
                        <div class="abilities-bg purple"></div>
                    </div>
                </div>
            </div>
            <div class="col-md-3 col-sm-4 flipper-container abilities-item">
                <div class="flipper">
                    <div class="flipper-front">
                        <div class="abilities-header">Работа с дилерами</div>
                        <div class="abilities-ico">
                            <img src="assets/img/ecommerce/abilities4.png">
                        </div>
                        <div class="abilities-bg blue"></div>
                    </div>
                    <div class="flipper-back">
                        <ul>
                            <li>Отдельные цены для дилеров</li>
                            <li>Доступ к разделам, закрытым для посетителей</li>
                            <li>Индивидуальный доступ к документам</li>
                            <li>Возможность выгрузки товарной базы по API</li>
                        </ul>
                        <div class="abilities-bg blue"></div>
                    </div>
                </div>
            </div>
            <div class="col-md-3 col-sm-4 flipper-container abilities-item">
                <div class="flipper">
                    <div class="flipper-front">
                        <div class="abilities-header">Управление заказами</div>
                        <div class="abilities-ico">
                            <img src="assets/img/ecommerce/abilities5.png">
                        </div>
                        <div class="abilities-bg violet"></div>
                    </div>
                    <div class="flipper-back">
                        <ul>
                            <li>Полный доступ ко всей истории заказов с гибким поиском</li>
                            <li>Настраиваемые состояния заказа и отдельные обработки для них</li>
                            <li>Контроль количества товаров на складе</li>
                        </ul>
                        <div class="abilities-bg violet"></div>
                    </div>
                </div>
            </div>
            <div class="col-md-3 col-sm-4 flipper-container abilities-item">
                <div class="flipper">
                    <div class="flipper-front">
                        <div class="abilities-header">Удобство покупателей</div>
                        <div class="abilities-ico">
                            <img src="assets/img/ecommerce/abilities6.png">
                        </div>
                        <div class="abilities-bg gray"></div>
                    </div>
                    <div class="flipper-back">
                        <ul>
                            <li>Скидки: накопительные и персональные</li>
                            <li>История заказов в личном кабинете</li>
                            <li>Возможность повторения предыдущих заказов</li>
                            <li>Возможность интеграции с SMS-сервисами</li>
                        </ul>
                        <div class="abilities-bg gray"></div>
                    </div>
                </div>
            </div>
            <div class="col-md-3 col-sm-4 flipper-container abilities-item">
                <div class="flipper">
                    <div class="flipper-front">
                        <div class="abilities-header">Безопасность</div>
                        <div class="abilities-ico">
                            <img src="assets/img/ecommerce/abilities7.png">
                        </div>
                        <div class="abilities-bg orange"></div>
                    </div>
                    <div class="flipper-back">
                        <ul>
                            <li>Защищённые пароли покупателей и администраторов</li>
                            <li>Формы с защитой от спама</li>
                            <li>Высокая защищённость от попыток взлома</li>
                        </ul>
                        <div class="abilities-bg orange"></div>
                    </div>
                </div>
            </div>
            <div class="col-md-3 col-sm-4 flipper-container abilities-item">
                <div class="flipper">
                    <div class="flipper-front">
                        <div class="abilities-header">Возможности для рекламы</div>
                        <div class="abilities-ico">
                            <img src="assets/img/ecommerce/abilities8.png">
                        </div>
                        <div class="abilities-bg red2"></div>
                    </div>
                    <div class="flipper-back">
                        <ul>
                            <li>Гибкие возможности для SEO-птимизации и продвижения магазина</li>
                            <li>Автоматический экспорт товаров в "Яндекс.Маркет" и другие агрегаторы</li>
                            <li>Лёгкая и гибкая настройка целей в Яндекс.Метрике и Google Analytics</li>
                        </ul>
                        <div class="abilities-bg red2"></div>
                    </div>
                </div>
            </div>
        </div>
        <div class="text-center">
            <button class="button stages-button-fold button-collapse" data-target=".abilities" data-action="hide">
                <table cellpadding="0" cellspacing="0">
                    <tr>
                        <td valign="top">
                            <img src="assets/img/ico_arr_up_rounded_white.png">
                        </td>
                        <td style="padding-left: 7px;">
                            <span class="stages-button-fold-text">Свернуть</span>
                        </td>
                    </tr>
                </table>
            </button>
        </div>
    </div>
</div>



<div class="container-fluid stages-row stages-row-2">
    <a class="stages-anchor" name="stage2"></a>
    <div class="container">
        <div class="row">
            <div class="col-md-6 stages-inner">
                <div class="stages-header">Какие факторы влияют на стоимость интернет магазина ?</div>
                <div class="stages-text">
                    <p>
                        Хороший интернет-магазин — это всегда индивидуальное, не шаблонное решение. Он должен учитывать специфику вашего товара и предпочтения вашей аудитории.
                    </p>
                    <p>
                        Есть несколько важных вопросов, ответы на которые мы с вами должны получить перед началом работы.
                    </p>
                </div>
                <button class="button button-gray-smoke button-collapse" data-action="show" data-target=".costfactors">
                    <span class="button-collapse-ico-down"></span>
                    <span class="button-collapse-text">Узнать больше</span>
                </button>
                <button class="button button-gray-smoke button-collapse display-none" data-action="hide" data-target=".costfactors">
                    <span class="button-collapse-ico-up"></span>
                    <span class="button-collapse-text">Свернуть</span>
                </button>
            </div>
            <div class="col-md-6 stages-img">
                <img class="img-responsive" src="assets/img/ecommerce/stage2.png">
            </div>
        </div>
    </div>
</div>

<div class="container-fluid stages-row-data costfactors">
    <div class="container">
        <div class="row">
            <div class="costfactors-item col-md-4 col-sm-12">
                <div class="costfactors-header">
                    <div>Специфика товара</div>
                    <div style="padding-top: 37px;" class="costfactors-img">
                        <img src="assets/img/ecommerce/factors1.png">
                    </div>
                </div>
                <div class="costfactors-inner">
                    <ul class="list list-question-blue">
                        <li>
                            Нужна ли возможность выбирать из нескольких вариантов поставки при заказе?
                            <br> Например, выбирать размер и цвет для одной модели платья или различные варианты обивки для одной модели мебели.
                        </li>
                        <li>
                            Насколько разнообразный ассортимент товаров у вас будет?
                            <br> Сильно различающиеся группы товаров часто требуют индивидуального представления на сайте.
                        </li>
                        <li>
                            Будут ли ваши товары типовыми или индивидуально собираемыми для покупателя?
                            <br> Например, продажа детских игрушек и продажа конфигурируемых серверов требуют разной организации процесса заказа.
                        </li>
                    </ul>
                </div>
            </div>
            <div class="costfactors-item col-md-4 col-sm-12">
                <div class="costfactors-header">
                    <div>
                        Возможности<br>для посетителей
                    </div>
                    <div class="costfactors-img">
                        <img src="assets/img/ecommerce/factors2.png">
                    </div>
                </div>
                <div class="costfactors-inner">
                    <ul class="list list-question">
                        <li>
                            Какие способы удержания покупателей на сайте вы хотите использовать?
                            <br> Есть множество инструментов для повышения вероятности покупки: скидки, акции, спецпредложения,сопутствующие товары, рекомендации, email-рассылки...
                        </li>
                        <li>
                            Как много различных товаров вы планируете продавать?
                            <br> Для удобной работы с большим ассортиментом имеет смысл использовать специальные механизмы подбора, фильтры, умный поиск, различные виды сортировки.
                        </li>
                        <li>
                            Интересна ли вам аудитория пользователей смартфонов?
                            <br> Ваш магазин может быть дополнен адаптивной вёрсткой для удобной работы с мобильных устройств.
                        </li>
                        <li>
                            Хотите сделать процесс заказа максимально удобным?
                            <br> Ваш сайт может автоматически рассчитывать стоимость доставки, принимать оплату от покупателей по банковским картам и другим платёжным системам и предоставлять покупателям инструменты для отслеживания своих заказов.
                        </li>
                    </ul>
                </div>
            </div>
            <div class="costfactors-item col-md-4 col-sm-12">
                <div class="costfactors-header">
                    <div>
                        Автоматизация
                        <br>бэк-офиса
                    </div>
                    <div class="costfactors-img">
                        <img src="assets/img/ecommerce/factors3.png">
                    </div>
                </div>
                <div class="costfactors-inner">
                    <ul class="list list-question-violet">
                        <li>
                            Желаете, чтобы ассортимент на сайте оперативно обновлялся из вашей торговой системы?
                            <br> Интеграция магазина с 1C или другими ERP-системами позволит автоматизировать обновление каталога, цен и остатков, а также проведение заказов.
                        </li>
                        <li>
                            У вас есть несколько поставщиков, предоставляющих свои прайс-листы?
                            <br> Сайт можно научить автоматически обновлять ассортимент и цены по этим данным и даже автоматически заказывать нужное количество товаров у поставщиков.
                        </li>
                        <li>
                            Планируете оптимизировать работу ваших менеджеров?
                            <br> Вашим услугам удобные инструменты по обработке заказов, статистика продаж, автоматизация взаимодействия со службами доставки и расчёт KPI для продавцов.
                        </li>
                        <li>
                            Думаете о том, как добиться максимальной эффективности рекламы?
                            <br> Сайт можно научить автоматически собирать лиды, выгружать информацию в "Яндекс.Маркет" и другие агрегаторы, собирать статистику продаж по источникам трафика.
                        </li>
                    </ul>
                </div>
            </div>
        </div>
        <div class="text-center">
            <button class="stages-button-fold button button-collapse" data-target=".costfactors" data-action="hide">
                <table cellpadding="0" cellspacing="0">
                    <tr>
                        <td>
                            <img src="assets/img/ico_arr_up_rounded_white.png">
                        </td>
                        <td style="padding-left: 7px;">
                            <span class="stages-button-fold-text">Свернуть</span>
                        </td>
                    </tr>
                </table>
            </button>
        </div>
    </div>
</div>

<div class="container-fluid stages-row stages-row-3">
    <a class="stages-anchor" name="stage3"></a>
    <div class="container">
        <div class="row">
            <div class="col-md-6 stages-img">
                <img class="img-responsive" src="assets/img/ecommerce/stage3.png">
            </div>
            <div class="col-md-6 stages-inner">
                <div class="stages-header">
                    Этапы создания
                    <br>интернет-магазина:
                </div>
                <div class="stages-text">
                    <p>
                        Интернет-магазин имеет смысл разрабатывать поэтапно, ориентируясь на ваши нужды и планы развития. Уже в конце первого этапа у вас будет отличный продающий сайт.
                    </p>
                    <p>
                        Каждый следующий этап расширяет его возможности, учитывая опыт живой эксплуатации сайта и потребности ваших клиентов.
                    </p>
                </div>
                <button class="button button-red button-collapse" data-action="show" data-target=".steps">
                    <span class="button-collapse-ico-down"></span>
                    <span class="button-collapse-text">Узнать больше</span>
                </button>
                <button class="button button-red button-collapse display-none" data-action="hide" data-target=".steps">
                    <span class="button-collapse-ico-up"></span>
                    <span class="button-collapse-text">Свернуть</span>
                </button>
            </div>
        </div>
    </div>
</div>

<div class="container-fluid stages-row-data steps">
    <div class="container">
        <div class="row">
            <div class="steps-item col-md-12 col-sm-12 col-xs-12 col-lg-4">
                <div class="steps-header">
                    <div>Проектирование
                        <br>и согласование
                        <br>техзадания</div>
                    <div class="steps-img"><img src="assets/img/ecommerce/steps1.png">
                    </div>
                </div>
                <div class="steps-inner">
                    <ul class="list list-arrow-gray">
                        <li>Сбор данных и обсуждение ваших пожеланий</li>
                        <li>Подготовка коммерческого предложения</li>
                        <li>Заключение договора и старт работ после внесения вами 30% предоплаты</li>
                        <li>Подготовка и согласование технического задания и прототипов интернет-магазина</li>
                        <li>При необходимости — регистрация доменного имени и хостинг-площадки</li>
                    </ul>
                </div>
            </div>
            <div class="steps-item col-md-12 col-sm-12 col-xs-12 col-lg-4">
                <div class="steps-header">
                    <div>Дизайн интернет-магазина</div>
                    <div style="padding-top: 42px;" class="steps-img"><img src="assets/img/ecommerce/steps2.png">
                    </div>
                </div>
                <div class="steps-inner">
                    <ul class="list list-arrow-green">
                        <li>Проработка интерфейсов и пользовательских сценариев</li>
                        <li>Дизайн главной страницы интернет-магазина</li>
                        <li>Дизайн основных страниц каталога и карточки товара</li>
                        <li>Корзина и страница оформления заказа</li>
                        <li>Интерактивные элементы, привлекающие внимание</li>
                    </ul>
                </div>
            </div>
            <div class="steps-item col-md-12 col-sm-12 col-xs-12 col-lg-4">
                <div class="steps-header">
                    <div>Верстка,
                        <br>программирование,
                        <br>интеграция с 1С</div>
                    <div class="steps-img"><img src="assets/img/ecommerce/steps3.png">
                    </div>
                </div>
                <div class="steps-inner">
                    <ul class="list list-arrow-violet">
                        <li>Дизайн переводится в html-код и интегрируется с системой управления сайтом</li>
                        <li>Тестирование интернет-магазина на нашем тестовом сервере</li>
                        <li>Далее происходит настройка обмена данными с 1С или другой торговой системой</li>
                        <li>Подключение платежных систем: WebMoney, Яндекс.Деньги, Qiwi или других</li>
                        <li>Сайт готов! Он радостно переезжает на вашу хостинг-площадку</li>
                    </ul>
                </div>
            </div>
        </div>
        <div class="text-center">
            <button class="stages-button-fold button button-collapse" data-target=".steps" data-action="hide">
                <table cellpadding="0" cellspacing="0">
                    <tr>
                        <td>
                            <img src="assets/img/ico_arr_up_rounded_white.png">
                        </td>
                        <td style="padding-left: 7px;">
                            <span class="stages-button-fold-text">Свернуть</span>
                        </td>
                    </tr>
                </table>
            </button>
        </div>
    </div>
</div>


<div class="gift container">
    <h2 class="text-center margin-bigger">Заказывая интернет-магазин у нас<span class="hidden-sm hidden-xs"><br></span> вы получаете приятные бонусы:</h2>
    <div class="row">
        <div class="gift-img col-md-4 col-md-offset-2">
            <img class="img-responsive m-auto" src="assets/img/ecommerce/gift.png">
        </div>
        <div class="gift-items col-md-5">
            <div class="gift-item">
                <div class="gift-item-img">
                    <img class="img-responsive" src="assets/img/ecommerce/webisonline.png" alt="">
                </div>
                <div class="gift-item-text">
                    Бесплатное подключение онлайн-косультанта Web<span style="color: #81aa00;">is</span>Online для 5 операторов сроком на 1 год
                </div>
            </div>
            <div class="gift-item">
                <div class="gift-item-img">
                    <img class="img-responsive" src="assets/img/ecommerce/yandex_market.png" alt="">
                </div>
                <div class="gift-item-text">
                    Синхронизация с <span style="color: #dc0000;">Я</span>ндекс маркет
                </div>
            </div>
            <div class="gift-item">
                <div class="gift-item-img">
                    <img class="img-responsive" src="assets/img/ecommerce/google_analytics.png" alt="">
                </div>
                <div class="gift-item-text">
                    Установка и настройка <span style="color: #1250f1;">G</span><span style="color: #f12a34;">o</span><span style="color: #ffbe15;">o</span><span style="color: #1950ff;">g</span><span style="color: #009e15;">l</span><span style="color: #ed3334;">e</span> <span style="color: #4e4e4e;">Analitycs</span>
                </div>
            </div>
            <div class="gift-item">
                <div class="gift-item-img">
                    <img class="img-responsive" src="assets/img/ecommerce/google_adwords.png">
                </div>
                <div class="gift-item-text">
                    Подготовка медиаплана для <span style="color: #dc0000;">Я</span>ндекс директ и <span style="color: #1250f1;">G</span><span style="color: #f12a34;">o</span><span style="color: #ffbe15;">o</span><span style="color: #1950ff;">g</span><span style="color: #009e15;">l</span><span style="color: #ed3334;">e</span> <span style="color: #4e4e4e;">Adwords</span>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="space-40"></div>
@stop