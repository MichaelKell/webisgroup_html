@extends('site.layout', ['Title' => 'Abante CMS'])

@section('head-styles-before')
<link rel="stylesheet" href="assets/plugins/stackable/stacktable.css">
@stop

@section('head-styles')
<link rel="stylesheet" href="assets/css/cms.css">
@stop

@section('footer')
<script type="text/javascript" src="assets/plugins/stackable/stacktable.js"></script>
<script type="text/javascript" src="assets/js/cms.js"></script>
@stop

@section('content')
<div class="header">
    <div class="header-inner container">
        <div class="row">
            <div class="col-md-11 col-md-offset-1">
                <h1>
            Мощная, удобная и надежная<br> система управления сайтом
        </h1>
                <div class="header-thesis">Abante CMS</div>
            </div>
        </div>
    </div>
</div>

<div class="abante container margin-top-lg-m95 margin-top-md-m45">
    <div class="row">
        <div class="col-md-4 col-sm-12 abante-img">
            <img class="img-responsive" src="assets/img/cms/abante.png" alt="">
        </div>
        <div class="col-md-8 col-sm-12">
            <div class="abante-header">Работает и развивется с 2000 года</div>
            <div class="row abante-items">
                <div class="col-md-3 col-sm-3 abante-item">
                    <div class="abante-item-ico">
                        <img src="assets/img/cms/abante_ico1.png" class="img-responsive" alt="">
                    </div>
                    <div class="abante-item-text">
                        Управление всем содержимым сайта
                    </div>
                </div>
                <div class="col-md-3 col-sm-3 abante-item">
                    <div class="abante-item-ico">
                        <img src="assets/img/cms/abante_ico2.png" alt="">
                    </div>
                    <div class="abante-item-text">
                        Простота <span class="hidden-sm hidden-xs"><br></span>и удобство работы
                    </div>
                </div>
                <div class="col-md-3 col-sm-3 abante-item">
                    <div class="abante-item-ico">
                        <img src="assets/img/cms/abante_ico3.png" alt="">
                    </div>
                    <div class="abante-item-text">
                        Надежность <span class="hidden-sm hidden-xs"><br></span>и устойчивость
                    </div>
                </div>
                <div class="col-md-3 col-sm-3 abante-item">
                    <div class="abante-item-ico">
                        <img src="assets/img/cms/abante_ico4.png" alt="">
                    </div>
                    <div class="abante-item-text">
                        Быстродействие и масштабируемость
                    </div>
                </div>
            </div>
            <div class="abante-button">
                <button class="button button-green fb-form" data-fancybox-href="#form-popup-default">Запросить тестовый доступ</button>
            </div>
        </div>
    </div>
</div>


<div class="why container-fluid margin-top-md-20">
    <a class="page-anchor" data-name="12 причин выбрать Abante CMS" name="12reasons"></a>
    <h2>12 причин выбрать Abante CMS</h2>
    <div class="container">
        <div class="row">
            <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12 why-item flipper-container">
                <div class="flipper">
                    <div class="flipper-front">
                        <div class="why-img">
                            <div class="v-middle">
                                <img src="assets/img/cms/why1.png" alt="">
                            </div>
                        </div>
                        <div class="why-header">
                            Неограниченный объём информации
                        </div>
                        <div class="why-item-bg"></div>
                    </div>
                    <div class="flipper-back">
                        <div class="why-text">
                            Если у Вас возникает необходимость в открытии нового раздела на сайте, это можно сделать моментально. Без ограничений по количеству страниц, файлов или структуре сайта.
                        </div>
                        <div class="why-item-bg"></div>
                    </div>
                </div>
            </div>
            <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12 why-item flipper-container">
                <div class="flipper">
                    <div class="flipper-front">
                        <div class="why-img">
                            <div class="v-middle">
                                <img src="assets/img/cms/why2.png" alt="">
                            </div>
                        </div>
                        <div class="why-header">
                            Необыкновенно простое управление сайтом
                        </div>
                        <div class="why-item-bg"></div>
                    </div>
                    <div class="flipper-back">
                        <div class="why-text">
                            Управление сайтом не потребует каких-либо специальных навыков для управления и Вам не придётся специально платить кому-либо за поддержку сайта.
                        </div>
                        <div class="why-item-bg"></div>
                    </div>
                </div>
            </div>
            <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12 why-item flipper-container">
                <div class="flipper">
                    <div class="flipper-front">
                        <div class="why-img">
                            <div class="v-middle">
                                <img src="assets/img/cms/why3.png" alt="">
                            </div>
                        </div>
                        <div class="why-header">
                            Гарантийная поддержка
                        </div>
                        <div class="why-item-bg"></div>
                    </div>
                    <div class="flipper-back">
                        <div class="why-text">
                            Вместе с сайтом Вы получаете неограниченную гарантийную поддержку. То есть, ваш сайт всегда будет корректно работать, и мы всегда будем готовы ответить на ваши вопросы.
                        </div>
                        <div class="why-item-bg"></div>
                    </div>
                </div>
            </div>
            <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12 why-item flipper-container">
                <div class="flipper">
                    <div class="flipper-front">
                        <div class="why-img">
                            <div class="v-middle">
                                <img src="assets/img/cms/why4.png" alt="">
                            </div>
                        </div>
                        <div class="why-header">
                            Удобный редактор с богатыми возможностями
                        </div>
                        <div class="why-item-bg"></div>
                    </div>
                    <div class="flipper-back">
                        <div class="why-text">
                            Редактируйте тексты, вставляйте ссылки, фотографии и фотогалереи. Работайте с таблицами и добавляйте видеоролики. Информация на сайте обновляется мгновенно.
                        </div>
                        <div class="why-item-bg"></div>
                    </div>
                </div>
            </div>
            <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12 why-item flipper-container">
                <div class="flipper">
                    <div class="flipper-front">
                        <div class="why-img">
                            <div class="v-middle">
                                <img src="assets/img/cms/why5.png" alt="">
                            </div>
                        </div>
                        <div class="why-header">
                            Гибкая настройка каталога товаров
                        </div>
                        <div class="why-item-bg"></div>
                    </div>
                    <div class="flipper-back">
                        <div class="why-text">
                            Вы можете определить, какие характеристики показывать для тех или иных групп товаров, а пользователи смогут бытро находить нужные позиции благодаря системе фильтров.
                        </div>
                        <div class="why-item-bg"></div>
                    </div>
                </div>
            </div>
            <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12 why-item flipper-container">
                <div class="flipper">
                    <div class="flipper-front">
                        <div class="why-img">
                            <div class="v-middle">
                                <img src="assets/img/cms/why6.png" alt="">
                            </div>
                        </div>
                        <div class="why-header">
                            Невероятно простой импорт/экспорт данных
                        </div>
                        <div class="why-item-bg"></div>
                    </div>
                    <div class="flipper-back">
                        <div class="why-text">
                            Вы можете быстро импортировать данные из Word, Excel или других источников нажатием одной кнопки.
                        </div>
                        <div class="why-item-bg"></div>
                    </div>
                </div>
            </div>
            <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12 why-item flipper-container">
                <div class="flipper">
                    <div class="flipper-front">
                        <div class="why-img">
                            <div class="v-middle">
                                <img src="assets/img/cms/why7.png" alt="">
                            </div>
                        </div>
                        <div class="why-header">
                            Отлаженная синхронизация с 1С
                        </div>
                        <div class="why-item-bg"></div>
                    </div>
                    <div class="flipper-back">
                        <div class="why-text">
                            Мы настроим выгрузку цен и характеристик товаров на ваш сайт
                            <br>и обратную выгрузку заказов с сайта в вашу 1С
                        </div>
                        <div class="why-item-bg"></div>
                    </div>
                </div>
            </div>
            <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12 why-item flipper-container">
                <div class="flipper">
                    <div class="flipper-front">
                        <div class="why-img">
                            <div class="v-middle">
                                <img src="assets/img/cms/why8.png" alt="">
                            </div>
                        </div>
                        <div class="why-header">
                            Адаптивный дизайн сайта
                        </div>
                        <div class="why-item-bg"></div>
                    </div>
                    <div class="flipper-back">
                        <div class="why-text">
                            Каждый сайт поставляется в комплекте с мобильной версией. Обновление информации на основном и мобильном сайте происходит одновременно.
                        </div>
                        <div class="why-item-bg"></div>
                    </div>
                </div>
            </div>
            <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12 why-item flipper-container">
                <div class="flipper">
                    <div class="flipper-front">
                        <div class="why-img">
                            <div class="v-middle">
                                <img src="assets/img/cms/why9.png" alt="">
                            </div>
                        </div>
                        <div class="why-header">
                            Дружественные пути к страницам
                        </div>
                        <div class="why-item-bg"></div>
                    </div>
                    <div class="flipper-back">
                        <div class="why-text">
                            URL страниц сайта, которые вы назначаете сами, понравится и вашим посетителям, и поисковикам. Это удобство восприятия для пользователя и дружественная структура для поисковых роботов.
                        </div>
                        <div class="why-item-bg"></div>
                    </div>
                </div>
            </div>
            <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12 why-item flipper-container">
                <div class="flipper">
                    <div class="flipper-front">
                        <div class="why-img">
                            <div class="v-middle">
                                <img src="assets/img/cms/why10.png" alt="">
                            </div>
                        </div>
                        <div class="why-header">
                            Безграничные SEO-возможности
                        </div>
                        <div class="why-item-bg"></div>
                    </div>
                    <div class="flipper-back">
                        <div class="why-text">
                            Простой и понятный контроль над мета-тэгами и заголовками, поддержка микроразметки. Удобная установка специальных текстовых блоков и счетчиков.
                        </div>
                        <div class="why-item-bg"></div>
                    </div>
                </div>
            </div>
            <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12 why-item flipper-container">
                <div class="flipper">
                    <div class="flipper-front">
                        <div class="why-img">
                            <div class="v-middle">
                                <img src="assets/img/cms/why11.png" alt="">
                            </div>
                        </div>
                        <div class="why-header">
                            Поддержка многоязычных сайтов
                        </div>
                        <div class="why-item-bg"></div>
                    </div>
                    <div class="flipper-back">
                        <div class="why-text">
                            Вы сможете без труда поддерживать все языковые версии сайта в одной системе управления. Структура в каждом языке независима: вы можете тонко настраивать сайт под нужные рынки.
                        </div>
                        <div class="why-item-bg"></div>
                    </div>
                </div>
            </div>
            <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12 why-item flipper-container">
                <div class="flipper">
                    <div class="flipper-front">
                        <div class="why-img">
                            <div class="v-middle">
                                <img src="assets/img/cms/why12.png" alt="">
                            </div>
                        </div>
                        <div class="why-header">
                            Гибкая система распределения доступа к управлению
                        </div>
                        <div class="why-item-bg"></div>
                    </div>
                    <div class="flipper-back">
                        <div class="why-text">
                            Распределяйте права на управление сайтом среди своих сотрудников. Каждый будет иметь доступ к управлению только назначенным разделом или страницей.
                        </div>
                        <div class="why-item-bg"></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>


<div class="functions container">
    <a class="page-anchor" data-name="Функции системы" name="functions"></a>
    <h2>Возможности системы</h2>
    <div class="functions-table-wrap">
        <table class="functions-table" cellpadding="0" cellspacing="0" width="100%" border="0">
            <tr>
                <td width="26%" align="left" class="white functions-bg-gray pl-no-tooltip">Функция</td>
                <td width="20%" align="center" class="white functions-bg-blue">Корпоративный сайт</td>
                <td width="18%" align="center" class="white functions-bg-green">Интернет-магазин basic</td>
                <td width="18%" align="center" class="white functions-bg-orange">Интернет-магазин advanced</td>
                <td width="20%" align="center" class="white functions-bg-violet">Custom</td>
            </tr>
            <tr>
                <td align="center" colspan="5" class="functions-divider functions-bg-gray-light font-regular text-upper black">
                    <div class="functions-tooltip">
                        <div class="functions-tooltip-inner">
                            <div class="text-upper f-18 black lh-12 margin-bottom-5">Базовые возможности</div>
                            Присутствуют во всех пакетных предложениях Abante CMS. Позволяют осуществлять базовую работу над интерфейсом и внутренним содержимым сайта, включая управление структурой сайта, редактирование его страниц и файловых хранилищ, написание новостей и многое другое
                        </div>
                    </div>
                    Базовые возможности
                </td>
            </tr>
            <tr>
                <td class="pl-no-tooltip">
                    Защищённый паролем доступ
                </td>
                <td align="center">
                    <span class="ico-check-blue"></span>
                </td>
                <td align="center">
                    <span class="ico-check-green"></span>
                </td>
                <td align="center">
                    <span class="ico-check-orange"></span>
                </td>
                <td align="center">
                    <span class="ico-check-violet"></span>
                </td>
            </tr>
            <tr>
                <td class="pl-with-tooltip">
                    <div class="functions-tooltip-wrap">
                        <div class="functions-tooltip-ico">
                            <div class="functions-tooltip">
                                <div class="functions-tooltip-inner">
                                    <div class="f-18 black font-light lh-12 margin-bottom-5">Управление структурой сайта</div>
                                    Позволяет осуществлять базовое управление содержимым сайта без ограничений
                                </div>
                            </div>
                        </div>
                        <div class="functions-tooltip-text">Управление структурой сайта</div>
                    </div>
                </td>
                <td align="center">
                    <span class="ico-check-blue"></span>
                </td>
                <td align="center">
                    <span class="ico-check-green"></span>
                </td>
                <td align="center">
                    <span class="ico-check-orange"></span>
                </td>
                <td align="center">
                    <span class="ico-check-violet"></span>
                </td>
            </tr>
            <tr>
                <td class="pl-no-tooltip">
                    Управление базой файлов
                </td>
                <td align="center">
                    <span class="ico-check-blue"></span>
                </td>
                <td align="center">
                    <span class="ico-check-green"></span>
                </td>
                <td align="center">
                    <span class="ico-check-orange"></span>
                </td>
                <td align="center">
                    <span class="ico-check-violet"></span>
                </td>
            </tr>
            <tr>
                <td class="pl-with-tooltip">
                    <div class="functions-tooltip-wrap">
                        <div class="functions-tooltip-ico">
                            <div class="functions-tooltip">
                                <div class="functions-tooltip-inner">
                                    <div class="f-18 black font-light lh-12 margin-bottom-5">Публикация фотографий и загруженных файлов</div>
                                    Вставка в текст фотографий и ссылок на загруженные файлы. Увеличение по клику. Автоматическая подгонка размеров изображений при загрузке.
                                </div>
                            </div>
                        </div>
                        <div class="functions-tooltip-text">Публикация фотографий и загруженных файлов</div>
                    </div>
                </td>
                <td align="center">
                    <span class="ico-check-blue"></span>
                </td>
                <td align="center">
                    <span class="ico-check-green"></span>
                </td>
                <td align="center">
                    <span class="ico-check-orange"></span>
                </td>
                <td align="center">
                    <span class="ico-check-violet"></span>
                </td>
            </tr>
            <tr>
                <td class="pl-no-tooltip">
                    Визуальное редактирование информации на страницах
                </td>
                <td align="center">
                    <span class="ico-check-blue"></span>
                </td>
                <td align="center">
                    <span class="ico-check-green"></span>
                </td>
                <td align="center">
                    <span class="ico-check-orange"></span>
                </td>
                <td align="center">
                    <span class="ico-check-violet"></span>
                </td>
            </tr>
            <tr>
                <td class="pl-no-tooltip">
                    Автоматическая организация фотогалерей
                </td>
                <td align="center">
                    <span class="ico-check-blue"></span>
                </td>
                <td align="center">
                    <span class="ico-check-green"></span>
                </td>
                <td align="center">
                    <span class="ico-check-orange"></span>
                </td>
                <td align="center">
                    <span class="ico-check-violet"></span>
                </td>
            </tr>
            <tr>
                <td class="pl-no-tooltip">
                    Публикация новостей, RSS
                </td>
                <td align="center">
                    <span class="ico-check-blue"></span>
                </td>
                <td align="center">
                    <span class="ico-check-green"></span>
                </td>
                <td align="center">
                    <span class="ico-check-orange"></span>
                </td>
                <td align="center">
                    <span class="ico-check-violet"></span>
                </td>
            </tr>
            <tr>
                <td class="pl-no-tooltip">
                    Автообновляемая карта сайта
                </td>
                <td align="center">
                    <span class="ico-check-blue"></span>
                </td>
                <td align="center">
                    <span class="ico-check-green"></span>
                </td>
                <td align="center">
                    <span class="ico-check-orange"></span>
                </td>
                <td align="center">
                    <span class="ico-check-violet"></span>
                </td>
            </tr>
            <tr>
                <td class="pl-no-tooltip">
                    Управление доступом администраторов
                </td>
                <td align="center">
                    <span class="ico-check-blue"></span>
                </td>
                <td align="center">
                    <span class="ico-check-green"></span>
                </td>
                <td align="center">
                    <span class="ico-check-orange"></span>
                </td>
                <td align="center">
                    <span class="ico-check-violet"></span>
                </td>
            </tr>
            <tr>
                <td class="pl-no-tooltip">
                    Управление основными настройками сайта
                </td>
                <td align="center">
                    <span class="ico-check-blue"></span>
                </td>
                <td align="center">
                    <span class="ico-check-green"></span>
                </td>
                <td align="center">
                    <span class="ico-check-orange"></span>
                </td>
                <td align="center">
                    <span class="ico-check-violet"></span>
                </td>
            </tr>
            <tr>
                <td class="pl-no-tooltip">
                    Поиск по сайту
                </td>
                <td align="center">
                    <span class="ico-check-blue"></span>
                </td>
                <td align="center">
                    <span class="ico-check-green"></span>
                </td>
                <td align="center">
                    <span class="ico-check-orange"></span>
                </td>
                <td align="center">
                    <span class="ico-check-violet"></span>
                </td>
            </tr>
            <tr>
                <td class="pl-no-tooltip">
                    Оптимизация сайта для поисковых систем
                </td>
                <td align="center">
                    <span class="ico-check-blue"></span>
                </td>
                <td align="center">
                    <span class="ico-check-green"></span>
                </td>
                <td align="center">
                    <span class="ico-check-orange"></span>
                </td>
                <td align="center">
                    <span class="ico-check-violet"></span>
                </td>
            </tr>
            <tr>
                <td class="pl-no-tooltip">
                    Возможность поискового продвижения
                </td>
                <td align="center">
                    <span class="ico-check-blue"></span>
                </td>
                <td align="center">
                    <span class="ico-check-green"></span>
                </td>
                <td align="center">
                    <span class="ico-check-orange"></span>
                </td>
                <td align="center">
                    <span class="ico-check-violet"></span>
                </td>
            </tr>
            <tr>
                <td class="pl-no-tooltip">
                    Техническая поддержка
                </td>
                <td align="center">
                    <span class="ico-check-blue"></span>
                </td>
                <td align="center">
                    <span class="ico-check-green"></span>
                </td>
                <td align="center">
                    <span class="ico-check-orange"></span>
                </td>
                <td align="center">
                    <span class="ico-check-violet"></span>
                </td>
            </tr>
            <tr>
                <td align="center" colspan="5" class="functions-divider functions-bg-gray-light font-regular text-upper black">
                    <div class="functions-tooltip">
                        <div class="functions-tooltip-inner">
                            <div class="text-upper f-18 black lh-12 margin-bottom-5">Дополнительные возможности</div>
                            Позволяют настроить сайт под нужды конкретного бизнеса</div>
                    </div>
                    Дополнительные возможности</td>
            </tr>
            <tr>
                <td>
                    Многоязычность
                </td>
                <td align="center">
                    <span class="ico-check-blue"></span>
                </td>
                <td align="center">
                    <span class="ico-check-green"></span>
                </td>
                <td align="center">
                    <span class="ico-check-orange"></span>
                </td>
                <td align="center">
                    <span class="ico-check-violet"></span>
                </td>
            </tr>
            <tr>
                <td>
                    Каталог продукции
                </td>
                <td align="center">
                    <span class="gray font-bold">&ndash;</span>
                </td>
                <td align="center">
                    <span class="ico-check-green"></span>
                </td>
                <td align="center">
                    <span class="ico-check-orange"></span>
                </td>
                <td align="center">
                    <span class="ico-check-violet"></span>
                </td>
            </tr>
            <tr>
                <td>
                    Корзина заказа
                </td>
                <td align="center">
                    <span class="gray font-bold">&ndash;</span>
                </td>
                <td align="center">
                    <span class="ico-check-green"></span>
                </td>
                <td align="center">
                    <span class="ico-check-orange"></span>
                </td>
                <td align="center">
                    <span class="ico-check-violet"></span>
                </td>
            </tr>
            <tr>
                <td>
                    Обмен информацией с 1С<br> и другими торговыми системами
                </td>
                <td align="center">
                    <span class="gray font-bold">&ndash;</span>
                </td>
                <td align="center">
                    <span class="gray font-bold">&ndash;</span>
                </td>
                <td align="center">
                    <span class="ico-check-orange"></span>
                </td>
                <td align="center">
                    <span class="ico-check-violet"></span>
                </td>
            </tr>
            <tr>
                <td>
                    Онлайн-оплата заказов
                </td>
                <td align="center">
                    <span class="gray font-bold">&ndash;</span>
                </td>
                <td align="center">
                    <span class="gray font-bold">&ndash;</span>
                </td>
                <td align="center">
                    <span class="ico-check-orange"></span>
                </td>
                <td align="center">
                    <span class="ico-check-violet"></span>
                </td>
            </tr>
            <tr>
                <td>
                    Автоматический расчёт<br> стоимости доставки заказа
                </td>
                <td align="center">
                    <span class="gray font-bold">&ndash;</span>
                </td>
                <td align="center">
                    <span class="gray font-bold">&ndash;</span>
                </td>
                <td align="center">
                    <span class="ico-check-orange"></span>
                </td>
                <td align="center">
                    <span class="ico-check-violet"></span>
                </td>
            </tr>
            <tr>
                <td>
                    Импорт информации из блогов
                </td>
                <td align="center">
                    <span class="gray font-bold">&ndash;</span>
                </td>
                <td align="center">
                    <span class="gray font-bold">&ndash;</span>
                </td>
                <td align="center">
                    <span class="gray font-bold">&ndash;</span>
                </td>
                <td align="center">
                    <span class="ico-check-violet"></span>
                </td>
            </tr>
            <tr>
                <td>
                    Каталог недвижимости
                </td>
                <td align="center">
                    <span class="gray font-bold">&ndash;</span>
                </td>
                <td align="center">
                    <span class="gray font-bold">&ndash;</span>
                </td>
                <td align="center">
                    <span class="gray font-bold">&ndash;</span>
                </td>
                <td align="center">
                    <span class="ico-check-violet"></span>
                </td>
            </tr>
            <tr>
                <td>
                    Каталог вакансий
                </td>
                <td align="center">
                    <span class="gray font-bold">&ndash;</span>
                </td>
                <td align="center">
                    <span class="gray font-bold">&ndash;</span>
                </td>
                <td align="center">
                    <span class="gray font-bold">&ndash;</span>
                </td>
                <td align="center">
                    <span class="ico-check-violet"></span>
                </td>
            </tr>
            <tr>
                <td>
                    Версии страниц для печати
                </td>
                <td align="center">
                    <span class="ico-check-blue"></span>
                </td>
                <td align="center">
                    <span class="ico-check-green"></span>
                </td>
                <td align="center">
                    <span class="ico-check-orange"></span>
                </td>
                <td align="center">
                    <span class="ico-check-violet"></span>
                </td>
            </tr>
            <tr>
                <td>Универсальный готовый дизайн</td>
                <td colspan="4" align="center">может быть подключен к любой версии Abante CMS</td>
            </tr>
            <tr>
                <td align="center" colspan="5" class="functions-divider functions-bg-gray-light font-regular text-upper black">
                    Технические особенности
                </td>
            </tr>
            <tr>
                <td>
                    Оптимизация скорости работы
                </td>
                <td align="center">
                    <span class="ico-check-blue"></span>
                </td>
                <td align="center">
                    <span class="ico-check-green"></span>
                </td>
                <td align="center">
                    <span class="ico-check-orange"></span>
                </td>
                <td align="center">
                    <span class="ico-check-violet"></span>
                </td>
            </tr>
            <tr>
                <td>
                    Масштабируемость системы
                </td>
                <td align="center">
                    <span class="ico-check-blue"></span>
                </td>
                <td align="center">
                    <span class="ico-check-green"></span>
                </td>
                <td align="center">
                    <span class="ico-check-orange"></span>
                </td>
                <td align="center">
                    <span class="ico-check-violet"></span>
                </td>
            </tr>
            <tr>
                <td>
                    Смарт-тэги для дополнительной функциональности
                </td>
                <td align="center">
                    <span class="ico-check-blue"></span>
                </td>
                <td align="center">
                    <span class="ico-check-green"></span>
                </td>
                <td align="center">
                    <span class="ico-check-orange"></span>
                </td>
                <td align="center">
                    <span class="ico-check-violet"></span>
                </td>
            </tr>
            <tr>
                <td>
                    Дружественные URL страниц
                </td>
                <td align="center">
                    <span class="ico-check-blue"></span>
                </td>
                <td align="center">
                    <span class="ico-check-green"></span>
                </td>
                <td align="center">
                    <span class="ico-check-orange"></span>
                </td>
                <td align="center">
                    <span class="ico-check-violet"></span>
                </td>
            </tr>
            <tr>
                <td align="center" colspan="5" class="functions-divider functions-bg-gray-light font-regular text-upper black">
                    Дополнительные услуги
                </td>
            </tr>
            <tr>
                <td
                  >Разработка уникального дизайна, верстка, подключение необходимых модулей, настройка сервера, установка на хостинг
                </td>
                <td align="center">
                    <span class="ico-check-blue"></span>
                </td>
                <td align="center">
                    <span class="ico-check-green"></span>
                </td>
                <td align="center">
                    <span class="ico-check-orange"></span>
                </td>
                <td align="center">
                    <span class="ico-check-violet"></span>
                </td>
            </tr>
            <tr>
                <td>
                  Верстка, необходимое подключение модулей, настройка сервера, установка на хостинг предоставленного вами дизайна
                </td>
                <td align="center">
                  <span class="ico-check-blue"></span>
                </td>
                <td align="center">
                  <span class="ico-check-green"></span>
                </td>
                <td align="center">
                  <span class="ico-check-orange"></span>
                </td>
                <td align="center">
                  <span class="ico-check-violet"></span>
                </td>
            </tr>
        </table>
        <div class="functions-table-fold">
            <span class="fold-unfold"></span> Развернуть таблицу
        </div>
    </div>
</div>

<div class="payments container margin-bottom-md-20">
    <a class="page-anchor" data-name="Система интегрирована<br>с сервисами" name="services"></a>
    <h2>Система интегрирована с сервисами</h2>
    <div class="payments-slider">
        <div class="payments-img">
            <img class="img-responsive" src="assets/img/cms/payment1.jpg" alt="">
        </div>
        <div class="payments-img">
            <img class="img-responsive" src="assets/img/cms/payment2.jpg" alt="">
        </div>
        <div class="payments-img">
            <img class="img-responsive" src="assets/img/cms/payment3.jpg" alt="">
        </div>
        <div class="payments-img">
            <img class="img-responsive" src="assets/img/cms/payment4.jpg" alt="">
        </div>
        <div class="payments-img">
            <img class="img-responsive" src="assets/img/cms/payment5.jpg" alt="">
        </div>
        <div class="payments-img">
            <img class="img-responsive" src="assets/img/cms/payment6.jpg" alt="">
        </div>
    </div>
</div>



@stop