@extends('site.layout', ['Title' => 'Контактная информация'])

@section('head-styles')
<link rel="stylesheet" href="assets/css/contact.css">
@stop

@section('content')
<div class="header">
	<div class="header-inner container">
		<div class="row">
			<div class="col-md-11 col-md-offset-1">
				<h1>
					Контактная<span class="hidden-md"><br></span> информация
				</h1>
			</div>
		</div>
	</div>
</div>

<div class="about container margin-top-lg-m105 margin-top-md-m85 margin-top-sm-m75">
	<div class="row">
		<div class="col-md-11 col-md-offset-1">
			Если Вы хотите задать вопрос, узнать больше о Webis Group или обсудить перспективы сотрудничества —<span class="hidden-sm hidden-xs"><br></span> пожалуйста используйте контактные данные, приведённые на этой странице. 
		</div>
	</div>
</div>

<div class="container contacts">
	<div class="row">
		<div class="col-md-4 col-sm-4 contacts-item">
			<div class="contacts-item-inner">
				<div class="contacts-img">
					<img class="img-responsive" src="assets/img/contact/phone.png" alt="">
				</div>
				<div class="contacts-text">
					<div class="big margin-bottom-5">Контактные телефоны:</div>
					<div class="big font-light margin-bottom-5 nowrap"><span class="green">+7</span> <span >(495) 636-29-78</span></div>
					<div class="big font-light nowrap"><span class="green">+7</span> <span>(495) 765-45-71</span></div>
				</div>
			</div>
		</div>
		<div class="col-md-4 col-sm-4 contacts-item">
			<div class="contacts-item-inner">
				<div class="contacts-img">
					<img class="img-responsive" src="assets/img/contact/skype.png" alt="">
				</div>
				<div class="contacts-text">
					<div class="big margin-bottom-5">Skype:</div>
					<div class="nowrap margin-bottom-5"><img src="assets/img/contact/skype_online.png" alt=""> <a href="skype:pgrigory">pgrigory</a></div>
					<div class="nowrap"><img src="assets/img/contact/skype_online.png" alt=""> <a href="skype:andrew.davydoff">andrew.davydoff</a></div>
					<div class="margin-left-20 gray f-14 font-regular">&nbsp;(English capable)</div>
				</div>
			</div>
		</div>
		<div class="col-md-4 col-sm-4 contacts-item">
			<div class="contacts-item-inner">
				<div class="contacts-img">
					<img class="img-responsive" src="assets/img/contact/phone2.png" alt="">
				</div>
				<div class="contacts-text">
					<div class="big margin-bottom-5">Заказать обратный звонок</div>
					<button class="button button-empty blue fb-form nowrap margin-left-sm-m25 margin-left-md-0 margin-left-xs-0" data-fancybox-href="#form-popup-default">Перезвоните мне</button>
				</div>
			</div>
		</div>
	</div>

	<div class="row">
		<div class="col-md-4 col-sm-4 contacts-item margin-bottom-0">
			<div class="contacts-item-inner">
				<div class="contacts-img">
					&nbsp;
				</div>
				<div class="contacts-text big nowrap">
					Электронная почта:
				</div>
			</div>
		</div>
	</div>

	<div class="row">
		<div class="col-md-4 col-sm-4 contacts-item">
			<div class="contacts-item-inner">
				<div class="contacts-img">
					<img class="img-responsive" src="assets/img/contact/email1.png" alt="">
				</div>
				<div class="contacts-text">
					<div class="margin-bottom-5"><a href="mailto:contact@webisgroup.ru" target="_blank">contact@webisgroup.ru</a></div>
					<div>Для любых информационных запросов о нашей деятельности</div>
				</div>
			</div>
		</div>
		<div class="col-md-4 col-sm-4 contacts-item">
			<div class="contacts-item-inner">
				<div class="contacts-img">
					<img class="img-responsive" src="assets/img/contact/email2.png" alt="">
				</div>
				<div class="contacts-text">
					<div class="margin-bottom-5"><a href="mailto:sales@webisgroup.ru" target="_blank">sales@webisgroup.ru</a></div>
					<div>По вопросам, связанным с оформлением заказов и аспектами сотрудничества</div>
				</div>
			</div>
		</div>
		<div class="col-md-4 col-sm-4 contacts-item">
			<div class="contacts-item-inner">
				<div class="contacts-img">
					<img class="img-responsive" src="assets/img/contact/email3.png" alt="">
				</div>
				<div class="contacts-text">
					<div class="margin-bottom-5"><a href="mailto:support@webisgroup.ru" target="_blank">support@webisgroup.ru</a></div>
					<div>Служба технической поддержки работает круглосуточно. Отправьте письмо на этот адрес и мы будем рады помочь вам</div>
				</div>
			</div>
		</div>
	</div>
</div>

<div class="advantages container-fluid">
	<div class="container">
		<div class="row">
			<div class="advantages-item col-sm-6 col-md-6">
				<div class="advantages-item-inner">
					<div class="advantages-img">
						<img class="img-responsive" src="assets/img/contact/advantages1.png" alt="">
					</div>
					<div class="advantages-text">
						<div class="big margin-bottom-15">
							Мы умеем работать удалённо
						</div>
						<p>
							Если Вы находитесь в другом регионе или за пределами Российской Федерации, все вопросы можно решить дистанционно:
						</p>
						<ul class="list list-default">
							<li>Мы обсудим с Вами пожелания к проекту по телефону, электронной почте или по Skype;</li>
							<li>Оригиналы документов мы вышлем заказным письмом;</li>
							<li>Результат работы мы передадим Вам на электронном носителе: по электронной почте или разместив его в Сети;</li>
							<li>Результат работы мы передадим Вам на электронном носителе: по электронной почте или разместив его в Сети</li>
						</ul>
					</div>
				</div>
			</div>
			<div class="advantages-item col-sm-6 col-md-6">
				<div class="advantages-item-inner">
					<div class="advantages-img">
						<img class="img-responsive" src="assets/img/contact/advantages2.png" alt="">
					</div>
					<div class="advantages-text text-default">
						<div class="big margin-bottom-15">Отправьте нам он-лайн заявку</div>
						<form class="form-ajax" action="./">
							<input type="hidden" name="form" value="default">
							<div class="input-spacing">
								<input name="Name" data-required="name" type="text" class="input input-blue" placeholder="Как вас зовут?">
							</div>
							<div class="input-spacing">
								<input name="Phone" data-required="phone" type="text" class="input input-blue" placeholder="Ваш телефон">
							</div>
							<div class="input-spacing">
								<input name="Email" data-required="email" type="text" class="input input-blue" placeholder="Ваш e-mail">
							</div>
							<div class="input-spacing">
								<input name="Site" type="text" class="input input-blue" placeholder="Ваш сайт (если есть)">
							</div>
							<div class="input-spacing">
								<textarea name="Question" class="input input-blue height-as-input-2" rows="4" placeholder="Ваш комментарий или вопрос"></textarea>
							</div>
							<div class="input-spacing">
								<div class="input-heading">Сколько будет 2 + 3 ?</div>
								<input name="Answer" type="text" class="input input-blue" placeholder="Введите число" data-required="answer">
							</div>
							<div class="text-center">
								<button class="button button-red-transparent">Отправить заявку</button>
							</div>
						</form>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

<div class="offices container-fluid">
	<div class="container">
		<div class="row">
			<div class="col-md-4 col-md-offset-1 offices-item">
				<div class="offices-inner">
					<div class="offices-img">
						<img class="img-responsive" src="assets/img/contact/office1.png" alt="">
					</div>
					<div class="offices-text">
						<div class="big margin-bottom-5">Офис в Москве:</div>
						<div>
							123242, г.Москва,<br>
							<span class="nowrap">ул. Малая Грузинская, д. 20/13</span>
						</div>
					</div>
				</div>
				<div class="offices-map">
					<img class="img-responsive" src="assets/img/contact/office_map1.jpg" alt="">
				</div>
			</div>
			<div class="col-md-4 col-md-offset-2 offices-item">
				<div class="offices-inner">
					<div class="offices-img">
						<img class="img-responsive" src="assets/img/contact/office2.png" alt="">
					</div>
					<div class="offices-text">
						<div class="big margin-bottom-5">Офис в Дубне:</div>
						<div>
							<span>141980</span>, <span>г. Дубна</span>,<br>
							<span>ул. Университетская, д. 9</span>
						</div>
					</div>
				</div>
				<div class="offices-map">
					<img class="img-responsive" src="assets/img/contact/office_map2.jpg" alt="">
				</div>
			</div>
		</div>
	</div>
</div>

<div class="requisites container">
	<div class="row">
		<div class="col-md-10 col-md-offset-1 requisites-item">
			<div class="big">Наши реквизиты</div>
			<div><span class="font-regular">Полное наименование:</span> Общество с ограниченной ответственностью «Вебис Груп»</div>
			<div><span class="font-regular">ИНН</span> 5010032339 <span class="hidden-xs">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span><span class="hidden-sm hidden-md hidden-lg"><br></span><span class="font-regular">ОГРН</span> 1055001808916</div>
		</div>
	</div>
</div>
@stop