@extends('site.layout', ['Title' =>'Контекстная реклама. Старая версия'])

@section('head-styles')
<link rel="stylesheet" href="assets/css/context_old.css">
@stop

@section('footer')
<script src="assets/js/context_old.js"></script>
@stop

@section('content')
<div class="header">
	<div class="header-inner container">
		<div class="row">
			<div class="col-md-11 col-md-offset-1">
				<h1>
					Контекстная реклама
				</h1>
				<div class="header-thesis">
					<div class="margin-bottom-15">Тщательная подготовка компании</div>
					<div>Максимальный результат <span class="hidden-xs"><br></span>при оптимальном бюджете</div>
				</div>
			</div>
		</div>
	</div>
</div>

<div class="about container margin-top-md-m150 margin-top-sm-m120">
	<h2>Богатый опыт организации <span class="hidden-xs"><br></span>интернет-рекламы</h2>
	<div class="text-center margin-bottom-30">
		Контекстная реклама — реклама, соответствующая текущим интересам пользователя. <span class="hidden-sm hidden-xs"><br></span>
		Интерес определяется тем, что пользователь ищет или недавно искал в поисковой системе, <span class="hidden-sm hidden-xs"><br></span>
		либо содержанием просматриваемых им страниц. Рекламодатель сам задаёт набор ключевых <span class="hidden-sm hidden-xs"><br></span>
		слов, по которым будет показываться его объявление. Оно может показываться на поиске <span class="hidden-sm hidden-xs"><br></span>
		— в ответ на запрос, содержащий ключевые слова,— или на тематических сайтах
	</div>
	<div class="row">
		<div class="about-item col-md-4 col-sm-4">
			<div class="about-item-img">
				<img class="img-responsive" src="assets/img/context_old/about1.png" alt="">
			</div>
			<div class="about-item-header">
				Эффективный охват<span class="hidden-sm hidden-xs"><br></span> целевой аудитории
			</div>
			<div class="about-item-text">
				Вы получаете потенциальных клиентов 
				и платите только за результат. 
				Кроме того, мы предоставляем Вам 
				широкий выбор рекламных площадок. 
				Это означает, что Вы можете размещать 
				объявления на наиболее эффективных 
				сайтах без ненужных переплат. 
			</div>
		</div>
		<div class="about-item col-md-4 col-sm-4">
			<div class="about-item-img">
				<img class="img-responsive" src="assets/img/context_old/about2.png" alt="">
			</div>
			<div class="about-item-header">
				Гибкость рекламной<span class="hidden-sm hidden-xs"><br></span> кампании
			</div>
			<div class="about-item-text">
				Мы поможем вам выявить наиболее 
				выгодные направления рекламы 
				и распределить рекламный бюджет 
				в этих направлениях. Вы получите 
				подробную информацию о поведении 
				посетителей вашего сайта, их качестве 
				и вероятности совершения покупки
			</div>
		</div>
		<div class="about-item col-md-4 col-sm-4">
			<div class="about-item-img">
				<img class="img-responsive" src="assets/img/context_old/about3.png" alt="">
			</div>
			<div class="about-item-header">
				Существенное снижение<span class="hidden-sm hidden-xs"><br></span> стоимости контакта
			</div>
			<div class="about-item-text">
				Мы выявим недостатки и дадим 
				рекомендации по оптимизации страниц 
				сайта, что позволит минимизировать 
				количество посетителей, покинувших 
				Ваш сайт из-за того, что они не смогли 
				найти нужную информацию о доступных 
				товарах и услугах.
			</div>
		</div>
	</div>
	<div class="text-center margin-top-25">
		<hr size="2" color="#e6e6e6" class="margin-top-0 margin-bottom-25">
		<button class="button button-red-transparent fb-form" data-fancybox-href="#form-popup-default">Заказать <span class="hidden-xs">рекламную</span> кампанию</button>
	</div>
</div>


<div class="advantages">
	<h2>Преимущества контекстной рекламы</h2>
	<p class="text-center">
		Преимуществом контекстной рекламы сайта является то, что она размещается на самых посещаемых <span class="hidden-xs"><br></span>
		интернет-ресурсах — в поисковых системах. Мы размещаем контекстную интернет рекламу в самых известных <span class="hidden-xs"><br></span>
		поисковиках — в России это Яндекс, Mail.ru, Google, Рамблер, Aport, основные мировые площадки — Google и Yahoo. <span class="hidden-xs"><br></span>
		Ежедневно на эти сайты приходят множество людей в поисках нужного товара или услуги.<span class="hidden-xs"><br></span>
		При профессиональном подходе и тщательном планировании контекстная реклама сайта становится одним <span class="hidden-xs"><br></span>
		из наиболее эффективных способов значительного повышения объёма продаж.
	</p>
	<p class="font-regular text-center">
		Мы поможем Вам разместить контекстную интернет рекламу на следующих рекламных площадках: 
	</p>
</div>

<div class="services container">
      <div class="services-slider">
        <div class="services-img">
        	<div class="services-img-inner">
          		<img class="img-responsive" src="assets/img/context_old/service1.jpg" alt="">
          	</div>
        </div>
        <div class="services-img">
        	<div class="services-img-inner">
          		<img class="img-responsive" src="assets/img/context_old/service2.jpg" alt="">
          	</div>
        </div>
        <div class="services-img">
        	<div class="services-img-inner">
          		<img class="img-responsive" src="assets/img/context_old/service3.jpg" alt="">
          	</div>
        </div>
        <div class="services-img">
        	<div class="services-img-inner">
          		<img class="img-responsive" src="assets/img/context_old/service4.jpg" alt="">
          	</div>
        </div>
        <div class="services-img">
        	<div class="services-img-inner">
          		<img class="img-responsive" src="assets/img/context_old/service5.jpg" alt="">
          	</div>
        </div>
      </div>
</div>

@stop