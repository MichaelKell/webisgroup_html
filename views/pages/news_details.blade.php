@extends('site.layout', ['Title' => 'Заголовок новости'])

@section('head-styles')
<link rel="stylesheet" href="assets/css/news.css">
@endsection

@section('content')
<div class="header">
	<div class="header-inner container">
		<div class="row">
			<div class="col-md-7 col-md-offset-1">
				<h1 class="h1-inner">
					Заголовок новости
				</h1>
			</div>
		</div>
	</div>
</div>

<div class="tabs-navbar margin-top-md-m130 margin-top-sm-m100">
	<div class="container">
		<div class="row">
			<div class="col-md-10 col-md-offset-1 col-sm-12 col-xs-6 tabs-navbar-nav">
				<ul id="tinynav">
					<li class="tabs-navbar-nav-item">
						<a href="#">О нас</a>
					</li>
					<li class="tabs-navbar-nav-item active">
						<a href="#">Новости</a>
					</li>
					<li class="tabs-navbar-nav-item">
						<a href="#">Вакансии</a>
					</li>
				</ul>
			</div>
		</div>
	</div>
</div>

<div class="container news-details margin-top-md-20">
	<div class="row">
		<div class="col-md-10 col-md-offset-1">
			<p>
				Lorem ipsum dolor sit amet, consectetur adipisicing elit. Earum voluptate accusantium autem rem. Aliquid soluta animi, quas similique sint nisi voluptatibus est atque repudiandae. Voluptatum aliquam eveniet a eaque quidem quasi non iusto, est, provident, qui fugiat perspiciatis saepe consequuntur optio recusandae? Iste ratione, iure? Tempore eligendi error amet esse sequi, odio iste, mollitia voluptates animi aperiam ratione distinctio, tempora asperiores deserunt exercitationem ducimus ab. Rerum eius nisi animi vero quia suscipit, inventore delectus laudantium perferendis nemo sapiente ut eveniet unde qui est reiciendis! Non similique pariatur illo ea, praesentium saepe recusandae facere, doloribus asperiores nulla natus. In, culpa, iste!
			</p>
			<p>
				Lorem ipsum dolor sit amet, consectetur adipisicing elit. Earum voluptate accusantium autem rem. Aliquid soluta animi, quas similique sint nisi voluptatibus est atque repudiandae. Voluptatum aliquam eveniet a eaque quidem quasi non iusto, est, provident, qui fugiat perspiciatis saepe consequuntur optio recusandae? Iste ratione, iure? Tempore eligendi error amet esse sequi, odio iste, mollitia voluptates animi aperiam ratione distinctio, tempora asperiores deserunt exercitationem ducimus ab. Rerum eius nisi animi vero quia suscipit, inventore delectus laudantium perferendis nemo sapiente ut eveniet unde qui est reiciendis! Non similique pariatur illo ea, praesentium saepe recusandae facere, doloribus asperiores nulla natus. In, culpa, iste!
			</p>
			<p>
				Lorem ipsum dolor sit amet, consectetur adipisicing elit. Earum voluptate accusantium autem rem. Aliquid soluta animi, quas similique sint nisi voluptatibus est atque repudiandae. Voluptatum aliquam eveniet a eaque quidem quasi non iusto, est, provident, qui fugiat perspiciatis saepe consequuntur optio recusandae? Iste ratione, iure? Tempore eligendi error amet esse sequi, odio iste, mollitia voluptates animi aperiam ratione distinctio, tempora asperiores deserunt exercitationem ducimus ab. Rerum eius nisi animi vero quia suscipit, inventore delectus laudantium perferendis nemo sapiente ut eveniet unde qui est reiciendis! Non similique pariatur illo ea, praesentium saepe recusandae facere, doloribus asperiores nulla natus. In, culpa, iste!
			</p>
			<p>
				Lorem ipsum dolor sit amet, consectetur adipisicing elit. Earum voluptate accusantium autem rem. Aliquid soluta animi, quas similique sint nisi voluptatibus est atque repudiandae. Voluptatum aliquam eveniet a eaque quidem quasi non iusto, est, provident, qui fugiat perspiciatis saepe consequuntur optio recusandae? Iste ratione, iure? Tempore eligendi error amet esse sequi, odio iste, mollitia voluptates animi aperiam ratione distinctio, tempora asperiores deserunt exercitationem ducimus ab. Rerum eius nisi animi vero quia suscipit, inventore delectus laudantium perferendis nemo sapiente ut eveniet unde qui est reiciendis! Non similique pariatur illo ea, praesentium saepe recusandae facere, doloribus asperiores nulla natus. In, culpa, iste!
			</p>
		</div>
	</div>
</div>
<div class="space-50"></div>
@endsection