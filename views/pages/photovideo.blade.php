@extends('site.layout', ['Title' => 'Фото и видеопроизводство'])

@section('head-styles')
	<link rel="stylesheet" href="assets/css/photovideo.css">
@stop

@section('content')
<div class="header">
	<div class="header-inner container">
		<div class="row">
			<div class="col-md-11 col-md-offset-1">
				<h1>
					Фото и<span class="hidden-md hidden-lg"><br></span> видео<span class="hidden-sm hidden-md hidden-lg">-</span>производство
				</h1>
			</div>
		</div>
	</div>
</div>

<div class="photovideo container margin-top-lg-m145 margin-top-md-m125 margin-top-sm-m115 margin-top-xs-m45">
	<h2>Лучше сто раз увидеть</h2>
	<div class="photovideo-thesis">
		...И немного текста, какого-то общего содержания. Буквально пара строчек.<span class="hidden-sm hidden-xs"><br></span> Ну что мы текста не придумаем что ли? Да и надо то его совсем чуть-чуть.<span class="hidden-sm hidden-xs"><br></span> Ну буквально вот столько, ну или вот столько
	</div>
	<div class="row">
		<div class="col-md-4 col-sm-12 photovideo-item">
			<div class="photovideo-img">
				<img class="img-responsive" src="assets/img/photovideo/circle1.png" alt="">
			</div>
			<div class="photovideo-header blue">Рекламная<br>и постановочная съёмка</div>
			<div class="photovideo-text">
				<p>
					Придумаем и реализуем любой рекламный сюжет. Сюда входит целый комплекс услуг:
				</p>
				<ul class="list list-default-blue">
					<li>Проработка концепции, сценария и раскадровки</li>
					<li>Расчет бюджета</li>
					<li>Поиск места для съёмки</li>
					<li>Подбор моделей</li>
					<li>Создание образов и декораций</li>
					<li>Сама съёмки</li>
					<li>Пост-обработка материала</li>
				</ul>
			</div>
		</div>
		<div class="col-md-4 col-sm-12 photovideo-item">
			<div class="photovideo-img">
				<img class="img-responsive" src="assets/img/photovideo/circle2.png" alt="">
			</div>
			<div class="photovideo-header yellow">Фотосъёмка товаров<br>для интернет-магазинов</div>
			<div class="photovideo-text">
				<p>Грамотная работа с фоном и освещением - то, что в первую очередь отличает качественную съёмку, раскрывающую лучшие характеристики товара. </p>
				<p>Качественная фотосъемка позволит идеально представить товар для посетителей сайта.</p>
				<p>По-настоящему качественная рекламная фотография умеет вызывать желание потрогать то, что на ней изображено. Если у посетителя такое желание возникло &ndash; он ваш.</p>
			</div>
		</div>
		<div class="col-md-4 col-sm-12 photovideo-item">
			<div class="photovideo-img">
				<img class="img-responsive" src="assets/img/photovideo/circle3.png" alt="">
			</div>
			<div class="photovideo-header violet">Создание презентационных<br>и рекламных роликов</div>
			<div class="photovideo-text">
				<p>Рекламные видеоролики остаются одним из основных инструментов для донесения вашего рекламного сообщения до широкой аудитории. </p>
				<p>Качественная видеореклама доносит информацию до потребителей быстро, изящно и непринуждённо, при этом она вызывает доверие, западает в память и мотивирует на покупку ещё долгое время после просмотра ролика.</p>
				<p>Поэтому для максимального и качественного охвата аудитории видео производство по-прежнему очень важно.</p>
			</div>
		</div>
	</div>
</div>

<div class="start-ready">
	<button class="button button-red-bright fb-form" data-fancybox-href="#form-popup-default">Заказать съемку</button>
</div>

@endsection