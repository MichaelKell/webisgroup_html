@extends('site.layout', ['Title' => 'Новости'])

@section('head-styles')
<link rel="stylesheet" href="assets/css/news.css">
@endsection

@section('footer')
<script src="assets/plugins/jquery.jscroll.min.js"></script>
<script src="assets/js/news.js"></script>
@endsection

@section('content')
<div class="header">
	<div class="header-inner container">
		<div class="row">
			<div class="col-md-11 col-md-offset-1">
				<h1 class="h1-inner">
					Новости
				</h1>
				<div class="header-thesis">
					Мы делаем сайты, разрабатываем сервисы<br>и отраслевые программные продукты
				</div>
			</div>
		</div>
	</div>
</div>

<div class="tabs-navbar margin-top-md-m130 margin-top-sm-m100">
	<div class="container">
		<div class="row">
			<div class="col-md-10 col-md-offset-1 col-sm-12 col-xs-6 tabs-navbar-nav">
				<ul id="tinynav">
					<li class="tabs-navbar-nav-item">
						<a href="#">О нас</a>
					</li>
					<li class="tabs-navbar-nav-item active">
						<a href="#">Новости</a>
					</li>
					<li class="tabs-navbar-nav-item">
						<a href="#">Вакансии</a>
					</li>
				</ul>
			</div>
		</div>
	</div>
</div>



<div class="news-list container margin-top-50">
	<div class="row">
		<div class="news-list-item col-md-4 col-sm-6 col-padding-sm-35">
			<div class="news-list-img">
				<a href="#">
					<img src="assets/img/temp/news1.jpg" class="img-responsive" alt="">
					<div class="overlay-eye"></div>
				</a>
			</div>
			<div class="news-list-text">
				<div class="news-list-header">Заголовок новости</div>
				<div class="news-list-brief">Здесь будет краткое описание новости, чтобы было понятно о чем эта новость не переходя по ссылке</div>
				<div class="news-list-more">
					<span class="orange">&raquo;</span> <a class="default" href="#">подробнее</a>
				</div>
			</div>
		</div>
		<div class="news-list-item col-md-4 col-sm-6 col-padding-sm-35">
			<div class="news-list-img">
				<a href="#">
					<img src="assets/img/temp/news1.jpg" class="img-responsive" alt="">
					<div class="overlay-eye"></div>
				</a>
			</div>
			<div class="news-list-text">
				<div class="news-list-header">Заголовок новости</div>
				<div class="news-list-brief">Здесь будет краткое описание новости, чтобы было понятно о чем эта новость не переходя по ссылке</div>
				<div class="news-list-more">
					<span class="orange">&raquo;</span> <a class="default" href="#">подробнее</a>
				</div>
			</div>
		</div>
		<div class="news-list-item col-md-4 col-sm-6 col-padding-sm-35">
			<div class="news-list-img">
				<a href="#">
					<img src="assets/img/temp/news1.jpg" class="img-responsive" alt="">
					<div class="overlay-eye"></div>
				</a>
			</div>
			<div class="news-list-text">
				<div class="news-list-header">Заголовок новости</div>
				<div class="news-list-brief">Здесь будет краткое описание новости, чтобы было понятно о чем эта новость не переходя по ссылке</div>
				<div class="news-list-more">
					<span class="orange">&raquo;</span> <a class="default" href="#">подробнее</a>
				</div>
			</div>
		</div>
		<div class="news-list-item col-md-4 col-sm-6 col-padding-sm-35">
			<div class="news-list-img">
				<a href="#">
					<img src="assets/img/temp/news1.jpg" class="img-responsive" alt="">
					<div class="overlay-eye"></div>
				</a>
			</div>
			<div class="news-list-text">
				<div class="news-list-header">Заголовок новости</div>
				<div class="news-list-brief">Здесь будет краткое описание новости, чтобы было понятно о чем эта новость не переходя по ссылке</div>
				<div class="news-list-more">
					<span class="orange">&raquo;</span> <a class="default" href="#">подробнее</a>
				</div>
			</div>
		</div>
		<div class="news-list-item col-md-4 col-sm-6 col-padding-sm-35">
			<div class="news-list-img">
				<a href="#">
					<img src="assets/img/temp/news1.jpg" class="img-responsive" alt="">
					<div class="overlay-eye"></div>
				</a>
			</div>
			<div class="news-list-text">
				<div class="news-list-header">Заголовок новости</div>
				<div class="news-list-brief">Здесь будет краткое описание новости, чтобы было понятно о чем эта новость не переходя по ссылке</div>
				<div class="news-list-more">
					<span class="orange">&raquo;</span> <a class="default" href="#">подробнее</a>
				</div>
			</div>
		</div>
		<div class="news-list-item col-md-4 col-sm-6 col-padding-sm-35">
			<div class="news-list-img">
				<a href="#">
					<img src="assets/img/temp/news1.jpg" class="img-responsive" alt="">
					<div class="overlay-eye"></div>
				</a>
			</div>
			<div class="news-list-text">
				<div class="news-list-header">Заголовок новости</div>
				<div class="news-list-brief">Здесь будет краткое описание новости, чтобы было понятно о чем эта новость не переходя по ссылке</div>
				<div class="news-list-more">
					<span class="orange">&raquo;</span> <a class="default" href="#">подробнее</a>
				</div>
			</div>
		</div>
		<div class="news-list-item col-md-4 col-sm-6 col-padding-sm-35">
			<div class="news-list-img">
				<a href="#">
					<img src="assets/img/temp/news1.jpg" class="img-responsive" alt="">
					<div class="overlay-eye"></div>
				</a>
			</div>
			<div class="news-list-text">
				<div class="news-list-header">Заголовок новости</div>
				<div class="news-list-brief">Здесь будет краткое описание новости, чтобы было понятно о чем эта новость не переходя по ссылке</div>
				<div class="news-list-more">
					<span class="orange">&raquo;</span> <a class="default" href="#">подробнее</a>
				</div>
			</div>
		</div>
		<div class="news-list-item col-md-4 col-sm-6 col-padding-sm-35">
			<div class="news-list-img">
				<a href="#">
					<img src="assets/img/temp/news1.jpg" class="img-responsive" alt="">
					<div class="overlay-eye"></div>
				</a>
			</div>
			<div class="news-list-text">
				<div class="news-list-header">Заголовок новости</div>
				<div class="news-list-brief">Здесь будет краткое описание новости, чтобы было понятно о чем эта новость не переходя по ссылке</div>
				<div class="news-list-more">
					<span class="orange">&raquo;</span> <a class="default" href="#">подробнее</a>
				</div>
			</div>
		</div>
		<div class="news-list-item col-md-4 col-sm-6 col-padding-sm-35">
			<div class="news-list-img">
				<a href="#">
					<img src="assets/img/temp/news1.jpg" class="img-responsive" alt="">
					<div class="overlay-eye"></div>
				</a>
			</div>
			<div class="news-list-text">
				<div class="news-list-header">Заголовок новости</div>
				<div class="news-list-brief">Здесь будет краткое описание новости, чтобы было понятно о чем эта новость не переходя по ссылке</div>
				<div class="news-list-more">
					<span class="orange">&raquo;</span> <a class="default" href="#">подробнее</a>
				</div>
			</div>
		</div>
		<div class="news-list-item col-md-4 col-sm-6 col-padding-sm-35">
			<div class="news-list-img">
				<a href="#">
					<img src="assets/img/temp/news1.jpg" class="img-responsive" alt="">
					<div class="overlay-eye"></div>
				</a>
			</div>
			<div class="news-list-text">
				<div class="news-list-header">Заголовок новости</div>
				<div class="news-list-brief">Здесь будет краткое описание новости, чтобы было понятно о чем эта новость не переходя по ссылке</div>
				<div class="news-list-more">
					<span class="orange">&raquo;</span> <a class="default" href="#">подробнее</a>
				</div>
			</div>
		</div>
		<div class="news-list-item col-md-4 col-sm-6 col-padding-sm-35">
			<div class="news-list-img">
				<a href="#">
					<img src="assets/img/temp/news1.jpg" class="img-responsive" alt="">
					<div class="overlay-eye"></div>
				</a>
			</div>
			<div class="news-list-text">
				<div class="news-list-header">Заголовок новости</div>
				<div class="news-list-brief">Здесь будет краткое описание новости, чтобы было понятно о чем эта новость не переходя по ссылке</div>
				<div class="news-list-more">
					<span class="orange">&raquo;</span> <a class="default" href="#">подробнее</a>
				</div>
			</div>
		</div>
		<div class="news-list-item col-md-4 col-sm-6 col-padding-sm-35">
			<div class="news-list-img">
				<a href="#">
					<img src="assets/img/temp/news1.jpg" class="img-responsive" alt="">
					<div class="overlay-eye"></div>
				</a>
			</div>
			<div class="news-list-text">
				<div class="news-list-header">Заголовок новости</div>
				<div class="news-list-brief">Здесь будет краткое описание новости, чтобы было понятно о чем эта новость не переходя по ссылке</div>
				<div class="news-list-more">
					<span class="orange">&raquo;</span> <a class="default" href="#">подробнее</a>
				</div>
			</div>
		</div>
	</div>
</div>

<div class="news-panel container margin-top-md-20">
	<div class="row">
		<div class="col-md-12">
			<div class="news-panel-line"></div>
			<div class="row row-right-xm">
				<div class="col-md-6 col-sm-6 col-xs-6 news-panel-col">
					<a class="news-all default" href="./?view=all">
						<table cellpadding="0" cellspacing="0">
							<tr>
								<td>
									<i class="fa fa-list"></i>
								</td>
								<td class="padding-left-5">
									Все новости
								</td>
							</tr>
						</table>
					</a>
				</div>
				<div class="col-md-6 col-sm-6 col-xs-6 news-panel-col">
					<div class="news-nav">
						<a href="#" class="news-nav-prev"></a>
						<a href="#" class="news-nav-item active">1</a>
						<a href="#" class="news-nav-item">2</a>
						<a href="#" class="news-nav-item">3</a>
						<span class="news-nav-item col-padding-0">
						...
						</span>
						<a href="#" class="news-nav-item">5</a>
						<a href="#" class="news-nav-next"></a>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

<div class="space-50"></div>

@endsection