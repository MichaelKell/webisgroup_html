@extends('site.layout', ['Title' => 'Портфолио, пример работы'])

@section('head-styles')
<link rel="stylesheet" href="assets/css/portfolio.css">
@stop

@section('content')
<div class="header-default margin-top-m50"></div>

<div class="tabs-navbar margin-top-md-m110 margin-top-sm-m80">
	<div class="container">
		<div class="row">
			<div class="col-md-10 col-md-offset-1 col-sm-12 col-xs-6 tabs-navbar-nav">
				<ul id="tinynav">
					<li class="tabs-navbar-nav-item active">
						<a href="#">Сайты</a>
					</li>
					<li class="tabs-navbar-nav-item">
						<a href="#">Фирменный стиль</a>
					</li>
					<li class="tabs-navbar-nav-item">
						<a href="#">3D и мультимедиа</a>
					</li>
					<li class="tabs-navbar-nav-item">
						<a href="#">Прочее</a>
					</li>
				</ul>
			</div>
			<div class="col-xs-6 hidden-sm hidden-md hidden-lg tabs-navbar-nav-contact">
				<a href="#form-popup-default" class="fb-form">
					Оставить заявку
				</a>
			</div>
		</div>
	</div>
</div>

<div class="portfolio-navbar">
	<div class="container">
		<div class="portfolio-nav">
			<a href="#" class="portfolio-nav-prev" title="">
				<div class="portfolio-nav-prev-inner">
					<div class="portfolio-nav-prev-text">
						Сайт "Виватэкс-М"<br>
						<span class="portfolio-nav-no">
							1
						</span>	
					</div>
					<div class="portfolio-nav-prev-arrow">
						<img src="assets/img/arr_left.png" alt="prev" class="img-responsive">
					</div>
				</div>
			</a>
			<h1 class="portfolio-nav-header">Сайт компании «Миртен»</h1>
			<a href="#" class="portfolio-nav-next" title="">
				<div class="portfolio-nav-prev-inner">
					<div class="portfolio-nav-next-arrow">
						<img src="assets/img/arr_right.png" alt="next" class="img-responsive">
					</div>
					<div class="portfolio-nav-next-text">
						Сайт Himiko<br> 
						<span class="portfolio-nav-no">
							3
						</span>	
					</div>
				</div>
			</a>
		</div>
	</div>
</div>

<div class="portfolio-info container">
	<div class="row">
		<div class="col-md-4 col-md-offset-1 col-sm-4 portfolio-info-logo">
			<img src="assets/img/temp/port_work.jpg" alt="" class="img-responsive">
		</div>
		<div class="col-md-6 col-sm-6 portfolio-info">
			Компания «МИРТЕН» – стабильное, стремительно развивающееся предприятие, предлагающее широкий выбор продукции.
			Специалисты Webis Group разработали интернет-магазин с удобным поиском по параметрам для каждого вида продукции, а также лентой новостей.
			В основе сайта – собственная разработка Webis Group – Abante CMS.
		</div>
	</div>
</div>

<div class="portfolio-body container">
	<div class="row">
		<div class="col-md-10 col-md-offset-1 col-sm-12">
			<img src="assets/img/temp/port_work_main1.jpg" alt="" class="img-responsive margin-auto">
			<div class="space-15"></div>
			<p>
				Lorem ipsum dolor sit amet, consectetur adipisicing elit. Vero, nostrum.
				Lorem ipsum dolor sit amet, consectetur adipisicing elit. Vero, nostrum.
				Lorem ipsum dolor sit amet, consectetur adipisicing elit. Vero, nostrum.
			</p>
			<img src="assets/img/temp/port_work_main.jpg" alt="" class="img-responsive margin-auto">
			<div class="space-15"></div>
			<p>
				Lorem ipsum dolor sit amet, consectetur adipisicing elit. Vero, nostrum.
				Lorem ipsum dolor sit amet, consectetur adipisicing elit. Vero, nostrum.
				Lorem ipsum dolor sit amet, consectetur adipisicing elit. Vero, nostrum.
			</p>
			<div class="space-40"></div>
		</div>
	</div>
</div>
@stop