@extends('site.layout', ['Title' => 'Создание сайтов'])

@section('head-styles')
<link rel="stylesheet" href="assets/css/web.css">
@stop

@section('footer')
<script type="text/javascript" src="assets/js/web.js"></script>
@stop


@section('content')
<div class="header">
	<div class="container">
		<div class="row">
			<div class="col-md-10 col-md-offset-2 col-sm-offset-2 col-sm-10">
		  		<h1>Запуск сайтов<br>любой сложности</h1>
		  	</div>
		</div>
	 </div>
</div>

<div class="sites container margin-top-lg-m170 margin-top-md-m130 margin-top-sm-m110 margin-top-xs-m55">
	<h2 class="text-center">Наши сайты умеют:</h2>
	<div class="sites-content">
		<div class="sites-img">
			<img src="assets/img/web/sites.png">
		</div>
		<ul class="sites-items">
			<li class="sites-item">Продавать товары <span class="visible-lg-inline"><br></span>и услуги</li>
			<li class="sites-item">Привлекать клиентов <span class="visible-lg-inline"><br></span>и повышать продажи</li>
			<li class="sites-item">Собирать большую <span class="visible-lg-inline"><br></span>аудиторию</li>
			<li class="sites-item">Выводить на рынок <span class="visible-lg-inline"><br></span>новые бренды</li>
			<li class="sites-item">Поддерживать работу <span class="visible-lg-inline"><br></span>дилерской сети</li>
			<li class="sites-item">Упрощать общение <span class="visible-lg-inline"><br></span>с клиентами</li>
		</ul>
	</div>
</div>

<div class="solutions container-fluid">
	<div class="container">
	  	<a class="page-anchor" data-name="Какой сайт нужен вам?" name="prices"></a>
		<h2>А какой сайт нужен вам?</h2>
		<div class="row">
			<div class="col-lg-4 col-md-4 col-sm-6 col-xs-6">
				<div class="solutions-item">
				<div class="solutions-header">Лендинг пейдж</div>
				<div class="solutions-ico"><div class="center"><img src="assets/img/web/solutions1.png"></div></div>
				<div class="solutions-text">
					<div class="text-center"><span class="f-16">от</span> <span class="font-regular f-30">40 000</span> <i class="fa fa-rub lh-12 f-16"></i></div>
					<ul class="list list-arrow">
						<li>Индивидуальный дизайн</li>
						<li>Продающие тексты</li>
						<li>Система управления</li>
					</ul>
				</div>
			</div>
			</div>
			<div class="col-lg-4 col-md-4 col-sm-6 col-xs-6">
				<div class="solutions-item">
				<div class="solutions-header">Корпоративный сайт</div>
				<div class="solutions-ico"><div class="center"><img src="assets/img/web/solutions2.png"></div></div>
				<div class="solutions-text">
					<div class="text-center"><span class="f-16">от</span> <span class="font-regular f-30">80 000</span> <i class="fa fa-rub lh-12 f-16"></i></div>
					<ul class="list list-arrow-blue"> 
						<li>Индивидуальный дизайн</li>
						<li>Система управления</li>
					</ul>
				</div>
			</div>
			</div>
			<div class="col-lg-4 col-md-4 col-sm-6 col-xs-6">
				<div class="solutions-item">
				<div class="solutions-header">Отраслевое решение</div>
				<div class="solutions-ico"><div class="center"><img src="assets/img/web/solutions3.png"></div></div>
				<div class="solutions-text">
					<div class="text-center"><span class="f-16">от</span> <span class="font-regular f-30">90 000</span> <i class="fa fa-rub lh-12 f-16"></i></div>
					<ul class="list list-arrow-green">
						<li>Индивидуальный дизайн</li>
						<li>Система управления</li>
						<li>Каталог продукции</li>
					</ul>
				</div>
			</div>
			</div>
			<div class="col-lg-4 col-md-4 col-sm-6 col-xs-6">
				<div class="solutions-item">
				<div class="solutions-header">Интернет-магазин</div>
				<div class="solutions-ico"><div class="center"><img src="assets/img/web/solutions4.png"></div></div>
				<div class="solutions-text">
					<div class="text-center"><span class="f-16">от</span> <span class="font-regular f-30">130 000</span> <i class="fa fa-rub lh-12 f-16"></i></div>
					<ul class="list list-arrow-violet">
						<li>Индивидуальный дизайн</li>
						<li>Система управления</li>
						<li>Каталог с корзиной заказа и поиском товара</li>
					</ul>
				</div>
				</div>
			</div>
			<div class="col-lg-4 col-md-4 col-sm-6 col-xs-6">
				<div class="solutions-item">
				<div class="solutions-header">Промо-сайт</div>
				<div class="solutions-ico"><div class="center"><img src="assets/img/web/solutions5.png"></div></div>
				<div class="solutions-text text-default">
					<div class="text-center"><span class="f-16">от</span> <span class="font-regular f-30">250 000</span> <i class="fa fa-rub lh-12 f-16"></i></div>
					<ul class="list list-arrow-yellow">
						<li>Индивидуальный дизайн</li>
						<li>Разработка концепции</li>
						<li>Система управления</li>
					</ul>
				</div>
			</div>
			</div>
			<div class="col-lg-4 col-md-4 col-sm-6 col-xs-6">
				<div class="solutions-item">
				<div class="solutions-header">Нечто необычное</div>
				<div class="solutions-ico"><div class="center"><img src="assets/img/web/solutions6.png"></div></div>
				<div class="solutions-text text-center font-regular lh-14">
					<div class="space-5"></div>
					<div>У вас есть необычная задача?</div>
					<div class="torquise margin-bottom-15">Обсудим. Продумаем. И сделаем!</div>
					<button class="button button-empty torquise fb-form" data-fancybox-href="#form-popup-default">Свяжитесь с нами</button>
				</div>
			</div>
			</div>
		</div>
		<div class="solutions-button">
			<a href="#">
				<button class="button button-red"><img class="margin-top-m5" src="assets/img/ico_suitcase.png">&nbsp;&nbsp;Посмотреть наши работы</button>
			</a>
		</div>
	</div>
</div>


<div class="container margin-top-30">
  	<h2 class="text-center">Заказать сайт или задать вопрос:</h2>
	<form class="form-ajax" action="./" method="POST">
		<input type="hidden" name="form" value="">
		<input type="hidden" name="required" value="Name, Email, Phone, Question">
		<input type="hidden" name="action" value="doPostForm">
		<input type="hidden" name="mailSubject" value="Запрос на сайте webisgroup.ru">
		<div class="row">
			<div class="col-lg-8 col-md-10 col-lg-offset-2 col-md-offset-1">
				<div class="row">
					<div class="col-md-6">
						<div class="input-spacing">
							<div class="input-group input-group-before">
								<span class="input-group-addon"><img src="assets/img/ico_input_user_red.png" alt="Ваше имя"></span>
								<input data-required="Name" placeholder="Как вас зовут?" type="text" name="Name" value="" class="input">
							</div>
						</div>
						<div class="input-spacing">
							<div class="input-group input-group-before">
								<span class="input-group-addon"><img src="assets/img/ico_input_phone_red.png" alt="Ваш телефон"></span>
								<input data-required="Phone" placeholder="Ваш телефон" type="text" name="Phone" value="" class="input">
							</div>
						</div>
						<div class="input-spacing">
							<div class="input-group input-group-before">
								<span class="input-group-addon"><img src="assets/img/ico_input_envelope_red.png" alt="Ваш email"></span>
								<input data-required="Email" placeholder="Ваш Email" type="text" name="Email" value="" class="input">
							</div>
						</div>
					</div>
					<div class="col-md-6 input-spacing input-spacing-md-0">
						<textarea data-required="Question" name="Question" placeholder="Пожалуйста, кратко опишите вашу задачу" class="input height-as-input-3"></textarea>
					</div>
				</div>
				<div class="text-center input-spacing-last">
					<button type="submit" class="button button-red-transparent">Отправить заявку</button>
				</div>
			</div>
		</div>
	</form>
</div>


<!--approaches start-->
<div class="container approaches">
	<h2>
		<a class="page-anchor" data-name="Наш подход к созданию сайтов" name="approaches"></a>
		Наш подход к созданию сайтов
	</h2>
	<div class="row approaches-item">
		<div class="approaches-img col-lg-6 col-md-6 col-sm-12 col-xs-12">
			<img class="img-responsive" src="assets/img/web/approaches1_1.png">
		</div>
		<div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 approaches-content">
			<div class="approaches-header">Прототипирование</div>
			<div class="approaches-text">
				<p>Разработка прототипа сайта или приложения нужна для того, чтобы и разработчик, и заказчик имели чёткое представление об основных особенностях и возможностях проекта. </p>
				<p>Прототипирование позволяет отлаживать внешний вид и структуру интерфейсов на ранних этапах разработки, существенно снижая время согласования и разработки проекта. </p>
			</div>
		</div>
	</div>
	<div class="row row-right-md approaches-item">
		<div class="approaches-img col-lg-6 col-md-6 col-sm-12 col-xs-12">
			<img class="img-responsive" src="assets/img/web/approaches2.png">
		</div>
		<div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 approaches-content">
			<div class="approaches-header">Проработка пользовательских сценариев</div>
			<div class="approaches-text">
				<p>Целью взаимодействия посетителя с сайтом может быть контакт с компанией, покупка в интернет-магазине, регистрация или другое действие.</p>
				<p>Мы моделируем сценарии поведения на сайте для различных групп пользователей и планируем навигацию и представление информации таким образом, чтобы сайт приводил пользователей к запланированной цели.</p>
			</div>
		</div>
	</div>
	<div class="row approaches-item">
		<div class="approaches-img col-lg-6 col-md-6 col-sm-12 col-xs-12">
			<img class="img-responsive" src="assets/img/web/approaches3.png">
		</div>
		<div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 approaches-content">
			<div class="approaches-header">Дизайн</div>
			<div class="approaches-text">
				<p>Качественный дизайн сайта - это гораздо больше, чем красивая и оригинальная картинка.</p>
				<p>Он должен разговаривать с целевой аудиторией, акцентировать преимущества ваших предложений, помогать посетителям получить нужную информацию и принять решение о заказе.</p>
			</div>
		</div>
	</div>
	<div class="row row-right-md approaches-item">
		<div class="approaches-img col-lg-6 col-md-6 col-sm-12 col-xs-12">
			<img class="img-responsive" src="assets/img/web/approaches4.png">
		</div>
		<div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 approaches-content">
			<div class="approaches-header">Agile-разработка</div>
			<div class="approaches-text">
				<p>Гибкая методология или agile development - особая организация процесса разработки, основанная на итерациях.</p>
				<p>Agile-методы позволяет свести к минимуму риски, возникающие при разработке сложных и протяжённых во времени проектов.</p>
				<p>Заказчику предоставляются инструменты эффективного контроля над выполнением работ, так как уже после первой итерации проект готов к выпуску.</p>
			</div>
		</div>
	</div>
	<div class="row approaches-item">
		<div class="approaches-img col-lg-6 col-md-6 col-sm-12 col-xs-12">
			<img class="img-responsive" src="assets/img/web/approaches5.png">
		</div>
		<div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 approaches-content">
			<div class="approaches-header">Техническая реализация</div>
			<div class="approaches-text">
				<h4 class="red">На Abante CMS</h4>
				<div>Abante CMS - собственная разработка Webis Group с многолетним опытом успешных внедрений в самых разных сферах бизнеса.<br><a class="default" href="#">Узнать больше</a></div>
				<h4 class="orange">На &laquo;1C битрикс&raquo;</h4>
				<div>Широко известная  система управления сайтом, предоставляющая богатые возможности для разработки и поддержки корпоративных сайтов, интернет-магазинов, порталов и сообществ.</div>
			</div>
		</div>
	</div>
	<div class="row row-right-md approaches-item">
		<div class="approaches-img col-lg-6 col-md-6 col-sm-12 col-xs-12">
			<img class="img-responsive" src="assets/img/web/approaches6.png">
		</div>
		<div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 approaches-content">
			<div class="approaches-header">Тестирование сайта</div>
			<div class="approaches-text">
				<p>После сборки сайт проходит несколько этапов тестирования, в зависимости от его назначения: функциональное тестирование, нагрузочное тестирование, тестирование на уязвимости.</p>
				<p>Мы тестируем сайты во всех браузерах, что минимизирует дефекты верстки. Проверяем юзабилити всех пользовательских сценариев и удобство навигации, чтобы посещение сайта было приятным.</p>
			</div>
		</div>
	</div>
	<div class="row approaches-item">
		<div class="approaches-img col-lg-6 col-md-6 col-sm-12 col-xs-12">
			<img class="img-responsive" src="assets/img/web/approaches72.png">
		</div>
		<div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 approaches-content">
			<div class="approaches-header">Поддержка сайта</div>
			<div class="approaches-text">
				<p>Мы делаем такие сайты, которые позволяют владельцам самостоятельно управлять ими без привлечения специалистов.</p>
				<p>Все наши клиенты пользуются преимуществами бессрочной и бесплатной гарантийной поддержки и консультаций.</p>
				<p>Мы можем взять на себя все работы по текущей эксплуатации сайта (например, обновление информации) и по его развитию. </p>
				<p>Даже если ваш сайт был сделан не нами - мы сможем заняться его поддержкой и развитием.</p>
			</div>
		</div>
	</div>
</div>
<!--approaches end-->

<div class="gift container">
	<h2 class="text-center margin-bigger">Заказывая сайт у нас<span class="hidden-sm hidden-xs"><br></span> вы получаете приятные бонусы:</h2>
	<div class="row">
    	<div class="gift-img col-md-4 col-md-offset-2">
      		<img class="img-responsive m-auto" src="assets/img/ecommerce/gift.png"></img>
    	</div>
    	<div class="gift-items col-md-5">
      		<div class="gift-item">
        		<div class="gift-item-img">
          			<img class="img-responsive" src="assets/img/ecommerce/webisonline.png" alt="">
        		</div>
        		<div class="gift-item-text">
           			Бесплатное подключение онлайн-косультанта Web<span style="color: #81aa00;">is</span>Online для 5 операторов сроком на 1 год
        		</div>
      		</div>
      		<div class="gift-item">
        		<div class="gift-item-img">
          			<img class="img-responsive" src="assets/img/ecommerce/google_analytics.png" alt="">
        		</div>
        		<div class="gift-item-text">
          			Установка и настройка <span style="color: #1250f1;">G</span><span style="color: #f12a34;">o</span><span style="color: #ffbe15;">o</span><span style="color: #1950ff;">g</span><span style="color: #009e15;">l</span><span style="color: #ed3334;">e</span> <span style="color: #4e4e4e;">Analitycs</span>
        		</div>
      		</div>
      		<div class="gift-item">
        		<div class="gift-item-img">
          			<img class="img-responsive" src="assets/img/ecommerce/google_adwords.png">
        		</div>
        		<div class="gift-item-text">
           			Подготовка медиаплана для <span style="color: #dc0000;">Я</span>ндекс директ и <span style="color: #1250f1;">G</span><span style="color: #f12a34;">o</span><span style="color: #ffbe15;">o</span><span style="color: #1950ff;">g</span><span style="color: #009e15;">l</span><span style="color: #ed3334;">e</span> <span style="color: #4e4e4e;">Adwords</span>
        		</div>
      		</div>
    	</div>
  	</div>
</div>

<div class="space-20"></div>

<div class="container-fluid reviews">
	<h3 class="h2">Отзывы</h3>
	<div class="container">
	   <div class="reviews-block">
            <div id="reviews-web" class="owl-carousel owl-theme">
                <div class="reviews-item">
                    <div>
                        <div class="reviews-img">      
                           <img src="assets/img/temp/logo.png" alt="">
                        </div>
                        <br>
                        <p>Мы хотим сказать огромное спасибо за особенное внимание к деталям, из которых строится успешный проект, нестандартный подход к решениям задач и контроль качества на всех этапах работы.</p>
                    </div>                     
                </div>
                <div class="reviews-item">
                    <div>
                        <div class="reviews-img">      
                           <img src="assets/img/temp/logo6.png" alt="">
                        </div>
                        <br>
                        <p>What we've found with Webis Group is an extremely creative approach with results delivered in time. Such partnership made it possible to deliver excellent results to our clients.</p>
                    </div>                     
                </div>
                <div class="reviews-item">
                    <div>
                        <div class="reviews-img">      
                           <img src="assets/img/temp/logo3.png" alt="">
                        </div>
                        <br>
                        <p>Сотрудники-компании разработчика продемонстрировали не только высокий профессионализм, но и очевидную готовность работать с нестандартными задачами.</p>
                    </div>                     
                </div>
            </div>
        </div>
    </div>
</div>

<div class="container-fluid advantages">
	<div class="container">
		<div class="row">
			<div class="advantages-item col-lg-3 col-md-3 col-sm-6 col-xs-12">
				<div class="advantages-img">
					<div class="center">
						<img src="assets/img/web/advantages1.png">
					</div>
				</div>
				<div class="advantages-text">
					Входим в ТОП 100 компаний, работающих с крупнейшими организациями России
				</div>
			</div>
			<div class="advantages-item col-lg-3 col-md-3 col-sm-6 col-xs-12">
				<div class="advantages-img">
					<div class="center">
						<img src="assets/img/web/advantages2.png">
					</div>
				</div>
				<div class="advantages-text ">
					Собственные технологии разработки - умеем делать сложные проекты
				</div>
			</div>
			<div class="advantages-item col-lg-3 col-md-3 col-sm-6 col-xs-12">
				<div class="advantages-img">
					<div class="center">
						<img src="assets/img/web/advantages3.png">
					</div>
				</div>
				<div class="advantages-text">
					Реализуем ваши идеи дополнив нашим опытом и знаниями
				</div>
			</div>
			<div class="advantages-item col-lg-3 col-md-3 col-sm-6 col-xs-12">
				<div class="advantages-img">
					<div class="center">
						<img src="assets/img/web/advantages4.png">
					</div>
				</div>
				<div class="advantages-text">
					Создаем продающие сайты автоматизируем продажи
				</div>
			</div>
		</div>
	</div>
</div>
<div class="space-30"></div>
@stop