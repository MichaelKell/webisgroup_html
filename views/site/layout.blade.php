<!DOCTYPE html>
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <meta id="viewport" name="viewport" content="width=device-width, initial-scale=1">
	<title>{{$Title}}</title>
	<meta name="keywords" content="">
	<meta name="description" content="">
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Roboto:300,300i,400,700,700i">
    <link rel="stylesheet" href="assets/plugins/font-awesome/css/font-awesome.min.css">
    @yield('head-styles-before')
    <link rel="stylesheet" href="assets/plugins/bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet" href="assets/plugins/owl/owl-carousel/owl.carousel.css">
    <link rel="stylesheet" href="assets/plugins/owl/owl-carousel/owl.theme.css">
    <link rel="stylesheet" href="assets/plugins/owl/owl-carousel/owl.transitions.css">
    <link rel="stylesheet" href="assets/plugins/fancybox/jquery.fancybox.css">
    <link rel="stylesheet" href="assets/plugins/cube/cubeportfolio_latest.min.css">
    <link rel="stylesheet" href="assets/plugins/smoothdivscroll/smoothDivScroll.css">
    <link rel="stylesheet" href="assets/plugins/formstyler/jquery.formstyler.css">
    <link rel="stylesheet" href="assets/css/helpers.css">
    <link rel="stylesheet" href="assets/css/theme.css">
    @yield('head-styles')
    <script src="assets/plugins/js_errors.js"></script>
</head>
<body>
    @include('partials._nav')
    <main>
        @yield('content')
    </main>
    <footer class="footer">
        <div class="container">
            <div class="row">
                <div class="col-md-offset-2 col-sm-offset-1 col-md-8 col-sm-10 col-xs-12">
                    <div class="footer-form">
                        <h2 class="margin-top-0 margin-bottom-0">Интересны наши услуги?</h2>
                        <p class="margin-top-md-30 margin-top-sm-20 margin-top-xs-15 margin-bottom-md-40 margin-bottom-sm-30 margin-bottom-xs-20 text-center">
                        Мы предлагаем оптимальное соотношение цены и качества.<span class="hidden-xs"><br></span>
                        Оставьте заявку или позвоните нам по телефону +7 (495) 636-29-78,<span class="hidden-xs"><br></span>
                        мы подготовим для вас коммерческое предложение<span class="hidden-xs"><br></span>
                        с расчетом стоимости и сроков.
                        </p>
                        <div class="text-center">
                            <button class="button button-red-transparent fb-form" data-fancybox-href="#form-popup-default">оставить заявку</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="container margin-top-55">
            <div class="row">
                <div class="col-md-2 col-sm-4 col-xs-6">
                    <div class="footer-brand">
                        <img src="assets/img/logo_footer.png" alt="Webis Group">
                    </div>
                    <div class="footer-phone nowrap">+ 7 (495) 636-29-78</div>
                </div>
                <div class="col-md-10 col-sm-8 col-xs-6">
                    <ul class="footer-menu">
                        <li><a href="#">Портфолио</a></li>
                        <li><a href="#">Компания</a></li>
                        <li><a href="#">Клиенты</a></li>
                        <li><a href="#">Компания</a></li>
                    </ul>   
                </div>
            </div>
        </div>
    </footer>
    <div class="display-none">
        <div id="form-popup-default">
            <form class="form-ajax" action="./" method="POST">
                <input type="hidden" name="form" value="">
                <input type="hidden" name="popup_id" value="form-popup-default">
                <input type="hidden" name="required" value="Name, Email, Phone, Question">
                <input type="hidden" name="action" value="doPostForm">
                <input type="hidden" name="ans" value="">
                <input type="hidden" name="mailSubject" value="Запрос на сайте webisgroup.ru">
                <h3 class="margin-top-0">Быстрая связь с компанией</h3>
                <div class="row">
                    <div class="col-md-6">
                        <div class="input-spacing">
                            <div class="input-group input-group-before">
                                <span class="input-group-addon"><img src="assets/img/ico_input_user_red.png" alt="Ваше имя"></span>
                                <input data-required="Name" placeholder="Как вас зовут?" type="text" name="Name" value="" class="input">
                            </div>
                        </div>
                        <div class="input-spacing">
                            <div class="input-group input-group-before">
                                <span class="input-group-addon"><img src="assets/img/ico_input_phone_red.png" alt="Ваш телефон"></span>
                                <input data-required="Phone" placeholder="Ваш телефон" type="text" name="Phone" value="" class="input">
                            </div>
                        </div>
                        <div class="input-spacing">
                            <div class="input-group input-group-before">
                                <span class="input-group-addon"><img src="assets/img/ico_input_envelope_red.png" alt="Ваш email"></span>
                                <input data-required="Email" placeholder="Ваш Email" type="text" name="Email" value="" class="input">
                            </div>
                        </div>
                        <div class="input-spacing">
                            <div class="input-heading">Сколько будет 8 + 6 ?</div>
                            <input data-required="answer" placeholder="Введите число" type="text" name="Answer" value="" class="input">
                        </div>
                    </div>
                    <div class="col-md-6 input-spacing input-spacing-md-0">
                        <textarea data-required="Question" name="Question" placeholder="Пожалуйста, кратко опишите вашу задачу" class="input height-as-input-4-heading-1"></textarea>
                    </div>
                </div>
               <div class="text-center input-spacing-last">
                    <button type="submit" class="button button-red-transparent">Отправить заявку</button>
                </div>
            </form>
        </div>
    </div>
    <script type="text/javascript" src="assets/plugins/jquery-1.11.1.min.js"></script>  
    <script type="text/javascript" src="assets/plugins/fancybox/jquery.fancybox.pack.js"></script>          
    <script type="text/javascript" src="assets/plugins/bootstrap/js/bootstrap.min.js"></script>
    <script type="text/javascript" src="assets/plugins/jquery.flexslider.js"></script>
    <script type="text/javascript" src="assets/plugins/jquery.transit.min.js"></script>
    <script type="text/javascript" src="assets/plugins/owl/owl-carousel/owl.carousel.js"></script>
    <script type="text/javascript" src="assets/plugins/jquery.easing.1.3.js" type="text/javascript"></script>
    <script type="text/javascript" src="assets/plugins/spin.js"></script>
    <script type="text/javascript" src="assets/plugins/smoothdivscroll/jquery.kinetic.js"></script>
    <script type="text/javascript" src="assets/plugins/smoothdivscroll/jquery.mousewheel.js"></script>
    <script type="text/javascript" src="assets/plugins/smoothdivscroll/jquery-ui-1.10.3.custom.js"></script>
    <script type="text/javascript" src="assets/plugins/smoothdivscroll/jquery.smoothDivScroll-1.3.js"></script>
    <script type="text/javascript" src="assets/plugins/jquery-ui.js"></script>
    <script type="text/javascript" src="assets/plugins/cube/jquery.cubeportfolio_latest.min.js"></script>
    <script type="text/javascript" src="assets/plugins/tinynav.min.js"></script>
    <script type="text/javascript" src="assets/plugins/formstyler/jquery.formstyler.min.js"></script>
    <script type="text/javascript" src="assets/plugins/preload.js"></script>
    <script type="text/javascript" src="assets/js/helpers.js"></script>  
    <script type="text/javascript" src="assets/js/modules.js"></script>
    @yield('footer')
    <script type="text/javascript" src="assets/js/init.js"></script>
</body>
</html>