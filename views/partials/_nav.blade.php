<nav class="navbar-fixed-top">
    <div class="container">
        <a href="/" class="navbar-brand">
            <img src="assets/img/webisgroup.png" alt="Webis Group">
        </a>
        <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar-mainmenu-collapse" aria-expanded="false">
                <span class="sr-only">Меню</span>
                <span class="menu-icon"></span>
            </button>
        </div>
        <div class="collapse navbar-collapse" id="navbar-mainmenu-collapse">
            <ul class="nav navbar-nav">
                <li  class="navbar-item">
                    <a href="/portfolio">
                        Портфолио
                    </a>
                </li>
                <li  class="navbar-item">
                    <a href="/about">
                        Компания
                    </a>
                </li>
                <li class="navbar-item dropdown dropdown-services">
                    <a href="javascript:void(0)">
                        Услуги <b class="caret"></b>
                    </a>
                    <div class="dropdown-menu dropdown-menu-services">
                        <div class="dropdown-menu-col">
                            <div class="dropdown-menu-col-header">Разработка</div>
                            <ul>
                                <li class="dropdown-menu-item-service">
                                    <span class="dropdown-menu-item-service-inner">
                                        <a href="/web">
                                            <span class="dropdown-menu-item-service-ico">
                                                <img src="assets/img/ico_services1.png" alt="">
                                            </span>
                                            <span class="dropdown-menu-item-service-header">Создание сайтов</span>
                                        </a>
                                    </span>
                                </li>
                                <li class="dropdown-menu-item-service">
                                    <span class="dropdown-menu-item-service-inner">
                                        <a href="/ecommerce">
                                            <span class="dropdown-menu-item-service-ico">
                                                <img src="assets/img/ico_services2.png" alt="">
                                            </span>
                                            <span class="dropdown-menu-item-service-header">Создание интернет-магазинов</span>
                                        </a>
                                    </span>
                                </li>
                                <li class="dropdown-menu-item-service">
                                    <span class="dropdown-menu-item-service-inner">
                                        <a href="/mobile_sites">
                                            <span class="dropdown-menu-item-service-ico">
                                                <img src="assets/img/ico_services3.png" alt="">
                                            </span>
                                            <span class="dropdown-menu-item-service-header">Адаптация сайтов для мобильных устройств</span>
                                        </a>
                                    </span>
                                </li>
                                <li class="dropdown-menu-item-service">
                                    <span class="dropdown-menu-item-service-inner">
                                        <a href="/fstyle">
                                            <span class="dropdown-menu-item-service-ico">
                                                <img src="assets/img/ico_services4.png" alt="">
                                            </span>
                                            <span class="dropdown-menu-item-service-header">Фирменный стиль</span>
                                        </a>
                                    </span>
                                </li>
                                <li class="dropdown-menu-item-service">
                                    <span class="dropdown-menu-item-service-inner">
                                        <a href="/mobile_apps">
                                            <span class="dropdown-menu-item-service-ico">
                                                <img src="assets/img/ico_services5.png" alt="">
                                            </span>
                                            <span class="dropdown-menu-item-service-header">Создание мобильных приложений</span>
                                        </a>
                                    </span>
                                </li>
                                <li class="dropdown-menu-item-service">
                                    <span class="dropdown-menu-item-service-inner">
                                        <a href="/pc">
                                            <span class="dropdown-menu-item-service-ico">
                                                <img src="assets/img/ico_services6.png" alt="">
                                            </span>
                                            <span class="dropdown-menu-item-service-header">Разработка программных комплексов</span>
                                        </a>
                                    </span>
                                </li>
                            </ul>
                        </div>
                        <div class="dropdown-menu-col">
                            <div class="dropdown-menu-col-header">Реклама и продвижение</div>
                            <ul>
                                <li class="dropdown-menu-item-service">
                                    <span class="dropdown-menu-item-service-inner">
                                        <a href="/seo">
                                            <span class="dropdown-menu-item-service-ico">
                                                <img src="assets/img/ico_services9.png" alt="">
                                            </span>
                                            <span class="dropdown-menu-item-service-header">Продвижение сайтов и SEO копирайтинг</span>
                                        </a>
                                    </span>
                                </li>
                                <li class="dropdown-menu-item-service">
                                    <span class="dropdown-menu-item-service-inner">
                                        <a href="#">
                                            <span class="dropdown-menu-item-service-ico">
                                                <img src="assets/img/ico_services8.png" alt="">
                                            </span>
                                            <span class="dropdown-menu-item-service-header">Контекстная реклама</span>
                                        </a>
                                    </span>
                                </li>
                                <li class="dropdown-menu-item-service">
                                    <span class="dropdown-menu-item-service-inner">
                                        <a href="/socials">
                                            <span class="dropdown-menu-item-service-ico">
                                                <img src="assets/img/ico_services11.png" alt="">
                                            </span>
                                            <span class="dropdown-menu-item-service-header">Продвижение в социальных сетях</span>
                                        </a>
                                    </span>
                                </li>
                                <li class="dropdown-menu-item-service">
                                    <span class="dropdown-menu-item-service-inner">
                                        <a href="/media">
                                            <span class="dropdown-menu-item-service-ico">
                                                <img src="assets/img/ico_services12.png" alt="">
                                            </span>
                                            <span class="dropdown-menu-item-service-header">Медийная реклама в интернете</span>
                                        </a>
                                    </span>
                                </li>
                                <li class="dropdown-menu-item-service">
                                    <span class="dropdown-menu-item-service-inner">
                                        <a href="/conversion">
                                            <span class="dropdown-menu-item-service-ico">
                                                <img src="assets/img/ico_services10.png" alt="">
                                            </span>
                                            <span class="dropdown-menu-item-service-header">Повышение конверсии и маркетинговый аудит</span>
                                        </a>
                                    </span>
                                </li>
                                <li class="dropdown-menu-item-service">
                                    <span class="dropdown-menu-item-service-inner">
                                        <a href="/analytics">
                                            <span class="dropdown-menu-item-service-ico">
                                                <img src="assets/img/ico_services21.png" alt="">
                                            </span>
                                            <span class="dropdown-menu-item-service-header">Веб аналитика</span>
                                        </a>
                                    </span>
                                </li>
                            </ul>
                        </div>
                        <div class="dropdown-menu-col">
                            <div class="dropdown-menu-col-header">Поддержка</div>
                            <ul>
                                <li class="dropdown-menu-item-service">
                                    <span class="dropdown-menu-item-service-inner">
                                        <a href="/photovideo">
                                            <span class="dropdown-menu-item-service-ico">
                                                <img src="assets/img/ico_services18.png" alt="">
                                            </span>
                                            <span class="dropdown-menu-item-service-header">Фото- и видеопроизводство</span>
                                        </a>
                                    </span>
                                </li>
                                <li class="dropdown-menu-item-service">
                                    <span class="dropdown-menu-item-service-inner">
                                        <a href="/presentations">
                                            <span class="dropdown-menu-item-service-ico">
                                                <img src="assets/img/ico_services20.png" alt="">
                                            </span>
                                            <span class="dropdown-menu-item-service-header">Создание презентаций</span>
                                        </a>
                                    </span>
                                </li>
                                <li class="dropdown-menu-item-service">
                                    <span class="dropdown-menu-item-service-inner">
                                        <a href="/support">
                                            <span class="dropdown-menu-item-service-ico">
                                                <img src="assets/img/ico_services15.png" alt="">
                                            </span>
                                            <span class="dropdown-menu-item-service-header">Поддержка сайтов</span>
                                        </a>
                                    </span>
                                </li>
                                <li class="dropdown-menu-item-service">
                                    <span class="dropdown-menu-item-service-inner">
                                        <a href="/hosting">
                                            <span class="dropdown-menu-item-service-ico">
                                                <img src="assets/img/ico_services16.png" alt="">
                                            </span>
                                            <span class="dropdown-menu-item-service-header">Хостинг</span>
                                        </a>
                                    </span>
                                </li>
                                <li class="dropdown-menu-item-service">
                                    <span class="dropdown-menu-item-service-inner">
                                        <a href="/3d">
                                            <span class="dropdown-menu-item-service-ico">
                                                <img src="assets/img/ico_services14.png" alt="">
                                            </span>
                                            <span class="dropdown-menu-item-service-header">3D моделирование</span>
                                        </a>
                                    </span>
                                </li>
                                <li class="dropdown-menu-item-service">
                                    <span class="dropdown-menu-item-service-inner">
                                        <a href="/consulting">
                                            <span class="dropdown-menu-item-service-ico">
                                                <img src="assets/img/ico_services17.png" alt="">
                                            </span>
                                            <span class="dropdown-menu-item-service-header">Внедренческий IT-консалтинг</span>
                                        </a>
                                    </span>
                                </li>
                            </ul>
                        </div>
                        <div class="clearfix"></div>
                    </div>
                </li>
                <li  class="navbar-item">
                    <a href="/clients">
                        Клиенты
                    </a>
                </li>
                <li  class="navbar-item">
                    <a href="/contact">
                        Контакты
                    </a>
                </li>
                <li class="navbar-phone">
                    <a href="tel:84956362978">
                        <div class="navbar-phone-ico"><img src="assets/img/phone_ico.png" alt="телефон"></div>
                        <div class="navbar-phone-text">+7 (495) 636-29-78</div>
                    </a>   
                </li>
                <li class="navbar-contact">
                    <button class="button button-empty button-small f-16 fb-form" data-fancybox-href="#form-popup-default">оставить заявку</button>
                </li>
                <li class="navbar-lang">
                    <a href="#">
                        <span class="navbar-lang-line">ENG</span>
                    </a>
                </li>
            </ul>
        </div>
    </div>
</nav>