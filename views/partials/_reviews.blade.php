<div class="reviews">
    <div class="container text-center">
        <h2>Отзывы клиентов</h2>
        <div class="row">
            <div id="reviews" class="owl-carousel">
                <div>
                    <div class="col-md-8 col-md-offset-2 reviews-item">  
                        <div class="reviews-item-img">
                            <img src="assets/img/temp/logo.png" alt="">
                        </div>
                        <br>
                        <p>Мы хотим сказать огромное спасибо за особенное внимание к деталям, из которых строится успешный проект, нестандартный подход к решениям задач и контроль качества на всех этапах работы.</p>                                    
                    </div>                     
                </div>
                <div>
                    <div class="col-md-8 col-md-offset-2 reviews-item">  
                        <div class="reviews-item-img">
                            <img src="assets/img/temp/logo6.png" alt="">
                        </div>
                        <br>
                        <p>What we've found with Webis Group is an extremely creative approach with results delivered in time. Such partnership made it possible to deliver excellent results to our clients.</p>                                    
                    </div>                     
                </div>
                <div>
                    <div class="col-md-8 col-md-offset-2 reviews-item">  
                        <div class="reviews-item-img">
                            <img src="assets/img/temp/logo3.png" alt="">
                        </div>
                        <br>
                        <p>Сотрудники-компании разработчика продемонстрировали не только высокий профессионализм, но и очевидную готовность работать с нестандартными задачами.</p>
                    </div>                     
                </div>
            </div>
        </div>
    </div>
</div>