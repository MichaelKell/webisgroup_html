@import '../mixins.scss';

$stages-opacity: 0.82;
.stages{
	h2{
		margin-top: 50px;
		margin-bottom: 30px;
	}
	&-inner{
		background: url(../img/seo/stages.jpg) top right no-repeat;
		background-size: cover;
		padding: 30px 0;
		overflow: hidden;
		@media (max-width: 767px){
			background: #f2f2f2;
		}
	}
	h2{
		color: #009fe3;
	}
	&-item{
		margin-bottom: 8px;
		color: #fff;
		&-inner{
			width: 90%;
			@media (max-width: 991px){
				width: 100%;
			}
			display: table;
			position: relative;
			height: 187px;
		}
		& &-inner:after{
			content: '';
			position: absolute;
			opacity: 0.92;
			@media (max-width: 1199px){
				display: none;
			}
		}
		& &-inner:before{
			content: '';
			position: absolute;
			width: 300px;
			height: 100%;
			top: 0;
			right: -300px;
			background: rgba(1, 140, 198, $stages-opacity);
			@media (min-width: 1199px){
				display: none;
			}
			@media (max-width: 600px){
				display: none;
			}
		}
		&:nth-child(1) &-inner:after{
			width: 684px;
			height: 227px;
			background: url(../img/seo/after1.png) no-repeat;
			bottom: 0;
			right: -684px;
		}
		&:nth-child(2) &-inner:after{
			width: 765px;
			height: 429px;
			background: url(../img/seo/after2.png) no-repeat;
			bottom: 0;
			right: -765px;
		}
		&:nth-child(3) &-inner:after{
			width: 765px;
			height: 353px;
			background: url(../img/seo/after3.png) no-repeat;
			bottom: 0;
			right: -765px;
		}
		&:nth-child(4) &-inner:after{
			width: 765px;
			height: 301px;
			background: url(../img/seo/after4.png) no-repeat;
			bottom: -23px;
			right: -765px;
		}
		&:nth-child(5) &-inner:after{
			width: 765px;
			height: 289px;
			background: url(../img/seo/after5.png) no-repeat;
			top: 0px;
			right: -765px;
		}
		&:nth-child(6) &-inner:after{
			width: 765px;
			height: 233px;
			background: url(../img/seo/after6.png) no-repeat;
			top: 0px;
			right: -765px;
		}
	}
	&-arrow, &-number, &-content{
		display: table-cell;
	}
	&-content{
		padding: 21px 0;
		padding-right: 21px;
		background: linear-gradient(to left, rgba(1, 140, 198, $stages-opacity), rgba(89, 175, 209, $stages-opacity));
		@media (max-width: 350px){
			padding-left: 21px;
		}
	}
	&-number{
		background: rgba(89, 175, 209, $stages-opacity);
		width: 92px;
		min-width: 92px;
		vertical-align: middle;
		text-align: center;
		font-size: 42px;
		@include font('bold');
		@media (max-width: 600px){
			width: 60px;
			min-width: 60px;
			vertical-align: top;
			padding-top: 21px;
		}
		@media (max-width: 350px){
			display: none;
		}
	}
	&-arrow{
		width: 127px;
		min-width: 127px;
		background: url(../img/seo/arrow.png) right center no-repeat;
		background-size: auto 100%;
		@media (max-width: 1440px){
			width: 80px;
			min-width: 80px;
		}
		@media (max-width: 600px){
			display: none;
		}
	}
	&-header{
		@include font('bold');
		font-size: 20px;
		text-transform: uppercase;
		margin-bottom: 8px;
	}
	&-text{
		@include font('regular');
		font-size: 18px;
	}
}