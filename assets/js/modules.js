var ajaxForm = function() {
    var obj = {};
    obj.init = function($form) {
        $form.on("submit", function() {
            var $this = $(this);
            if (!formValidator.validate($this)) {
                return false;
            }
            send($this.serialize(), function(response) {
                $.fancybox(response.html, {
                    type: 'html'
                });
                if (response.success == "1") {
                    $this.get(0).reset();
                }
            });
            return false;
        });
    };
    var send = function(data, callback) {
        $.ajax({
            url: "./",
            type: 'POST',
            data: data,
            dataType: "json",
            success: callback,
            error: function(error) {
                console.log(error);
                //alert('Try again later');
            }
        });
    };

    return obj;
}();
var buttonCollapse = function() {
    var obj = {};
    obj.init = function() {
        $(".button-collapse[data-action='show']").on("click", show);
        $(".button-collapse[data-action='hide']").on("click", hide);
        $(".button-collapse[data-anchor='" + document.location.hash + "'][data-action='show']").click();
    };
    var show = function() {
        var $this = $(this);
        var target = $this.data("target");
        var anchor = $this.data("anchor");
        var offset = $this.data("anchor-offset");
        var group = $this.data("group");
        $(".button-collapse[data-group='" + group + "']").each(function() {
            $($(this).data("target")).css({
                "display": "none"
            });
            $(".button-collapse[data-action='show'][data-target='" + $(this).data("target") + "']").fadeIn();
            $(".button-collapse[data-action='hide'][data-target='" + $(this).data("target") + "']").fadeOut();
        });
        $(target).slideDown(400, 'swing', function() {
            if (anchor !== undefined) goToAnchor(anchor, offset);
        });
        $this.fadeOut('normal', function() {
            $(".button-collapse[data-action='hide'][data-target='" + target + "']").fadeIn();
        });

    };
    var hide = function() {
        var $this = $(this);
        var target = $this.data("target");
        var anchor = $this.data("anchor");
        var offset = $this.data("anchor-offset");
        $(target).slideUp();
        if (anchor !== undefined) {
            goToAnchor(anchor, offset);
        }
        $(".button-collapse[data-action='hide'][data-target='" + target + "']").fadeOut('normal', function() {
            $(".button-collapse[data-action='show'][data-target='" + target + "']").fadeIn();
        });
        $this.fadeOut('normal', function() {
            $(".button-collapse[data-action='show'][data-target='" + target + "']").fadeIn();
        });
    };
    return obj;
}();
var fixInputHighlight = function() {
    $(".input").on("focus", function() {
        var $parent = $(this).parent();
        if (!$parent.hasClass("input-group")) {
            return;
        }
        $parent.find(".input").css({
            "borderColor": "#66afe9"
        });
        $parent.find(".input-group-addon").css({
            "borderColor": "#66afe9",
            "boxShadow": "none"
        });
        $parent.css({
            "boxShadow": 'inset 0 1px 1px rgba(0,0,0,.075), 0 0 8px rgba(102, 175, 233, .6)'
        });
    });
    $(".input").on("blur", function() {
        var $parent = $(this).parent();
        if (!$parent.hasClass("input-group")) {
            return;
        }
        $parent.find(".input").css({
            "borderColor": "#bebebe"
        });
        $parent.find(".input-group-addon").css({
            "borderColor": "#bebebe",
            "boxShadow": "inset 1px 1px 1px rgba(0, 0, 0, .075)"
        });
        $parent.css({
            "boxShadow": 'none'
        });
    });
};
var formValidator = function() {
    var obj = {};
    var errors;

    obj.init = function(opts) {
        errors = opts.errors;
        $(".form-validate").on("submit", function() {
            var $this = $(this);
            if (!validate($this))
                return false;
            $this.submit();
        });
    };
    
    obj.validate = function($form) {
        var error = "";
        var $fields = $form.find("[data-required]");
        $fields.each(function() {
            var $field = $(this);
            var val = $field.val();
            var type = $field.data('required').toLowerCase();
            if (val === "")
                error += errors[type] + "\n";
            if (type == 'email' && val !== "" && !validateEmail(val)) {
                error += "Некорректный адрес email\n";
            }
            if (type == 'answer' && val !== "" && isNaN(val))
                error += "Пожалуйста напишите ответ цифрами\n";
        });
        if (error !== "") {
            alert(error);
            return false;
        }
        return true;
    }

    var validateEmail = function(str) {
        var at = "@";
        var dot = ".";
        var lat = str.indexOf(at);
        var lstr = str.length;
        var ldot = str.indexOf(dot);
        if (str.indexOf(at) == -1) {
            return false;
        }
        if (str.indexOf(at) == -1 || str.indexOf(at) == 0 || str.indexOf(at) == lstr) {
            return false;
        }
        if (str.indexOf(dot) == -1 || str.indexOf(dot) == 0 || str.indexOf(dot) == lstr) {
            return false;
        }
        if (str.indexOf(at, (lat + 1)) != -1) {
            return false;
        }
        if (str.substring(lat - 1, lat) == dot || str.substring(lat + 1, lat + 2) == dot) {
            return false;
        }
        if (str.indexOf(dot, (lat + 2)) == -1) {
            return false;
        }
        if (str.indexOf(" ") != -1) {
            return false;
        }
        return true;
    };

    return obj;
}();
var navbar = function() {
    var obj = {};
    obj.init = function() {
        fixMobileDropdowns();
        fixMobileExpandActive();
        fixMobileExpandToFullHeight();
    };
    var fixMobileDropdowns = function() {
        $('nav').on("mouseenter mouseleave", ".navbar-collapse:not(.in) .dropdown", function() {
            $(this).toggleClass('opened js-activated');
        });
        $('nav').on("click", ".navbar-collapse.in .dropdown", function() {
            $(this).toggleClass('opened js-activated');
        });
        $('nav').on("click", ".navbar-collapse.in .dropdown>a", function(e) {
            e.preventDefault();
        });
    };
    var fixMobileExpandToFullHeight = function() {
        $(".navbar-collapse").on("show.bs.collapse", function() {
            $(this).css({
                "display": 'none'
            });
        });
        $(".navbar-collapse").on("shown.bs.collapse", function() {
            $(this).css({
                "max-height": $(window).height() - $('.navbar-header').height() - 13
            });
            $(this).slideDown();
        });
    };
    var fixMobileExpandActive = function() {
        $(".navbar-collapse").on("shown.bs.collapse", function() {
            $(this).find('.dropdown-menu .active').closest('.dropdown').addClass('opened js-activated');
        });
    };
    return obj;
}();
var resultsSlider = function() {
    var obj = {};
    var $container, $slides, charts = [];
    obj.init = function(selector, opts) {
        $container = $(selector);
        if ($container.length < 1) return false;
        $slides = $container.children();
        displaySlide($slides.eq(opts.start));
        $(".results-panel-button").on("click", moveNext);
        AmCharts.ready(initCharts);
    };
    var moveNext = function() {
        var next = getNextSlide();
        displaySlide($slides.eq(next));
        charts.forEach(function(chart) {
            chart.animateAgain();
        });
    };
    var displaySlide = function($slide) {
        $slides.css({
            "display": "none"
        });
        $slides.removeClass("active");
        $slide.fadeIn();
        $slide.addClass("active");
        updateSliderData($slide.data());
    };
    var updateSliderData = function(data) {
        $(".results-panel-project").text(data.project);
        $(".results-panel-project").attr("href", "http://" + data.project);
    };
    var getCurrentSlide = function() {
        return $container.children(".active").index();
    };
    var getNextSlide = function() {
        var current = getCurrentSlide();
        var total = $slides.length;
        var next = current + 1;
        if (next >= total) return 0;
        else
            return next;
    };
    var initCharts = function() {
        var chartOpts = {
            "type": "serial",
            "addClassNames": true,
            "theme": "light",
            "autoMargins": false,
            "marginLeft": 30,
            "marginRight": 8,
            "marginTop": 0,
            "marginBottom": 26,
            "language": "ru",
            "startDuration": 1,
            "balloon": {
                "adjustBorderColor": false,
                "horizontalPadding": 10,
                "verticalPadding": 8,
                "color": "#ffffff"
            },
            "graphs": [{
                "alphaField": "alpha",
                "balloonText": "<span style='font-size:12px;'>[[title]] in [[category]]:<br><span style='font-size:20px;'>[[value]]</span> [[additional]]</span>",
                "fillAlphas": 1,
                "title": "Income",
                "type": "column",
                "valueField": "income",
                "dashLengthField": "dashLengthColumn"
            }, {
                "id": "graph2",
                "balloonText": "<span style='font-size:12px;'>[[title]] in [[category]]:<br><span style='font-size:20px;'>[[value]]</span> [[additional]]</span>",
                "bullet": "round",
                "lineThickness": 3,
                "bulletSize": 7,
                "bulletBorderAlpha": 1,
                "bulletColor": "#FFFFFF",
                "useLineColorForBulletBorder": true,
                "bulletBorderThickness": 3,
                "fillAlphas": 0,
                "lineAlpha": 1,
                "title": "Expenses",
                "valueField": "expenses",
                "dashLengthField": "dashLengthLine"
            }],
            "categoryField": "месяц",
            "categoryAxis": {
                "gridPosition": "start",
                "axisAlpha": 0,
                "tickLength": 0
            },
            "export": {
                "enabled": true
            }
        };
        var data = getChartsData();
        chartOpts.dataProvider = data;
        $slides.each(function(i) {
            charts.push(AmCharts.makeChart('chart-left-' + (i + 1), cloneObject(chartOpts)));
            charts.push(AmCharts.makeChart('chart-right-' + (i + 1), cloneObject(chartOpts)));
        });
    };
    var getChartsData = function() {
        return [{
            "месяц": "Ноябрь",
            "income": 10,
            "expenses": 9.1
        }, {
            "месяц": "Декабрь",
            "income": 12.5,
            "expenses": 11.1
        }, {
            "месяц": "Январь",
            "income": 17.5,
            "expenses": 15.1
        }, {
            "месяц": "Февраль",
            "income": 16.5,
            "expenses": 15.5
        }, {
            "месяц": "Март",
            "income": 20.5,
            "expenses": 19.1,
            "dashLengthLine": 5
        }, {
            "месяц": "Апрель",
            "income": 23.5,
            "expenses": 21.1,
            "dashLengthColumn": 5,
            "alpha": 0.2,
            "additional": "(projection)"
        }];
    };
    return obj;
}();