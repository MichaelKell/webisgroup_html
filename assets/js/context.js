$(".header-calldog-area").on("mouseover", function(){
	if($(this).hasClass('locked')){
		return false;
	}
	$(this).addClass('locked');
	$(".header-riffle").stop().fadeOut(200);
	$(".header-dog").stop().animate({bottom: 48}, 'swing');
});

$(".header-calldog-area").on("mouseleave", function(){
	$(".header-dog").animate({bottom: -198}, 'swing');
	$(".header-riffle").fadeIn(400);
	$(".header-calldog-area").removeClass('locked');
});