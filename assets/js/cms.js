preload([
        'assets/img/ico_eye.png',
        'assets/img/portslider_arr_left_active.png',
        'assets/img/portslider_arr_right_active.png',
    ]);


$(".payments-slider").owlCarousel({
    navigation: false,
    pagination: false,
    slideSpeed: 350,
    items: 6,
    itemsDesktop: [1199,6],
    itemsDesktopSmall:  [979,5],
    itemsTablet:    [768,3],
    itemsMobile:    [479,2]
  });

$('.functions-table').stacktable({myClass:'stacked'});

$(".functions-table-fold").on("click", function(){
    var $this = $(this);
    if(!$this.hasClass("active")){
        $(".functions-table-wrap").animateToAutoHeight(1100);
        $this.addClass("active");
        $this.html('<span class="fold-fold"></span> Свернуть таблицу');
        $this.css({"bottom":"0", "position":"relative"});
    }else{
        $this.removeClass("active");
        $this.html('<span class="fold-unfold"></span> Развернуть таблицу');
        $this.css({"bottom":"0",  "position":"absolute"});
        $(".functions-table-wrap").animate({"height": "477px"}, 700);
        goToAnchor("#functions");
    }
});
