$(window).load(function() {
	var scale = setSiteMinWidth(440);
});

$(".fb-img, .fb-form").fancybox({
	helpers: {
		overlay: {
			locked: false
		}
	},
});

$(".fb-img").fancybox({
	wrapCSS: 'fancybox-img'
});

$(".fb-form").fancybox({
	wrapCSS: 'fancybox-form'
});


$(".port-slider-item, .cbp-caption").hover(function() {
	$(this).find(".port-slider-content-animate, .cbp-caption-animate").show('drop', {direction: 'down'}, 300);
}, function() {
	$(this).find(".port-slider-content-animate, .cbp-caption-animate").css({"display": "none"});
});

$('#reviews').owlCarousel({
	navigation: false,
	slideSpeed: 300,
	paginationSpeed: 400,
	singleItem: true
});

$("#tinynav").tinyNav({
	active: 'active'
});

$(".tinynav").styler();

formValidator.init({
	errors: {
		'name': "Пожалуйста введите Ваше имя",
		'phone': "Пожалуйста укажите Ваш телефон",
		'email': "Пожалуйста укажите Ваш адрес email",
		'question': "Пожалуйста опишите в нескольких словах Ваши пожелания",
		'answer' : "Пожалуйста введите Ответ на вопрос"
	}
});

ajaxForm.init($(".form-ajax"));

fixInputHighlight();

navbar.init();

buttonCollapse.init();

resultsSlider.init("#results-slides", {start: 0});