var portfolio = function(){
    var obj = {};
    var $portfolio = $('#grid-container');

    obj.init = function(){
        initPortfolio();
        $(window).on('scroll load', onScroll);
        $('#cube-portfolio-more').on('click', loadMore);
    };

    var initPortfolio = function(){
        $portfolio.cubeportfolio({
            mediaQueries: [{
                width: 768,
                cols: 3
            }, {
                width: 555,
                cols: 2
            }, {
                width: 10,
                cols: 1
            }],
            defaultFilter: '*',
            animationType: 'flipOutDelay',
            gapHorizontal: 25,
            gapVertical: 25,
            gridAdjustment: 'responsive',
            caption: 'overlayBottom',
            displayType: 'lazyLoading',
            displayTypeSpeed: 100,
            singlePageDelegate: '.cbp-singlePage',
            singlePageDeeplinking: true,
            singlePageStickyNavigation: true,
            singlePageCounter: '<div class="cbp-popup-singlePage-counter">{{current}} of {{total}}</div>',
            singlePageInlineDelegate: '.cbp-singlePageInline',
            singlePageInlinePosition: 'above',
            singlePageInlineInFocus: true,
            singlePageInlineCallback: function(url, element) {}
        });
    };

    var onScroll = function(){
        if($(window).scrollTop() + $(window).height() + 100 > $portfolio.offset().top + $portfolio.height() && $('#cube-portfolio-more').length > 0){
            $('#cube-portfolio-more').get(0).click();
        }   
    };

    var loadMore = function(){
        $(window).off('scroll', onScroll);
        var p = parseInt($('input[name="p"]').val());
        var total_pages = parseInt($('input[name="total_pages"]').val());
        var cid = $('input[name="cid"]').val();
        if(p >= total_pages){
            $('#cube-portfolio-more').hide();
            return false;
        }
        $.ajax({
            url: './',
            data: {
                p: p,
                cid: cid 
            },
            success: function(data){
                $portfolio.cubeportfolio('appendItems', data, function(){
                    $('input[name="p"]').val(p+1);
                    $(window).on('scroll', onScroll);
                });
            },
            error: function(err){
                console.log('error: ' + err);
            }
        });
    };

    return obj;
}();

portfolio.init();