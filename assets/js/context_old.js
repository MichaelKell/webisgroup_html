$(".services-slider").owlCarousel({
    navigation: false,
    pagination: false,
    slideSpeed: 350,
    items: 5,
    itemsDesktop: [1199,4],
    itemsDesktopSmall:  [979,4],
    itemsTablet:    [768,3],
    itemsMobile:    [479,2]
});