$(window).load(function(){
	$('#clients').isotope({
		percentPosition: true,
		sortBy : 'original-order',
	});
});

$(window).on("resize", function(){
	setTimeout(function(){
		$('#clients').isotope('layout');
	}, 1000)
});