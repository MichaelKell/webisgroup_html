var fixInputHighlight = function() {
    $(".input").on("focus", function() {
        var $parent = $(this).parent();
        if (!$parent.hasClass("input-group")) {
            return;
        }
        $parent.find(".input").css({
            "borderColor": "#66afe9"
        });
        $parent.find(".input-group-addon").css({
            "borderColor": "#66afe9",
            "boxShadow": "none"
        });
        $parent.css({
            "boxShadow": 'inset 0 1px 1px rgba(0,0,0,.075), 0 0 8px rgba(102, 175, 233, .6)'
        });
    });
    $(".input").on("blur", function() {
        var $parent = $(this).parent();
        if (!$parent.hasClass("input-group")) {
            return;
        }
        $parent.find(".input").css({
            "borderColor": "#bebebe"
        });
        $parent.find(".input-group-addon").css({
            "borderColor": "#bebebe",
            "boxShadow": "inset 1px 1px 1px rgba(0, 0, 0, .075)"
        });
        $parent.css({
            "boxShadow": 'none'
        });
    });
};