var buttonCollapse = function() {
    var obj = {};
    obj.init = function() {
        $(".button-collapse[data-action='show']").on("click", show);
        $(".button-collapse[data-action='hide']").on("click", hide);
        $(".button-collapse[data-anchor='" + document.location.hash + "'][data-action='show']").click();
    };
    var show = function() {
        var $this = $(this);
        var target = $this.data("target");
        var anchor = $this.data("anchor");
        var offset = $this.data("anchor-offset");
        var group = $this.data("group");
        $(".button-collapse[data-group='" + group + "']").each(function() {
            $($(this).data("target")).css({
                "display": "none"
            });
            $(".button-collapse[data-action='show'][data-target='" + $(this).data("target") + "']").fadeIn();
            $(".button-collapse[data-action='hide'][data-target='" + $(this).data("target") + "']").fadeOut();
        });
        $(target).slideDown(400, 'swing', function() {
            if (anchor !== undefined) goToAnchor(anchor, offset);
        });
        $this.fadeOut('normal', function() {
            $(".button-collapse[data-action='hide'][data-target='" + target + "']").fadeIn();
        });

    };
    var hide = function() {
        var $this = $(this);
        var target = $this.data("target");
        var anchor = $this.data("anchor");
        var offset = $this.data("anchor-offset");
        $(target).slideUp();
        if (anchor !== undefined) {
            goToAnchor(anchor, offset);
        }
        $(".button-collapse[data-action='hide'][data-target='" + target + "']").fadeOut('normal', function() {
            $(".button-collapse[data-action='show'][data-target='" + target + "']").fadeIn();
        });
        $this.fadeOut('normal', function() {
            $(".button-collapse[data-action='show'][data-target='" + target + "']").fadeIn();
        });
    };
    return obj;
}();