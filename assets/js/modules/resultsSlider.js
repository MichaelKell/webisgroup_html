var resultsSlider = function() {
    var obj = {};
    var $container, $slides, charts = [];
    obj.init = function(selector, opts) {
        $container = $(selector);
        if ($container.length < 1) return false;
        $slides = $container.children();
        displaySlide($slides.eq(opts.start));
        $(".results-panel-button").on("click", moveNext);
        AmCharts.ready(initCharts);
    };
    var moveNext = function() {
        var next = getNextSlide();
        displaySlide($slides.eq(next));
        charts.forEach(function(chart) {
            chart.animateAgain();
        });
    };
    var displaySlide = function($slide) {
        $slides.css({
            "display": "none"
        });
        $slides.removeClass("active");
        $slide.fadeIn();
        $slide.addClass("active");
        updateSliderData($slide.data());
    };
    var updateSliderData = function(data) {
        $(".results-panel-project").text(data.project);
        $(".results-panel-project").attr("href", "http://" + data.project);
    };
    var getCurrentSlide = function() {
        return $container.children(".active").index();
    };
    var getNextSlide = function() {
        var current = getCurrentSlide();
        var total = $slides.length;
        var next = current + 1;
        if (next >= total) return 0;
        else
            return next;
    };
    var initCharts = function() {
        var chartOpts = {
            "type": "serial",
            "addClassNames": true,
            "theme": "light",
            "autoMargins": false,
            "marginLeft": 30,
            "marginRight": 8,
            "marginTop": 0,
            "marginBottom": 26,
            "language": "ru",
            "startDuration": 1,
            "balloon": {
                "adjustBorderColor": false,
                "horizontalPadding": 10,
                "verticalPadding": 8,
                "color": "#ffffff"
            },
            "graphs": [{
                "alphaField": "alpha",
                "balloonText": "<span style='font-size:12px;'>[[title]] in [[category]]:<br><span style='font-size:20px;'>[[value]]</span> [[additional]]</span>",
                "fillAlphas": 1,
                "title": "Income",
                "type": "column",
                "valueField": "income",
                "dashLengthField": "dashLengthColumn"
            }, {
                "id": "graph2",
                "balloonText": "<span style='font-size:12px;'>[[title]] in [[category]]:<br><span style='font-size:20px;'>[[value]]</span> [[additional]]</span>",
                "bullet": "round",
                "lineThickness": 3,
                "bulletSize": 7,
                "bulletBorderAlpha": 1,
                "bulletColor": "#FFFFFF",
                "useLineColorForBulletBorder": true,
                "bulletBorderThickness": 3,
                "fillAlphas": 0,
                "lineAlpha": 1,
                "title": "Expenses",
                "valueField": "expenses",
                "dashLengthField": "dashLengthLine"
            }],
            "categoryField": "месяц",
            "categoryAxis": {
                "gridPosition": "start",
                "axisAlpha": 0,
                "tickLength": 0
            },
            "export": {
                "enabled": true
            }
        };
        var data = getChartsData();
        chartOpts.dataProvider = data;
        $slides.each(function(i) {
            charts.push(AmCharts.makeChart('chart-left-' + (i + 1), cloneObject(chartOpts)));
            charts.push(AmCharts.makeChart('chart-right-' + (i + 1), cloneObject(chartOpts)));
        });
    };
    var getChartsData = function() {
        return [{
            "месяц": "Ноябрь",
            "income": 10,
            "expenses": 9.1
        }, {
            "месяц": "Декабрь",
            "income": 12.5,
            "expenses": 11.1
        }, {
            "месяц": "Январь",
            "income": 17.5,
            "expenses": 15.1
        }, {
            "месяц": "Февраль",
            "income": 16.5,
            "expenses": 15.5
        }, {
            "месяц": "Март",
            "income": 20.5,
            "expenses": 19.1,
            "dashLengthLine": 5
        }, {
            "месяц": "Апрель",
            "income": 23.5,
            "expenses": 21.1,
            "dashLengthColumn": 5,
            "alpha": 0.2,
            "additional": "(projection)"
        }];
    };
    return obj;
}();