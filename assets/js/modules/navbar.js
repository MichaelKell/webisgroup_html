var navbar = function() {
    var obj = {};
    obj.init = function() {
        fixMobileDropdowns();
        fixMobileExpandActive();
        fixMobileExpandToFullHeight();
    };
    var fixMobileDropdowns = function() {
        $('nav').on("mouseenter mouseleave", ".navbar-collapse:not(.in) .dropdown", function() {
            $(this).toggleClass('opened js-activated');
        });
        $('nav').on("click", ".navbar-collapse.in .dropdown", function() {
            $(this).toggleClass('opened js-activated');
        });
        $('nav').on("click", ".navbar-collapse.in .dropdown>a", function(e) {
            e.preventDefault();
        });
    };
    var fixMobileExpandToFullHeight = function() {
        $(".navbar-collapse").on("show.bs.collapse", function() {
            $(this).css({
                "display": 'none'
            });
        });
        $(".navbar-collapse").on("shown.bs.collapse", function() {
            $(this).css({
                "max-height": $(window).height() - $('.navbar-header').height() - 13
            });
            $(this).slideDown();
        });
    };
    var fixMobileExpandActive = function() {
        $(".navbar-collapse").on("shown.bs.collapse", function() {
            $(this).find('.dropdown-menu .active').closest('.dropdown').addClass('opened js-activated');
        });
    };
    return obj;
}();