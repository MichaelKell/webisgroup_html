var formValidator = function() {
    var obj = {};
    var errors;

    obj.init = function(opts) {
        errors = opts.errors;
        $(".form-validate").on("submit", function() {
            var $this = $(this);
            if (!validate($this))
                return false;
            $this.submit();
        });
    };
    
    obj.validate = function($form) {
        var error = "";
        var $fields = $form.find("[data-required]");
        $fields.each(function() {
            var $field = $(this);
            var val = $field.val();
            var type = $field.data('required').toLowerCase();
            if (val === "")
                error += errors[type] + "\n";
            if (type == 'email' && val !== "" && !validateEmail(val)) {
                error += "Некорректный адрес email\n";
            }
            if (type == 'answer' && val !== "" && isNaN(val))
                error += "Пожалуйста напишите ответ цифрами\n";
        });
        if (error !== "") {
            alert(error);
            return false;
        }
        return true;
    }

    var validateEmail = function(str) {
        var at = "@";
        var dot = ".";
        var lat = str.indexOf(at);
        var lstr = str.length;
        var ldot = str.indexOf(dot);
        if (str.indexOf(at) == -1) {
            return false;
        }
        if (str.indexOf(at) == -1 || str.indexOf(at) == 0 || str.indexOf(at) == lstr) {
            return false;
        }
        if (str.indexOf(dot) == -1 || str.indexOf(dot) == 0 || str.indexOf(dot) == lstr) {
            return false;
        }
        if (str.indexOf(at, (lat + 1)) != -1) {
            return false;
        }
        if (str.substring(lat - 1, lat) == dot || str.substring(lat + 1, lat + 2) == dot) {
            return false;
        }
        if (str.indexOf(dot, (lat + 2)) == -1) {
            return false;
        }
        if (str.indexOf(" ") != -1) {
            return false;
        }
        return true;
    };

    return obj;
}();