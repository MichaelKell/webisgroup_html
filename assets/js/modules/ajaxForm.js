var ajaxForm = function() {
    var obj = {};
    obj.init = function($form) {
        $form.on("submit", function() {
            var $this = $(this);
            if (!formValidator.validate($this)) {
                return false;
            }
            send($this.serialize(), function(response) {
                $.fancybox(response.html, {
                    type: 'html'
                });
                if (response.success == "1") {
                    $this.get(0).reset();
                }
            });
            return false;
        });
    };
    var send = function(data, callback) {
        $.ajax({
            url: "./",
            type: 'POST',
            data: data,
            dataType: "json",
            success: callback,
            error: function(error) {
                console.log(error);
                //alert('Try again later');
            }
        });
    };

    return obj;
}();