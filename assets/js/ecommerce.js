preload([
        'assets/img/bullet2_active.png',
        'assets/img/ico_eye.png',
        'assets/img/portslider_arr_left_active.png',
        'assets/img/portslider_arr_right_active.png',
    ]);


var $slider = $('#slider');
$slider.preload($("#slider-container"), $slider.find('li'), function(){
    $slider.flexslider({
        directionNav: true,
        pauseOnAction: false,
        pauseOnHover: true,
        slideshowSpeed: 5000,        
        animationSpeed: 900,
        customDirectionNav: $(".slider-direction-button")
    });
});

var portfolioFeed = function() {
    var obj = {};
    var $el;

    obj.init = function($element) {
        $el = $element;
        var opts = {
            manualContinuousScrolling: true,
            hotSpotMouseDownSpeedBooster: 1.5
        };
        var env = getEnv();
        if (env == 'xs') {
            opts = {
                manualContinuousScrolling: true,
                hotSpotScrolling: false
            };
        }
        $el.smoothDivScroll(opts);
        if (env == 'xs') {
            initMobileNav();
        }
        $(window).resize(function() {
            obj.init($el, opts)
        });
    };

    var initMobileNav = function() {
        $el.find(".scrollingHotSpotRight").on('mousedown', function() {
            move(100);
        });
        $el.find(".scrollingHotSpotLeft").on('mousedown', function() {
            move(-100);
        });
    };

    var move = function(offset) {
        $el.smoothDivScroll("move", offset);
    };

    return obj;
}();

portfolioFeed.init($(".port-slider"));