$(".news-nav a, a.news-all").scrollPosReaload({ smooth:false });

$(".escroll-container").jscroll({	/*endless scroll*/
    nextSelector: 'a.escroll-next-link:last',
    autoTrigger: true,
    loadingHtml: "<div class='text-center margin-top-10'><i class='fa fa-spinner fa-spin f-48 gray' aria-hidden='true'></i></div>",
});