preload([
        'assets/img/bullet2_active.png',
        'assets/img/ico_eye.png',
        'assets/img/portslider_arr_left_active.png',
        'assets/img/portslider_arr_right_active.png'
]);


/*main slider*/
var $slider = $('#slider');
var $animation_images = $("#slider img");
var $slider_images = $slider.find('.slider-slide').add($animation_images);
$slider.preload($(".slider"), $slider_images, function() {
    $slider.flexslider({
        selector: '.slider-slides > li',
        namespace: "slider-",
        directionNav: false,
        pauseOnAction: false,
        pauseOnHover: true,
        slideshowSpeed: 5000,
        animationSpeed: 900,
        init: function() {
            sliderAnimations.init();
        },
    });
});

/*cubeportfolio*/
cubeportfolio.init();

/*clients*/
$(".clients-scroller").smoothDivScroll({
    manualContinuousScrolling: true,
    autoScrollingMode: "onStart",
    autoScrollingInterval: 35
});



var cubeportfolio = function(){
    var obj = {};
    var gridContainer = $('#grid-container'),
    filtersContainer = $('#filters-container'),
    wrap,
    filtersCallback;

    var init = function(){
        initGrid();
        initFilters();
        initCounter();
    };

    var initGrid = function(){
        gridContainer.cubeportfolio({
            mediaQueries: [
                            {width: 768, cols: 3},
                            {width: 555, cols: 2},
                            {width: 10,cols: 1}
                          ],
            defaultFilter: '*',
            animationType: 'flipOutDelay',
            gapHorizontal: 25,
            gapVertical: 25,
            gridAdjustment: 'responsive',
            caption: 'overlayBottom',
            displayType: 'sequentially',
            displayTypeSpeed: 100,
            singlePageDelegate: '.cbp-singlePage',
            singlePageDeeplinking: true,
            singlePageStickyNavigation: true,
            singlePageCounter: '<div class="cbp-popup-singlePage-counter">{{current}} of {{total}}</div>',
            singlePageInlineDelegate: '.cbp-singlePageInline',
            singlePageInlinePosition: 'above',
            singlePageInlineInFocus: true,
            singlePageInlineCallback: function(url, element) {}
        });
    };

    var initFilters = function(){
        if (filtersContainer.hasClass('cbp-l-filters-dropdown')) {
            wrap = filtersContainer.find('.cbp-l-filters-dropdownWrap');
            wrap.on({'mouseover.cbp': function() {
                wrap.addClass('cbp-l-filters-dropdownWrap-open');
            }, 'mouseleave.cbp': function() {
                wrap.removeClass('cbp-l-filters-dropdownWrap-open');
            }});
            filtersCallback = function(me) {
                wrap.find('.cbp-filter-item').removeClass('cbp-filter-item-active');
                wrap.find('.cbp-l-filters-dropdownHeader').text(me.text());
                me.addClass('cbp-filter-item-active');
                wrap.trigger('mouseleave.cbp');
            };
        } else {
            filtersCallback = function(me) {
                me.addClass('cbp-filter-item-active').siblings().removeClass('cbp-filter-item-active');
            };
        }
        filtersContainer.on('click.cbp', '.cbp-filter-item', function() {
            var me = $(this);
            if (me.hasClass('cbp-filter-item-active')) {
                return;
            }
            if (!$.data(gridContainer[0], 'cubeportfolio').isAnimating) {
                filtersCallback.call(null, me);
            }
            gridContainer.cubeportfolio('filter', me.data('filter'), function() {});
        });
    };

    var initCounter = function(){
        gridContainer.cubeportfolio('showCounter', filtersContainer.find('.cbp-filter-item'), function() {
            var match = /#cbpf=(.*?)([#|?&]|$)/gi.exec(location.href), item;
            if (match !== null) {
                item = filtersContainer.find('.cbp-filter-item').filter('[data-filter="' + match[1] + '"]');
                if (item.length) {
                    filtersCallback.call(null, item);
                }
            }
        });
    };

    return obj;
}();


var sliderAnimations = function() {
    var obj = {};

    obj.init = function() {
        $(".slider-animation").on("mouseenter tap", runAnimation);
    };

    var runAnimation = function() {
        var id = $(this).attr("id");
        switch (id) {
            case "metronom":
                animateMetronom();
                break;
            case "zorg":
                animateZorg(2);
                break;
            case "bottle":
                animateFrames("#bottle");
                break;
            case "chair_wrap":
                animateChair();
                break;
            case "stackdeck":
                animateFrames("#stackdeck");
                break;
            case "chat":
                animateChat(2);
                break;
            default:
                console.log("undefined slider animation id - " + id);
        }
    };

    var animateChair = function() {
        var $chair = $("#chair");
        var $shadows = $("#chair_shadows");
        if (!$chair.hasClass('lock')) {
            $chair.addClass('lock');
            $chair.transition({rotate: '16deg'}, 1200, 'in-out', function() {
                $shadows.css({left: "80px"});
                $chair.transition({rotate: '-9deg'}, 1100, 'in-out', function() {
                    $shadows.css({left: "110px"});
                    $chair.transition({rotate: '4deg'}, 650, 'in-out', function() {
                        $chair.transition({rotate: '0deg'}, 550, 'in-out', function() {
                            $chair.removeClass('lock');
                        });
                    });
                });
            });
        }
    };

    var animateMetronom = function() {
        $arrow = $("#metronom_arr");
        if (!$arrow.hasClass('lock')) {
            $arrow.addClass('lock');
            $arrow.transition({rotate: '33deg'}, 1050, 'easeOutBack', function() {
                $arrow.transition({rotate: '-33deg'}, 1000, 'easeOutBack', function() {
                    $arrow.removeClass('lock');
                });
            });
        }
    };

    var animateZorg = function(times) {
        var count = 0;
        var interval = 400;
        var inervalID = setIntervalAndExecute(function() {
            if (times == count) {
                clearInterval(inervalID);
                return;
            }
            if ($("#zorg_screen2").css("display") == "block") {
                current = 2;
                other = 1;
            } else {
                current = 1;
                other = 2;
            }
            $("#zorg_screen" + current).fadeOut(500);
            $("#zorg_screen" + other).fadeIn(2000);
            count++;
        }, interval);
    };

    var animateChat = function(times) {
        var $chat = $("#chat2");
        var count = 0;
        var interval = 300;
        var intervalID = setIntervalAndExecute(function() {
            if (times == count) {
                clearInterval(intervalID);
                return;
            }
            if (count % 2 == 0) {
                $chat.css({bottom: "80px"});
            } else {
                $chat.css({bottom: "60px"});
            }
            count++;
        }, interval);
    };

    var animateFrames = function(container) {
        $container = $(container);
        if ($container.hasClass("lock")){
            return;
        }
        $container.addClass("lock");
        var count = 0;
        var total = parseInt($container.data('frame-last'));
        var speed = parseInt($container.data('speed'));
        var bg_offset = parseInt($container.data('bg-offset'));
        var current_offset;
        $container.css({"background-position": "0px 0px"});
        var intervalID = setIntervalAndExecute(function() {
            current_offset = count * bg_offset;
            $container.css({"background-position": "-" + current_offset.toString() + "px 0px"});
            if (count == total) {
                $container.removeClass("lock");
                $container.css({"background-position": "0px 0px"});
                clearInterval(intervalID);
                return;
            }
            count++;
        }, speed);
    };

    return obj;
}();