var portfolio = function(){
	var obj = {};
	var preventGapsBreakpoint = 999;
	var $el;
	var opts = {
		gapHorizontal: 0,
		gapVertical: 0,
		layoutMode: 'mosaic',
		gridAdjustment: 'default',
		sortToPreventGaps: true,
		displayType: 'default',
		caption: 'fadeIn'
	};

	obj.init = function(el){
		$el = el;
		//$el.cubeportfolio(opts);
		init(true);
		initResize();
	};

	var init = function(firstinit){
			var w = $(window).width();
			var hadClass = $el.hasClass("prevent-gaps");
			if( w > preventGapsBreakpoint ){
				$el.removeClass("prevent-gaps");
			}else{
				$el.addClass("prevent-gaps");
			}
			var hasClass = $el.hasClass("prevent-gaps");

			if(hasClass != hadClass || firstinit){
				if(hasClass){
					//console.log("prevent");
					var newopts = opts;
					newopts.sortToPreventGaps = true;
					if(!firstinit){
						$el.cubeportfolio('destroy', function(){
							$el.cubeportfolio(newopts);
						});;
					}else{
						$el.cubeportfolio(newopts);
					}
				}else{
					//console.log("default");
					if(!firstinit){
						$el.cubeportfolio('destroy', function(){
							$el.cubeportfolio(opts);
						});
					}else{
						$el.cubeportfolio(opts);
					}
				}
			}
	};

	var initResize = function(){
		$(window).on("resize",function(){
			init();
		});
	};

	return obj;
}();

portfolio.init($("#js-grid-mosaic"));