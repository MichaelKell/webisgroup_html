function getEnv() {
    var envs = ['xs', 'sm', 'md', 'lg'];
    var $el = $('<div>');
    $el.appendTo($('body'));
    for (var i = envs.length - 1; i >= 0; i--) {
        var env = envs[i];
        $el.addClass('hidden-' + env);
        if ($el.is(':hidden')) {
            $el.remove();
            return env;
        }
    }
}

var spinner = function() {
    var obj = {};
    var opts = {
        lines: 12,
        length: 10,
        width: 4,
        radius: 10,
        color: '#9a9a9a',
        left: '50%',
        top: '50%',
        className: "spinner"
    };
    obj.spin = function($el) {
        $el.append(new Spinner(opts).spin().el);
    };
    obj.stop = function($el) {
        $el.find('.spinner').remove();
    };
    return obj;
}();

function setIntervalAndExecute(fn, t) {
    fn();
    return (setInterval(fn, t));
}

function goToAnchor(hash, offset) {
    if (offset === undefined) offset = -50;
    $('html,body').animate({
        scrollTop: $("a[name='" + hash.substring(1, hash.length) + "']").offset().top + parseInt(offset)
    }, 700, 'swing');
    document.location.hash = hash;
}

(function($) {
    $.fn.animateToAutoHeight = function(duration) {
        var curHeight = this.css('height'),
            height = this.css('height', 'auto').height(),
            easing = 'swing',
            callback = $.noop,
            parameters = {
                height: height
            };
        this.css('height', curHeight);
        for (var i in arguments) {
            switch (typeof arguments[i]) {
                case 'object':
                    parameters = arguments[i];
                    parameters.height = height;
                    break;
                case 'string':
                    if (arguments[i] == 'slow' || arguments[i] == 'fast') duration = arguments[i];
                    else easing = arguments[i];
                    break;
                case 'number':
                    duration = arguments[i];
                    break;
                case 'function':
                    callback = arguments[i];
                    break;
            }
        }
        this.animate(parameters, duration, easing, function() {
            $(this).css('height', 'auto');
            callback.call(this, arguments);
        });
        return this;
    };
})(jQuery);

(function($) {
    $.fn.preload = function($container, $images, callback) {
        spinner.spin($container);
        var $el = $(this);
        var cnt_images = $images.length;
        var loaded = 0;
        $images.each(function() {
            var $img = $(this);
            var source;
            if ($img.prop("tagName") != "IMG") {
                source = $img.css('background-image').replace(/^url\(['"]?/, '').replace(/['"]?\)$/, '');
            } else {
                source = $img.attr("src");
            }
            $('<img/>').load(function() {
                ++loaded;
                if (loaded == cnt_images) {
                    spinner.stop($container);
                    $el.css({
                        "visibility": "visible"
                    });
                    callback();
                }
            }).attr('src', source);
        });
    };
})(jQuery);

$.fn.scrollPosReaload = function(opts) {
    var defaults = {
        smooth: false
    };
    var options = $.extend({}, defaults, opts);
    if (localStorage) {
        var posReader = localStorage["posStorage"];
        if (posReader) {
            if (localStorage["smooth"] == "true") {
                $('html,body').animate({
                    scrollTop: posReader
                }, 700, 'swing');
            } else {
                $(window).scrollTop(posReader);
            }
            localStorage.removeItem("posStorage");
            localStorage.removeItem("smooth");
        }
        $(this).click(function(e) {
            localStorage["posStorage"] = $(window).scrollTop();
            localStorage["smooth"] = options.smooth;
        });
        return true;
    }
    return false;
};

function cloneObject(obj) {
    var copy;
    if (null == obj || "object" != typeof obj) return obj;
    if (obj instanceof Date) {
        copy = new Date();
        copy.setTime(obj.getTime());
        return copy;
    }
    if (obj instanceof Array) {
        copy = [];
        for (var i = 0, len = obj.length; i < len; i++) {
            copy[i] = cloneObject(obj[i]);
        }
        return copy;
    }
    if (obj instanceof Object) {
        copy = {};
        for (var attr in obj) {
            if (obj.hasOwnProperty(attr)) copy[attr] = cloneObject(obj[attr]);
        }
        return copy;
    }
    throw new Error("Unable to copy obj! Its type isn't supported.");
}

function setSiteMinWidth(mw) {
    if (/Android|webOS|iPhone|iPad|iPod|BlackBerry/i.test(navigator.userAgent)) {
        var ww = ($(window).width() < window.screen.width) ? $(window).width() : window.screen.width;
        var ratio = ww / mw;
        if (ww < mw) {
            $('#viewport').attr('content', 'initial-scale=' + ratio + ', maximum-scale=' + ratio + ', minimum-scale=' + ratio + ', user-scalable=yes, width=' + ww);
        } else {
            $('#viewport').attr('content', 'initial-scale=1.0, maximum-scale=2, minimum-scale=1.0, user-scalable=yes, width=device-width');
        }
    }
    return ratio;
}