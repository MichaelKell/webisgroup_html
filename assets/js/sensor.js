$(".sol-slider").owlCarousel({
    autoPlay : 3000,
    stopOnHover : true,
    navigation:true,
    paginationSpeed : 1000,
    goToFirstSpeed : 2000,
    singleItem : true,
    autoHeight : true,
    transitionStyle:"fade",
    beforeInit: function(){
        var src = $('.sol-slider-item').eq(0).data("slider-bg");
        $(".sol-slider").css({"background": "url("+src+") bottom center no-repeat"});
    },
    afterMove: function(el){
        var src = el.find('.sol-slider-item').eq(this.currentItem).data("slider-bg");
        $(".sol-slider").css({"background": "url("+src+") bottom center no-repeat"});
    }
});