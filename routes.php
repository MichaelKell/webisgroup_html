<?php
use Phroute\Phroute\Exception\HttpRouteNotFoundException;

$router->get('/', function() use ($blade){
	return $blade->render('pages.index');
});

$router->get('{page}', function($page) use ($blade){
	$view = 'pages.'.$page;
	if(!$blade->exists($view)){
		throw new HttpRouteNotFoundException;
	}
	return $blade->render($view);
});